import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ColorPickerModule } from '@app/color-picker/color-picker.module';
import { StampChoiceComponent } from '@app/tools/shared/components/stamp-choice/stamp-choice.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { AttributesPanelComponent } from './components/attributes-panel/attributes-panel.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { ConfirmResetDrawingComponent } from './components/confirm-reset-drawing/confirm-reset-drawing.component';
import { DrawingComponent } from './components/drawing/drawing.component';
import { EditorComponent } from './components/editor/editor.component';
import { ExportComponent } from './components/export/export.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { SavingComponent } from './components/saving/saving.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UserGuideComponent } from './components/user-guide/user-guide.component';
import { UserSubguideComponent } from './components/user-subguide/user-subguide.component';
import { BrushAttributesComponent } from './tools/brush/brush-attributes/brush-attributes.component';
import { BucketAttributesComponent } from './tools/bucket/bucket-attributes/bucket-attributes.component';
import { EllipseAttributesComponent } from './tools/ellipse/ellipse-attributes/ellipse-attributes.component';
import { EraserAttributesComponent } from './tools/eraser/eraser-attributes/eraser-attributes.component';
import { FeatherPenAttributesComponent } from './tools/feather-pen/feather-pen-attributes/feather-pen-attributes.component';
import { GridAttributesComponent } from './tools/grid/grid-attributes/grid-attributes.component';
import { LineAttributesComponent } from './tools/line/line-attributes/line-attributes.component';
import { PencilAttributesComponent } from './tools/pencil/pencil-attributes/pencil-attributes.component';
import { PolygonAttributesComponent } from './tools/polygon/polygon-attributes/polygon-attributes.component';
import { RectangleAttributesComponent } from './tools/rectangle/rectangle-attributes/rectangle-attributes.component';
import { SelectionAttributesComponent } from './tools/selection/selection-attributes/selection-attributes.component';
import { DrawTypeComponent } from './tools/shared/components/draw-type/draw-type.component';
import { DropDownComponent } from './tools/shared/components/drop-down/drop-down.component';
import { FontStyleComponent } from './tools/shared/components/font-style/font-style.component';
import { HasJunctionComponent } from './tools/shared/components/has-junction/has-junction.component';
import { SidesNumberComponent } from './tools/shared/components/sides-number/sides-number.component';
import { SliderComponent } from './tools/shared/components/slider/slider.component';
import { TextAlignComponent } from './tools/shared/components/text-align/text-align.component';
import { TextureComponent } from './tools/shared/components/texture/texture.component';
import { SpraypaintAttributesComponent } from './tools/spraypaint/spraypaint-attributes/spraypaint-attributes.component';
import { StampAttributesComponent } from './tools/stamp/stamp-attributes/stamp-attributes.component';
import { TextAttributesComponent } from './tools/text/text-attributes/text-attributes.component';

@NgModule({
    declarations: [
        AppComponent,
        EditorComponent,
        SidebarComponent,
        DrawingComponent,
        CarouselComponent,
        MainPageComponent,
        BrushAttributesComponent,
        EraserAttributesComponent,
        PencilAttributesComponent,
        RectangleAttributesComponent,
        BrushAttributesComponent,
        EllipseAttributesComponent,
        LineAttributesComponent,
        PencilAttributesComponent,
        AttributesPanelComponent,
        UserGuideComponent,
        UserSubguideComponent,
        DrawTypeComponent,
        TextureComponent,
        HasJunctionComponent,
        ExportComponent,
        SavingComponent,
        PolygonAttributesComponent,
        SidesNumberComponent,
        BucketAttributesComponent,
        SpraypaintAttributesComponent,
        StampAttributesComponent,
        SliderComponent,
        StampChoiceComponent,
        ConfirmResetDrawingComponent,
        SelectionAttributesComponent,
        GridAttributesComponent,
        TextAttributesComponent,
        DropDownComponent,
        FontStyleComponent,
        TextAlignComponent,
        StampChoiceComponent,
        FeatherPenAttributesComponent,
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        MatButtonModule,
        HttpClientModule,
        AppRoutingModule,
        NoopAnimationsModule,
        MatCardModule,
        MatSliderModule,
        MatChipsModule,
        MatFormFieldModule,
        MatIconModule,
        MatTooltipModule,
        MatGridListModule,
        MatMenuModule,
        MatSelectModule,
        MatSliderModule,
        MatDialogModule,
        MatTabsModule,
        MatSlideToggleModule,
        MatSliderModule,
        MatButtonToggleModule,
        FormsModule,
        MatRadioModule,
        MatFormFieldModule,
        MatChipsModule,
        MatInputModule,
        MatSelectModule,
        MatSnackBarModule,
        ColorPickerModule,
        MatButtonToggleModule,
        MatRadioModule,
        FormsModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
