export enum Cardinal {
    NorthWest = 'Nord-Ouest',
    North = 'Nord',
    NorthEast = 'Nord-Est',
    West = 'Ouest',
    Center = 'Centre',
    East = 'Est',
    SouthWest = 'Sud-Ouest',
    South = 'Sud',
    SouthEast = 'Sud-Est',
}
