const WIDTH = 100;
const HEIGHT = 100;

export class CanvasTestHelper {
    static canvas(): HTMLCanvasElement {
        const canvas = document.createElement('canvas');
        canvas.width = WIDTH;
        canvas.height = HEIGHT;
        return canvas;
    }

    static clearCanvas(ctx: CanvasRenderingContext2D): void {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }
}
