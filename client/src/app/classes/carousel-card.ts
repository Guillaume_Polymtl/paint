export interface CarouselCard {
    title: string;
    imageSource: string;
    tags: string[];
}

export let cards: CarouselCard[] = [
    {
        title: 'Parole 1',
        imageSource: '../../assets/carousel/1.png',
        tags: [],
    },

    {
        title: 'Parole 2',
        imageSource: '../../assets/carousel/2.png',
        tags: ['soutien', 'espoir'],
    },

    {
        title: 'Parole 3',
        imageSource: '../../assets/carousel/3.png',
        tags: ['amour'],
    },

    {
        title: 'Parole 4',
        imageSource: '../../assets/carousel/4.png',
        tags: ['courage', 'soutien'],
    },
];
