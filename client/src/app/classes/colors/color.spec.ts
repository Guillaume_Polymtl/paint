import { Color, MAX_ALPHA, MAX_COLOR_INTENSITY, MIN_ALPHA, MIN_COLOR_INTENSITY, NOMINAL_ALPHA, NOMINAL_COLOR_INTENSITY } from './color';

const INVALID_NEGATIVE_INTENSITY = MIN_COLOR_INTENSITY - 1;
const INVALID_POSITIVE_INTENSITY = MAX_COLOR_INTENSITY + 1;
const INVALID_NEGATIVE_ALPHA = MIN_ALPHA - 1;
const INVALID_POSTIVIE_ALPHA = MAX_ALPHA + 1;

describe('Color', () => {
    it('should create without an alpha and have expected values', () => {
        let color: Color = new Color(MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY);
        expect(color.red).toEqual(MIN_COLOR_INTENSITY);
        expect(color.green).toEqual(MIN_COLOR_INTENSITY);
        expect(color.blue).toEqual(MIN_COLOR_INTENSITY);
        color = new Color(MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
        expect(color.red).toEqual(MAX_COLOR_INTENSITY);
        expect(color.green).toEqual(MAX_COLOR_INTENSITY);
        expect(color.blue).toEqual(MAX_COLOR_INTENSITY);
        color = new Color(NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY);
        expect(color.red).toEqual(NOMINAL_COLOR_INTENSITY);
        expect(color.green).toEqual(NOMINAL_COLOR_INTENSITY);
        expect(color.blue).toEqual(NOMINAL_COLOR_INTENSITY);
    });

    it('should create without an alpha and have an alpha of 1', () => {
        const color = new Color(MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY);
        expect(color.alpha).toEqual(1);
    });

    it('should create with an alpha and have expected values', () => {
        let color: Color = new Color(MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_ALPHA);
        expect(color.red).toEqual(MIN_COLOR_INTENSITY);
        expect(color.green).toEqual(MIN_COLOR_INTENSITY);
        expect(color.blue).toEqual(MIN_COLOR_INTENSITY);
        expect(color.alpha).toEqual(MIN_ALPHA);
        color = new Color(MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_ALPHA);
        expect(color.red).toEqual(MAX_COLOR_INTENSITY);
        expect(color.green).toEqual(MAX_COLOR_INTENSITY);
        expect(color.blue).toEqual(MAX_COLOR_INTENSITY);
        expect(color.alpha).toEqual(MAX_ALPHA);
        color = new Color(NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_ALPHA);
        expect(color.red).toEqual(NOMINAL_COLOR_INTENSITY);
        expect(color.green).toEqual(NOMINAL_COLOR_INTENSITY);
        expect(color.blue).toEqual(NOMINAL_COLOR_INTENSITY);
        expect(color.alpha).toEqual(NOMINAL_ALPHA);
    });

    it('should properly sanitize invalid negative color intensities', () => {
        const color = new Color(INVALID_NEGATIVE_INTENSITY, INVALID_NEGATIVE_INTENSITY, INVALID_NEGATIVE_INTENSITY, INVALID_NEGATIVE_ALPHA);
        expect(color.red).toEqual(MIN_COLOR_INTENSITY);
        expect(color.green).toEqual(MIN_COLOR_INTENSITY);
        expect(color.blue).toEqual(MIN_COLOR_INTENSITY);
        expect(color.alpha).toEqual(MIN_ALPHA);
    });

    it('should properly sanitize invalid positive color intensities', () => {
        const color = new Color(INVALID_POSITIVE_INTENSITY, INVALID_POSITIVE_INTENSITY, INVALID_POSITIVE_INTENSITY, INVALID_POSTIVIE_ALPHA);
        expect(color.red).toEqual(MAX_COLOR_INTENSITY);
        expect(color.green).toEqual(MAX_COLOR_INTENSITY);
        expect(color.blue).toEqual(MAX_COLOR_INTENSITY);
        expect(color.alpha).toEqual(MAX_ALPHA);
    });

    it('should properly determine if 2 colors are equivalent', () => {
        const color = new Color(NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_ALPHA);
        const color2 = new Color(NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_ALPHA);
        expect(color.equals(color2)).toBeTrue();
        const color3 = new Color(MIN_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_ALPHA);
        expect(color.equals(color3)).toBeFalse();
        const color4 = new Color(NOMINAL_COLOR_INTENSITY, MIN_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_ALPHA);
        expect(color.equals(color4)).toBeFalse();
        const color5 = new Color(NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, MIN_COLOR_INTENSITY, NOMINAL_ALPHA);
        expect(color.equals(color5)).toBeFalse();
        const color6 = new Color(NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, MIN_ALPHA);
        expect(color.equals(color6)).toBeFalse();
    });
});
