export const MIN_COLOR_INTENSITY = 0;
export const MAX_COLOR_INTENSITY = 255;

export const NOMINAL_COLOR_INTENSITY = 128; // Helpful for testing
export const NOMINAL_COLOR_INTENSITY_HEX = '80'; // 128 in hex is '0x80'

export const MAX_ALPHA = 1;
export const MIN_ALPHA = 0;
export const NOMINAL_ALPHA = 0.5; // Helpful for testing

export const MAX_COLOR_HISTORY = 10;

export const MAX_TOLRERANCE = 100;
export const MIN_TOLERANCE = 0;

export const PIXEL_RED_OFFSET = 0;
export const PIXEL_GREEN_OFFSET = 1;
export const PIXEL_BLUE_OFFSET = 2;
export const PIXEL_ALPHA_OFFSET = 3;
export const PIXEL_DATA_SIZE = 4;

export enum ColorType {
    PRIMARY = 'Primary',
    SECONDARY = 'Secondary',
}

export class Color {
    private redIntensity: number;
    private greenIntensity: number;
    private blueIntensity: number;
    private alphaOpacity: number;

    constructor(red: number, green: number, blue: number, alpha?: number) {
        this.redIntensity = this.sanitizeColorIntensity(red);
        this.greenIntensity = this.sanitizeColorIntensity(green);
        this.blueIntensity = this.sanitizeColorIntensity(blue);
        if (typeof alpha === 'undefined') {
            alpha = 1;
        }
        this.alphaOpacity = this.sanitizeAlpha(alpha);
    }

    get red(): number {
        return this.redIntensity;
    }

    get blue(): number {
        return this.blueIntensity;
    }

    get green(): number {
        return this.greenIntensity;
    }

    get alpha(): number {
        return this.alphaOpacity;
    }

    private sanitizeAlpha(value: number): number {
        return this.sanitize(value, MAX_ALPHA, MIN_ALPHA);
    }

    private sanitizeColorIntensity(value: number): number {
        return this.sanitize(value, MAX_COLOR_INTENSITY, MIN_COLOR_INTENSITY);
    }

    private sanitize(value: number, max: number, min: number): number {
        if (value < min) {
            return min;
        } else if (value > max) {
            return max;
        } else {
            return value;
        }
    }

    // checks wether or not 2 colors are equivalent
    equals(color: Color): boolean {
        if (this.red !== color.red) {
            return false;
        }
        if (this.blue !== color.blue) {
            return false;
        }
        if (this.green !== color.green) {
            return false;
        }
        if (this.alpha !== color.alpha) {
            return false;
        }
        return true;
    }

    toString(): string {
        return `rgba(${this.redIntensity},${this.greenIntensity},${this.blueIntensity},${this.alphaOpacity})`;
    }
}
export const RED = new Color(MAX_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const YELLOW = new Color(MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const GREEN = new Color(MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const CYAN = new Color(MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const BLUE = new Color(MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const PURPLE = new Color(MAX_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY);

export const BLACK = new Color(MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY);
export const WHITE = new Color(MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY);

// tslint:disable-next-line: no-magic-numbers
export const SILVER = new Color(191, 191, 191, MAX_COLOR_INTENSITY);

// tslint:disable-next-line: no-magic-numbers
export const ORANGE = new Color(MAX_COLOR_INTENSITY, Math.round(MAX_COLOR_INTENSITY / 4), MIN_COLOR_INTENSITY, MAX_COLOR_INTENSITY);

export const DEFAULT_COLOR_HISTORY: Color[] = [YELLOW, ORANGE, RED, PURPLE, BLUE, CYAN, GREEN, BLACK, WHITE, SILVER];
