import { BLACK, WHITE } from './color';

export const DEFAULT_HEX = '00';
export const CIRCLE_DIAMETER = 2 * Math.PI;

// Palette
export const PALETTE_PICKER_DIAMETER = 10;
export const PALETTE_PICKER_LINE_WIDTH = 2;
export const PALETTE_PICKER_COLOR = WHITE;
export const PALETTE_TOLERANCE = 10;
export const PALETTE_JUMP = 1;

// Picker
export const COLOR_PICKER_TITLE = 'Couleurs';
export const DIALOG_CLASS = 'no-padding-dialog-container';

// Slider
export const SLIDER_LINE_WIDTH = 3;
export const SLIDER_LINE_COLOR = BLACK;
export const SLIDER_WIDTH = 10;
export const SLIDER_TOLERANCE = 10;
