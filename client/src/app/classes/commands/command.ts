import { Vec2 } from '@app/classes/vec2';

export abstract class Command {
    protected ctx: CanvasRenderingContext2D;
    cornerPosition: Vec2 = { x: 0, y: 0 };

    constructor(ctx: CanvasRenderingContext2D) {
        this.ctx = ctx;
    }

    abstract update(): void;

    abstract undo(): void;

    // abstract redo(): void;
}
