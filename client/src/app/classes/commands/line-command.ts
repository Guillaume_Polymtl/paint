import { Command } from '@app/classes/commands/command';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { LineService } from '@app/tools/line/line.service';

export class LineCommand extends Command {
    imageData: ImageData;
    isStart: boolean = false;
    popPoint: Vec2 = { x: 0, y: 0 };

    constructor(public lineService: LineService, drawingService: DrawingService) {
        super(drawingService.baseCtx);
        this.update();
    }

    update(): void {
        const length = this.lineService.pathData.length;
        /* istanbul ignore else*/
        if (length > 1) {
            const point1: Vec2 = this.lineService.pathData[length - 2];
            const point2: Vec2 = this.lineService.pathData[length - 1];
            const width =
                Math.max(point1.x, point2.x) - Math.min(point1.x, point2.x) + this.lineService.getWidth() + this.lineService.getJunctionDiameter();
            const height =
                Math.max(point1.y, point2.y) - Math.min(point1.y, point2.y) + this.lineService.getWidth() + this.lineService.getJunctionDiameter();
            const cornerX = Math.min(point1.x, point2.x) - this.lineService.getWidth() / 2 - this.lineService.getJunctionDiameter() / 2;
            const cornerY = Math.min(point1.y, point2.y) - this.lineService.getWidth() / 2 - this.lineService.getJunctionDiameter() / 2;
            this.cornerPosition.x = cornerX;
            this.cornerPosition.y = cornerY;
            this.imageData = this.ctx.getImageData(cornerX, cornerY, width, height);
        } else if (length === 1) {
            const center: Vec2 = this.lineService.pathData[0];
            const radius = Math.ceil(this.lineService.getJunctionDiameter() / 2);
            const cornerX = center.x > radius ? center.x - radius : 0;
            const cornerY = center.y > radius ? center.y - radius : 0;
            this.cornerPosition.x = cornerX;
            this.cornerPosition.y = cornerY;
            this.imageData = this.ctx.getImageData(cornerX, cornerY, 2 * radius, 2 * radius);
        }
    }

    undo(): void {
        const imageData = this.imageData;
        const cornerPosition: Vec2 = { x: this.cornerPosition.x, y: this.cornerPosition.y };
        this.update();
        this.ctx.putImageData(imageData, cornerPosition.x, cornerPosition.y);
        this.popPoint = this.lineService.pathData.pop() as Vec2;
    }
}
