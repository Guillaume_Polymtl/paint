export enum Filters {
    Aucun = 'Aucun',
    Sepia = 'Sepia',
    Inversion = 'Inversion',
    Constraste = 'Constraste',
    GrayScale = 'GrayScale',
    Rotation = 'Rotation',
}
