import { UserGuide } from './user-guide';

const ATTRIBUTE_PANEL_CONFIG = 'Le panneau d’attributs permet les configurations suivantes:';
const TRAIT_COLOR = 'couleur du trait';
const FILL_COLOR = 'couleur de remplissage';
const CONTOUR_COLOR = 'couleur de contour';
const TRAIT_TEXTURE = 'texture du trait';
const FILL_TYPE = 'type de tracé';
const PIXEL_SIZE = 'taille en pixels';
const CONTOUR_WIDTH = 'épaisseur du contour en pixels';
const AND = 'et';
const JUNCTION_TYPE = 'type de jonction (normal ou avec point)';
const JUNCTION_DIAMETER = 'diamètre des points de jonction (si l’option avec point est choisie)';
const GUIDE_ASSETS = '../../../assets/guide/';

export let guide: UserGuide = {
    title: "Guide d'utilisation",
    subguides: [
        {
            title: 'Divers',
            subguides: [
                {
                    title: 'Créer ou continuer un dessin',
                    img: `${GUIDE_ASSETS}new-drawing.gif`,
                    content: [
                        `
                        Pour créer un nouveau dessin, je peux via l'option "Créer un nouveau dessin" depuis le point d'entrée.
                        Je peux aussi créer un raccourci à partir de la combinaison Ctrl + O.
                    `,
                    ],
                },
                {
                    title: 'Carousels de dessins',
                    img: `${GUIDE_ASSETS}carousel.gif`,
                    content: [
                        `Affiche une fenêtre modale contenant un carrousel de dessins sauvegardés sur le serveur. Le
                    carrousel présente les dessins sous forme de petites fiches comportant chacune : le dessin, le nom du
                    dessin et les étiquettes associées au dessin. Les fiches ont toutes la même taille et l’espace alloué sur
                    celles-ci pour le dessin est fixe.`,
                        `
                    Le contenu du carrousel peut être filtré en spécifiant une ou plusieurs étiquettes. Lorsqu’une ou
                    plusieurs étiquettes sont sélectionnées, seulement les fiches qui contiennent au moins une des
                    étiquettes font partie du carrousel. Si aucune étiquette n’est choisie, toutes les fiches sont présentes.`,
                        `
                    L’utilisateur peut ouvrir (charger) un de ces dessins en cliquant dessus. Cette action fait afficher la vue
                    de dessin qui présente l’image demandée. Les fiches doivent aussi prévoir un bouton permettant à
                    l’utilisateur de supprimer un dessin du serveur. Tous les dessins sont publics. Un utilisateur peut donc
                    ouvrir ou supprimer n’importe quel dessin.`,
                    ],
                },
                {
                    title: 'Sauvegarde automatique et manuelle',
                    img: `${GUIDE_ASSETS}auto-save.gif`,
                    content: [
                        `Lorsqu’un dessin est en cours d’édition, une sauvegarde automatique doit a lieu. La sauvegarde
                    est locale, donc située sur le navigateur de l’utilisateur. Chaque fois qu’un des trois évènements
                    suivants a lieu, une sauvegarde automatique est déclenchée juste après :
                    • La création d’un dessin
                    • L’ouverture (chargement) d’un dessin
                    • Une modification de la surface de dessin
                    Une modification de la surface de dessin implique soit un changement du contenu de la matrice de
                    pixels, soit un changement au niveau de sa taille (redimensionnement).`,
                    ],
                },
                {
                    title: 'Exportation',
                    img: `${GUIDE_ASSETS}export.png`,
                    content: [
                        `Offre de créer une image à partir de la surface de dessin et de l’exporter dans un des formats
                    suivants : JPG ou PNG. L’utilisateur devra entrer un nom pour le fichier exporté.
                    S’il le souhaite, plutôt que de sauvegarder localement le dessin exporté, l’utilisateur doit pouvoir
                    l’envoyer par courriel. Dans un tel cas, l’utilisateur ne devra pas seulement fournir un nom de fichier,
                    mais aussi l’adresse du destinataire.
                    La fonctionnalité d’exportation doit aussi permettre d’appliquer un filtre sur l’image avant que
                    l’exportation ne soit effectuée. Une variété de cinq filtres différents doit être disponible. Par exemple,
                    un filtre pourrait retirer la couleur d’une image pour la transformer en niveaux de gris. Bien qu’il doive
                    y avoir plusieurs filtres disponibles, il ne doit être possible d’en appliquer qu’un seul lors d’une
                    exportation.
                    Finalement, l’interface d’exportation doit toujours présenter une vignette de prévisualisation de
                    l’image qui sera exportée.`,
                    ],
                },
            ],
        },
        {
            title: 'Dessiner',
            subguides: [
                {
                    title: 'Outils',
                    subguides: [
                        {
                            title: 'Dessin',
                            subguides: [
                                {
                                    title: 'Efface',
                                    img: `${GUIDE_ASSETS}eraser.gif`,
                                    content: [
                                        `
                                        Cet outil permet d’effacer (rendre blanc) des pixels de la surface de dessin. 
                                        Lorsque le clic gauche de la souris est enfoncé, 
                                            tous les pixels se trouvant sous l’icône du pointeur de la souris deviennent blancs. 
                                        Si le bouton gauche est maintenu enfoncé et que la souris est déplacée, 
                                            tous les prochains pixels qui entrent en contact avec 
                                            l’icône pendant son déplacement deviennent blancs eux aussi.
                                    `,
                                        `
                                        ${ATTRIBUTE_PANEL_CONFIG} ${TRAIT_COLOR} ${AND} ${PIXEL_SIZE}.
                                    `,
                                    ],
                                },
                                {
                                    title: 'Crayon',
                                    img: `${GUIDE_ASSETS}pencil.gif`,
                                    content: [
                                        `Le crayon est l’outil de base du logiciel de dessin.
                                        Il ne sert qu’à faire de simples traits sans texture particulière. Il a une pointe ronde.
                                    `,
                                        `
                                        ${ATTRIBUTE_PANEL_CONFIG} ${TRAIT_COLOR} ${AND} ${PIXEL_SIZE}.
                                    `,
                                    ],
                                },
                                {
                                    title: 'Pinceau',
                                    img: `${GUIDE_ASSETS}2.gif`,
                                    content: [
                                        `
                                        Cet outil est similaire au crayon. 
                                        Il n’en diffère que par la texture du trait. 
                                        L’application offre un choix de cinq textures différentes.
                                    `,
                                        `
                                        ${ATTRIBUTE_PANEL_CONFIG} ${TRAIT_COLOR}, ${PIXEL_SIZE} ${AND} ${TRAIT_TEXTURE}.
                                    `,
                                    ],
                                },
                                {
                                    title: 'Plume',
                                    img: `${GUIDE_ASSETS}3.gif`,
                                    content: [
                                        `Cet outil ressemble lui aussi au crayon. Sa différence est dans la forme de sa pointe qui doit être une
                                    mince ligne plutôt que d’être ronde.
                                    Le panneau d’attributs doit permettre les configurations suivantes :
                                    • Longueur de la ligne en pixels
                                    • Angle d’orientation de la ligne en degrés
                                    12
                                    Note : l’épaisseur doit être fixée à 2 pixels
                                    Il est aussi possible de changer l’angle de la ligne à l’aide de la roulette de la souris. À chaque cran de
                                    roulette, une rotation de 15 degrés est effectuée. Si la touche Alt du clavier est maintenue enfoncée
                                    pendant cette action, la rotation sera plutôt de 1 degré.
                                    Le sens de la roulette associé à un incrément positif ou négatif de l’angle est laissé au choix des
                                    développeurs.`,
                                    ],
                                },
                                {
                                    title: 'Sceau de peinture',
                                    img: `${GUIDE_ASSETS}bucket.gif`,
                                    content: [
                                        `Cet outil permet d’effectuer un « remplissage » qui colore une ou plusieurs étendues de pixels en
                                    fonction de leur couleur. L’outil peut s’utiliser selon deux modes d’opérations : « pixels contigus » et
                                    « pixels non contigus ».`,
                                        'Pixels contigus: clique gauche',
                                        'Pixel non contigus: clique droite',
                                    ],
                                },
                                {
                                    title: 'Aérosol',
                                    img: `${GUIDE_ASSETS}aerosol.gif`,
                                    content: [
                                        `Cet outil simule un effet de peinture en aérosol. Dès que le bouton est enfoncé, un jet de peinture est
                                    vaporisé sous le pointeur de la souris. L’outil continu ensuite d’émettre de la peinture à intervalle
                                    régulier jusqu’à qu’à ce que le bouton soit relâché. Pour plus de réalisme, le motif de vaporisation doit
                                    présenter de légères variations à chaque fois qu’il est émis. L’outil Airbrush de JS Paint montre le
                                    comportement attendu.
                                    Le panneau d’attributs doit permettre les configurations suivantes :
                                    • Nombre d’émissions par seconde
                                    • Diamètre du jet en pixels
                                    • Diamètre des gouttelettes composant le jet`,
                                    ],
                                },
                            ],
                        },
                        {
                            title: 'Formes',
                            subguides: [
                                {
                                    title: 'Ligne',
                                    img: `${GUIDE_ASSETS}line.gif`,
                                    content: [
                                        `Cet outil permet de tracer une ligne composée d’un ou plusieurs segments.
                                        Un premier clic définit la position de départ de la ligne. 
                                        Ensuite, chaque clic qui suit « connecte » avec le clic qui le précède 
                                            pour former un segment de la ligne.
                                        Tous les clics doivent être faits sur la surface de dessin.
                                        Cliquer hors de cette dernière n’aura aucun effet.
                                    `,
                                        `
                                        Si la touche Shift est enfoncée,
                                        le segment temporaire s’orientera selon l’un des angles suivants: 
                                            0, 45, 90, 135, 180, 225, 270 ou 315 degrés.
                                        Ce nouvel alignement reste en vigueur tant que la touche Shift est maintenue enfoncée.
                                    `,
                                        `Un double-clic indique qu’il s’agit du dernier point. 
                                        Un dernier segment est alors formé et la ligne est terminée.
                                        Si le double-clic est effectué à plus ou moins 20 pixels 
                                            (sur chaque axe) du point initial de la ligne, 
                                            ce sera alors les coordonnées du point initial qui seront utilisées 
                                            pour le deuxième point du segment final.
                                        Concrètement, la ligne sera fermée.
                                        Elle formera une boucle.
                                        Notez que la fonctionnalité de fermeture ne tient pas compte de la touche Shift. 
                                        La fermeture a donc lieu, même si la touche Shift est enfoncée, 
                                            et ce peu importe l’orientation du segment temporaire.
                                    `,
                                        `                                   
                                        Pendant la construction d’une ligne, 
                                            si l’utilisateur appuie sur la touche d’échappement (Escape), 
                                            la totalité de la ligne est annulée. 
                                        Si l’utilisateur appuie sur la touche de retour arrière (Backspace), 
                                            le point le plus récent est supprimé. 
                                        S’il s’agit du premier point, celui-ci n’est toutefois pas supprimé.
                                    `,
                                        `
                                        ${ATTRIBUTE_PANEL_CONFIG} ${JUNCTION_TYPE}, 
                                            ${JUNCTION_DIAMETER}, ${PIXEL_SIZE} ${AND} ${FILL_TYPE}.
                                    `,
                                    ],
                                },
                                {
                                    title: 'Rectangle',
                                    img: `${GUIDE_ASSETS}rectangle.gif`,
                                    content: [
                                        `
                                        Cet outil permet de dessiner des rectangles.
                                        À tout moment pendant la première ou la deuxième étape de la création, 
                                            si la touche Shift est maintenue appuyée, 
                                            la forme deviendra un carré plutôt qu’un rectangle.
                                        Celui-ci occupe la plus grande aire possible et sera ancré au coin initial du rectangle d’encadrement.
                                    `,
                                        `
                                        Note : la mise à jour de la forme, 
                                            c’est-à-dire le passage de rectangle à carré ou de carré à rectangle,
                                        se fait dès que l’état de la touche « Shift » change. 
                                        Autrement dit, 
                                            dès que la touche est appuyée ou relâchée. 
                                            Il n’est pas nécessaire de bouger la souris pour que la forme se mette à jour.
                                    `,
                                        `
                                        ${ATTRIBUTE_PANEL_CONFIG} ${FILL_COLOR}, ${CONTOUR_COLOR}, ${CONTOUR_WIDTH} ${AND} ${FILL_TYPE}.
                                    `,
                                    ],
                                },
                                {
                                    title: 'Ellipse',
                                    img: `${GUIDE_ASSETS}ellipse.gif`,
                                    content: [
                                        `
                                        Cet outil adopte le même comportement que celui permettant de dessiner des rectangles. 
                                        La seule différence est que la forme dessinée est une ellipse. 
                                        Comme pour l’outil rectangle, l’utilisation de la touche Shift aura un effet. 
                                        Ici, elle transformera l’ellipse en cercle.
                                    `,
                                        `
                                        ${ATTRIBUTE_PANEL_CONFIG} ${FILL_COLOR}, ${CONTOUR_COLOR}, ${CONTOUR_WIDTH} ${AND} ${FILL_TYPE}.
                                    `,
                                    ],
                                },
                                {
                                    title: 'Polygone',
                                    img: `${GUIDE_ASSETS}polygon.gif`,
                                    content: [
                                        `L’outil de polygone fonctionne comme l'outil rectangle. Il permet toutefois à l’utilisateur de
                                    spécifier le nombre de côtés de la forme à dessiner.
                                    Le panneau d’attributs doit permettre les configurations suivantes : ${FILL_TYPE}, ${FILL_COLOR}, ${CONTOUR_COLOR}, ${CONTOUR_WIDTH} ${AND} nombre de côtés.
                                    Note : les polygones dessinés sont réguliers et convexes.`,
                                    ],
                                },
                                {
                                    title: 'Étampe',
                                    img: `${GUIDE_ASSETS}stamp.gif`,
                                    content: [
                                        `L’étampe est un outil qui permet d’apposer de petites images sur le dessin. Pour l’utiliser, il suffit d’un
                                    simple clic du bouton gauche à l’endroit où l’on désire apposer l’image. Le clic doit être fait sur la
                                    surface de dessin. Si l’étampe ne peut pas être apposée complètement parce qu’une partie d’elle se
                                    trouverait hors de la surface de dessin, ce n’est pas grave. L’étampe est quand même dessinée, mais
                                    partiellement.
                                    Pour représenter l’étampe, l’icône du pointeur de la souris doit être l’image que l’étampe permet
                                    d’apposer sur le dessin. Cette image devra être positionnée correctement de sorte que lorsque le clic
                                    est fait, l’image apposée soit de même taille et orientation que celle représentant le pointeur de la
                                    souris.
                                    Le panneau d’attributs doit permettre les configurations suivantes :
                                    • Facteur de mise à échelle de l’étampe
                                    • Angle d’orientation de l’étampe en degrés
                                    • Choix d’image (l’utilisateur choisit l’image de l’étampe dans une liste)
                                    Les valeurs minimale et maximale du facteur de mise à échelle doivent faire en sorte qu’il sera autant
                                    possible de réduire l’image à une fraction de sa taille réelle que de l’agrandir jusqu’à plusieurs fois sa
                                    taille réelle.
                                    19
                                    Tout comme pour la ligne de l’outil plume, il est possible de changer l’angle d’orientation de l’étampe
                                    avec la roulette de la souris.`,
                                    ],
                                },
                                {
                                    title: 'Texte',
                                    img: `${GUIDE_ASSETS}text.gif`,
                                    content: [
                                        `Cet outil permet d’écrire des chaines de caractères sur la surface de dessin. Pour l’utiliser, il suffit de
                                    cliquer à l’endroit où l’on veut écrire. Il n’y a pas de retour à la ligne automatique, l’utilisateur doit
                                    appuyer sur la touche d’entrée (Enter) pour créer une nouvelle ligne. Pendant l’édition du texte, il
                                    devra être possible de se déplacer dans le texte avec les touches directionnelles (flèches) du clavier et
                                    17
                                    de supprimer avec la touche de retour arrière (Backspace) ou la touche de suppression (Delete). Si
                                    pendant l’édition, du texte se trouve hors de la surface de dessin, celui-ci ne doit pas être affiché
                                    même s’il continue d’exister. Un caractère partiellement hors de la surface de dessin devra être
                                    affiché partiellement. Pour terminer la création d’un texte, il suffit de cliquer à l’extérieur de celui-ci
                                    ou de sélectionner un autre outil. Lorsqu’un texte est terminé, toute partie se trouvant en dehors de
                                    la surface de dessin est perdue. Appuyer sur la touche d’échappement (Escape) annule la création du
                                    texte. Pendant la création du texte, tous les raccourcis sauf la touche d’échappement sont ignorés. Le
                                    texte doit être coloré en utilisant la couleur principale.
                                    Le panneau d’attributs doit permettre les configurations suivantes :
                                    • La police du texte
                                    • La taille de la police
                                    • Des mutateurs pour mettre le texte en gras et/ou italique.
                                    • Alignement du texte : gauche, centre, droit
                                    Note : les attributs s’appliquent à l’entièreté de la chaine de caractères. Par exemple, il n’est pas
                                    possible de seulement mettre un mot ou une lettre en gras.`,
                                    ],
                                },
                            ],
                        },
                        {
                            title: 'Sélection',
                            subguides: [
                                {
                                    title: 'Rectangle et ellipse',
                                    img: `${GUIDE_ASSETS}selection.gif`,
                                    content: [
                                        `
                                    Cet outil a un fonctionnement semblable à l’outil de création de rectangles. Le point initial du glisserdéposer
                                    pour créer le rectangle de sélection doit être situé sur la surface de dessin, mais en dehors
                                    d’une boite englobante et ne doit pas être sur un point de contrôle (s’il y a une sélection active).
                                    Après le point initial du glisser-déposer choisi, l’utilisateur déplace le pointeur de la souris pour
                                    déterminer la taille de la sélection. À tout moment pendant cette étape, un aperçu du contour de
                                    sélection doit être affiché, mais pas la boite englobante.`,
                                        `
                                    Comme pour l’outil de création de rectangles, l’utilisation de la touche d’échappement (Escape) fait
                                    annuler l’opération et l’utilisation de la touche Shift permet de forcer la création d’un carré plutôt
                                    qu’un rectangle.`,
                                        `
                                    Il est important de préciser que l’outil sélectionne une région rectangulaire de pixels de la surface de
                                    dessin et que cela inclut les pixels blancs qui font partie de « l’arrière-plan » du dessin. La sélection ne
                                    doit donc pas seulement inclure ce qui a été dessiné par l’utilisateur.`,
                                    ],
                                },
                                {
                                    title: 'Pipette',
                                    img: `${GUIDE_ASSETS}eye-dropper.gif`,
                                    content: [
                                        `Cet outil est utilisé pour saisir la couleur sous le pointeur de la souris. Un clic avec le bouton gauche
                                    assigne la couleur saisie à la couleur principale. Un clic avec le bouton droit l’assigne à la couleur
                                    secondaire. La couleur saisie est celle du pixel sous le pointeur de la souris.`,
                                    ],
                                },
                                {
                                    title: 'Baguette magique',
                                    img: '../../../assets/guide/3.gif',
                                    content: [
                                        `Cet outil se comporte de façon très similaire à l’outil sceau de peinture. En fait, la seule différence est
                                    que les pixels identifiés par l’outil ne sont pas colorés, mais forme une sélection. La baguette magique
                                    offre les mêmes modes d’opération que l’outil sceau de peinture.
                                    Pour le mode pixels contigus, une sélection sera formée avec la région de pixels. Celle-ci sera
                                    délimitée par un contour de sélection qui sera lui-même encadré par une boite englobante. Dans le
                                    cas du mode pixels non contigus, la sélection sera formée d’un ou plusieurs groupes de pixels
                                    disjoints. Chaque groupe aura son propre contour de sélection. L’ensemble de ces groupes sera lui
                                    aussi encadré par une boite englobante.`,
                                    ],
                                },
                            ],
                        },
                    ],
                },
                {
                    title: 'Manipulations de sélection',
                    subguides: [
                        {
                            title: 'Déplacement',

                            img: '../../../assets/guide/2.png',
                            content: [
                                `Une sélection peut être déplacée de deux façons. Soit avec la souris, soit avec le clavier.
                            Le déplacement via la souris s’effectue à l’aide d’un glisser-déposer avec le bouton gauche de la
                            souris. Le point initial du glisser-déposer doit se situer à l’intérieur de la boite englobante. Le
                            déplacement subi par la sélection doit être identique à celui fait par le pointeur de la souris. C’est-àdire
                            qu’en tout temps, donc du début jusqu’à la fin du déplacement, le point de la sélection situé sous
                            le pointeur reste le même.
                            Note : L’affichage doit être mis à jour dynamiquement pendant un déplacement par glisser-déposer.
                            24
                            Le déplacement via le clavier se fait à l’aide des touches directionnelles (les 4 flèches). À chaque fois
                            qu’une touche directionnelle est appuyée, la sélection se déplace de 3 pixels dans la direction
                            représentée par la touche. Si la touche est maintenue enfoncée, après un délai de 500 ms, une
                            séquence de déplacement à répétition débutera. Le déplacement aura donc lieu à toutes les 100 ms
                            jusqu’à ce que la touche soit relâchée.`,
                            ],
                        },
                        {
                            title: 'Redimensionnement',
                            img: '../../../assets/guide/3.jpg',
                            content: [
                                `C’est en déplaçant les points de contrôle de la boite englobante que l’on parvient à redimensionner la
                            sélection. Chaque déformation subie par la boite englobante est aussi appliquée, dans les mêmes
                            proportions, à la sélection qu’elle encadre.
                            Les points de contrôle de coin permettent d’agrandir ou de rapetisser la boite englobante sur ses deux
                            axes en même temps. Lorsqu’un point est éloigné ou rapproché de son opposé, cela étire ou
                            contracte la boite englobante. Pendant ces manipulations, le coin opposé reste fixe.
                            Redimensionnement sur les deux axes
                            Si la touche Shift est maintenue appuyée pendant le déplacement d’un point de contrôle de coin,
                            celui-ci ne pourra se déplacer que sur la droite passant par lui-même et son coin opposé. Autrement
                            dit, le redimensionnement n’altèrera pas le rapport d’aspect de la sélection. À tout moment si la
                            touche est relâchée, la boite englobante devra prendre la forme appropriée selon la position du
                            pointeur de la souris. Si la touche Shift est enfoncée à nouveau, la contrainte est aussitôt réappliquée.
                            Redimensionnement sur les deux axes en conservant le rapport d’aspect
                            25
                            Pour déformer la boite sur un seul axe à la fois, il faut utiliser les points de contrôle de côté. Lorsqu’un
                            de ces points est déplacé, le côté opposé de la boite reste fixe.
                            Redimensionnement qui écrase un triangle sur sa largeur
                            Si un déplacement de point de contrôle est tel que ce dernier se retrouve de l’autre côté de son
                            opposé, alors le redimensionnement aura aussi l’effet de produire l’image « miroir ». Sur un axe, sur
                            l’autre ou sur les deux en même temps selon la position du point de contrôle déplacé.`,
                            ],
                        },
                        {
                            title: 'Rotation',
                            img: '../../../assets/guide/3.jpg',
                            content: [
                                `Pour faire pivoter une sélection, il suffit d’utiliser la roulette de la souris. Le fonctionnement est le
                            même que pour l’outil plume.
                            Une sélection pivote autour de son centre. Autrement dit, elle pivote autour du centre de sa boite
                            englobante. L’alignement de la boite englobante après rotation est laissé à votre choix.`,
                            ],
                        },
                    ],
                },
                {
                    title: 'Palette de couleur',
                    img: `${GUIDE_ASSETS}color.gif`,
                    content: [
                        `
                        La couleur est un attribut « universel » qui n’est pas mémorisé pour chaque outil, 
                        mais plutôt partagé par tous les outils.
                    `,
                        `
                        Le panneau d’attributs contient deux couleurs configurables. 
                        Nous les appellerons les couleurs principale et secondaire.
                        Le panneau a un bouton permettant d’intervertir ces deux couleurs. 
                        Il a aussi avoir deux composants d’interface qui permettent de définir leur opacité. 
                        L’opacité définit à quel degré une couleur appliquée laisse transparaitre celle sur laquelle on l’applique.
                        Une couleur parfaitement opaque masquera la couleur originale. 
                        À l’opposé, une opacité nulle équivaut à l’absence de pigments.
                        Donc, une « couleur » qui ne colore pas du tout.
                    `,
                        `
                        Pour modifier l’une ou l’autre des couleurs, l’utilisateur doit cliquer dessus. 
                        Cette action fait apparaitre une palette de couleurs à partir de laquelle il pourra en choisir une nouvelle.
                        Une fois la couleur choisie et confirmée, la palette est masquée. 
                        La confirmation peut prendre plusieurs formes : bouton, clic à l’extérieur de la palette, etc.
                    `,
                        `Pour permettre à l’utilisateur d’être plus précis, 
                            la palette offre aussi la possibilité d’y entrer les valeurs de rouge, 
                            vert et bleu manuellement dans des champs texte. 
                        Les valeurs doivent être spécifiées en hexadécimale.
                        Finalement, le panneau mémorise les 10 dernières couleurs utilisées et les offrir comme choix.
                    `,
                        `
                        Note : un changement d’opacité ne compte pas comme un changement de couleur.
                        Un outil utilise toujours la couleur principale pour dessiner. 
                        La couleur secondaire est utilisée pour les contours lorsqu’il y en a.`,
                    ],
                },
                {
                    title: 'Annuler-refaire',
                    img: '../../../assets/guide/2.png',
                    content: [
                        `L’application doit permettre d’annuler les dernières actions que l’utilisateur a exécutées. Dans ce
                    contexte, une action signifie toute intervention de l’utilisateur menant à une modification de la
                    surface de dessin. Ceci inclut le redimensionnement de la surface de dessin. Les interventions ne
                    touchant pas les données du dessin, par exemple changer d’outil ou configurer les attributs d’un outil,
                    sont ignorées.
                    En activant la fonction annuler à répétition, l’utilisateur peut « reculer » dans l’état de son dessin, et
                    ce jusqu’à en revenir à l’état de départ. Si le dessin était un « nouveau dessin », alors la surface de
                    départ est une surface complètement blanche. Si le dessin avait été chargé depuis le carrousel de
                    dessins, alors la surface de départ est le dessin dans son état initial tel qu’il a été chargé.
                    Il doit aussi être possible de refaire ce qui a été annulé. Pour cela, il faut garder en mémoire les
                    actions qui ont été annulées, et ce dans leur ordre d’annulation. Ainsi, il sera possible de refaire
                    chaque action annulée en suivant l’ordre inverse comme dans une pile dernier entré, premier sorti
                    (LIFO). Dès qu’une nouvelle action (ajout, suppression, modification) est exécutée, la pile des actions
                    annulées est supprimée. Autrement dit, si l’on exécute une action qui modifie la surface de dessin
                    après avoir annulé des actions, ces dernières sont perdues et ne pourront plus être refaites. Par
                    exemple, la séquence suivante : créer un cercle (C1), créer un cercle (C2), créer un cercle (C3),
                    annuler, créer un rectangle (R1) produit la pile C1, C2, R1. Il est donc impossible de refaire C3.
                    Comme pour les fonctions utilisant le presse-papier, annuler et refaire devront elles aussi être
                    accessibles par des raccourcis clavier et des boutons. Ces derniers devront être placés sur la barre
                    latérale.
                    Selon l’état de l’application, annuler et/ou refaire devront parfois être désactivées. C’est le cas,
                    lorsqu’il n’y a rien à annuler et/ou refaire dans la pile.
                    Les deux options devront aussi être désactivées lorsqu’un outil est en cours d’utilisation. On considère
                    un outil « en cours d’utilisation » lorsque l’utilisateur est en train de réaliser une opération avec. Par
                    exemple, lorsque l’utilisateur déplace la souris pendant un glisser-déposer pour tracer un trait ou
                    créer une forme ou une sélection. Deux autres cas de « en cours d’utilisation » sont pendant la phase
                    28
                    d’ajout de segments avec l’outil ligne et pendant l’écriture d’une chaine de caractères avec l’outil
                    texte. Dès que l’opération est complétée et que l’outil redevient « au repos », les options sont
                    réactivées (si applicable).
                    Lorsqu’il y a une sélection active, annuler et refaire devront encore être désactivée. Cela implique que
                    les manipulations ou transformations appliquées pendant une sélection ne peuvent être annulées
                    individuellement. Autrement dit, lorsqu’une sélection est terminée, exécuter la fonction annuler
                    ramène le dessin à son état tout juste avant la sélection, peu importe le nombre de changements
                    (déplacement, redimensionnement, rotation) subits par la sélection.
                    Note : on ne veut pas d’une implémentation naïve pour annuler-refaire. C’est-à-dire qu’il ne faut pas
                    simplement empiler la matrice de pixels au grand complet à chaque fois qu’un changement a lieu. Un
                    effort doit être fait pour minimiser la consommation de ressources par ces fonctionnalités.`,
                    ],
                },
                {
                    title: 'Grille et magnétisme',
                    img: '../../../assets/guide/3.png',
                    content: [
                        `L’application doit permettre d’afficher une grille superposée à la surface de dessin et de son contenu.
                    Son point d’origine est le coin supérieur gauche de la surface. Il doit être possible d’activer ou
                    désactiver la grille, de lui assigner une valeur d’opacité et finalement d’indiquer la taille (en pixels) des
                    carrés la composant.
                    Note : la valeur d’opacité minimale devra être facilement visible. Il n’y a aucun intérêt à avoir une
                    valeur minimale nulle puisqu’il est possible de simplement désactiver la grille. La grille est une option
                    de la surface de dessin. Elle ne doit pas être présente lorsqu’un dessin est sauvegardé, exporté ou
                    envoyé par courriel.`,
                        `Lorsque cette option est activée, chaque fois qu’une boite englobante est déplacée à l’aide de la
                    souris celle-ci se « collera » ou s’alignera sur la ligne de grille la plus près. Cet alignement se fait en x
                    et en y.
                    Prenons en exemple un cas où le magnétisme est activé pour une grille ayant des carrés de cinq pixels
                    de côté. Avec cette configuration, une sélection déplacée ne suivra pas le mouvement du pointeur de
                    la souris point par point. Elle fera plutôt des bonds de cinq pixels pour s’aligner avec les lignes de la
                    grille. Les bonds sont déterminés en arrondissant les valeurs de positions. Ainsi, une sélection située à
                    la position 45 ne bougera pas sur l’axe si on la glisse aux positions 43, 44, 46 ou 47 de cet axe. Si le
                    29
                    déplacement de la souris l’envoie aux positions 42 ou 41, la sélection sera aussitôt placée à 40. Dans
                    l’autre sens, si on l’amène à 48 ou 49, la position sera arrondie à 50.
                    Quand une boite englobante est déplacée avec les touches directionnelles, si l’option de magnétisme
                    est activée, la sélection ne se déplacera pas à coup de 3 pixels. Elle se déplacera plutôt de façon à
                    s’aligner sur la ligne de grille la plus près dans la direction correspondant à la touche appuyée.
                    Le point utilisé pour l’alignement sur la grille doit pouvoir être configurable. Il peut s’agir de l’un des
                    neuf points suivants :
                    • Le centre de la boite englobante
                    • L’un des huit points de contrôle de la boite englobante
                    Une boite englobante alignée sur la grille par son point de contrôle supérieur gauche
                    Note : Le magnétisme ne nécessite pas que la grille soit visible pour fonctionner.`,
                    ],
                },
            ],
        },
    ],
    // tslint:disable-next-line: max-file-line-count
};
