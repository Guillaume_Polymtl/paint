// Regex Constants

// Alphanumeric Expresions, 3 to 10 chars word allowed space and dash included
export const REGEX_TITLE: RegExp = /^[A-Za-z0-9- ]{3,10}$/;
// EMAIL REGEX VALIDATION
export const REGEX_EMAIL: RegExp = /^((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/m;
// Alphanumeric Expresions, 3 to 10 chars word allowed and dash included
export const REGEX_TAG: RegExp = /^[A-Za-z0-9]{3,10}$/;
