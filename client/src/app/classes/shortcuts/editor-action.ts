export enum EditorAction {
    DISPLAY_GRID = 'Display Grid',
    MAGNETISM = 'Magnetism',
    INCREASE_GRID_SIZE = 'Increase Grid Size',
    DECREASE_GRID_SIZE = 'dECREASE Grid Size',
}