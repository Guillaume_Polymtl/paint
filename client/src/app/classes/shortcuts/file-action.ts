export enum FileAction {
    CREATE = 'Create',
    CLEAR = 'Clear',
    CONTINUE = 'Continue',
    SAVE = 'Save',
    CAROUSEL = 'Carousel',
    EXPORT = 'Export',
    NONE = 'None',
}
