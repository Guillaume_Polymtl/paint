export enum ManipulationAction {
    CUT = 'Cut',
    COPY = 'Copy',
    PASTE = 'Paste',
    DELETE = 'Delete',
    SELECT_ALL = 'Select All',
    MOVE_LEFT = 'Move Left',
    MOVE_RIGHT = 'Move Right',
    MOVE_DOWN = 'Move down',
    MOVE_UP = 'Move up',
    UNDO = 'Undo',
    REDO = 'Redo',
}