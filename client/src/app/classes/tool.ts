import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { Color, ColorType } from './colors/color';
import { ToolName } from './toolbar/tool-name';
import { Vec2 } from './vec2';

// Ceci est justifié vu qu'on a des fonctions qui seront gérés par les classes enfant
// tslint:disable:no-empty
export class Tool {
    name: ToolName;
    mouseDownCoord: Vec2;
    mouseDown: boolean = false;
    color: Color;
    secondaryColor: Color;

    constructor(protected drawingService: DrawingService, protected colorService: ColorService) {
        this.onInit();
    }

    onInit(): void {
        this.colorService.getColor(ColorType.PRIMARY).subscribe((color: Color) => {
            this.color = color;
        });
        this.colorService.getColor(ColorType.SECONDARY).subscribe((color: Color) => {
            this.secondaryColor = color;
        });
    }

    onToolChange(): void {}

    onMouseDown(event: MouseEvent): void {}

    onMouseUp(event: MouseEvent): void {}

    onMouseMove(event: MouseEvent): void {}

    onContextMenu(event: MouseEvent): void {}

    onMouseLeave(event: MouseEvent): void {}

    onMouseClick(event: MouseEvent): void {}

    onMouseDblClick(event: MouseEvent): void {}

    onKeyDown(event: KeyboardEvent): void {}

    onKeyUp(event: KeyboardEvent): void {}

    onMouseWheel(event: WheelEvent): void {}

    getPositionFromMouse(event: MouseEvent): Vec2 {
        return { x: event.offsetX, y: event.offsetY };
    }
}
