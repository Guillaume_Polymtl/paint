import { Type } from '@angular/core';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { PanelComponent } from '@app/components/attributes-panel/panel.component';

export class Panel {
    constructor(public name: ToolName, public component: Type<PanelComponent>) {}
}
