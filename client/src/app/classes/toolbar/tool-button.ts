import { ToolName } from './tool-name';

export class ToolButton {
    name: ToolName;
    selected: boolean;
}
