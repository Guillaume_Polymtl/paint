import { Vec2 } from '@app/classes/vec2';

export enum DrawTypes {
    Contour = 'Contour',
    Fill = 'Fill',
    FillWithContour = 'FillWithContour',
}

export enum KeyValues {
    Escape = 'Escape',
    Backspace = 'Backspace',
    Shift = 'Shift',
    Alt = 'alt',
    ArrowUp = 'ArrowUp',
    ArrowDown = 'ArrowDown',
    ArrowLeft = 'ArrowLeft',
    ArrowRight = 'ArrowRight',
    Enter = 'Enter',
    Tab = 'Tab',
    CapsLock = 'CapsLock',
    Insert = 'Insert',
    PageUp = 'PageUp',
    PageDown = 'PageDown',
    Delete = 'Delete',
    Plus = '+',
    Minus = '-',
    GridMode = 'g',
    Shit = 'Shift',
    MagnetMode = 'm',
    setNewAngles = 'Alt',
}

export enum ARROW {
    UP,
    RIGHT,
    DOWN,
    LEFT,
    EscapeKey = 'Escape',
    BackspaceKEy = 'Backspace',
    ShiftKey = 'Shift',
    ArrowupKey = 'ArrowUp',
    ArrowdownKey = 'ArrowDown',
    ArrowleftKey = 'ArrowLeft',
    ArrowrightKey = 'ArrowRight',
    Alt = 'alt',
}

export enum Angles {
    _0 = 0,
    _45 = 45,
    _90 = 90,
    _135 = 135,
    _180 = 180,
    _Neg45 = -45,
    _Neg90 = -90,
    _Neg135 = -135,
}

export enum ImageState {
    Loading = 'Loading',
    Complete = 'Complete',
    LoadingError = 'LoadingError',
    UsedCanvas = 'UsedCanvas',
    DeletionError = 'DeletionError',
    NoCorrespondence = 'NoCorrespondence',
    Idle = 'Idle',
}

export enum SelectionState {
    Idle = 'Idle',
    Selecting = 'Selecting',
    Moving = 'Moving',
    Resizing = 'Resizing',
    Rotating = 'Rotating',
}

// pinceau constants
export const BRUSH_DEFAULT_WIDTH = 50;
export const BRUSH_MIN_WIDTH = 20;
export const BRUSH_MAX_WIDTH = 100;
export enum BrushTextures {
    Texture1 = 1,
    Texture2 = 2,
    Texture3 = 3,
    Texture4 = 4,
    Texture5 = 5,
}

// Canvas Constants
export const CANVAS_DEFAULT_WIDTH = 1000;
export const CANVAS_DEFAULT_HEIGHT = 800;

// Line constants
export const LINE_DELTA_ANGLE = 45;
export const LINE_MIN_DIST_FOR_CLOSURE = 20;
export const LINE_DEFAULT_WIDTH = 2;
export const LINE_DEFAULT_COLOR = 'black';
export const LINE_MIN_WIDTH = 1;
export const LINE_MAX_WIDTH = 20;
export const LINE_DEFAULT_JUNCTION_DIAMETER = 2;
export const LINE_MIN_JUNCTION_DIAMETER = 1;
export const LINE_MAX_JUNCTION_DIAMETER = 20;

// Rectangle Constants
export const RECT_DEFAULT_DRAWTYPE: DrawTypes = DrawTypes.Contour;
export const RECT_DEFAULT_FILL_COLOR = 'black';
export const RECT_DEFAULT_CONTOUR_COLOR = 'blue';
export const RECT_DEFAULT_CONTOUR_WIDTH = 5;
export const RECT_MIN_CONTOUR_WIDTH = 1;
export const RECT_MAX_CONTOUR_WIDTH = 20;

// Ellipse Constants
export const ELLIPSE_DEFAULT_DRAWTYPE: DrawTypes = DrawTypes.Contour;
export const ELLIPSE_DEFAULT_FILL_COLOR = 'black';
export const ELLIPSE_DEFAULT_CONTOUR_COLOR = 'red';
export const ELLIPSE_DEFAULT_CONTOUR_WIDTH = 5;
export const ELLIPSE_MIN_CONTOUR_WIDTH = 1;
export const ELLIPSE_MAX_CONTOUR_WIDTH = 20;
export const ELLIPSE_DEFAULT_RECT_WIDTH = 1;
export const ELLIPSE_DEFAULT_RECT_COLOR = 'black';
export const ELLIPSE_PERIMETER_DASH = 5;

// Pencil constants
export const PENCIL_DEFAULT_WIDTH = 25;
export const PENCIL_MIN_WIDTH = 1;
export const PENCIL_MAX_WIDTH = 50;

// Feather Pen constants
export const FEATHER_DEFAULT_LENGTH = 5;
export const FEATHER_DEFAULT_ANGLE = 15;
export const FEATHER_DEFAULT_WIDTH = 2;
export const FEATHER_MIN_LENGTH = 1;
export const FEATHER_MAX_LENGTH = 50;
export const MAX_ANGLE = 360;
export const MIN_ANGLE = 0;
export const MIN_ANGLE_STEP = 1;
export const MAX_ANGLE_STEP = 15;

// Grid constants
export const DEFAULT_THICKNESS = 50;
export const DEFAULT_OPACITY = 0.5;
export const MIN_THICKNESS = 15;
export const MAX_THICKNESS = 100;
export const MIN_OPACITY = 0.1;
export const MAX_OPACITY = 1;
export const THICKNESS_STEP = 5;
export const OPACITY_STEP = 0.1;

// Eraser constants
export const ERASER_LINE_WIDTH = 2;
export const ERASER_DEFAULT_WIDTH = 20;
export const ERASER_MIN_WIDTH = 5;
export const ERASER_MAX_WIDTH = 100;

// Selection constants
export const NUMBER_PIXELS_OFFSET = 3;
export const SELECTION_REPEAT_DELAY = 100;
export const SELECTION_REPEAT_START_TIME = 500;
export const SELECTION_DASH_LINE = 6;
export const SELECTION_ANCHOR_SIZE = 10;
export const SELECTION_TRANSLATION_DIRECTIONS: Map<KeyValues, Vec2> = new Map<KeyValues, Vec2>()
    .set(KeyValues.ArrowUp, { x: 0, y: -1 })
    .set(KeyValues.ArrowDown, { x: 0, y: 1 })
    .set(KeyValues.ArrowLeft, { x: -1, y: 0 })
    .set(KeyValues.ArrowRight, { x: 1, y: 0 });
export const DEFAULT_TIMEOUT_ID = -1; // should be negative

// Rotation constants
export const DIVIDER_FOR_ROTATION_ANGLE = 12;
export const ALT_DIVIDER_FOR_ROTATION_ANGLE = 180;

// Polygon constants
export const POLYGON_DEFAULT_DRAWTYPE: DrawTypes = DrawTypes.Contour;
export const POLYGON_DEFAULT_FILL_COLOR = 'black';
export const POLYGON_DEFAULT_CONTOUR_COLOR = 'red';
export const POLYGON_DEFAULT_CONTOUR_WIDTH = 5;
export const POLYGON_MIN_CONTOUR_WIDTH = 1;
export const POLYGON_MAX_CONTOUR_WIDTH = 20;
export const POLYGON_DEFAULT_SIDES = 5;
export const POLYGON_MIN_SIDES = 3;
export const POLYGON_MAX_SIDES = 12;
export const POLYGON_DEFAULT_PERIMETER_WIDTH = 1;
export const POLYGON_DEFAULT_PERIMETER_COLOR = 'black';
export const POLYGON_DEFAULT_PERIMETER_DASH = 5;

// Eye dropper constants
export const EYE_DROPPER_SCALE_FACTOR = 25;
export const EYE_DROPPER_IMAGE_DATA_HALF_SIZE = 5;
export const EYE_DROPPER_CANVAS_SIZE = EYE_DROPPER_IMAGE_DATA_HALF_SIZE * 2 * EYE_DROPPER_SCALE_FACTOR;

// sparypaint constants
export const SPRAYPAINT_DEFAULT_JET_DIAMETER = 25;
export const SPRAYPAINT_MIN_JET_DIAMETER = 1;
export const SPRAYPAINT_MAX_JET_DIAMETER = 50;
export const SPRAYPAINT_DEFAULT_DROPLET_DIAMETER = 2;
export const SPRAYPAINT_DROPLET_DIAMETER_MIN = 1;
export const SPRAYPAINT_DROPLET_DIAMETER_MAX = 5;
export const SPRAYPAINT_INTERVAL_MS = 10;
export const SECOND_DURATION_MS = 1000;
export const SPRAYPAINT_DEFAULT_EMISSIONS_PER_SECOND = 4000;
export const SPRAYPAINT_MIN_EMISSIONS_PER_SECOND = 1000;
export const SPRAYPAINT_MAX_EMISSIONS_PER_SECOND = 8000;

// Text constant
export const TEXT_DEFAULT_FONT_SIZE = 50;
export const TEXT_MIN_FONT_SIZE = 20;
export const TEXT_MAX_FONT_SIZE = 200;
export const CURSOR_WIDTH = 5;
export const POST_CURSOR_BUFFER = 20;
const ARIAL = 'Arial';
const COURIER_NEW = 'Courier New';
const COMIC_SANS_MS = 'Comic Sans MS';
const IMPACT = 'Impact';
const LUCIDA = 'Lucida Sans Unicode';
export const TEXT_DEFAULT_FONT_FAMILY = ARIAL;
export const TEXT_FONT_FAMILIES = [ARIAL, COURIER_NEW, COMIC_SANS_MS, IMPACT, LUCIDA];
export enum TEXT_ALIGN {
    LEFT = 'left',
    RIGHT = 'right',
    CENTER = 'center',
}

// stamp constants
export const STAMP_DEFAULT_WIDTH = 20;
export const STAMP_MIN_WIDTH = 10;
export const STAMP_MAX_WIDTH = 100;
export const STAMP_DEFAULT_ANGLE = 0;
export const STAMP_STEP_ANGLE = 15;
export const STAMP_ALT_ANGLE = 1;

// stamp textures
export enum StampChoices {
    StampChoices1 = 'lmao',
    StampChoices2 = 'love',
    StampChoices3 = 'omg',
    StampChoices4 = 'wink',
    StampChoices5 = 'boss',
}

export enum StampIconsPath {
    StampPath1 = '../../assets/stamp/lmao.svg',
    StampPath2 = '../../assets/stamp/love.svg',
    StampPath3 = '../../assets/stamp/omg.svg',
    StampPath4 = '../../assets/stamp/wink.svg',
    StampPath5 = '../../assets/stamp/boss.svg',
}
