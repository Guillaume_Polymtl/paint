export class UserGuide {
    title: string;
    img?: string;
    content?: string[];
    subguides?: UserGuide[];
}
