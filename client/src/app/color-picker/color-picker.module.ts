// code from Lukas Marx at https://malcoded.com/posts/angular-color-picker/
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSliderModule } from '@angular/material/slider';
import { ColorDialogComponent } from './components/color-dialog/color-dialog.component';
import { ColorPaletteComponent } from './components/color-palette/color-palette.component';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { ColorSliderComponent } from './components/color-slider/color-slider.component';

@NgModule({
    declarations: [ColorPickerComponent, ColorSliderComponent, ColorPaletteComponent, ColorDialogComponent],
    imports: [CommonModule, MatSliderModule, MatDialogModule, FormsModule, MatButtonModule, MatFormFieldModule, MatInputModule],
    exports: [ColorPickerComponent],
})
export class ColorPickerModule {}
