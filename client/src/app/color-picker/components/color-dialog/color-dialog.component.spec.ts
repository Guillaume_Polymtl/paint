import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BLACK, Color, MIN_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY_HEX, WHITE } from '@app/classes/colors/color';
import { ColorPaletteComponent } from '@app/color-picker/components/color-palette/color-palette.component';
import { ColorSliderComponent } from '@app/color-picker/components/color-slider/color-slider.component';
import { ColorDialogComponent } from './color-dialog.component';

describe('ColorDialogComponent', () => {
    let component: ColorDialogComponent;
    let fixture: ComponentFixture<ColorDialogComponent>;

    const dialogMock = {
        open: () => {
            return;
        },
        close: () => {
            return;
        },
    };

    const emptyString = '';

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ColorDialogComponent, ColorSliderComponent, ColorPaletteComponent],
            providers: [
                { provide: MatDialogRef, useValue: dialogMock },
                { provide: MAT_DIALOG_DATA, useValue: BLACK },
            ],
            imports: [MatButtonModule, MatFormFieldModule, MatInputModule, BrowserAnimationsModule, FormsModule, MatDialogModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have a default color of black', () => {
        expect(component.color).toEqual(BLACK);
    });

    it('should repeatedly change the current color and hex values ', () => {
        let color = new Color(MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY, MIN_COLOR_INTENSITY);
        component.colorSelection(color);
        expect(component.hexRed).toEqual('0');
        expect(component.hexGreen).toEqual('0');
        expect(component.hexBlue).toEqual('0');
        color = new Color(NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY);
        component.colorSelection(color);
        expect(component.hexRed).toEqual(NOMINAL_COLOR_INTENSITY_HEX);
        expect(component.hexGreen).toEqual(NOMINAL_COLOR_INTENSITY_HEX);
        expect(component.hexBlue).toEqual(NOMINAL_COLOR_INTENSITY_HEX);
    });

    it('should update the color on user input', () => {
        component.onUserInput(NOMINAL_COLOR_INTENSITY_HEX, NOMINAL_COLOR_INTENSITY_HEX, NOMINAL_COLOR_INTENSITY_HEX);
        const color = new Color(NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY);
        expect(component.color).toEqual(color);
    });

    it('should sanitize empty strings', () => {
        component.onUserInput(emptyString, emptyString, emptyString);
        expect(component.color.red).toEqual(0);
        expect(component.color.blue).toEqual(0);
        expect(component.color.green).toEqual(0);
    });

    it('should close the dialog on cancel', () => {
        const spy = spyOn(component.dialogRef, 'close').and.callThrough();
        component.onCancel();
        expect(spy).toHaveBeenCalled();
    });

    it('should close the dialog on select with the latest color', () => {
        const spy = spyOn(component.dialogRef, 'close').and.callThrough();
        component.colorSelection(WHITE);
        component.onSelect();
        expect(spy).toHaveBeenCalledWith(WHITE);
    });
});
