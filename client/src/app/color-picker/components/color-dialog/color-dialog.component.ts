import { ChangeDetectorRef, Component, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Color } from '@app/classes/colors/color';
import { DEFAULT_HEX } from '@app/classes/colors/ui';

@Component({
    selector: 'app-color-dialog',
    templateUrl: './color-dialog.component.html',
    styleUrls: ['./color-dialog.component.scss'],
})
export class ColorDialogComponent {
    defaultColor: Color;
    hexRed: string = DEFAULT_HEX;
    hexGreen: string = DEFAULT_HEX;
    hexBlue: string = DEFAULT_HEX;
    hue: string;
    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        public dialogRef: MatDialogRef<ColorDialogComponent>,
        @Optional() @Inject(MAT_DIALOG_DATA) public color: Color,
    ) {
        this.defaultColor = color;
        this.colorSelection(color);
    }
    colorSelection(color: Color): void {
        this.color = color;
        this.hexRed = this.color.red.toString(16).toUpperCase();
        this.hexGreen = this.color.green.toString(16).toUpperCase();
        this.hexBlue = this.color.blue.toString(16).toUpperCase();
    }
    onUserInput(hexRed: string, hexGreen: string, hexBlue: string): void {
        let red = parseInt(hexRed, 16);
        if (isNaN(red)) {
            red = 0;
        }
        let green = parseInt(hexGreen, 16);
        if (isNaN(green)) {
            green = 0;
        }
        let blue = parseInt(hexBlue, 16);
        if (isNaN(blue)) {
            blue = 0;
        }
        const color: Color = new Color(red, green, blue);
        this.color = color;
        // hacky way to detect changes
        this.changeDetectorRef.detectChanges();
    }
    onSelect(): void {
        this.dialogRef.close(this.color);
    }
    onCancel(): void {
        this.dialogRef.close(this.defaultColor);
    }
}
