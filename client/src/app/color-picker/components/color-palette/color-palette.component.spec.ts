import { SimpleChange } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MouseButton } from '@app/classes/mouse-buttons';

import { ColorPaletteComponent } from './color-palette.component';

describe('ColorPaletteComponent', () => {
    let component: ColorPaletteComponent;
    let fixture: ComponentFixture<ColorPaletteComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ColorPaletteComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorPaletteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    const leftClick = new MouseEvent('click', { button: MouseButton.Left });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should draw a circle around the selected position', () => {
        component.selectedPosition = { x: 0, y: 0 };
        component.draw();
        expect(component.ctx?.strokeStyle).toEqual('#ffffff');
    });

    it('should emit a new color on ngOnChanges', () => {
        const spy = spyOn(component.color, 'next');
        component.selectedPosition = { x: 0, y: 0 };
        component.ngOnChanges({
            hue: new SimpleChange(null, component.hue, true),
        });
        expect(spy).toHaveBeenCalled();
    });

    it('should not emit a color on ngOnChanges if the hue is not set', () => {
        const spy = spyOn(component.color, 'next');
        component.selectedPosition = { x: 0, y: 0 };
        component.ngOnChanges({
            data: new SimpleChange(null, null, true),
        });
        expect(spy).not.toHaveBeenCalled();
    });

    it('should set mouse down to false onMouseUp', () => {
        component.onMouseUp(leftClick);
        expect(component.mouseDown).toBeFalse();
        component.onWindowMouseUp(leftClick);
        expect(component.mouseDown).toBeFalse();
    });

    it('should set mouse down to true onMouseDown', () => {
        component.onMouseDown(leftClick);
        expect(component.mouseDown).toBeTrue();
    });

    it('should call emit color onMouseMove if mousedown is true', () => {
        component.mouseDown = true;
        const event = new MouseEvent('mousemove');
        const spy = spyOn(component, 'emitColor');
        component.onMouseMove(event);
        expect(spy).toHaveBeenCalled();
    });

    it('should not emit color onMouseMove if mousedown is falsy ', () => {
        component.mouseDown = false;
        const spy = spyOn(component, 'emitColor');
        component.onMouseMove(leftClick);
        expect(spy).not.toHaveBeenCalled();
    });

    it('should actually emit a color when emitColor is called', () => {
        const spy = spyOn(component.color, 'next');
        const position = { x: 0, y: 0 };
        component.emitColor(position);
        expect(spy).toHaveBeenCalled();
    });
});
