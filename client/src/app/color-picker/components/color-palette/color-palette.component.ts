// code from Lukas Marx at https://malcoded.com/posts/angular-color-picker/
import { AfterViewInit, Component, ElementRef, HostListener, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Color } from '@app/classes/colors/color';
import { CIRCLE_DIAMETER, PALETTE_PICKER_COLOR, PALETTE_PICKER_DIAMETER, PALETTE_PICKER_LINE_WIDTH } from '@app/classes/colors/ui';
import { Vec2 } from '@app/classes/vec2';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-color-palette',
    templateUrl: './color-palette.component.html',
    styleUrls: ['./color-palette.component.scss'],
})
export class ColorPaletteComponent implements AfterViewInit, OnChanges {
    @Input() height: number;
    @Input() width: number;
    @Input() hue: string;
    @Input() defaultColor: Color;
    @Output() colorSelection: Subject<Color> = new Subject();
    @Output() color: Subject<Color> = new Subject();
    @ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>;
    ctx: CanvasRenderingContext2D;
    mouseDown: boolean = false;
    selectedPosition: Vec2;

    ngAfterViewInit(): void {
        this.draw();
    }

    draw(): void {
        if (!this.ctx && this.canvas) {
            this.ctx = this.canvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        }
        if (this.ctx) {
            const width = this.canvas.nativeElement.width;
            const height = this.canvas.nativeElement.height;
            this.ctx.fillStyle = this.hue || 'rgba(255,255,255,1)';
            this.ctx.fillRect(0, 0, width, height);
            const whiteGrad = this.ctx.createLinearGradient(0, 0, width, 0);
            whiteGrad.addColorStop(0, 'rgba(255,255,255,1)');
            whiteGrad.addColorStop(1, 'rgba(255,255,255,0)');
            this.ctx.fillStyle = whiteGrad;
            this.ctx.fillRect(0, 0, width, height);
            const blackGrad = this.ctx.createLinearGradient(0, 0, 0, height);
            blackGrad.addColorStop(0, 'rgba(0,0,0,0)');
            blackGrad.addColorStop(1, 'rgba(0,0,0,1)');
            this.ctx.fillStyle = blackGrad;
            this.ctx.fillRect(0, 0, width, height);
            if (this.selectedPosition) {
                this.ctx.strokeStyle = PALETTE_PICKER_COLOR.toString();
                this.ctx.fillStyle = PALETTE_PICKER_COLOR.toString();
                this.ctx.beginPath();
                this.ctx.arc(this.selectedPosition.x, this.selectedPosition.y, PALETTE_PICKER_DIAMETER, 0, CIRCLE_DIAMETER);
                this.ctx.lineWidth = PALETTE_PICKER_LINE_WIDTH;
                this.ctx.stroke();
            }
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hue) {
            this.draw();
            if (this.selectedPosition) {
                this.color.next(this.getColorAtPosition(this.selectedPosition));
            }
        }
    }

    @HostListener('mouseup', ['$event'])
    onMouseUp(event: MouseEvent): void {
        event.preventDefault();
        event.stopPropagation();
        this.mouseDown = false;
        this.selectedPosition = this.getPositionFromMouseEvent(event);
        this.draw();
        this.emitColorSelection(this.selectedPosition);
    }

    @HostListener('window:mouseup', ['$event'])
    onWindowMouseUp(evt: MouseEvent): void {
        this.mouseDown = false;
    }

    onMouseDown(event: MouseEvent): void {
        event.preventDefault();
        this.selectedPosition = this.getPositionFromMouseEvent(event);
        this.draw();
        this.mouseDown = true;
        this.color.next(this.getColorAtPosition(this.selectedPosition));
    }

    onMouseMove(event: MouseEvent): void {
        if (this.mouseDown) {
            this.selectedPosition = this.getPositionFromMouseEvent(event);
            this.draw();
            this.emitColor(this.selectedPosition);
        }
    }

    emitColor(position: Vec2): void {
        const color = this.getColorAtPosition(position);
        this.color.next(color);
    }

    emitColorSelection(position: Vec2): void {
        const color = this.getColorAtPosition(position);
        this.colorSelection.next(color);
    }

    getColorAtPosition(position: Vec2): Color {
        const imageData = this.ctx.getImageData(position.x, position.y, 1, 1).data;
        return new Color(imageData[0], imageData[1], imageData[2], 1);
    }

    private getPositionFromMouseEvent(event: MouseEvent): Vec2 {
        return { x: event.offsetX, y: event.offsetY };
    }
}
