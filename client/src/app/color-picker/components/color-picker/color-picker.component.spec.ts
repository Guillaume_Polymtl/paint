import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatSliderChange, MatSliderModule } from '@angular/material/slider';
import { BLACK, Color, ColorType, MAX_COLOR_HISTORY, NOMINAL_COLOR_INTENSITY, WHITE } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { ColorService } from '@app/color-picker/services/color.service';
import { of } from 'rxjs';
import { ColorPickerComponent } from './color-picker.component';

class MatDialogMock {
    open(): object {
        {
            return {
                afterClosed: () => of(WHITE),
            };
        }
    }
}

describe('ColorPickerComponent', () => {
    let component: ColorPickerComponent;
    let fixture: ComponentFixture<ColorPickerComponent>;
    let colorServiceSpy: jasmine.SpyObj<ColorService>;
    const leftClick = new MouseEvent('onclick', { button: MouseButton.Left });
    const rightClick = new MouseEvent('onclick', { button: MouseButton.Right });
    const middleClick = new MouseEvent('onclick', { button: MouseButton.Middle });
    const color = new Color(NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY, NOMINAL_COLOR_INTENSITY);

    beforeEach(async(() => {
        colorServiceSpy = jasmine.createSpyObj('ColorService', ['invert', 'getColor', 'setColor', 'setAlpha']);
        colorServiceSpy.invert.and.returnValue();
        colorServiceSpy.setColor.and.returnValue();
        colorServiceSpy.getColor.and.returnValues(of(WHITE), of(WHITE));
        colorServiceSpy.setAlpha.and.returnValue();
        TestBed.configureTestingModule({
            declarations: [ColorPickerComponent],
            providers: [
                { provide: MatDialog, useClass: MatDialogMock },
                { provide: ColorService, useValue: colorServiceSpy },
            ],
            imports: [MatSliderModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorPickerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call colorService.setColor() after the dialog is closed', () => {
        component.selectColor(ColorType.PRIMARY, WHITE);
        expect(colorServiceSpy.setColor).toHaveBeenCalledWith(ColorType.PRIMARY, WHITE);
    });

    it('should have 10 colors in the history after init', () => {
        expect(component.colorHistory.length).toEqual(MAX_COLOR_HISTORY);
    });

    it('should replace the first color in the history if it is already present', () => {
        component.addColorToHistory(BLACK);
        expect(component.colorHistory[0]).toEqual(BLACK);
    });

    it('should not pop a color if the history is not full', () => {
        component.colorHistory = [];
        component.addColorToHistory(color);
        expect(component.colorHistory.length).toEqual(1);
    });

    it('should not add the same color multiple times', () => {
        component.colorHistory = [];
        component.addColorToHistory(color);
        component.addColorToHistory(color);
        expect(component.colorHistory.length).toEqual(1);
    });

    it('should pop the last value for a new color', () => {
        const newColor = new Color(1, 1, 1);
        component.addColorToHistory(newColor);
        expect(component.colorHistory[0]).toEqual(newColor);
    });

    it('should call colorService.invert', () => {
        component.invert();
        expect(colorServiceSpy.invert).toHaveBeenCalled();
    });

    it('should call colorService.setColor() on history left click', () => {
        component.selectColorFromHistory(leftClick, color);
        expect(colorServiceSpy.setColor).toHaveBeenCalledWith(ColorType.PRIMARY, color);
    });

    it('should call colorService.setColor() on history right click', () => {
        component.selectColorFromHistory(rightClick, color);
        expect(colorServiceSpy.setColor).toHaveBeenCalledWith(ColorType.SECONDARY, color);
    });

    it('should not call colorService.setColor() is not left or right', () => {
        component.selectColorFromHistory(middleClick, color);
        expect(colorServiceSpy.setColor).not.toHaveBeenCalled();
    });

    it('should call colorService.setColorOpacity() on MatSliderChange', () => {
        const slideChange = new MatSliderChange();
        slideChange.value = 0;
        component.setColorOpacity(slideChange, ColorType.PRIMARY);
        expect(colorServiceSpy.setAlpha).toHaveBeenCalledWith(slideChange.value, ColorType.PRIMARY);
    });

    it('should not call colorService.setColorOpacity() on MatSlider change if the value is null', () => {
        const slideChange = new MatSliderChange();
        slideChange.value = null;
        component.setColorOpacity(slideChange, ColorType.PRIMARY);
        expect(colorServiceSpy.setAlpha).not.toHaveBeenCalled();
    });
});
