// code from Lukas Marx at https://malcoded.com/posts/angular-color-picker/
import { _DisposeViewRepeaterStrategy } from '@angular/cdk/collections';
import { Component } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSliderChange } from '@angular/material/slider';
import { Color, ColorType, DEFAULT_COLOR_HISTORY, MAX_COLOR_HISTORY, MAX_TOLRERANCE } from '@app/classes/colors/color';
import { COLOR_PICKER_TITLE, DIALOG_CLASS } from '@app/classes/colors/ui';
import { MouseButton } from '@app/classes/mouse-buttons';
import { ColorDialogComponent } from '@app/color-picker/components/color-dialog/color-dialog.component';
import { ColorService } from '@app/color-picker/services/color.service';
import { combineLatest, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Component({
    selector: 'app-color-picker',
    templateUrl: './color-picker.component.html',
    styleUrls: ['./color-picker.component.scss'],
})
export class ColorPickerComponent {
    constructor(public colorService: ColorService, public dialog: MatDialog) {}
    readonly title: string = COLOR_PICKER_TITLE;

    color$: Observable<Color> = this.colorService.getColor(ColorType.PRIMARY);
    secondaryColor$: Observable<Color> = this.colorService.getColor(ColorType.SECONDARY);
    colorAlpha: number;
    secondaryColorAlpha: number;

    context$: Observable<{ primaryColor: Color; secondaryColor: Color }> = combineLatest([this.color$, this.secondaryColor$]).pipe(
        map(([primaryColor, secondaryColor]) => {
            this.colorAlpha = primaryColor.alpha * MAX_TOLRERANCE;
            this.secondaryColorAlpha = secondaryColor.alpha * MAX_TOLRERANCE;
            return { primaryColor, secondaryColor };
        }),
    );

    colorHistory: Color[] = DEFAULT_COLOR_HISTORY;
    readonly PRIMARY: ColorType = ColorType.PRIMARY;
    readonly SECONDARY: ColorType = ColorType.SECONDARY;

    private dialogConfig(color: Color): MatDialogConfig {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = color;
        dialogConfig.autoFocus = true;
        dialogConfig.disableClose = true;
        dialogConfig.panelClass = DIALOG_CLASS;
        return dialogConfig;
    }

    selectColor(type: ColorType, color: Color): void {
        const dialogConfig = this.dialogConfig(color);
        const dialogRef = this.dialog.open(ColorDialogComponent, dialogConfig);
        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe((newColor: Color) => {
                this.colorService.setColor(type, newColor);
                this.addColorToHistory(newColor);
            });
    }

    addColorToHistory(color: Color): void {
        // If the color is already in the history, put it at the beggining
        if (this.colorHistory.some((c: Color) => c.equals(color))) {
            const alreadyThere = this.colorHistory.find((c: Color) => c.equals(color)) as Color;
            this.colorHistory = this.colorHistory.filter((c: Color) => !c.equals(color));
            this.colorHistory.unshift(alreadyThere);
            return;
        }

        if (this.colorHistory.length === MAX_COLOR_HISTORY) {
            this.colorHistory.pop();
        }
        this.colorHistory.unshift(color);
    }

    invert(): void {
        this.colorService.invert();
    }

    selectColorFromHistory(event: MouseEvent, color: Color): void {
        if (event.button === MouseButton.Left) {
            this.colorService.setColor(ColorType.PRIMARY, color);
        } else if (event.button === MouseButton.Right) {
            this.colorService.setColor(ColorType.SECONDARY, color);
        }
        event.preventDefault();
    }

    setColorOpacity(event: MatSliderChange, type: ColorType): void {
        if (event.value !== null) {
            this.colorService.setAlpha(event.value / MAX_TOLRERANCE, type);
        }
    }
}
