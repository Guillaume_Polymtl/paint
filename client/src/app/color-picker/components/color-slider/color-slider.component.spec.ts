import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BLACK } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';

import { ColorSliderComponent } from './color-slider.component';

describe('ColorSliderComponent', () => {
    let component: ColorSliderComponent;
    let fixture: ComponentFixture<ColorSliderComponent>;
    const leftClick = new MouseEvent('onclick', { button: MouseButton.Left });
    const position = { x: 0, y: 0 };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ColorSliderComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorSliderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should draw the slider if the selected height is set', () => {
        component.selectedHeight = 1;
        component.draw();
        expect(component.ctx?.strokeStyle).toEqual('#000000');
    });

    it('should call ctx.getImageData when getting color at position', () => {
        const imageData = new ImageData(1, 1);
        component.ctx?.putImageData(imageData, 0, 0);
        const color = component.getColorAtPosition(position);
        expect(color).toEqual(BLACK);
    });

    /*it('emit color should actually emit a color', () => {
        const spy = spyOn(component.color, 'next');
        component.emitColor(position);
        expect(spy).toHaveBeenCalled();
    });*/

    it('onMouseUp should set mousedown to false', () => {
        component.onMouseUp(leftClick);
        expect(component.mouseDown).toBeFalse();
    });

    it('onMouseDown should set mouseDown to true and emit a color', () => {
        const spy = spyOn(component, 'emitColor');
        component.onMouseDown(leftClick);
        expect(component.mouseDown).toBeTrue();
        expect(spy).toHaveBeenCalled();
    });

    it('onMouseMove should emit a color if mouse is down', () => {
        const spy = spyOn(component, 'emitColor');
        component.mouseDown = true;
        component.onMouseMove(leftClick);
        expect(spy).toHaveBeenCalled();
    });

    it('onMouseMove should not emit a color if mouse is not down', () => {
        const spy = spyOn(component, 'emitColor');
        component.mouseDown = false;
        component.onMouseMove(leftClick);
        expect(spy).not.toHaveBeenCalled();
    });

    it('should draw the slider if the selectedHeight is set', () => {
        const spy = spyOn(component.ctx, 'closePath');
        component.selectedHeight = 0;
        component.draw();
        expect(spy).toHaveBeenCalled();
    });
});
