// code from Lukas Marx at https://malcoded.com/posts/angular-color-picker/
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, HostListener, Input, Output, ViewChild } from '@angular/core';
// import { BLACK, Color, MAX_COLOR_INTENSITY, YELLOW } from '@app/classes/colors/color';
import { BLACK, BLUE, Color, CYAN, GREEN, PURPLE, RED, YELLOW } from '@app/classes/colors/color';
import { SLIDER_LINE_COLOR, SLIDER_LINE_WIDTH, SLIDER_WIDTH } from '@app/classes/colors/ui';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { BehaviorSubject } from 'rxjs';

@Component({
    selector: 'app-color-slider',
    templateUrl: './color-slider.component.html',
    styleUrls: ['./color-slider.component.scss'],
})
export class ColorSliderComponent implements AfterViewInit {
    @Input() defaultColor: Color = BLACK;
    @ViewChild('canvas')
    canvas: ElementRef<HTMLCanvasElement>;
    @Output() color: BehaviorSubject<Color> = new BehaviorSubject(this.defaultColor);

    ctx: CanvasRenderingContext2D;
    mouseDown: boolean = false;
    selectedHeight: number;

    constructor(private changeDetectorRef: ChangeDetectorRef, private colorService: ColorService) {}

    ngAfterViewInit(): void {
        this.ctx = this.canvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.draw();
        this.changeDetectorRef.detectChanges();
    }

    addColorStops(height: number, width: number): void {
        const colors: string[] = [
            RED.toString(),
            YELLOW.toString(),
            GREEN.toString(),
            CYAN.toString(),
            BLUE.toString(),
            PURPLE.toString(),
            RED.toString(),
        ];
        const gradient = this.ctx.createLinearGradient(0, 0, 0, height);

        colors.forEach((color: string, index: number) => {
            gradient.addColorStop(index / (colors.length - 1), color);
        });

        this.ctx.beginPath();
        this.ctx.rect(0, 0, width, height);
        this.ctx.fillStyle = gradient;
        this.ctx.fill();
        this.ctx.closePath();
    }

    draw(): void {
        const width = this.canvas.nativeElement.width;
        const height = this.canvas.nativeElement.height;
        this.ctx.clearRect(0, 0, width, height);
        this.addColorStops(height, width);
        if (!this.selectedHeight && this.defaultColor) {
            // Try to find the closest height depending on the default color provided
            const x = 0;
            let hightestSimilarity = 0;
            for (let y = 0; y < height; y++) {
                const color = this.getColorAtPosition({ x, y });
                const similarity = this.colorService.similarityLevel(this.defaultColor, color);
                if (similarity > hightestSimilarity) {
                    hightestSimilarity = similarity;
                    this.selectedHeight = y;
                    this.emitColor({ x, y });
                }
            }
        }
        this.ctx.beginPath();
        this.ctx.strokeStyle = SLIDER_LINE_COLOR.toString();
        this.ctx.lineWidth = SLIDER_LINE_WIDTH;
        this.ctx.rect(0, this.selectedHeight - SLIDER_LINE_WIDTH, width, SLIDER_WIDTH);
        this.ctx.stroke();
        this.ctx.closePath();
    }

    getColorAtPosition(position: Vec2): Color {
        const imageData = this.ctx.getImageData(position.x as number, position.y as number, 1, 1).data;
        const color = new Color(imageData[0], imageData[1], imageData[2], 1);
        return color;
    }

    emitColor(position: Vec2): void {
        const rgbaColor = this.getColorAtPosition(position);
        this.color.next(rgbaColor);
    }

    @HostListener('window:mouseup', ['$event'])
    onMouseUp(event: MouseEvent): void {
        this.mouseDown = false;
    }

    onMouseDown(event: MouseEvent): void {
        this.mouseDown = true;
        this.selectedHeight = event.offsetY;
        this.draw();
        const position = this.getPositionFromMouseEvent(event);
        this.emitColor(position);
    }

    onMouseMove(event: MouseEvent): void {
        if (this.mouseDown) {
            this.selectedHeight = event.offsetY;
            this.draw();
            const position = this.getPositionFromMouseEvent(event);
            this.emitColor(position);
        }
    }

    private getPositionFromMouseEvent(event: MouseEvent): Vec2 {
        return { x: event.offsetX, y: event.offsetY };
    }
}
