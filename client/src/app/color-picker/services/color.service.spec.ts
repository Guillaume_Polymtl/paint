import { TestBed } from '@angular/core/testing';
import { BLACK, Color, ColorType, WHITE } from '@app/classes/colors/color';
import { ColorService } from './color.service';

describe('ColorService', () => {
    let service: ColorService;
    const primaryColor = BLACK;
    const secondaryColor = WHITE;
    const newOpacity = 0;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ColorService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should get the color', () => {
        expect(service.getColor(ColorType.PRIMARY)).toEqual(service.primaryColor.asObservable());
    });

    it('should set the color and emit a new value', () => {
        const spy = spyOn(service.primaryColor, 'next');
        service.setColor(ColorType.PRIMARY, primaryColor);
        expect(spy).toHaveBeenCalledWith(primaryColor);
    });

    it('should invert the colors and emit new values', () => {
        service.setColor(ColorType.PRIMARY, primaryColor);
        service.setColor(ColorType.SECONDARY, secondaryColor);
        const colorSpy = spyOn(service.primaryColor, 'next');
        const secondaryColorSpy = spyOn(service.secondaryColor, 'next');
        service.invert();
        expect(colorSpy).toHaveBeenCalledWith(secondaryColor);
        expect(secondaryColorSpy).toHaveBeenCalledWith(primaryColor);
    });

    it('should set the color opacity and emit the correct new value', () => {
        const spy = spyOn(service.primaryColor, 'next');
        service.setColor(ColorType.PRIMARY, primaryColor);
        service.setAlpha(newOpacity, ColorType.PRIMARY);
        const newColor = new Color(primaryColor.red, primaryColor.green, primaryColor.blue, newOpacity);
        expect(spy).toHaveBeenCalledWith(newColor);
    });
});
