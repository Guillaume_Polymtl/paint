import { Injectable } from '@angular/core';
import { BLACK, Color, ColorType, MAX_COLOR_INTENSITY, MAX_TOLRERANCE, MIN_COLOR_INTENSITY, WHITE } from '@app/classes/colors/color';
// import { BLACK, Color, ColorType, MAX_ALPHA, MAX_COLOR_INTENSITY, MIN_ALPHA, MIN_COLOR_INTENSITY, WHITE } from '@app/classes/colors/color';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ColorService {
    readonly MAX_COLOR_HISTORY: number = 10;
    readonly DEFAULT_OPACITY: number = 100;
    readonly MAX_OPACITY: number = 100;
    readonly MIN_OPACITY: number = 0;

    readonly primaryColor: BehaviorSubject<Color> = new BehaviorSubject<Color>(BLACK);
    readonly secondaryColor: BehaviorSubject<Color> = new BehaviorSubject<Color>(WHITE);

    private colors: Map<ColorType, BehaviorSubject<Color>> = new Map()
        .set(ColorType.PRIMARY, this.primaryColor)
        .set(ColorType.SECONDARY, this.secondaryColor);

    getColor(type: ColorType): Observable<Color> {
        const color = this.colors.get(type) as BehaviorSubject<Color>;
        return color.asObservable();
    }

    setColor(type: ColorType, color: Color): void {
        const currentColor = this.colors.get(type) as BehaviorSubject<Color>;
        /*const currentAlpha = currentColor.getValue().alpha;*/
        const nextColor = new Color(color.red, color.green, color.blue, color.alpha);
        currentColor.next(nextColor);
    }

    invert(): void {
        const color = this.primaryColor.getValue();
        const secondaryColor = this.secondaryColor.getValue();
        this.primaryColor.next(secondaryColor);
        this.secondaryColor.next(color);
    }

    // Alpha here is [0-255]
    setAlpha(alpha: number, type: ColorType): void {
        const color = this.colors.get(type) as BehaviorSubject<Color>;
        const nextColor = new Color(color.getValue().red, color.getValue().green, color.getValue().blue, alpha);
        color.next(nextColor);
    }

    // Returns how similar (from 0 to 100) 2 colors are
    similarityLevel(color1: Color, color2: Color): number {
        const RATIO = MAX_TOLRERANCE / (1 + 1 + 1);
        const total =
            this.intensitySimilarityLevel(color1.red, color2.red) +
            this.intensitySimilarityLevel(color1.blue, color2.blue) +
            this.intensitySimilarityLevel(color1.green, color2.green);
        return total * RATIO;
    }

    private intensitySimilarityLevel(intensity1: number, intensity2: number): number {
        return 1 - Math.abs(intensity1 - intensity2) / (MAX_COLOR_INTENSITY - MIN_COLOR_INTENSITY);
    }
}
