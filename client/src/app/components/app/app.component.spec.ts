import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ShortcutService } from '@app/services/shortcut/shortcut.service';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
    let app: AppComponent;
    let shortcutServiceSpy: jasmine.SpyObj<ShortcutService>;
    beforeEach(async(() => {
        shortcutServiceSpy = jasmine.createSpyObj('ShortcutService', ['handleKeyDown']);
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [AppComponent],
            providers: [{ provide: ShortcutService, useValue: shortcutServiceSpy }],
        });

        const fixture = TestBed.createComponent(AppComponent);
        app = fixture.componentInstance;
    }));

    it('should create the app', () => {
        expect(app).toBeTruthy();
    });

    it('should listen to keydown event and call shortcutService.handleKeyDown()', () => {
        const event = new KeyboardEvent('keydown', {});
        app.handleKeyDown(event);
        expect(shortcutServiceSpy.handleKeyDown).toHaveBeenCalledWith(event);
    });
});
