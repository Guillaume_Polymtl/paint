import { Component, HostListener } from '@angular/core';
import { ShortcutService } from '@app/services/shortcut/shortcut.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    constructor(private shortcutService: ShortcutService) {}
    @HostListener('document:keydown', ['$event'])
    handleKeyDown(event: KeyboardEvent): void {
        this.shortcutService.handleKeyDown(event);
    }
}
