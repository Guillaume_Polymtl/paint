import { Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Tool } from '@app/classes/tool';
import { Panel } from '@app/classes/toolbar/panel';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { ToolsService } from '@app/services/tools/tools.service';
import { BrushAttributesComponent } from '@app/tools/brush/brush-attributes/brush-attributes.component';
import { BucketAttributesComponent } from '@app/tools/bucket/bucket-attributes/bucket-attributes.component';
import { EllipseAttributesComponent } from '@app/tools/ellipse/ellipse-attributes/ellipse-attributes.component';
import { EraserAttributesComponent } from '@app/tools/eraser/eraser-attributes/eraser-attributes.component';
import { EyeDropperAttributesComponent } from '@app/tools/eye-dropper/eye-dropper-attributes/eye-dropper-attributes.component';
import { FeatherPenAttributesComponent } from '@app/tools/feather-pen/feather-pen-attributes/feather-pen-attributes.component';
import { GridAttributesComponent } from '@app/tools/grid/grid-attributes/grid-attributes.component';
import { LineAttributesComponent } from '@app/tools/line/line-attributes/line-attributes.component';
import { PencilAttributesComponent } from '@app/tools/pencil/pencil-attributes/pencil-attributes.component';
import { PolygonAttributesComponent } from '@app/tools/polygon/polygon-attributes/polygon-attributes.component';
import { RectangleAttributesComponent } from '@app/tools/rectangle/rectangle-attributes/rectangle-attributes.component';
import { SelectionAttributesComponent } from '@app/tools/selection/selection-attributes/selection-attributes.component';
import { SpraypaintAttributesComponent } from '@app/tools/spraypaint/spraypaint-attributes/spraypaint-attributes.component';
import { StampAttributesComponent } from '@app/tools/stamp/stamp-attributes/stamp-attributes.component';
import { TextAttributesComponent } from '@app/tools/text/text-attributes/text-attributes.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PanelComponent } from './panel.component';

@Component({
    selector: 'app-attributes-panel',
    templateUrl: './attributes-panel.component.html',
    styleUrls: ['./attributes-panel.component.scss'],
})
export class AttributesPanelComponent implements OnInit, OnDestroy {
    panels: Panel[] = [
        new Panel(ToolName.PENCIL, PencilAttributesComponent),
        new Panel(ToolName.ERASER, EraserAttributesComponent),
        new Panel(ToolName.RECTANGLE, RectangleAttributesComponent),
        new Panel(ToolName.BRUSH, BrushAttributesComponent),
        new Panel(ToolName.ELLIPSE, EllipseAttributesComponent),
        new Panel(ToolName.LINE, LineAttributesComponent),
        new Panel(ToolName.SELECT_RECTANGLE, SelectionAttributesComponent),
        new Panel(ToolName.SELECT_ELLIPSE, SelectionAttributesComponent),
        new Panel(ToolName.POLYGON, PolygonAttributesComponent),
        new Panel(ToolName.EYE_DROPPER, EyeDropperAttributesComponent),
        new Panel(ToolName.FEATHER, FeatherPenAttributesComponent),
        new Panel(ToolName.BUCKET, BucketAttributesComponent),
        new Panel(ToolName.MAGIC_WAND, SelectionAttributesComponent),
        new Panel(ToolName.GRID, GridAttributesComponent),
        new Panel(ToolName.SPRAYPAINT, SpraypaintAttributesComponent),
        new Panel(ToolName.TEXT, TextAttributesComponent),
        new Panel(ToolName.STAMP, StampAttributesComponent),
    ];

    @ViewChild('panel', { static: true, read: ViewContainerRef }) panelContainer: ViewContainerRef;
    private destroyed: Subject<boolean> = new Subject();

    currentPanelIndex: number = 0;
    constructor(private componentFactoryResolver: ComponentFactoryResolver, public toolsService: ToolsService) {}

    ngOnInit(): void {
        this.toolsService
            .getTool()
            .pipe(takeUntil(this.destroyed))
            .subscribe((tool: Tool) => {
                this.loadComponent(tool.name);
            });
    }

    ngOnDestroy(): void {
        this.destroyed.next();
        this.destroyed.complete();
    }

    private loadComponent(name: ToolName): void {
        this.panels.forEach((p, index) => {
            if (p.name === name) {
                const panel = this.panels[index];
                const componentFactory = this.componentFactoryResolver.resolveComponentFactory(panel.component);
                this.panelContainer.clear();
                this.panelContainer.createComponent<PanelComponent>(componentFactory);
            }
        });
    }
}
