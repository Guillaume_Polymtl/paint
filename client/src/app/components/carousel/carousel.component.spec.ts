import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatChipInputEvent, MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { cards, CarouselCard } from '@app/classes/carousel-card';
import { CarouselService } from '@app/services/carousel/carousel.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { CarouselComponent } from './carousel.component';

// tslint:disable:no-any
describe('CarouselComponent', () => {
    let component: CarouselComponent;
    let fixture: ComponentFixture<CarouselComponent>;
    let routerSpy: jasmine.SpyObj<Router>;
    let carouselServiceStub: CarouselService;

    beforeEach(async(() => {
        routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
        carouselServiceStub = new CarouselService(new DrawingService(routerSpy));
        TestBed.configureTestingModule({
            declarations: [CarouselComponent],
            imports: [BrowserAnimationsModule, MatFormFieldModule, MatChipsModule, MatIconModule],
            providers: [
                { provide: Router, useValue: routerSpy },
                { provide: CarouselService, useValue: carouselServiceStub },
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CarouselComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('updateCards should update cards and showncardsIndices attributes', () => {
        component.cards = [];
        component.shownCardsIndices = [];
        const expectedLength: number = Math.min(component.NUMBER_DISPLAYED_CARDS, cards.length);
        component.updateCards(cards);
        expect(component.cards).toEqual(cards);
        expect(component.shownCardsIndices.length).toEqual(expectedLength);
    });

    it('onPrevious should do nothing if cards attribute length is less than 2', () => {
        component.cards = [];
        const updateShownCardsSpy = spyOn<any>(component, 'updateShownCards');
        component.onPrevious();
        expect(updateShownCardsSpy).not.toHaveBeenCalled();
    });

    it('onPrevious should update shown cards if cards attribute length is greater than 2', () => {
        component.updateCards(cards);
        const expectedIndex: number = cards.length - 1;
        const updateShownCardsSpy = spyOn<any>(component, 'updateShownCards');
        component.onPrevious();
        expect(updateShownCardsSpy).toHaveBeenCalled();
        expect(component.shownCardsIndices[0]).toEqual(expectedIndex);
    });

    it('onNext should do nothing if cards attribute length is less than 2', () => {
        component.cards = [];
        const updateShownCardsSpy = spyOn<any>(component, 'updateShownCards');
        component.onNext();
        expect(updateShownCardsSpy).not.toHaveBeenCalled();
    });

    it('onNext should update shown cards if cards attribute length is greater than 2', () => {
        component.updateCards(cards);
        const expectedIndex = 1;
        const updateShownCardsSpy = spyOn<any>(component, 'updateShownCards');
        component.onNext();
        expect(updateShownCardsSpy).toHaveBeenCalled();
        expect(component.shownCardsIndices[0]).toEqual(expectedIndex);
    });

    // tslint:disable:no-magic-numbers
    it('updateShownCards should update showncardsIndices', () => {
        component.cards = new Array<CarouselCard>(5);
        component.shownCardsIndices = new Array<number>(3);
        component.shownCardsIndices[0] = 2;
        component.updateShownCards();
        expect(component.shownCardsIndices[1]).toEqual(3);
        expect(component.shownCardsIndices[2]).toEqual(4);
    });

    it('onKeyDown should call onPrevious if key is left arrow key', () => {
        const keyboardEvent: KeyboardEvent = new KeyboardEvent('keydown', { key: 'ArrowLeft' });
        const onPreviousSpy: jasmine.Spy<any> = spyOn<any>(component, 'onPrevious');
        component.onKeyDown(keyboardEvent);
        expect(onPreviousSpy).toHaveBeenCalled();
    });

    it('onKeyDown should call onNext if key is right arrow key', () => {
        const keyboardEvent: KeyboardEvent = new KeyboardEvent('keydown', { key: 'ArrowRight' });
        const onNextSpy: jasmine.Spy<any> = spyOn<any>(component, 'onNext');
        component.onKeyDown(keyboardEvent);
        expect(onNextSpy).toHaveBeenCalled();
    });

    it('openImage should call CarouselService openImage', () => {
        const openImageSpy: jasmine.Spy<any> = spyOn<any>(carouselServiceStub, 'openImage');
        const index = 0;
        component.openImage(index);
        expect(openImageSpy).toHaveBeenCalled();
    });

    it('cancelImageDrawing should call CarouselService cancelImageDrawing', () => {
        const cancelImageDrawingSpy: jasmine.Spy<any> = spyOn<any>(carouselServiceStub, 'cancelImageDrawing');
        component.cancelImageDrawing();
        expect(cancelImageDrawingSpy).toHaveBeenCalled();
    });

    it('confirmImageDrawing should call CarouselService confirmImageDrawing', () => {
        const confirmImageDrawingSpy: jasmine.Spy<any> = spyOn<any>(carouselServiceStub, 'confirmImageDrawing');
        component.confirmImageDrawing();
        expect(confirmImageDrawingSpy).toHaveBeenCalled();
    });

    it('addFilterTagByEvent should call CarouselService addFilterTagByEvent', () => {
        const addFilterTagByEventSpy: jasmine.Spy<any> = spyOn<any>(carouselServiceStub, 'addFilterTagByEvent');
        const event: MatChipInputEvent = ({ input: null, value: 'value' } as unknown) as MatChipInputEvent;
        component.addFilterTagByEvent(event);
        expect(addFilterTagByEventSpy).toHaveBeenCalled();
    });

    it('addFilterTag should call CarouselService addFilterTag', () => {
        const addFilterTagSpy: jasmine.Spy<any> = spyOn<any>(carouselServiceStub, 'addFilterTag');
        const tag = 'value;';
        component.addFilterTag(tag);
        expect(addFilterTagSpy).toHaveBeenCalled();
    });

    it('removeTag should call CarouselService removeTag', () => {
        const removeTagSpy: jasmine.Spy<any> = spyOn<any>(carouselServiceStub, 'removeTag');
        const tag = 'value;';
        component.removeTag(tag);
        expect(removeTagSpy).toHaveBeenCalled();
    });

    it('deleteImage should call CarouselService deleteImage', () => {
        const deleteImageSpy: jasmine.Spy<any> = spyOn<any>(carouselServiceStub, 'deleteImage');
        const indexInCardsArray = 1;
        const indexInIndicesArray = 2;
        component.deleteImage(indexInCardsArray, indexInIndicesArray);
        expect(deleteImageSpy).toHaveBeenCalled();
    });

    it('deleteImage should update shownCardsindicees if index equals 0 and cards.length > indexInCardsArray', () => {
        const updateShownCardsSpy: jasmine.Spy<any> = spyOn<any>(component, 'updateShownCards');
        component.cards = new Array<CarouselCard>(4);
        const indexInCardsArray = 1;
        const indexInIndicesArray = 0;
        component.deleteImage(indexInCardsArray, indexInIndicesArray);
        expect(updateShownCardsSpy).toHaveBeenCalled();
    });
});
