import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { CarouselCard } from '@app/classes/carousel-card';
import { ImageState } from '@app/classes/tools-utility';
import { CarouselService } from '@app/services/carousel/carousel.service';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

const LEFT_ARROW_KEY = 'ArrowLeft';
const RIGTH_ARROW_KEY = 'ArrowRight';

@Component({
    selector: 'app-caroussel',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit, OnDestroy {
    readonly NUMBER_DISPLAYED_CARDS: number = 3;
    readonly separatorKeysCodes: number[] = [ENTER, COMMA];
    readonly DUSTBIN_IMAGE_PATH: string = '../../../assets/carousel/dustbin.svg';
    readonly LOADING_IMAGE_PATH: string = '../../../assets/carousel/loading.gif';
    readonly ERROR_IMAGE_PATH: string = '../../../assets/carousel/sad.jpg';
    readonly NO_CARD_IMAGE_PATH: string = '../../../assets/carousel/empty.gif';
    cards: CarouselCard[];
    shownCardsIndices: number[];
    indexOfInterest: number;
    imageState: ImageState;
    destroyed: Subject<boolean> = new Subject<boolean>();

    constructor(public carouselService: CarouselService) {}

    ngOnInit(): void {
        this.cards = [];
        this.carouselService.init();
        const cardsChangeObservable = this.carouselService.cards.pipe(
            map((cards) => cards),
            takeUntil(this.destroyed),
        );
        cardsChangeObservable.subscribe((cards: CarouselCard[]) => this.updateCards(cards));
        const stateChangeObservable = this.carouselService.imageState.pipe(
            map((imageState) => imageState),
            takeUntil(this.destroyed),
        );
        stateChangeObservable.subscribe((imageState: ImageState) => (this.imageState = imageState));
    }

    ngOnDestroy(): void {
        this.destroyed.next(true);
        this.destroyed.complete();
    }

    updateCards(cards: CarouselCard[]): void {
        this.cards = cards;
        this.shownCardsIndices = new Array<number>(Math.min(this.NUMBER_DISPLAYED_CARDS, this.cards.length));
        this.shownCardsIndices[0] = 0;
        for (let i = 0; i < this.shownCardsIndices.length; ++i) {
            this.shownCardsIndices[i] = i;
        }
        this.indexOfInterest = Math.max(0, Math.ceil(this.shownCardsIndices.length / 2) - 1);
    }

    onPrevious(): void {
        // tslint:disable-next-line:no-magic-numbers
        if (this.cards.length >= 2) {
            const length: number = this.cards.length;
            // Le modulo simple ne fonctionne pas correctement, ici c'est une sorte de workaround
            this.shownCardsIndices[0] = (((this.shownCardsIndices[0] - 1) % length) + length) % length;
            this.updateShownCards();
        }
    }

    onNext(): void {
        // tslint:disable-next-line:no-magic-numbers
        if (this.cards.length >= 2) {
            this.shownCardsIndices[0] = (this.shownCardsIndices[0] + 1) % this.cards.length;
            this.updateShownCards();
        }
    }

    updateShownCards(): void {
        for (let i = 1; i < this.shownCardsIndices.length; ++i) {
            this.shownCardsIndices[i] = (this.shownCardsIndices[0] + i) % this.cards.length;
        }
    }

    @HostListener('window:keydown', ['$event'])
    onKeyDown(event: KeyboardEvent): void {
        const key: string = event.key;
        /* istanbul ignore else */
        if (key === LEFT_ARROW_KEY) this.onPrevious();
        else if (key === RIGTH_ARROW_KEY) this.onNext();
    }

    openImage(index: number): void {
        this.carouselService.openImage(index);
    }

    cancelImageDrawing(): void {
        this.carouselService.cancelImageDrawing();
    }

    confirmImageDrawing(): void {
        this.carouselService.confirmImageDrawing();
    }

    deleteImage(indexInCardsArray: number, indexInIndicesArray: number): void {
        this.carouselService.deleteImage(indexInCardsArray);
        if (indexInIndicesArray === 0 && this.cards.length > indexInCardsArray) {
            this.shownCardsIndices[0] = indexInCardsArray;
            this.updateShownCards();
        }
    }

    addFilterTagByEvent(event: MatChipInputEvent): void {
        this.carouselService.addFilterTagByEvent(event);
    }

    addFilterTag(tag: string): void {
        this.carouselService.addFilterTag(tag);
    }

    removeTag(tag: string): void {
        this.carouselService.removeTag(tag);
    }
}
