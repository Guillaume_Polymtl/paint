import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';

import { ConfirmResetDrawingComponent } from './confirm-reset-drawing.component';

describe('ConfirmResetDrawingComponent', () => {
    let component: ConfirmResetDrawingComponent;
    let fixture: ComponentFixture<ConfirmResetDrawingComponent>;

    const dialogMock = {
        open: () => {
            return;
        },
        close: () => {
            return;
        },
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ConfirmResetDrawingComponent],
            providers: [
                {
                    provide: MatDialogRef,
                    useValue: dialogMock,
                },
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfirmResetDrawingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call dialogRef.close(true) onConfirm', () => {
        const spy = spyOn(component.dialogRef, 'close').and.callThrough();
        component.onConfirm();
        expect(spy).toHaveBeenCalledWith(true);
    });

    it('should call dialogRef.close(false) onCancel', () => {
        const spy = spyOn(component.dialogRef, 'close').and.callThrough();
        component.onCancel();
        expect(spy).toHaveBeenCalledWith(false);
    });
});
