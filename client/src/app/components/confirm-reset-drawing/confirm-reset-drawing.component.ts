import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-confirm-reset-drawing',
    templateUrl: './confirm-reset-drawing.component.html',
    styleUrls: ['./confirm-reset-drawing.component.scss'],
})
export class ConfirmResetDrawingComponent {
    readonly title: string = 'Créer un nouveau dessin';
    readonly question: string = 'Le dessin sera supprimé. Êtes-vous certain?';
    readonly confirm: string = 'Oui';
    readonly cancel: string = 'Annuler';

    constructor(public dialogRef: MatDialogRef<ConfirmResetDrawingComponent>) {}

    onConfirm(): void {
        this.dialogRef.close(true);
    }

    onCancel(): void {
        this.dialogRef.close(false);
    }
}
