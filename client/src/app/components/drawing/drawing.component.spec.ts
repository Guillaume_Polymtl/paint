import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { FileAction } from '@app/classes/shortcuts/file-action';
import { Tool } from '@app/classes/tool';
import { CANVAS_DEFAULT_HEIGHT, CANVAS_DEFAULT_WIDTH } from '@app/classes/tools-utility';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { FileService } from '@app/services/file/file.service';
import { UndoRedoService } from '@app/services/undo-redo/undo-redo.service';
import { LineService } from '@app/tools/line/line.service';
import { PencilService } from '@app/tools/pencil/pencil.service';
import { of } from 'rxjs';
import { DrawingComponent } from './drawing.component';

class ToolStub extends Tool {}

describe('DrawingComponent', () => {
    let component: DrawingComponent;
    let fixture: ComponentFixture<DrawingComponent>;
    let toolStub: ToolStub;
    let lineStub: LineService;
    let drawingSpy: jasmine.SpyObj<DrawingService>;
    let colorService: ColorService;
    let fileServiceSpy: jasmine.SpyObj<FileService>;

    beforeEach(async(() => {
        drawingSpy = jasmine.createSpyObj('DrawingService', ['arePristine', 'clearCanvas', 'updateImageOnLoad', 'loadSavedDrawing']);
        drawingSpy.arePristine.and.returnValues(false);
        colorService = new ColorService();
        toolStub = new ToolStub(drawingSpy, colorService);
        const undoRedoStub: UndoRedoService = new UndoRedoService(drawingSpy);
        lineStub = new LineService(undoRedoStub, drawingSpy, colorService);
        fileServiceSpy = jasmine.createSpyObj('FileService', ['setAction', 'getAction']);
        fileServiceSpy.getAction.and.returnValue(of(FileAction.CREATE));
        TestBed.configureTestingModule({
            declarations: [DrawingComponent],
            providers: [
                { provide: DrawingService, useValue: drawingSpy },
                { provide: PencilService, useValue: toolStub },
                { provide: LineService, useValue: lineStub },
                {
                    provide: MatDialog,
                    useValue: {
                        open(): object {
                            {
                                return {
                                    afterClosed: () => of(true),
                                };
                            }
                        },
                    },
                },
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DrawingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.ngAfterViewInit();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should have a default WIDTH and HEIGHT', () => {
        const height = component.height;
        const width = component.width;
        expect(height).toEqual(CANVAS_DEFAULT_HEIGHT);
        expect(width).toEqual(CANVAS_DEFAULT_WIDTH);
    });

    it('should get stubTool', () => {
        const tool = component.tool;
        expect(tool).toEqual(toolStub);
    });

    it(" should call the tool's mouse move when receiving a mouse move event", () => {
        const event = {} as MouseEvent;
        const mouseEventSpy = spyOn(toolStub, 'onMouseMove').and.callThrough();
        component.onMouseMove(event);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });

    it(" should call the tool's mouse down when receiving a mouse down event", () => {
        const event = {} as MouseEvent;
        const mouseEventSpy = spyOn(toolStub, 'onMouseDown').and.callThrough();
        component.onMouseDown(event);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });

    it("should call the tool's contextmenu when receiving a contextmenu event", () => {
        const event = new MouseEvent('contextmenu', {});
        const mouseEventSpy = spyOn(toolStub, 'onContextMenu').and.callThrough();
        component.onContextMenu(event);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });

    it(" should call the tool's mouse up when receiving a mouse up event", () => {
        const event = {} as MouseEvent;
        const mouseEventSpy = spyOn(toolStub, 'onMouseUp').and.callThrough();
        component.onMouseUp(event);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });

    it(" should call the tool's key down when receiving a key down event", () => {
        const event = {} as KeyboardEvent;
        const keyboardEventSpy = spyOn(toolStub, 'onKeyDown').and.callThrough();
        component.onKeyDown(event);
        expect(keyboardEventSpy).toHaveBeenCalled();
        expect(keyboardEventSpy).toHaveBeenCalledWith(event);
    });

    it(" should call the tool's key up when receiving a key up event", () => {
        const event = {} as KeyboardEvent;
        const keyboardEventSpy = spyOn(toolStub, 'onKeyUp').and.callThrough();
        component.onKeyUp(event);
        expect(keyboardEventSpy).toHaveBeenCalled();
        expect(keyboardEventSpy).toHaveBeenCalledWith(event);
    });

    it(" should call the tool's mouse leave when receiving a mouse leave event", () => {
        const event = {} as MouseEvent;
        const mouseEventSpy = spyOn(toolStub, 'onMouseLeave').and.callThrough();
        component.onMouseLeave(event);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });
    it(" should call the tool's mouse click when receiving a mouse click event", () => {
        const event = {} as MouseEvent;
        const mouseEventSpy = spyOn(toolStub, 'onMouseClick').and.callThrough();
        component.onMouseClick(event);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });

    it(" should call the tool's mouse dblclick when receiving a mouse double click event", () => {
        const event = {} as MouseEvent;
        const mouseEventSpy = spyOn(toolStub, 'onMouseDblClick').and.callThrough();
        component.onMouseDblClick(event);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });
    it(" should call the tool's mouse wheel when receiving a wheel event", () => {
        const event = {} as WheelEvent;
        const mouseEventSpy = spyOn(toolStub, 'onMouseWheel').and.callThrough();
        component.onMouseWheel(event);
        expect(mouseEventSpy).toHaveBeenCalled();
        expect(mouseEventSpy).toHaveBeenCalledWith(event);
    });

    it('should handle file action CLEAR', () => {
        const spy = spyOn(component, 'clearCanvas');
        component.ngAfterViewInit();
        component.handleFileAction(FileAction.CLEAR);
        expect(spy).toHaveBeenCalled();
    });

    it('should handle file action CONTINUE', () => {
        component.ngAfterViewInit();
        component.handleFileAction(FileAction.CONTINUE);
        expect(drawingSpy.loadSavedDrawing).toHaveBeenCalled();
    });

    it('should reset the canvas', () => {
        component.clearCanvas(true);
        expect(drawingSpy.clearCanvas).toHaveBeenCalled();
    });

    it('should not reset the canvas', () => {
        component.clearCanvas(false);
        expect(drawingSpy.clearCanvas).not.toHaveBeenCalled();
    });
});
