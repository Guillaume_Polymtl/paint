import { AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { WHITE } from '@app/classes/colors/color';
import { FileAction } from '@app/classes/shortcuts/file-action';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { CANVAS_DEFAULT_HEIGHT, CANVAS_DEFAULT_WIDTH } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ConfirmResetDrawingComponent } from '@app/components/confirm-reset-drawing/confirm-reset-drawing.component';
import { DialogService } from '@app/services/dialog/dialog.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { FileService } from '@app/services/file/file.service';
import { ToolsService } from '@app/services/tools/tools.service';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-drawing',
    templateUrl: './drawing.component.html',
    styleUrls: ['./drawing.component.scss'],
})
export class DrawingComponent implements AfterViewInit, OnInit, OnDestroy {
    @ViewChild('canvasContainer', { static: false }) canvasContainer: ElementRef<HTMLDivElement>;
    @ViewChild('baseCanvas', { static: false }) baseCanvas: ElementRef<HTMLCanvasElement>;
    // On utilise ce canvas pour dessiner sans affecter le dessin final
    @ViewChild('previewGrid', { static: false }) previewGrid: ElementRef<HTMLCanvasElement>;
    @ViewChild('previewCanvas', { static: false }) previewCanvas: ElementRef<HTMLCanvasElement>;

    private baseCtx: CanvasRenderingContext2D;
    private previewCtx: CanvasRenderingContext2D;
    private canvasSize: Vec2 = { x: CANVAS_DEFAULT_WIDTH, y: CANVAS_DEFAULT_HEIGHT };
    ctxLoaded: Subject<void>;

    tool: Tool;
    destroyed: Subject<boolean> = new Subject<boolean>();
    constructor(
        private drawingService: DrawingService,
        public toolsService: ToolsService,
        public dialogService: DialogService,
        public fileService: FileService,
        public dialog: MatDialog,
    ) {}

    ngOnInit(): void {
        const toolChangeObservable = this.toolsService.getTool().pipe(
            map((tool) => tool),
            takeUntil(this.destroyed),
        );
        toolChangeObservable.subscribe((tool: Tool) => {
            this.tool = tool;
            if (this.previewCanvas) {
                if (tool.name === ToolName.STAMP || tool.name === ToolName.ERASER) {
                    this.previewCanvas.nativeElement.style.cursor = 'none';
                    this.baseCanvas.nativeElement.style.cursor = 'none';
                    this.previewGrid.nativeElement.style.cursor = 'none';
                } else {
                    this.previewCanvas.nativeElement.style.cursor = 'crosshair';
                    this.baseCanvas.nativeElement.style.cursor = 'crosshair';
                    this.previewGrid.nativeElement.style.cursor = 'crosshair';
                }
            }
        });
        this.ctxLoaded = new Subject<void>();
        const loadedChangeObservable = this.ctxLoaded.pipe(
            map((loaded) => loaded),
            takeUntil(this.destroyed),
        );
        loadedChangeObservable.subscribe(() => {
            this.drawingService.isLoaded = true;
            this.drawingService.updateImageOnLoad();
        });

        const fileActionObservable = this.fileService.getAction().pipe(
            map((action) => action),
            takeUntil(this.destroyed),
        );
        fileActionObservable.subscribe((action: FileAction) => this.handleFileAction(action));
    }

    ngOnDestroy(): void {
        this.destroyed.next(true);
        this.destroyed.complete();
    }

    ngAfterViewInit(): void {
        this.baseCtx = this.baseCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.previewCtx = this.previewCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.baseCtx.fillStyle = WHITE.toString();
        this.baseCtx.fillRect(0, 0, this.width, this.height);
        this.drawingService.grid = this.previewGrid.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.drawingService.baseCtx = this.baseCtx;
        this.drawingService.previewCtx = this.previewCtx;
        this.drawingService.canvas = this.baseCanvas.nativeElement;
        this.ctxLoaded.next();
    }

    // Resets the canvas
    // If there is something on the canvas, will ask the user for confirmation
    handleFileAction(action: FileAction): void {
        if (action === FileAction.CLEAR && !this.drawingService.arePristine()) {
            const dialogRef: MatDialogRef<ConfirmResetDrawingComponent> = this.dialog.open(ConfirmResetDrawingComponent);
            dialogRef.afterClosed().subscribe((reset: boolean) => {
                this.clearCanvas(reset);
            });
        } else if (action === FileAction.CONTINUE) {
            const width = Number(localStorage.getItem('width') as string);
            const height = Number(localStorage.getItem('height') as string);
            const source = localStorage.getItem('data') as string;
            this.drawingService.loadSavedDrawing(source, width, height);
        }
    }

    clearCanvas(clear: boolean): void {
        if (clear) {
            this.tool.onToolChange();
            this.drawingService.clearCanvas(this.drawingService.baseCtx);
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            localStorage.clear();
        }
    }

    @HostListener('mousemove', ['$event'])
    onMouseMove(event: MouseEvent): void {
        this.tool.onMouseMove(event);
    }

    @HostListener('mousedown', ['$event'])
    onMouseDown(event: MouseEvent): void {
        this.tool.onMouseDown(event);
    }

    @HostListener('mouseup', ['$event'])
    onMouseUp(event: MouseEvent): void {
        this.tool.onMouseUp(event);
    }

    @HostListener('wheel', ['$event'])
    onMouseWheel(event: WheelEvent): void {
        this.tool.onMouseWheel(event);
    }

    @HostListener('contextmenu', ['$event'])
    onContextMenu(event: MouseEvent): void {
        event.preventDefault();
        this.tool.onContextMenu(event);
    }

    @HostListener('mouseleave', ['$event'])
    onMouseLeave(event: MouseEvent): void {
        this.tool.onMouseLeave(event);
    }

    @HostListener('click', ['$event'])
    onMouseClick(event: MouseEvent): void {
        this.tool.onMouseClick(event);
    }

    @HostListener('dblclick', ['$event'])
    onMouseDblClick(event: MouseEvent): void {
        this.tool.onMouseDblClick(event);
    }

    @HostListener('window:keydown', ['$event'])
    onKeyDown(event: KeyboardEvent): void {
        this.tool.onKeyDown(event);
    }

    @HostListener('window:keyup', ['$event'])
    onKeyUp(event: KeyboardEvent): void {
        this.tool.onKeyUp(event);
    }

    get width(): number {
        return this.canvasSize.x;
    }

    get height(): number {
        return this.canvasSize.y;
    }
}
