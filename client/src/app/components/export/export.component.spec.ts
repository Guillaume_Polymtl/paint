import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ExportModes } from '@app/classes/export-modes';
import { Filters } from '@app/classes/filters';
import { Formats } from '@app/classes/formats';
import { ExportService } from '@app/services/export/export.service';
import { BehaviorSubject } from 'rxjs';
import { ExportComponent } from './export.component';

// tslint:disable: no-string-literal
describe('ExportComponent', () => {
    let component: ExportComponent;
    let fixture: ComponentFixture<ExportComponent>;
    let exportServiceSpy: jasmine.SpyObj<ExportService>;

    beforeEach(async(() => {
        exportServiceSpy = jasmine.createSpyObj('ExportService', [
            'drawPreview',
            'onFormatUpdate',
            'onFilterUpdate',
            'onTitleUpdate',
            'onEmailUpdate',
            'onExportModeUpdate',
            'validateTitle',
            'validateEmail',
            'exportByEmail',
            'export',
            'canvas2Image',
            'exportConfirmation',
        ]);
        exportServiceSpy.currentFormat = new BehaviorSubject<Formats>(Formats.JPEG);
        exportServiceSpy.currentFilter = new BehaviorSubject<Filters>(Filters.Aucun);
        exportServiceSpy.currentExportMode = new BehaviorSubject<ExportModes>(ExportModes.Téléchargement);
        exportServiceSpy.isTitleValid = new BehaviorSubject<boolean>(false);
        exportServiceSpy.isEmailValid = new BehaviorSubject<boolean>(false);

        TestBed.configureTestingModule({
            declarations: [ExportComponent],
            providers: [
                {
                    provide: MatDialogRef,
                    useValue: {
                        moduleDef: ExportComponent,
                        close: () => null,
                    },
                },
                {
                    provide: ExportService,
                    useValue: exportServiceSpy,
                },
            ],
            imports: [MatRadioModule, MatGridListModule, MatSnackBarModule],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(ExportComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.ngAfterViewInit();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(' should update valid format', () => {
        component.onFormatUpdate('JPEG');
        expect(component['exportService'].currentFormat.value).toEqual(Formats.JPEG);
    });
    it(' should not update format if get invalid format', () => {
        component['exportService'].currentFormat.next(Formats.JPEG);
        component.onFormatUpdate('none');
        expect(component['exportService'].currentFormat.value).toEqual(Formats.JPEG);
    });

    it(' should update valid export mode', () => {
        component.onExportModeUpdate('Courriel');
        expect(component['exportService'].currentExportMode.value).toEqual(ExportModes.Courriel);
    });
    it(' should not update export mode if get invalid export mode', () => {
        component['exportService'].currentExportMode.next(ExportModes.Courriel);
        component.onExportModeUpdate('none');
        expect(component['exportService'].currentExportMode.value).toEqual(ExportModes.Courriel);
    });
    it(' should update valid filter', () => {
        component.onFilterUpdate('Constraste');
        expect(component['exportService'].currentFilter.value).toEqual(Filters.Constraste);
        expect(exportServiceSpy.drawPreview).toHaveBeenCalled();
    });

    it(' should not update filter if get invalid filter', () => {
        component['exportService'].currentFilter.next(Filters.Aucun);
        component.onFilterUpdate(('noSense' as unknown) as Filters);
        expect(component['exportService'].currentFilter.value).toEqual(Filters.Aucun);
        expect(exportServiceSpy.drawPreview).not.toHaveBeenCalled();
    });

    it('closeDialog shoud close dialog', () => {
        const spy = spyOn(component['dialogRef'], 'close');
        component.closeDialog();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('exportByEmail should export if title is valid and export mode is Courriel', () => {
        component['title'] = 'title';
        component['exportMode'] = ExportModes.Courriel;
        component['isTitleValid'] = true;
        component['isEmailValid'] = true;
        const spy = spyOn(component['dialogRef'], 'close');
        component.exportConfirmation();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(exportServiceSpy.exportByEmail).toHaveBeenCalled();
    });
    it('exportByEmail not should export if title and email is not valid', () => {
        component['title'] = 'title';
        component['exportMode'] = ExportModes.Courriel;
        component['isTitleValid'] = false;
        component['isEmailValid'] = true;
        const spy = spyOn(component['dialogRef'], 'close');
        const spy3 = spyOn(component['snack'], 'open');
        component.exportConfirmation();
        expect(spy).toHaveBeenCalledTimes(0);
        expect(spy3).toHaveBeenCalledTimes(1);
    });

    it('exportByEmail not should export if title and email is not valid', () => {
        component['title'] = 'title';
        component['exportMode'] = ExportModes.Courriel;
        component['isTitleValid'] = false;
        component['isEmailValid'] = false;
        const spy = spyOn(component['dialogRef'], 'close');
        const spy3 = spyOn(component['snack'], 'open');
        component.exportConfirmation();
        expect(spy).toHaveBeenCalledTimes(0);
        expect(spy3).toHaveBeenCalledTimes(1);
    });
    it('exportConfirmation should export if title is valid and export mode is Telechargement', () => {
        component['title'] = 'title';
        component['exportMode'] = ExportModes.Téléchargement;
        component['isTitleValid'] = true;
        const spy = spyOn(component['dialogRef'], 'close');
        component.exportConfirmation();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(exportServiceSpy.export).toHaveBeenCalled();
    });
    it('exportConfirmation should not export if title is not valid ', () => {
        component['title'] = 'title';
        component['isTitleValid'] = false;
        component['exportMode'] = ExportModes.Téléchargement;
        const spy3 = spyOn(component['snack'], 'open');
        component.exportConfirmation();
        expect(spy3).toHaveBeenCalledTimes(1);
    });

    it('onTitleUpdate should validate title', () => {
        component.onTitleUpdate(({
            target: {
                value: 'title',
            } as HTMLInputElement,
        } as unknown) as InputEvent);
        expect(exportServiceSpy.validateTitle).toHaveBeenCalled();
    });
    it('onTitleUpdate shoud not validate title when target is null', () => {
        component.onTitleUpdate(({
            target: null,
        } as unknown) as InputEvent);
        expect(exportServiceSpy.validateTitle).not.toHaveBeenCalled();
    });
    it('onEmailUpdate shoud validate email', () => {
        component.onEmailUpdate(({
            target: {
                value: 'title',
            } as HTMLInputElement,
        } as unknown) as InputEvent);
        expect(exportServiceSpy.validateEmail).toHaveBeenCalled();
    });
    it('onEmailUpdate shoud not validate email when target is null', () => {
        component.onEmailUpdate(({
            target: null,
        } as unknown) as InputEvent);
        expect(exportServiceSpy.validateEmail).not.toHaveBeenCalled();
    });
});
