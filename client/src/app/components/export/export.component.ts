import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ExportModes } from '@app/classes/export-modes';
import { Filters } from '@app/classes/filters';
import { Formats } from '@app/classes/formats';
import { ExportService } from 'src/app/services/export/export.service';

// tslint:disable: use-lifecycle-interface

@Component({
    selector: 'app-export',
    templateUrl: './export.component.html',
    styleUrls: ['./export.component.scss'],
})
export class ExportComponent implements OnInit, AfterViewInit {
    exportFormats: string[];
    format: string;
    exportFilters: string[];
    filter: string;
    exportModes: string[];
    exportMode: string;
    email: string;
    title: string;
    isTitleValid: boolean;
    isEmailValid: boolean;

    private formatsMap: Map<string, Formats>;
    private filtersMap: Map<string, Filters>;
    private exportModeMap: Map<string, ExportModes>;

    @ViewChild('canvas', { static: false }) canvas: ElementRef<HTMLCanvasElement>;
    @ViewChild('downloadedImage', { static: false }) downloadedImage: ElementRef<HTMLImageElement>;
    @ViewChild('previewCanvas', { static: false }) previewCanvas: ElementRef<HTMLCanvasElement>;

    constructor(private dialogRef: MatDialogRef<ExportComponent>, private exportService: ExportService, private snack: MatSnackBar) {}

    private initializeMaps(): void {
        this.formatsMap = new Map();
        this.formatsMap.set('JPEG', Formats.JPEG);
        this.formatsMap.set('PNG', Formats.PNG);

        this.filtersMap = new Map();
        this.filtersMap = new Map();
        this.filtersMap.set('Aucun', Filters.Aucun);
        this.filtersMap.set('Sepia', Filters.Sepia);
        this.filtersMap.set('Inversion', Filters.Inversion);
        this.filtersMap.set('Constraste', Filters.Constraste);
        this.filtersMap.set('GrayScale', Filters.GrayScale);
        this.filtersMap.set('Rotation', Filters.Rotation);

        this.exportModeMap = new Map();
        this.exportModeMap.set('Téléchargement', ExportModes.Téléchargement);
        this.exportModeMap.set('Courriel', ExportModes.Courriel);
    }

    ngOnInit(): void {
        this.exportService.currentFormat.subscribe((format: Formats) => {
            this.format = format.toString();
        });
        this.exportService.currentFilter.subscribe((filter: Filters) => {
            this.filter = filter.toString();
        });
        this.exportService.isTitleValid.subscribe((validity: boolean) => {
            this.isTitleValid = validity;
        });
        this.exportService.isEmailValid.subscribe((validity: boolean) => {
            this.isEmailValid = validity;
        });

        this.exportFormats = Object.keys(Formats);
        this.exportFilters = Object.keys(Filters);
        this.exportModes = Object.keys(ExportModes);

        this.isTitleValid = false;
        this.exportService.canvas2Image();
        this.initializeMaps();
    }

    ngAfterViewInit(): void {
        this.exportService.canvas = this.canvas.nativeElement as HTMLCanvasElement;
        this.exportService.downloadedImage = this.downloadedImage;
        this.exportService.previewCanvas = this.previewCanvas.nativeElement as HTMLCanvasElement;
    }

    onFormatUpdate(format: string): void {
        const newFormat = this.formatsMap.get(format);
        if (newFormat) {
            this.exportService.currentFormat.next(newFormat);
        }
    }

    onFilterUpdate(filter: string): void {
        const newFilter = this.filtersMap.get(filter);
        if (newFilter) {
            this.exportService.currentFilter.next(newFilter);
            this.exportService.drawPreview(false);
        }
    }

    onTitleUpdate(event: InputEvent): void {
        if (event.target !== null) {
            this.exportService.validateTitle((event.target as HTMLInputElement).value);
        }
    }

    onExportModeUpdate(exportMode: string): void {
        const newExportMode = this.exportModeMap.get(exportMode);
        if (newExportMode !== undefined) {
            this.exportService.currentExportMode.next(newExportMode);
        }
    }

    onEmailUpdate(event: InputEvent): void {
        if (event.target !== null) {
            this.exportService.validateEmail((event.target as HTMLInputElement).value);
        }
    }

    closeDialog(): void {
        this.dialogRef.close();
    }

    exportConfirmation(): void {
        if (this.exportMode === ExportModes.Courriel) {
            if (!this.isEmailValid) {
                this.snack.open('Email invalide , tentez une nouvelle fois.', '', { duration: 2000 });
            } else if (!this.isTitleValid) {
                this.snack.open('Titre invalide , tentez une nouvelle fois.', '', { duration: 2000 });
            } else {
                this.exportService.exportByEmail(this.title, this.email);
                this.closeDialog();
            }
        }
        if (this.exportMode === ExportModes.Téléchargement) {
            if (!this.isTitleValid) {
                this.snack.open('Titre invalide , tentez une nouvelle fois.', '', { duration: 2000 });
            } else {
                this.exportService.export(this.title);
                this.closeDialog();
            }
        }
    }
}
