import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { DialogService } from '@app/services/dialog/dialog.service';
import { IndexService } from '@app/services/index/index.service';
import { of } from 'rxjs';
import { MainPageComponent } from './main-page.component';

describe('MainPageComponent', () => {
    let component: MainPageComponent;
    let fixture: ComponentFixture<MainPageComponent>;
    let indexServiceSpy: jasmine.SpyObj<IndexService>;
    let dialogServiceSpy: jasmine.SpyObj<DialogService>;
    let routerSpy: jasmine.SpyObj<Router>;

    beforeEach(async(() => {
        // array ŝtring des fonctions
        indexServiceSpy = jasmine.createSpyObj('IndexService', ['basicGet', 'basicPost']);
        routerSpy = jasmine.createSpyObj('Router', ['navigate']);
        indexServiceSpy.basicGet.and.returnValue(of({ title: '', body: '' }));
        indexServiceSpy.basicPost.and.returnValue(of());
        dialogServiceSpy = jasmine.createSpyObj('DialogService', ['openUserGuide', 'openCarousel']);
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, BrowserAnimationsModule, MatDialogModule, HttpClientModule],
            declarations: [MainPageComponent],
            providers: [
                { provide: IndexService, useValue: indexServiceSpy },
                { provide: DialogService, useValue: dialogServiceSpy },
                { provide: Router, useValue: routerSpy },
            ],
        }).compileComponents();
    }));

    beforeEach(async () => {
        fixture = TestBed.createComponent(MainPageComponent);
        component = fixture.componentInstance;
        await fixture.whenStable();
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call basicGet when calling getMessagesFromServer', () => {
        component.getMessagesFromServer();
        expect(indexServiceSpy.basicGet).toHaveBeenCalled();
    });

    it('should call basicPost when calling sendTimeToServer', () => {
        component.sendTimeToServer();
        expect(indexServiceSpy.basicPost).toHaveBeenCalled();
    });

    it('should open user guide', () => {
        component.openUserGuide();
        expect(dialogServiceSpy.openUserGuide).toHaveBeenCalled();
    });

    it('should open carousel', () => {
        component.openCarousel();
        expect(dialogServiceSpy.openCarousel).toHaveBeenCalled();
    });

    it('should call router.navigate', () => {
        component.editor();
        component.continue();
        expect(routerSpy.navigate).toHaveBeenCalledTimes(2);
    });

    it(' continueDrawing should be true when localStorage > 0 ', () => {
        localStorage.setItem('data', CanvasTestHelper.canvas().toDataURL());
        component.updateStatus();
        expect(component.continueDrawing).toBeTrue();
    });

    it(' continueDrawing should be false when localStorage is null 0 ', () => {
        localStorage.clear();
        component.updateStatus();
        expect(component.continueDrawing).not.toBeTrue();
    });
});
