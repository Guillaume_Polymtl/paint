import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FileAction } from '@app/classes/shortcuts/file-action';
import { CarouselComponent } from '@app/components/carousel/carousel.component';
import { UserGuideComponent } from '@app/components/user-guide/user-guide.component';
import { DialogService } from '@app/services/dialog/dialog.service';
import { FileService } from '@app/services/file/file.service';
import { IndexService } from '@app/services/index/index.service';
import { Message } from '@common/communication/message';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent {
    readonly title: string = 'Bienvenue sur Polydessin';
    message: BehaviorSubject<string> = new BehaviorSubject<string>('');
    continueDrawing: boolean = true;

    constructor(private basicService: IndexService, private dialogService: DialogService, private router: Router, private fileService: FileService) {
        this.updateStatus();
    }

    editor(): void {
        localStorage.clear();
        this.router.navigate(['/editor']);
        this.fileService.setAction(FileAction.CREATE);
    }

    continue(): void {
        this.router.navigate(['/editor']);
        this.fileService.setAction(FileAction.CONTINUE);
    }

    openUserGuide(): MatDialogRef<UserGuideComponent> {
        return this.dialogService.openUserGuide();
    }

    openCarousel(): MatDialogRef<CarouselComponent> {
        return this.dialogService.openCarousel();
    }

    updateStatus(): void {
        if (localStorage.length > 0) {
            this.continueDrawing = true;
        } else {
            this.continueDrawing = false;
        }
    }

    sendTimeToServer(): void {
        const newTimeMessage: Message = {
            title: 'Hello from the client',
            body: 'Time is : ' + new Date().toString(),
        };
        // Important de ne pas oublier "subscribe" ou l'appel ne sera jamais lancé puisque personne l'observe
        this.basicService.basicPost(newTimeMessage).subscribe();
    }

    getMessagesFromServer(): void {
        this.basicService
            .basicGet()
            // Cette étape transforme le Message en un seul string
            .pipe(
                map((message: Message) => {
                    return `${message.title} ${message.body}`;
                }),
            )
            .subscribe(this.message);
    }
}
