import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { ExportService } from '@app/services/export/export.service';
import { SavingService } from '@app/services/saving/saving.service';
import { HttpStatus } from '@common/communication/http-status';
import { BehaviorSubject, throwError } from 'rxjs';
import { SavingComponent } from './saving.component';

// tslint:disable: no-string-literal
// tslint:disable: no-any

describe('SavingComponent', () => {
    let component: SavingComponent;
    let fixture: ComponentFixture<SavingComponent>;
    let savingServiceSpy: jasmine.SpyObj<SavingService>;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;

    beforeEach(async(() => {
        savingServiceSpy = jasmine.createSpyObj('SavingService', ['validateTitle', 'postImage', 'handleError']);
        savingServiceSpy.isTitleValid = new BehaviorSubject<boolean>(false);
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas']);

        TestBed.configureTestingModule({
            declarations: [SavingComponent],
            providers: [
                {
                    provide: MatDialogRef,
                    useValue: {
                        moduleDef: SavingComponent,
                        close: () => null,
                    },
                },
                {
                    provide: SavingService,
                    useValue: savingServiceSpy,
                },
                { provide: DrawingService, useValue: drawServiceSpy },
                MatSnackBar,
                SavingService,
                ExportService,
            ],
            imports: [MatSnackBarModule, HttpClientModule, MatDialogModule, BrowserAnimationsModule],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(SavingComponent);
        component = fixture.componentInstance;
        component['exportService']['drawingService'].canvas = CanvasTestHelper.canvas();
        fixture.detectChanges();
    });

    it('should create the component ', () => {
        expect(component).toBeTruthy();
    });

    it('closeDialog shoud close dialog', () => {
        const spy = spyOn(component['dialogRef'], 'close');
        component.closeDialog();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('save not should save if title is not valid', () => {
        component['title'] = 'title';
        component['isTitleValid'] = false;
        const spy = spyOn(component['dialogRef'], 'close');
        const spy3 = spyOn(component['snack'], 'open');
        component.save();
        expect(spy).toHaveBeenCalledTimes(0);
        expect(spy3).toHaveBeenCalledTimes(1);
    });

    it('save should post Image if title is valid', () => {
        component['title'] = 'title';
        component['isTitleValid'] = true;
        const spy1 = spyOn(component['dialogRef'], 'close');
        const spy3 = spyOn(component, 'post');
        component.save();
        expect(spy3).toHaveBeenCalled();
        expect(spy1).toHaveBeenCalledTimes(1);
    });

    it('addTag should add valid tag', () => {
        const spy = spyOn(component['snack'], 'open');
        component.addTag('valid');
        expect(component['tags'].has('VALID')).toEqual(true);
        expect(component['tagName']).toEqual('');
        expect(spy).not.toHaveBeenCalled();
    });

    it('addTag should show error, max number of tags reached', () => {
        const tags = new Set<string>();
        tags.add('test1');
        tags.add('test2');
        tags.add('test3');
        tags.add('test4');
        tags.add('test5');
        tags.add('test6');
        tags.add('test7');
        component['tags'] = tags;
        const spy = spyOn(component['snack'], 'open');
        component.addTag('test8');
        expect(spy).toHaveBeenCalledWith('Vous ne pouvez pas ajouter plus de 7 étiquettes', '', { duration: 2000 });
    });

    it('addTag should show error, tag name invalid', () => {
        const spy = spyOn(component['snack'], 'open');
        component.addTag('##');
        expect(component['tags'].has('##')).toEqual(false);
        expect(spy).toHaveBeenCalledWith('Étiquette invalide', '', { duration: 2000 });
    });

    it('removeTag should remove a tag', () => {
        const spy = spyOn(component['saveService'], 'removeTag');
        const tags = component['tags'];
        component.removeTag('test');
        expect(spy).toHaveBeenCalledWith('test', tags);
    });

    it('onTitleUpdate shoud validate title', () => {
        const spy = spyOn(component['saveService'], 'validateTitle');
        component.onTitleUpdate(({
            target: {
                value: 'title',
            } as HTMLInputElement,
        } as unknown) as InputEvent);
        expect(spy).toHaveBeenCalledWith('title');
    });
    it('onTitleUpdate shoud not validate title while target is null', () => {
        const spy = spyOn(component['saveService'], 'validateTitle');
        component.onTitleUpdate(({
            target: null,
        } as unknown) as InputEvent);
        expect(spy).not.toHaveBeenCalled();
    });

    it(' should post Image correctly', () => {
        const spy2 = spyOn(component['saveService'], 'postImage').and.callThrough();
        component.post();
        expect(spy2).toHaveBeenCalled();
    });

    it('should handle error when posting', () => {
        spyOn(component['saveService'], 'postImage').and.callFake(() => throwError({ status: HttpStatus.BAD_REQUEST }));
        const spy2 = spyOn(component['saveService'], 'handleError');
        component.post();
        expect(spy2).toHaveBeenCalled();
    });
});
