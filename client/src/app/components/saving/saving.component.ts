import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAX_TAGS } from '@app/classes/server/backend';
import { ExportService } from '@app/services/export/export.service';
import { SavingService } from '@app/services/saving/saving.service';
import { Image } from '@common/communication/image-data';

@Component({
    selector: 'app-saving',
    templateUrl: './saving.component.html',
    styleUrls: ['./saving.component.scss'],
})
export class SavingComponent implements AfterViewInit, OnInit {
    title: string;
    tags: Set<string>;
    tagName: string;
    isSaving: boolean;
    isTitleValid: boolean;

    @ViewChild('canvas', { static: false }) canvas: ElementRef<HTMLCanvasElement>;
    @ViewChild('previewCanvas', { static: false }) previewCanvas: ElementRef<HTMLCanvasElement>;

    constructor(
        private dialogRef: MatDialogRef<SavingComponent>,
        private saveService: SavingService,
        private exportService: ExportService,
        private snack: MatSnackBar,
    ) {
        this.tags = new Set<string>();
        this.tagName = '';
        this.isSaving = false;
        this.isTitleValid = false;
    }

    ngOnInit(): void {
        this.saveService.isTitleValid.subscribe((validity: boolean) => {
            this.isTitleValid = validity;
        });
        this.exportService.canvas2Image();
    }

    ngAfterViewInit(): void {
        this.exportService.canvas = this.canvas.nativeElement as HTMLCanvasElement;
        this.exportService.previewCanvas = this.previewCanvas.nativeElement;
    }

    closeDialog(): void {
        this.dialogRef.close();
    }

    removeTag(tag: string): void {
        this.saveService.removeTag(tag, this.tags);
    }

    onTitleUpdate(event: InputEvent): void {
        if (event.target) {
            this.saveService.validateTitle((event.target as HTMLInputElement).value);
        }
    }

    save(): void {
        if (!this.isTitleValid) {
            this.snack.open('Titre invalide , tentez une nouvelle fois.', '', { duration: 2000 });
        } else {
            this.post();
            this.closeDialog();
        }
    }

    addTag(tag: string): void {
        if (this.tags.size < MAX_TAGS) {
            const isTagValid = this.saveService.addTag(tag, this.tags);
            if (isTagValid) {
                this.tagName = '';
            } else {
                this.snack.open('Étiquette invalide', '', { duration: 2000 });
            }
        } else {
            this.snack.open('Vous ne pouvez pas ajouter plus de 7 étiquettes', '', { duration: 2000 });
        }
    }

    post(): void {
        this.isSaving = true;
        this.snack.open('Début de la sauvegarde', '', { duration: 2000 });
        this.saveService
            .postImage(this.title, this.tags, this.exportService.image.src, this.exportService.image.width, this.exportService.image.height)
            .subscribe(
                (data: Image) => {
                    /*Do nothing */
                },
                (error: HttpErrorResponse) => {
                    this.isSaving = false;
                    this.saveService.handleError(error);
                },
            );
    }
}
