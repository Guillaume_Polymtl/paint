import { Location } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { BLACK } from '@app/classes/colors/color';
import { FileAction } from '@app/classes/shortcuts/file-action';
import { Tool } from '@app/classes/tool';
import { ToolButton } from '@app/classes/toolbar/tool-button';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { ColorService } from '@app/color-picker/services/color.service';
import { AttributesPanelComponent } from '@app/components/attributes-panel/attributes-panel.component';
import { DialogService } from '@app/services/dialog/dialog.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { FileService } from '@app/services/file/file.service';
import { ToolsService } from '@app/services/tools/tools.service';
import { of } from 'rxjs';
import { SidebarComponent } from './sidebar.component';

class ToolStub extends Tool {}

describe('SidebarComponent', () => {
    let component: SidebarComponent;
    let fixture: ComponentFixture<SidebarComponent>;
    let dialogServiceSpy: jasmine.SpyObj<DialogService>;
    let locationSpy: jasmine.SpyObj<Location>;
    let routerSpy: jasmine.SpyObj<Router>;
    let fileServiceSpy: jasmine.SpyObj<FileService>;
    let toolsServiceSpy: jasmine.SpyObj<ToolsService>;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let colorServiceSpy: jasmine.SpyObj<ColorService>;
    let toolStub: ToolStub;

    beforeEach(async(() => {
        dialogServiceSpy = jasmine.createSpyObj('DialogService', ['openUserGuide', 'openExportPanel', 'openSavePanel']);
        routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
        fileServiceSpy = jasmine.createSpyObj('FileService', ['setAction']);
        toolsServiceSpy = jasmine.createSpyObj('ToolService', ['setTool', 'getTool']);
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas']);
        colorServiceSpy = jasmine.createSpyObj('ColorService', ['getColor']);
        colorServiceSpy.getColor.and.returnValue(of(BLACK));
        toolStub = new ToolStub(drawingServiceSpy, colorServiceSpy);
        toolsServiceSpy.getTool.and.returnValue(of(toolStub));

        locationSpy = jasmine.createSpyObj(Location, ['back']);
        TestBed.configureTestingModule({
            declarations: [SidebarComponent, AttributesPanelComponent],
            imports: [BrowserAnimationsModule, MatDialogModule, MatMenuModule, MatIconModule, MatTooltipModule],
            providers: [
                ToolsService,
                { provide: DialogService, useValue: dialogServiceSpy },
                { provide: Location, useValue: locationSpy },
                { provide: Router, useValue: routerSpy },
                { provide: FileService, useValue: fileServiceSpy },
                { provide: ToolsService, useValue: toolsServiceSpy },
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SidebarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('goback should return to previous location', () => {
        component.goBack();
        expect(locationSpy.back).toHaveBeenCalled();
    });

    it('should open the dialog', () => {
        component.openUserGuide();
        expect(dialogServiceSpy.openUserGuide).toHaveBeenCalled();
    });
    it('should open the export dialog', () => {
        component.openExportPanel();
        expect(dialogServiceSpy.openExportPanel).toHaveBeenCalled();
    });
    it('should open the saving dialog', () => {
        component.openSavePanel();
        expect(dialogServiceSpy.openSavePanel).toHaveBeenCalled();
    });

    it('should call fileservice.setAction()', () => {
        component.new();
        expect(fileServiceSpy.setAction).toHaveBeenCalledWith(FileAction.CLEAR);
    });

    it('should call toolsService.setTool', () => {
        const toolButton: ToolButton = {
            name: ToolName.ERASER,
            selected: false,
        };
        component.selectTool(ToolName.ERASER);
        expect(toolsServiceSpy.setTool).toHaveBeenCalledWith(toolButton.name);
    });
});
