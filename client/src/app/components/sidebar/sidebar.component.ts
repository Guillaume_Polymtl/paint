import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FileAction } from '@app/classes/shortcuts/file-action';
import { Tool } from '@app/classes/tool';
import { ToolButton } from '@app/classes/toolbar/tool-button';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { ExportComponent } from '@app/components/export/export.component';
import { SavingComponent } from '@app/components/saving/saving.component';
import { UserGuideComponent } from '@app/components/user-guide/user-guide.component';
import { DialogService } from '@app/services/dialog/dialog.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { FileService } from '@app/services/file/file.service';
import { ToolsService } from '@app/services/tools/tools.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
    tools: ToolButton[];
    toolName: typeof ToolName;
    currentTool: ToolName;

    constructor(
        private dialogService: DialogService,
        public drawingService: DrawingService,
        public toolsService: ToolsService,
        public location: Location,
        private fileService: FileService,
    ) {}

    openUserGuide(): MatDialogRef<UserGuideComponent> {
        return this.dialogService.openUserGuide();
    }
    openExportPanel(): MatDialogRef<ExportComponent> {
        return this.dialogService.openExportPanel();
    }
    openSavePanel(): MatDialogRef<SavingComponent> {
        return this.dialogService.openSavePanel();
    }
    new(): void {
        this.fileService.setAction(FileAction.CLEAR);
    }

    goBack(): void {
        this.location.back();
    }

    ngOnInit(): void {
        this.tools = [];
        this.toolsService.getTool().subscribe((tool: Tool) => {
            this.currentTool = tool.name;
        });
        this.toolName = ToolName;
    }

    selectTool(toolName: ToolName): void {
        this.toolsService.setTool(toolName);
    }
}
