import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserSubguideComponent } from '@app/components/user-subguide/user-subguide.component';
import { UserGuideComponent } from './user-guide.component';

// tslint:disable:no-any
describe('UserGuideComponent', () => {
    let component: UserGuideComponent;
    let fixture: ComponentFixture<UserGuideComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatTabsModule, MatDialogModule, BrowserAnimationsModule, MatButtonModule],
            declarations: [UserGuideComponent, UserSubguideComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserGuideComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
