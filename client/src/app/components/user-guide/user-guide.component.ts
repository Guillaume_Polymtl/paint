import { Component, OnInit } from '@angular/core';
import { guide } from '@app/classes/guide';
import { UserGuide } from '@app/classes/user-guide';

@Component({
    selector: 'app-user-guide',
    templateUrl: './user-guide.component.html',
    styleUrls: ['./user-guide.component.scss'],
})
export class UserGuideComponent implements OnInit {
    guide: UserGuide;

    ngOnInit(): void {
        this.guide = guide;
    }
}
