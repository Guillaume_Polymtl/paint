import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTabsModule } from '@angular/material/tabs';
import { UserSubguideComponent } from './user-subguide.component';

describe('UserSubguideComponent', () => {
    let component: UserSubguideComponent;
    let fixture: ComponentFixture<UserSubguideComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatTabsModule],
            declarations: [UserSubguideComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserSubguideComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
