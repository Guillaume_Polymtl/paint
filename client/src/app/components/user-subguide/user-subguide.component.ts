import { Component, Input } from '@angular/core';
import { UserGuide } from '@app/classes/user-guide';

@Component({
    selector: 'app-user-subguide',
    templateUrl: './user-subguide.component.html',
    styleUrls: ['./user-subguide.component.scss'],
})
export class UserSubguideComponent {
    @Input() guides: UserGuide[];
}
