import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { MatChipInputEvent } from '@angular/material/chips';
import { Router } from '@angular/router';
import { cards, CarouselCard } from '@app/classes/carousel-card';
import { ImageState } from '@app/classes/tools-utility';
import { CarouselService } from '@app/services/carousel/carousel.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { BehaviorSubject } from 'rxjs';

// tslint:disable: no-any
describe('CarouselService', () => {
    let service: CarouselService;
    let routerSpy: jasmine.SpyObj<Router>;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    const imageState: BehaviorSubject<ImageState> = new BehaviorSubject<ImageState>(ImageState.Idle);
    const TEST_CARDS: CarouselCard[] = [
        {
            title: 'title1',
            imageSource: 'source1',
            tags: ['hello', 'world'],
        },
        {
            title: 'title2',
            imageSource: 'source2',
            tags: ['hi', 'friend'],
        },
        {
            title: 'title3',
            imageSource: 'source3',
            tags: ['hey', 'there'],
        },
    ];
    const TEST_DURATION = 5000;

    beforeEach(async(() => {
        routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['loadImage', 'cancelImageDrawing', 'confirmImageDrawing'], {
            imageState,
        });
        TestBed.configureTestingModule({
            providers: [
                { provide: Router, useValue: routerSpy },
                { provide: DrawingService, useValue: drawingServiceSpy },
            ],
        });
    }));

    beforeEach(() => {
        service = TestBed.inject(CarouselService);
        service.cards = new BehaviorSubject<CarouselCard[]>(cards);
        service.imageState = new BehaviorSubject<ImageState>(ImageState.Idle);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('init should set imagestate value to Idle', () => {
        service.init();
        expect(service.imageState.value).toEqual(ImageState.Idle);
    });

    it('initImageState should set imageState to Idle if previousState', () => {
        service.imageState.next(ImageState.Loading);
        service.initImageState(ImageState.Loading);
        expect(service.imageState.value).toEqual(ImageState.Idle);
    });

    it('initImageState should not change imageState if not previousState', () => {
        service.imageState.next(ImageState.Loading);
        service.initImageState(ImageState.DeletionError);
        expect(service.imageState.value).toEqual(ImageState.Loading);
    });

    it('should complete destroyed if destructed', () => {
        const nextSpy: jasmine.Spy<any> = spyOn<any>(service.destroyed, 'next');
        const completeSpy: jasmine.Spy<any> = spyOn<any>(service.destroyed, 'complete');
        service.ngOnDestroy();
        expect(nextSpy).toHaveBeenCalledWith(true);
        expect(completeSpy).toHaveBeenCalled();
    });

    it('openImage should set imagestate value to Loading and call DrawingService loadImage', () => {
        service.openImage(0);
        expect(service.imageState.value).toEqual(ImageState.Loading);
        expect(drawingServiceSpy.loadImage).toHaveBeenCalled();
    });

    it('cancelImageDrawing should call DrawingService cancelImageDrawing', () => {
        service.cancelImageDrawing();
        expect(drawingServiceSpy.cancelImageDrawing).toHaveBeenCalled();
    });

    it('confirmImageDrawing should call DrawingService confirmImageDrawing', () => {
        service.confirmImageDrawing();
        expect(drawingServiceSpy.confirmImageDrawing).toHaveBeenCalled();
    });

    it('deleteImage should remove card from array', () => {
        const index = 0;
        const source: string = service.cards.value[index].imageSource;
        service.deleteImage(index);
        expect(service.cards.value[index].imageSource).not.toEqual(source);
    });

    it('deleteImage should set imageState value to DeletionError if an error no card deleted', () => {
        const nextSpy: jasmine.Spy<any> = spyOn<any>(service.imageState, 'next');
        service.cards.next([]);
        service.deleteImage(0);
        expect(nextSpy).toHaveBeenCalledWith(ImageState.DeletionError);
    });

    it('deleteImage should set imageState value to Idle after MESSAGE_DELAY ms', fakeAsync(() => {
        const initImageStateSpy: jasmine.Spy<any> = spyOn<any>(service, 'initImageState').and.callThrough();
        service.cards.next([]);
        service.deleteImage(0);
        tick(service.MESSAGE_DELAY + TEST_DURATION);
        expect(initImageStateSpy).toHaveBeenCalledWith(ImageState.DeletionError);
        expect(service.imageState.value).toEqual(ImageState.Idle);
    }));

    it('removeTag should remove tag from array', () => {
        service.filteringTags = ['a', 'b', 'c'];
        const expectedLength: number = service.filteringTags.length - 1;
        const tagToRemove = 'b';
        service.removeTag(tagToRemove);
        expect(service.filteringTags.length).toEqual(expectedLength);
        expect(service.filteringTags.find((tag) => tag === tagToRemove)).toEqual(undefined);
    });

    it('removeTag should call filter method', () => {
        service.filteringTags = [];
        const filterSpy: jasmine.Spy<any> = spyOn<any>(service, 'filter');
        const tagToRemove = 'a';
        service.removeTag(tagToRemove);
        expect(filterSpy).toHaveBeenCalled();
    });

    it('addFilterTag should not add tag if already registered', () => {
        service.filteringTags = ['a', 'b', 'c'];
        const expectedLength: number = service.filteringTags.length;
        const tagIndex = 1;
        const tagToAdd: string = service.filteringTags[tagIndex];
        service.addFilterTag(tagToAdd);
        expect(service.filteringTags.length).toEqual(expectedLength);
        expect(service.filteringTags.lastIndexOf(tagToAdd)).toEqual(tagIndex);
    });

    it('addFilterTag should add tag to array', () => {
        service.filteringTags = ['a', 'b'];
        const expectedLength: number = service.filteringTags.length + 1;
        const tagToAdd = 'c';
        service.addFilterTag(tagToAdd);
        expect(service.filteringTags.length).toEqual(expectedLength);
        expect(service.filteringTags.some((tag) => tag === tagToAdd)).toEqual(true);
    });

    it('addFilterTagEvent should call filter method', () => {
        const event: MatChipInputEvent = ({ input: null, value: '' } as unknown) as MatChipInputEvent;
        service.filteringTags = [];
        const filterSpy: jasmine.Spy<any> = spyOn<any>(service, 'filter');
        service.addFilterTagByEvent(event);
        expect(filterSpy).toHaveBeenCalled();
    });

    it('addFilterTagEvent should not add tag if already registered', () => {
        service.filteringTags = ['hello', 'world'];
        const expectedLength: number = service.filteringTags.length;
        const tagIndex = 0;
        const tagToAdd: string = service.filteringTags[tagIndex];
        const event: MatChipInputEvent = ({ input: null, value: tagToAdd } as unknown) as MatChipInputEvent;
        service.addFilterTagByEvent(event);
        expect(service.filteringTags.length).toEqual(expectedLength);
        expect(service.filteringTags.lastIndexOf(tagToAdd)).toEqual(tagIndex);
    });

    it('addFilterTagEvent should add tag to array', () => {
        service.filteringTags = ['hello', 'world'];
        const expectedLength: number = service.filteringTags.length + 1;
        const tagToAdd = '!';
        const event: MatChipInputEvent = ({ input: null, value: tagToAdd } as unknown) as MatChipInputEvent;
        service.addFilterTagByEvent(event);
        expect(service.filteringTags.length).toEqual(expectedLength);
        expect(service.filteringTags.some((tag) => tag === tagToAdd)).toEqual(true);
    });

    it('addFilterTagEvent should clear input', () => {
        service.filteringTags = [];
        const input: HTMLInputElement = document.createElement('input');
        input.value = 'value';
        const event: MatChipInputEvent = ({ input, value: 'hello' } as unknown) as MatChipInputEvent;
        service.addFilterTagByEvent(event);
        expect(input.value).toEqual('');
    });

    it('filter should send all cards if filteringTags is empty', () => {
        const getAllCardsSpy: jasmine.Spy<any> = spyOn(service, 'getAllCards').and.returnValue(TEST_CARDS);
        service.filteringTags = [];
        service.filter();
        expect(service.cards.value).toEqual(getAllCardsSpy());
    });

    it('filter should send all carousel cards with matching tags', () => {
        spyOn(service, 'getAllCards').and.returnValue(TEST_CARDS);
        service.filteringTags = ['hello', 'friend'];
        // tslint:disable-next-line: no-magic-numbers
        const expectedResults: CarouselCard[] = [TEST_CARDS[0], TEST_CARDS[1]];
        service.filter();
        expect(service.cards.value).toEqual(expectedResults);
    });

    it('filter should set imageState value to NoCorrespondence if no matching card found', () => {
        const nextSpy: jasmine.Spy<any> = spyOn<any>(service.imageState, 'next');
        spyOn(service, 'getAllCards').and.returnValue(TEST_CARDS);
        service.filteringTags = ['yeah'];
        service.filter();
        expect(nextSpy).toHaveBeenCalledWith(ImageState.NoCorrespondence);
    });

    it('filter should set imageState value to Idle after MESSAGE_DELAY ms', fakeAsync(() => {
        const initImageStateSpy: jasmine.Spy<any> = spyOn<any>(service, 'initImageState').and.callThrough();
        spyOn(service, 'getAllCards').and.returnValue(TEST_CARDS);
        service.filteringTags = ['yeah'];
        service.filter();
        tick(service.MESSAGE_DELAY + TEST_DURATION);
        expect(initImageStateSpy).toHaveBeenCalledWith(ImageState.NoCorrespondence);
        expect(service.imageState.value).toEqual(ImageState.Idle);
    }));
});
