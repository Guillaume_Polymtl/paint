import { Injectable, OnDestroy } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { cards, CarouselCard } from '@app/classes/carousel-card';
import { ImageState } from '@app/classes/tools-utility';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class CarouselService implements OnDestroy {
    readonly MESSAGE_DELAY: number = 5000;
    filteringTags: string[];
    cards: BehaviorSubject<CarouselCard[]>;
    imageState: BehaviorSubject<ImageState>;
    destroyed: Subject<boolean> = new Subject<boolean>();

    constructor(private drawingService: DrawingService) {}

    init(): void {
        this.filteringTags = [];
        this.imageState = new BehaviorSubject<ImageState>(ImageState.Idle);
        // Remplacer par un appel au serveur pour charger les infos des cartes
        this.cards = new BehaviorSubject<CarouselCard[]>(this.getAllCards());
        const stateChangeObservable = this.drawingService.imageState.pipe(
            map((imageState) => imageState),
            takeUntil(this.destroyed),
        );
        stateChangeObservable.subscribe((imageState: ImageState) => this.imageState.next(imageState));
    }

    initImageState(previousState: ImageState): void {
        if (this.imageState.value === previousState) {
            this.imageState.next(ImageState.Idle);
        }
    }

    ngOnDestroy(): void {
        this.destroyed.next(true);
        this.destroyed.complete();
    }

    getAllCards(): CarouselCard[] {
        return cards;
    }

    openImage(index: number): void {
        this.imageState.next(ImageState.Loading);
        const imageSource: string = this.cards.value[index].imageSource;
        this.drawingService.loadImage(imageSource);
    }

    cancelImageDrawing(): void {
        this.drawingService.cancelImageDrawing();
    }

    confirmImageDrawing(): void {
        this.drawingService.confirmImageDrawing();
    }

    deleteImage(indexInCardsArray: number): void {
        // TODO: Remplacer par un appel au serveur pour effacer sur sa machine et infos dans la base
        // et récupérer infos de suppression pour vérifier erreur
        const cardsArray: CarouselCard[] = this.cards.value;
        const deletedCard: CarouselCard[] = cardsArray.splice(indexInCardsArray, 1);
        this.cards.next(cardsArray);
        // Si suppression a échoué, afficher la raison
        if (deletedCard.length === 0) {
            this.imageState.next(ImageState.DeletionError);
            setTimeout(() => {
                this.initImageState(ImageState.DeletionError);
            }, this.MESSAGE_DELAY);
        }
    }

    filter(): void {
        const cardsArray: CarouselCard[] = this.getAllCards();
        if (this.filteringTags.length !== 0) {
            const filteredCards: CarouselCard[] = [];
            for (const card of cardsArray) {
                if (card.tags.find((cardTag) => this.filteringTags.some((tag) => tag.toLowerCase() === cardTag.toLowerCase(), this), this)) {
                    filteredCards.push(card);
                }
            }
            if (filteredCards.length === 0) {
                this.imageState.next(ImageState.NoCorrespondence);
                setTimeout(() => {
                    this.initImageState(ImageState.NoCorrespondence);
                }, this.MESSAGE_DELAY);
            }
            this.cards.next(filteredCards);
        } else {
            this.cards.next(cardsArray);
        }
    }

    addFilterTagByEvent(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        // Ajout de l'étiquette
        if ((value || '').trim()) {
            if (!this.filteringTags.some((tag) => tag.toLowerCase() === value.trim().toLowerCase()))
                this.filteringTags.push(value.trim().toLowerCase());
        }

        if (input) {
            input.value = '';
        }

        this.filter();
    }

    addFilterTag(tag: string): void {
        if (!this.filteringTags.some((cardTag) => cardTag.toLowerCase() === tag.toLowerCase())) this.filteringTags.push(tag.toLowerCase());
        this.filter();
    }

    removeTag(tag: string): void {
        const index = this.filteringTags.indexOf(tag.toLowerCase());

        if (index >= 0) {
            this.filteringTags.splice(index, 1);
        }

        this.filter();
    }
}
