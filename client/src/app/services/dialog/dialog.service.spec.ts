import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmResetDrawingComponent } from '@app/components/confirm-reset-drawing/confirm-reset-drawing.component';
import { of } from 'rxjs';
import { DialogService } from './dialog.service';

export class MatDialogMock {
    openDialogs: MatDialogRef<ConfirmResetDrawingComponent>[] = [];
    // When the component calls this.dialog.open(...) we'll return an object
    // with an afterClosed method that allows to subscribe to the dialog result observable.
    open(): object {
        this.openDialogs.push({
            afterClosed: () => of({ action: true }),
        } as MatDialogRef<ConfirmResetDrawingComponent>);
        return this.openDialogs[this.openDialogs.length - 1];
    }

    closeAll(): void {
        this.openDialogs = [];
        return;
    }
}

describe('DialogService', () => {
    let service: DialogService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [{ provide: MatDialog, useClass: MatDialogMock }],
        });
        service = TestBed.inject(DialogService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return a ref when opening the user guide', () => {
        const ref = service.openUserGuide();
        expect(ref).toBeDefined();
    });
    it('should return a ref when opening the export panel', () => {
        const ref = service.openExportPanel();
        expect(ref).toBeDefined();
    });

    it('should return a ref when opening the export panel', () => {
        const ref = service.openSavePanel();
        expect(ref).toBeDefined();
    });

    it('should return a ref when opening the carousel', () => {
        const ref = service.openCarousel();
        expect(ref).toBeDefined();
    });

    it('should return a ref when opening the confirm reset drawing', () => {
        const ref = service.confirmResetDrawing();
        expect(ref).toBeDefined();
    });

    it('should return true if a dialog is opened', () => {
        service.confirmResetDrawing();
        expect(service.isDialogOpened()).toEqual(true);
    });

    it('should return false if no dialog opened', () => {
        service.closeAll();
        expect(service.isDialogOpened()).toEqual(false);
    });
});
