import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CarouselComponent } from '@app/components/carousel/carousel.component';
import { ConfirmResetDrawingComponent } from '@app/components/confirm-reset-drawing/confirm-reset-drawing.component';
import { ExportComponent } from '@app/components/export/export.component';
import { SavingComponent } from '@app/components/saving/saving.component';
import { UserGuideComponent } from '@app/components/user-guide/user-guide.component';

@Injectable({
    providedIn: 'root',
})
export class DialogService {
    userguideRef: MatDialogRef<UserGuideComponent>;
    exportRef: MatDialogRef<ExportComponent>;
    savingRef: MatDialogRef<SavingComponent>;
    carouselRef: MatDialogRef<CarouselComponent>;

    confirmResetDrawingRef: MatDialogRef<ConfirmResetDrawingComponent>;

    constructor(private dialog: MatDialog) {}

    openUserGuide(): MatDialogRef<UserGuideComponent> {
        this.closeAll();
        const dialogRef: MatDialogRef<UserGuideComponent> = this.dialog.open(UserGuideComponent);
        return dialogRef;
    }

    confirmResetDrawing(): MatDialogRef<ConfirmResetDrawingComponent> {
        const dialogRef: MatDialogRef<ConfirmResetDrawingComponent> = this.dialog.open(ConfirmResetDrawingComponent);
        this.confirmResetDrawingRef = dialogRef;
        return dialogRef;
    }

    openExportPanel(): MatDialogRef<ExportComponent> {
        this.closeAll();
        const dialogRef: MatDialogRef<ExportComponent> = this.dialog.open(ExportComponent);
        this.exportRef = dialogRef;
        return dialogRef;
    }

    openSavePanel(): MatDialogRef<SavingComponent> {
        this.closeAll();
        const dialogRef: MatDialogRef<SavingComponent> = this.dialog.open(SavingComponent);
        this.savingRef = dialogRef;
        return dialogRef;
    }

    openCarousel(): MatDialogRef<CarouselComponent> {
        this.closeAll();
        const dialogRef: MatDialogRef<CarouselComponent> = this.dialog.open(CarouselComponent);
        this.carouselRef = dialogRef;
        return dialogRef;
    }

    isDialogOpened(): boolean {
        return !!this.dialog.openDialogs && this.dialog.openDialogs.length > 0;
    }

    closeAll(): void {
        this.dialog.closeAll();
    }
}
