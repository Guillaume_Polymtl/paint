import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { EXPORT_MAX_HEIGHT, EXPORT_MAX_WIDTH } from '@app/classes/export-constants';
import { ImageState } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from './drawing.service';

// tslint:disable: no-any
describe('DrawingService', () => {
    let service: DrawingService;
    let routerSpy: jasmine.SpyObj<Router>;
    const TEST_DURATION = 5000;

    beforeEach(async(() => {
        routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

        TestBed.configureTestingModule({
            providers: [{ provide: Router, useValue: routerSpy }],
        });
    }));

    beforeEach(() => {
        service = TestBed.inject(DrawingService);
        service.canvas = CanvasTestHelper.canvas();
        service.baseCtx = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        service.previewCtx = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should clear the whole canvas', () => {
        service.clearCanvas(service.baseCtx);
        const pixelBuffer = new Uint32Array(service.baseCtx.getImageData(0, 0, service.canvas.width, service.canvas.height).data.buffer);
        const hasColoredPixels = pixelBuffer.some((color) => color !== 0);
        expect(hasColoredPixels).toEqual(false);
    });

    it('should return true if the context is pristine', () => {
        const canvas = document.createElement('canvas');
        const context = canvas.getContext('2d') as CanvasRenderingContext2D;
        const result = service.isPristine(context);
        expect(result).toBeTrue();
    });

    it('should return false if the canvas is not pristine', () => {
        const canvas = document.createElement('canvas');
        const context = canvas.getContext('2d') as CanvasRenderingContext2D;
        canvas.height = 1;
        canvas.width = 1;
        context.fillStyle = 'red';
        context.fillRect(0, 0, 1, 1);
        const result = service.isPristine(context);
        expect(result).toBeFalse();
    });

    it('initImageState should set imageState to Idle if previousState', () => {
        service.imageState.next(ImageState.Loading);
        service.initImageState(ImageState.Loading);
        expect(service.imageState.value).toEqual(ImageState.Idle);
    });

    it('initImageState should not change imageState if not previousState', () => {
        service.imageState.next(ImageState.Loading);
        service.initImageState(ImageState.DeletionError);
        expect(service.imageState.value).toEqual(ImageState.Loading);
    });

    it('cancelImageDrawing should set imageState value to Idle', () => {
        service.imageState.next(ImageState.UsedCanvas);
        service.cancelImageDrawing();
        expect(service.imageState.value).toEqual(ImageState.Idle);
    });

    it('confirmImageDrawing should call drawImage with carouselImageToDraw', () => {
        service.carouselImageToDraw = new Image();
        const drawImageSpy: jasmine.Spy<any> = spyOn<any>(service, 'drawImage');
        service.confirmImageDrawing();
        expect(drawImageSpy).toHaveBeenCalledWith(service.carouselImageToDraw);
    });

    it('onLoadingError should set imageState value to LoadingError', () => {
        const nextSpy: jasmine.Spy<any> = spyOn<any>(service.imageState, 'next');
        service.imageState.next(ImageState.Loading);
        service.onLoadingError();
        expect(nextSpy).toHaveBeenCalledWith(ImageState.LoadingError);
    });

    it('onLoadingError should set imageState value to Idle after MESSAGE_DELAY ms', fakeAsync(() => {
        const initImageStateSpy: jasmine.Spy<any> = spyOn<any>(service, 'initImageState').and.callThrough();
        service.onLoadingError();
        tick(service.MESSAGE_DELAY + TEST_DURATION);
        expect(initImageStateSpy).toHaveBeenCalledWith(ImageState.LoadingError);
        expect(service.imageState.value).toEqual(ImageState.Idle);
    }));

    it('loadImage should call router navigateByUrl', () => {
        service.loadImage('');
        expect(routerSpy.navigateByUrl).toHaveBeenCalledWith('/editor');
    });

    it('loadImage should call checkImageLoading with carouselImageToLoad after MAX_IMAGE_LOADING_DELAY ms', fakeAsync(() => {
        const checkImageSpy: jasmine.Spy<any> = spyOn<any>(service, 'checkImageLoading');
        service.loadImage('');
        tick(service.MAX_IMAGE_LOADING_DELAY + TEST_DURATION);
        expect(checkImageSpy).toHaveBeenCalledWith(service.carouselImageToLoad);
    }));

    it('loadImage should not call updateImageOnLoad if not loaded', () => {
        const updateSpy: jasmine.Spy<any> = spyOn<any>(service, 'updateImageOnLoad');
        service.isLoaded = false;
        service.loadImage('');
        expect(updateSpy).not.toHaveBeenCalled();
    });

    it('loadImage should call updateImageOnLoad if loaded', () => {
        const updateSpy: jasmine.Spy<any> = spyOn<any>(service, 'updateImageOnLoad');
        service.isLoaded = true;
        service.loadImage('');
        expect(updateSpy).toHaveBeenCalled();
    });

    it('updateImageOnLoad should set imagetoload source to imageToLoadSource', () => {
        service.carouselImageToLoad = new Image();
        service.imageToLoadSource = 'http://source/';
        service.updateImageOnLoad();
        expect(service.carouselImageToLoad.src).toEqual(service.imageToLoadSource);
    });

    it('drawImage should set imageState value to complete', fakeAsync(() => {
        const initImageStateSpy: jasmine.Spy<any> = spyOn<any>(service, 'initImageState').and.callThrough();
        service.drawImage(new Image());
        tick(service.MESSAGE_DELAY + TEST_DURATION);
        expect(initImageStateSpy).toHaveBeenCalledWith(ImageState.Complete);
        expect(service.imageState.value).toEqual(ImageState.Idle);
    }));

    it('updateImageToDrawSize should keep image aspect ratio if width > height', () => {
        const naturalWidth = 200;
        const naturalHeight = 100;
        service.carouselImageToLoad = jasmine.createSpyObj('HTMLImageElement', [], {
            naturalWidth,
            naturalHeight,
        });
        const expectedSize: Vec2 = { x: 800, y: 400 };
        const canvasSize = 800;
        service.canvas.width = canvasSize;
        service.canvas.height = canvasSize;
        const size: Vec2 = service.updateImageToDrawSize();
        expect(size).toEqual(expectedSize);
    });

    it('updateImageToDrawSize should keep image aspect ratio if height > width', () => {
        const naturalWidth = 100;
        const naturalHeight = 200;
        service.carouselImageToLoad = jasmine.createSpyObj('HTMLImageElement', [], {
            naturalWidth,
            naturalHeight,
        });
        const expectedSize: Vec2 = { x: 400, y: 800 };
        const canvasSize = 800;
        service.canvas.width = canvasSize;
        service.canvas.height = canvasSize;
        const size: Vec2 = service.updateImageToDrawSize();
        expect(size).toEqual(expectedSize);
    });

    it('checkImageLoading should call onLoadingError if image not complete', () => {
        const onLoadindErrorSpy: jasmine.Spy<any> = spyOn<any>(service, 'onLoadingError');
        const testImage: HTMLImageElement = new Image();
        testImage.src = '/source';
        service.checkImageLoading(testImage);
        expect(onLoadindErrorSpy).toHaveBeenCalled();
    });

    it('checkImageLoading should set imageState too UsedCanvas if canvas not blank', () => {
        const imageSpy: jasmine.SpyObj<HTMLImageElement> = jasmine.createSpyObj('HTMLImageElement', ['removeEventListener'], { complete: true });
        const size = 1;
        service.baseCtx.fillRect(0, 0, size, size);
        service.checkImageLoading(imageSpy);
        expect(service.imageState.value).toEqual(ImageState.UsedCanvas);
    });

    it('checkImageLoading should drawImage if canvas is blank', () => {
        const imageSpy: jasmine.SpyObj<HTMLImageElement> = jasmine.createSpyObj('HTMLImageElement', ['removeEventListener'], { complete: true });
        const drawImageSpy: jasmine.Spy<any> = spyOn<any>(service, 'drawImage');
        service.clearCanvas(service.baseCtx);
        service.checkImageLoading(imageSpy);
        expect(drawImageSpy).toHaveBeenCalledWith(imageSpy);
    });

    it('arePristine should return false if canvas of previewCtx is not pristine and canvas of baseCtx isPristine', () => {
        const canvas = document.createElement('canvas');
        service.previewCtx = canvas.getContext('2d') as CanvasRenderingContext2D;
        canvas.height = 1;
        canvas.width = 1;
        service.previewCtx.fillStyle = 'red';
        service.previewCtx.fillRect(0, 0, 1, 1);
        const previewCtxIsPristine: boolean = service.isPristine(service.previewCtx);
        expect(previewCtxIsPristine).toBeFalse();
        expect(service.arePristine()).toBeFalse();
    });

    it('arePristine should return false if canvas of baseCtx is not pristine and canvas of previewCtx isPristine', () => {
        const canvas = document.createElement('canvas');
        service.baseCtx = canvas.getContext('2d') as CanvasRenderingContext2D;
        canvas.height = 1;
        canvas.width = 1;
        service.baseCtx.fillStyle = 'red';
        service.baseCtx.fillRect(0, 0, 1, 1);
        const baseCtxIsPristine: boolean = service.isPristine(service.baseCtx);
        expect(baseCtxIsPristine).toBeFalse();
        expect(service.arePristine()).toBeFalse();
    });

    it('arePristine should return true if previewCtx and baseCtx are both pristine', () => {
        const canvas = document.createElement('canvas');
        service.previewCtx = canvas.getContext('2d') as CanvasRenderingContext2D;
        service.baseCtx = canvas.getContext('2d') as CanvasRenderingContext2D;
        const previewCtxIsPristine: boolean = service.isPristine(service.previewCtx);
        const baseCtxIsPristine: boolean = service.isPristine(service.baseCtx);
        expect(previewCtxIsPristine).toEqual(true);
        expect(baseCtxIsPristine).toEqual(true);
        expect(service.arePristine()).toEqual(true);
    });

    it('autoSave should really save drawing', () => {
        localStorage.clear();
        service.autoSave();
        expect(localStorage.length).toBeGreaterThan(0);
    });

    it('drawSavedDrawing should draw Image', () => {
        const imgTest = new Image();
        const spy = spyOn(service.baseCtx, 'drawImage');
        service.drawSavedDrawing(imgTest);
        expect(spy).toHaveBeenCalled();
    });
    it('loadSavedDrawing should load Image', () => {
        const imgTest = new Image(EXPORT_MAX_WIDTH, EXPORT_MAX_HEIGHT);
        let spy: jasmine.Spy;
        service.loadSavedDrawing(imgTest.src, imgTest.width, imgTest.height);
        spy = spyOn<any>(service, 'drawSavedDrawing');
        imgTest.dispatchEvent(new Event('load'));
        expect(spy).toBeTruthy();
    });
});
