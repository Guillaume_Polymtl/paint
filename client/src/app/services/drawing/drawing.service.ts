import { EventEmitter, Injectable, Output } from '@angular/core';
import { Router } from '@angular/router';
import { BLACK } from '@app/classes/colors/color';
import { ImageState } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class DrawingService {
    readonly MAX_IMAGE_LOADING_DELAY: number = 500; // ms // currently simply image_load...
    readonly MESSAGE_DELAY: number = 5000;
    @Output() resetDrawing: EventEmitter<boolean> = new EventEmitter<false>();

    baseCtx: CanvasRenderingContext2D;
    previewCtx: CanvasRenderingContext2D;
    grid: CanvasRenderingContext2D;
    canvas: HTMLCanvasElement;

    isLoaded: boolean;
    carouselImageToLoad: HTMLImageElement;
    imageToLoadSource: string;
    imageState: BehaviorSubject<ImageState>;
    carouselImageToDraw: HTMLImageElement;

    constructor(private router: Router) {
        this.isLoaded = false;
        this.imageState = new BehaviorSubject<ImageState>(ImageState.Idle);
    }

    loadSavedDrawing(source: string, width: number, height: number): void {
        const img = new Image(width, height);
        img.addEventListener('load', this.drawSavedDrawing.bind(this, img));
        img.src = source;
    }

    drawSavedDrawing(img: HTMLImageElement): void {
        this.baseCtx.drawImage(img, 0, 0);
    }

    clearCanvas(context: CanvasRenderingContext2D): void {
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        // Reset attributes
        context.setLineDash([0]);
        context.strokeStyle = BLACK.toString();
    }

    autoSave(): void {
        localStorage.setItem('data', this.baseCtx.canvas.toDataURL());
        localStorage.setItem('width', this.baseCtx.canvas.width.toString());
        localStorage.setItem('height', this.baseCtx.canvas.height.toString());
    }

    initImageState(previousState: ImageState): void {
        if (this.imageState.value === previousState) {
            this.imageState.next(ImageState.Idle);
        }
    }

    checkImageLoading(image: HTMLImageElement): void {
        this.imageState.next(ImageState.Idle);
        image.removeEventListener('load', this.updateImageToDrawSize.bind(this));
        if (image.complete) {
            // this.dialogRef.close();
            this.carouselImageToDraw = image;
            if (this.isPristine(this.baseCtx)) {
                this.drawImage(image);
            } else {
                this.imageState.next(ImageState.UsedCanvas);
            }
        } else {
            this.onLoadingError();
        }
    }

    loadImage(imageSource: string): void {
        this.imageToLoadSource = imageSource;
        this.router.navigateByUrl('/editor');
        this.carouselImageToLoad = new Image();
        setTimeout(this.checkImageLoading.bind(this), this.MAX_IMAGE_LOADING_DELAY, this.carouselImageToLoad);
        // TODO: Revoir code.
        if (this.isLoaded) this.updateImageOnLoad();
    }

    updateImageOnLoad(): void {
        if (this.carouselImageToLoad) {
            this.carouselImageToLoad.onerror = this.onLoadingError.bind(this);
            this.carouselImageToLoad.addEventListener('load', this.updateImageToDrawSize.bind(this));
            this.carouselImageToLoad.src = this.imageToLoadSource;
        }
    }

    updateImageToDrawSize(): Vec2 {
        // Mise à l'échelle de l'image en gardant le aspect ratio
        const aspectRatio: number = this.carouselImageToLoad.naturalWidth / this.carouselImageToLoad.naturalHeight;

        if (this.carouselImageToLoad.naturalWidth > this.carouselImageToLoad.naturalHeight) {
            this.carouselImageToLoad.width = this.canvas.width;
            this.carouselImageToLoad.height = this.carouselImageToLoad.width / aspectRatio;
        } else {
            this.carouselImageToLoad.height = this.canvas.height;
            this.carouselImageToLoad.width = this.canvas.height * aspectRatio;
        }
        return { x: this.carouselImageToLoad.width, y: this.carouselImageToLoad.height };
    }

    cancelImageDrawing(): void {
        this.imageState.next(ImageState.Idle);
    }

    confirmImageDrawing(): void {
        this.drawImage(this.carouselImageToDraw);
    }

    onLoadingError(): void {
        this.imageState.next(ImageState.LoadingError);
        setTimeout(() => {
            this.initImageState(ImageState.LoadingError);
        }, this.MESSAGE_DELAY);
    }

    drawImage(image: HTMLImageElement): void {
        this.clearCanvas(this.previewCtx);
        this.clearCanvas(this.baseCtx);
        this.baseCtx.drawImage(image, 0, 0, image.width, image.height);
        this.imageState.next(ImageState.Complete);
        setTimeout(() => {
            this.initImageState(ImageState.Complete);
        }, this.MESSAGE_DELAY);
    }

    isPristine(context: CanvasRenderingContext2D): boolean {
        const blank = document.createElement('canvas');
        blank.width = context.canvas.width;
        blank.height = context.canvas.height;
        if (context.canvas.toDataURL() === blank.toDataURL()) {
            return true;
        } else {
            return false;
        }
    }

    arePristine(): boolean {
        return this.isPristine(this.previewCtx) && this.isPristine(this.baseCtx);
    }
}
