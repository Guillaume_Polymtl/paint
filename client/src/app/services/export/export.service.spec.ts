import { HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ElementRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { EXPORT_MAX_HEIGHT, EXPORT_MAX_WIDTH } from '@app/classes/export-constants';
import { Filters } from '@app/classes/filters';
import { Formats } from '@app/classes/formats';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { HttpStatus } from '@common/communication/http-status';
import { Observable, throwError } from 'rxjs';
import { ExportService } from './export.service';

// tslint:disable: no-string-literal
// tslint:disable: no-any
// tslint:disable: no-magic-numbers
describe('ExportService', () => {
    let service: ExportService;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;

    beforeEach(() => {
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas']);
        drawServiceSpy.canvas = CanvasTestHelper.canvas();
        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawServiceSpy }],
            imports: [HttpClientTestingModule, MatSnackBarModule],
        });
        service = TestBed.inject(ExportService);
        service.image = new Image();
        service.downloadedImage = {
            nativeElement: ({
                setAttribute: () => 'href',
            } as unknown) as ElementRef,
        };
        service.canvas = CanvasTestHelper.canvas();
        service.previewCanvas = CanvasTestHelper.canvas();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should export image with PNG filter', () => {
        const spy = spyOn(service['downloadedImage'].nativeElement, 'setAttribute');
        service['currentFormat'].next(Formats.PNG);
        service['currentFilter'].next(Filters.Inversion);
        const spy2 = spyOn(service['previewCanvas'], 'toDataURL').and.callFake(() => {
            return 'http://data/';
        });
        service.export('title');
        expect(spy.calls.allArgs()).toEqual([
            ['href', 'http://data/'],
            ['download', 'title.PNG'],
        ]);
        expect(spy).toHaveBeenCalledTimes(2);
        expect(spy2).toHaveBeenCalled();
    });

    it('should export image with JPEG filter', () => {
        const spy = spyOn(service['downloadedImage'].nativeElement, 'setAttribute');
        service['currentFormat'].next(Formats.JPEG);
        service['currentFilter'].next(Filters.Inversion);
        const spy2 = spyOn(service['previewCanvas'], 'toDataURL').and.callFake(() => {
            return 'http://data/';
        });
        service.export('title');
        expect(spy.calls.allArgs()).toEqual([
            ['href', 'http://data/'],
            ['download', 'title.JPEG'],
        ]);
        expect(spy).toHaveBeenCalledTimes(2);
        expect(spy2).toHaveBeenCalled();
    });

    it(' should not export image without filter', () => {
        const spy = spyOn(service['downloadedImage'].nativeElement, 'setAttribute');
        service['currentFormat'].next(Formats.JPEG);
        service['currentFilter'].next((undefined as unknown) as Filters);
        const spy2 = spyOn(service['previewCanvas'], 'toDataURL').and.callFake(() => {
            return 'http://data/';
        });
        service.export('title');
        expect(spy.calls.allArgs()).toEqual([
            ['href', 'http://data/'],
            ['download', 'title.JPEG'],
        ]);
        expect(spy).toHaveBeenCalledTimes(2);
        expect(spy2).toHaveBeenCalled();
    });

    it('should exportByEmail an image with JPEG filter', () => {
        const title = 'test';
        const email = 'test@email.com';
        service['currentFormat'].next(Formats.JPEG);
        service['currentFilter'].next(Filters.Inversion);
        const spy2 = spyOn(service, 'postByEmail').and.callFake(() => new Observable<string>());
        service.exportByEmail(title, email);
        expect(spy2).toHaveBeenCalled();
    });

    it('should handle error when exporting', () => {
        const title = 'test';
        const email = 'test@email.com';
        spyOn(service, 'postByEmail').and.callFake(() => throwError({ status: HttpStatus.BAD_REQUEST }));
        const spy2 = spyOn(service, 'handleError');
        service.exportByEmail(title, email);
        expect(spy2).toHaveBeenCalled();
    });

    it('postByEmail should post valid Image', () => {
        const spy = spyOn(service['http'], 'post');
        const title = 'test';
        const email = 'test@email.com';
        const dataUrl = 'data:image/png;base64,...';
        const ext = 'jpeg';
        service.postByEmail(title, email, dataUrl, ext);
        expect(spy).toHaveBeenCalled();
    });

    it('should preview element isScaleCalled first time', () => {
        service.previewCanvas.height = EXPORT_MAX_HEIGHT;
        service.previewCanvas.width = EXPORT_MAX_WIDTH;
        service['currentFilter'].next(Filters.Aucun);
        const spy = spyOn<any>(service.canvas.getContext('2d') as CanvasRenderingContext2D, 'scale');
        service.drawPreview(false);
        expect(spy).not.toHaveBeenCalled();
    });

    it('should create an image and a canvas when canvas2Image is called', () => {
        let spy: jasmine.Spy;
        service.canvas2Image();
        spy = spyOn<any>(service, 'drawPreview');
        service['image'].dispatchEvent(new Event('load'));
        expect(spy).toBeTruthy();
    });

    it('validateTitle should set valid title', () => {
        service.validateTitle('imagetest');
        expect(service['isTitleValid'].value).toEqual(true);
    });

    it('validateTitle should not set invalid title', () => {
        service.validateTitle('2');
        expect(service['isTitleValid'].value).toEqual(false);
    });

    it('validateEmail should set valid email', () => {
        service.validateEmail('test@test.com');
        expect(service['isEmailValid'].value).toEqual(true);
    });

    it('validateTitle should not set invalid email', () => {
        service.validateTitle('2');
        expect(service['isEmailValid'].value).toEqual(false);
    });

    it('handleError should respond BAD_REQUEST because of invalid email ', () => {
        const spy = spyOn(service['snack'], 'open');
        const error = new HttpErrorResponse({ status: HttpStatus.BAD_REQUEST });
        service.handleError(error);
        expect(spy).toHaveBeenCalled();
    });

    it('handleError should respond FORBIDDEN if API key is missing', () => {
        const spy = spyOn(service['snack'], 'open');
        const error = new HttpErrorResponse({ status: HttpStatus.FORBIDDEN });
        service.handleError(error);
        expect(spy).toHaveBeenCalled();
    });

    it('handleError should respond UNPROCESSABLE because of invalid payload ', () => {
        const spy = spyOn(service['snack'], 'open');
        const error = new HttpErrorResponse({ status: HttpStatus.UNPROCESSABLE });
        service.handleError(error);
        expect(spy).toHaveBeenCalled();
    });

    it('handleError should respond TOO MANY email sended', () => {
        const spy = spyOn(service['snack'], 'open');
        const error = new HttpErrorResponse({ status: HttpStatus.TOO_MANY });
        service.handleError(error);
        expect(spy).toHaveBeenCalled();
    });

    it('handleError should respond an INTERNAL ERROR', () => {
        const spy = spyOn(service['snack'], 'open');
        const error = new HttpErrorResponse({ status: HttpStatus.INTERNAL_ERROR });
        service.handleError(error);
        expect(spy).toHaveBeenCalled();
    });

    it('handleError should respond a NOT FOUND error', () => {
        const spy = spyOn(service['snack'], 'open');
        const error = new HttpErrorResponse({ status: HttpStatus.NOT_FOUND });
        service.handleError(error);
        expect(spy).toHaveBeenCalled();
    });
});
