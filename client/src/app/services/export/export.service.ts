import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ElementRef, Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EXPORT_MAX_HEIGHT, EXPORT_MAX_WIDTH } from '@app/classes/export-constants';
import { ExportModes } from '@app/classes/export-modes';
import { Filters } from '@app/classes/filters';
import { Formats } from '@app/classes/formats';
import { REGEX_EMAIL, REGEX_TITLE } from '@app/classes/regex';
import { EMAIL_URL } from '@app/classes/server/backend';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { HttpStatus } from '@common/communication/http-status';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ExportService {
    canvas: HTMLCanvasElement;
    previewCanvas: HTMLCanvasElement;
    downloadedImage: ElementRef;
    currentFormat: BehaviorSubject<Formats>;
    currentFilter: BehaviorSubject<Filters>;
    currentExportMode: BehaviorSubject<ExportModes>;
    isTitleValid: BehaviorSubject<boolean>;
    isEmailValid: BehaviorSubject<boolean>;
    image: HTMLImageElement;

    private filtersMap: Map<Filters, string>;

    constructor(private drawingService: DrawingService, private http: HttpClient, private snack: MatSnackBar) {
        this.currentFilter = new BehaviorSubject<Filters>(Filters.Aucun);
        this.currentFormat = new BehaviorSubject<Formats>(Formats.JPEG);
        this.currentExportMode = new BehaviorSubject<ExportModes>(ExportModes.Téléchargement);
        this.isTitleValid = new BehaviorSubject<boolean>(false);
        this.isEmailValid = new BehaviorSubject<boolean>(false);
        this.initializeMap();
    }

    private checkCorrectFormat(): void {
        if (this.currentFormat.getValue() === Formats.JPEG) {
            this.image.src = this.previewCanvas.toDataURL('image/jpeg');
        }
        if (this.currentFormat.getValue() === Formats.PNG) {
            this.image.src = this.previewCanvas.toDataURL('image/png');
        }
    }

    export(imgTitle: string): void {
        const ctx = this.previewCanvas.getContext('2d') as CanvasRenderingContext2D;
        this.applyFilter(ctx);
        ctx.drawImage(this.image, 0, 0);
        this.checkCorrectFormat();
        const finalString = imgTitle + '.' + this.currentFormat.getValue().toString();
        this.downloadedImage.nativeElement.setAttribute('href', this.image.src);
        this.downloadedImage.nativeElement.setAttribute('download', finalString);
    }

    exportByEmail(imgTitle: string, email: string): void {
        const ctx = this.previewCanvas.getContext('2d') as CanvasRenderingContext2D;
        this.applyFilter(ctx);
        ctx.drawImage(this.image, 0, 0);
        this.checkCorrectFormat();
        const extension = this.currentFormat.getValue().toString();
        this.postByEmail(imgTitle, email, this.image.src, extension).subscribe(
            () => {
                this.snack.open('Courriel envoyé avec succès', '', { duration: 3000 });
            },
            (error: HttpErrorResponse) => {
                this.handleError(error);
            },
        );
    }

    handleError(error: HttpErrorResponse): void {
        switch (error.status) {
            case HttpStatus.BAD_REQUEST:
                this.snack.open('Adresse courriel invalide ou manquante', '', { duration: 2000 });
                break;
            case HttpStatus.FORBIDDEN:
                this.snack.open('Clé API manquante ou invalide', '', { duration: 2000 });
                break;
            case HttpStatus.UNPROCESSABLE:
                this.snack.open('Adresse courriel ou fichier joint manquant', '', { duration: 2000 });
                break;
            case HttpStatus.TOO_MANY:
                this.snack.open("Quota d'envoi de courriels dépassé", '', { duration: 2000 });
                break;
            case HttpStatus.INTERNAL_ERROR:
                this.snack.open("Erreur interne au serveur d'envoi", '', { duration: 2000 });
                break;
            default:
                this.snack.open('Erreur de communication avec le serveur', '', { duration: 2000 });
                break;
        }
    }

    postByEmail(imgTitle: string, email: string, imgSrc: string, ext: string): Observable<string> {
        return this.http.post(
            EMAIL_URL,
            {
                to: email,
                payload: imgSrc,
                extension: ext,
                title: imgTitle,
            },
            { responseType: 'text' },
        );
    }

    private applyFilter(ctx: CanvasRenderingContext2D): void {
        const tempFilter = this.filtersMap.get(this.currentFilter.getValue());
        if (tempFilter !== undefined) {
            ctx.filter = tempFilter;
        }
    }

    drawPreview(isScaleCalled: boolean): void {
        const contextBinded = this.canvas.getContext('2d') as CanvasRenderingContext2D;
        contextBinded.clearRect(0, 0, contextBinded.canvas.width, contextBinded.canvas.height);
        this.applyFilter(contextBinded);
        let scaleX = EXPORT_MAX_WIDTH / this.previewCanvas.width;
        let scaleY = EXPORT_MAX_HEIGHT / this.previewCanvas.height / 2;
        if (scaleX > 1) {
            scaleX = 1;
        }
        if (scaleY > 1) {
            scaleY = 1;
        }
        if (isScaleCalled) {
            contextBinded.scale(scaleX, scaleY);
        }
        contextBinded.drawImage(this.image, 0, 0);
    }
    private initializeMap(): void {
        this.filtersMap = new Map();
        this.filtersMap.set(Filters.Aucun, 'none');
        this.filtersMap.set(Filters.Constraste, 'contrast(0.3)');
        this.filtersMap.set(Filters.Rotation, 'hue-rotate(90deg)');
        this.filtersMap.set(Filters.Inversion, 'invert(100)');
        this.filtersMap.set(Filters.Sepia, 'sepia(100)');
        this.filtersMap.set(Filters.GrayScale, 'grayscale(100)');
    }

    canvas2Image(): void {
        this.image = new Image();
        this.image.addEventListener('load', this.processAndDraw.bind(this));
        this.image.src = this.drawingService.canvas.toDataURL();
    }

    processAndDraw(): void {
        this.previewCanvas.width = this.image.width;
        this.previewCanvas.height = this.image.height;
        this.drawPreview(true);
    }

    validateTitle(title: string): void {
        this.isTitleValid.next(REGEX_TITLE.test(title));
    }
    validateEmail(email: string): void {
        this.isEmailValid.next(REGEX_EMAIL.test(email));
    }
}
