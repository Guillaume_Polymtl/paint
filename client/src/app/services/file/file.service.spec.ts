import { TestBed } from '@angular/core/testing';
import { FileAction } from '@app/classes/shortcuts/file-action';

import { FileService } from './file.service';

describe('FileService', () => {
    let service: FileService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(FileService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should emit a new action from setAction', () => {
        const spy = spyOn(service.action, 'next');
        service.setAction(FileAction.CREATE);
        expect(spy).toHaveBeenCalled();
    });
});
