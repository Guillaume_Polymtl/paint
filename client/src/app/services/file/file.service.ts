import { Injectable, Output } from '@angular/core';
import { FileAction } from '@app/classes/shortcuts/file-action';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class FileService {
    @Output() action: BehaviorSubject<FileAction> = new BehaviorSubject(FileAction.NONE);

    setAction(action: FileAction): void {
        this.action.next(action);
    }

    getAction(): Observable<FileAction> {
        return this.action;
    }
}
