import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpStatus } from '@common/communication/http-status';
import { SavingService } from './saving.service';

// tslint:disable: no-string-literal
// tslint:disable: no-magic-numbers

describe('SavingService', () => {
    let service: SavingService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule, MatSnackBarModule],
        });
        service = TestBed.inject(SavingService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
    it('validateTitle should set valid title', () => {
        service.validateTitle('imagetest');
        expect(service['isTitleValid'].value).toEqual(true);
    });

    it('validateTitle should not set invalid title', () => {
        service.validateTitle('#2');
        expect(service['isTitleValid'].value).toEqual(false);
    });

    it('addTag should add valid tag', () => {
        const stringSet = new Set<string>();
        const tag = 'tag1';
        const ret = service.addTag(tag, stringSet);
        expect(stringSet.has(tag.toUpperCase())).toEqual(true);
        expect(ret).toEqual(true);
    });

    it('addTag should not add invalid tag', () => {
        const stringSet = new Set<string>();
        const ret = service.addTag('###', stringSet);
        expect(stringSet.has('###')).toEqual(false);
        expect(ret).toEqual(false);
    });

    it('removeTag should remove tag', () => {
        const stringSet = new Set<string>();
        stringSet.add('tag');
        expect(stringSet.has('tag')).toEqual(true);
        service.removeTag('tag', stringSet);
        expect(stringSet.has('tag')).toEqual(false);
    });

    it('handleError should return success message', () => {
        const spy = spyOn(service['snacks'], 'open');
        service.handleError(
            new HttpErrorResponse({
                status: HttpStatus.OK,
            }),
        );
        expect(spy).toHaveBeenCalledWith('Image correctement sauvegardé', '', { duration: 3000 });
    });

    it('handleError should return error message', () => {
        const spy = spyOn(service['snacks'], 'open');
        service.handleError(
            new HttpErrorResponse({
                status: HttpStatus.NOT_FOUND,
            }),
        );
        expect(spy).toHaveBeenCalledWith("Image n'a pas pu être sauvegardé ", '', { duration: 2000 });
    });

    it('postImage should post', () => {
        const spy = spyOn(service['http'], 'post');
        const tags = new Set<string>();
        tags.add('tag1');
        tags.add('tag2');
        service.postImage('imagetest', tags, 'src/img/', 1000, 800);
        expect(spy).toHaveBeenCalled();
    });
});
