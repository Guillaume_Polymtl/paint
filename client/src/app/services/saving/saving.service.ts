import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { REGEX_TAG, REGEX_TITLE } from '@app/classes/regex';
import { SAVE_URL } from '@app/classes/server/backend';
import { HttpStatus } from '@common/communication/http-status';
import { Image } from '@common/communication/image-data';
import { BehaviorSubject, Observable } from 'rxjs';

// tslint:disable: object-literal-shorthand
@Injectable({
    providedIn: 'root',
})
export class SavingService {
    isTitleValid: BehaviorSubject<boolean>;

    constructor(private http: HttpClient, private snacks: MatSnackBar) {
        this.isTitleValid = new BehaviorSubject<boolean>(false);
    }

    validateTitle(title: string): void {
        this.isTitleValid.next(REGEX_TITLE.test(title));
    }

    private checkTagValidity(tag: string): boolean {
        return REGEX_TAG.test(tag);
    }

    addTag(tag: string, data: Set<string>): boolean {
        if (this.checkTagValidity(tag)) {
            data.add(tag.toUpperCase());
            return true;
        }
        return false;
    }

    removeTag(tag: string, data: Set<string>): void {
        data.delete(tag);
    }

    handleError(error: HttpErrorResponse): void {
        if (error.status === HttpStatus.OK) {
            this.snacks.open('Image correctement sauvegardé', '', { duration: 3000 });
        } else {
            this.snacks.open("Image n'a pas pu être sauvegardé ", '', { duration: 2000 });
        }
    }

    postImage(title: string, tagsSet: Set<string>, imgSrc: string, width: number, height: number): Observable<Image> {
        let tags: string[];
        tags = [];
        tagsSet.forEach((elt) => {
            tags.push(elt);
        });
        return this.http.post<Image>(SAVE_URL, {
            title,
            tags,
            src: imgSrc,
            width: width,
            height: height,
        });
    }
}
