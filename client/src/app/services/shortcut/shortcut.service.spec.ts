import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FileAction } from '@app/classes/shortcuts/file-action';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { DialogService } from '@app/services/dialog/dialog.service';
import { FileService } from '@app/services/file/file.service';
import { ToolsService } from '@app/services/tools/tools.service';
import { FeatherPenService } from '@app/tools/feather-pen/feather-pen.service';
import { GridService } from '@app/tools/grid/grid.service';
import { MagnetService } from '@app/tools/selection/magnet/magnet.service';
import { StampService } from '@app/tools/stamp/stamp.service';
import { ShortcutService } from './shortcut.service';

describe('ShortcutService', () => {
    let service: ShortcutService;
    let fileServiceSpy: jasmine.SpyObj<FileService>;
    let toolsServiceSpy: jasmine.SpyObj<ToolsService>;
    let dialogServiceSpy: jasmine.SpyObj<DialogService>;
    let gridServiceSpy: jasmine.SpyObj<GridService>;
    let magnetServiceSpy: jasmine.SpyObj<MagnetService>;
    let featherPenServiceSpy: jasmine.SpyObj<FeatherPenService>;
    let stampServiceSpy: jasmine.SpyObj<StampService>;

    beforeEach(() => {
        fileServiceSpy = jasmine.createSpyObj('FileService', ['setAction']);
        toolsServiceSpy = jasmine.createSpyObj('ToolsService', ['setTool', 'blockShortcut']);
        dialogServiceSpy = jasmine.createSpyObj('DialogService', ['openCarousel', 'openExportPanel', 'openSavePanel', 'isDialogOpened']);
        dialogServiceSpy.isDialogOpened.and.returnValue(false);
        gridServiceSpy = jasmine.createSpyObj('GridService', ['inverseViewMode', 'incrementThickness', 'decrementThickness']);
        featherPenServiceSpy = jasmine.createSpyObj('FeatherPenService', ['setAngle']);
        stampServiceSpy = jasmine.createSpyObj('StampService', ['setAngle']);

        magnetServiceSpy = jasmine.createSpyObj('MagnetService', ['inverseMode']);
        TestBed.configureTestingModule({});
        TestBed.configureTestingModule({
            providers: [
                { provide: FileService, useValue: fileServiceSpy },
                { provide: ToolsService, useValue: toolsServiceSpy },
                { provide: DialogService, useValue: dialogServiceSpy },
                { provide: GridService, useValue: gridServiceSpy },
                { provide: MagnetService, useValue: magnetServiceSpy },
                { provide: FeatherPenService, useValue: featherPenServiceSpy },
                { provide: StampService, useValue: stampServiceSpy },
            ],
            imports: [RouterTestingModule],
        }).compileComponents();
        service = TestBed.inject(ShortcutService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should call fileService.setAction with the proper action', () => {
        const event = new KeyboardEvent('keydown', { key: 'o', ctrlKey: true });
        service.handleKeyDown(event);
        expect(fileServiceSpy.setAction).toHaveBeenCalledWith(FileAction.CREATE);
    });

    it('should open Carousel if FileAction for Carousel', () => {
        const event = new KeyboardEvent('keydown', { key: 'g', ctrlKey: true });
        service.handleKeyDown(event);
        expect(dialogServiceSpy.openCarousel).toHaveBeenCalled();
    });

    it('should open Export if FileAction for Export Panel and path is editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(true);
        const event = new KeyboardEvent('keydown', { key: 'e', ctrlKey: true });
        service.handleKeyDown(event);
        expect(dialogServiceSpy.openExportPanel).toHaveBeenCalled();
    });

    it('should not open Export if FileAction for Export Panel and path is not editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(false);
        const event = new KeyboardEvent('keydown', { key: 'e', ctrlKey: true });
        service.handleKeyDown(event);
        expect(dialogServiceSpy.openExportPanel).not.toHaveBeenCalled();
    });

    it('should open Save if FileAction for Save Panel and path is editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(true);
        const event = new KeyboardEvent('keydown', { key: 's', ctrlKey: true });
        service.handleKeyDown(event);
        expect(dialogServiceSpy.openSavePanel).toHaveBeenCalled();
    });

    it('should not open Save if FileAction for Save Panel and path is not editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(false);
        const event = new KeyboardEvent('keydown', { key: 's', ctrlKey: true });
        service.handleKeyDown(event);
        expect(dialogServiceSpy.openSavePanel).not.toHaveBeenCalled();
    });

    it('should call toolsService.setTool with the proper tool if path is editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(true);
        const event = new KeyboardEvent('keydown', { key: 'e' });
        service.handleKeyDown(event);
        expect(toolsServiceSpy.setTool).toHaveBeenCalledWith(ToolName.ERASER);
    });

    it('should not call toolsService.setTool if path is not editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(false);
        const event = new KeyboardEvent('keydown', { key: 'e' });
        service.handleKeyDown(event);
        expect(toolsServiceSpy.setTool).not.toHaveBeenCalled();
    });

    it('should not inverse grid viewMode if g key is pressed and path is not editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(false);
        const event = new KeyboardEvent('keydown', { key: 'g' });
        service.handleKeyDown(event);
        expect(gridServiceSpy.inverseViewMode).not.toHaveBeenCalled();
    });

    it('should inverse grid viewMode if g key is pressed and path is editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(true);
        const event = new KeyboardEvent('keydown', { key: 'g' });
        service.handleKeyDown(event);
        expect(gridServiceSpy.inverseViewMode).toHaveBeenCalled();
    });

    it('should increment grid thickness if + key is pressed and path is editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(true);
        const event = new KeyboardEvent('keydown', { key: '+' });
        service.handleKeyDown(event);
        expect(gridServiceSpy.incrementThickness).toHaveBeenCalled();
    });

    it('should decrement grid thickness if - key is pressed and path is editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(true);
        const event = new KeyboardEvent('keydown', { key: '-' });
        service.handleKeyDown(event);
        expect(gridServiceSpy.decrementThickness).toHaveBeenCalled();
    });

    it('should not do anything if the key is not a shortcut', () => {
        spyOn(service, 'isEditorPath').and.returnValue(true);
        const event = new KeyboardEvent('keydown', { key: 'x' });
        service.handleKeyDown(event);
        expect(toolsServiceSpy.setTool).not.toHaveBeenCalled();
        expect(fileServiceSpy.setAction).not.toHaveBeenCalled();
    });

    it('should not process shortcut if dialog opened', () => {
        dialogServiceSpy.isDialogOpened.and.returnValue(true);
        const event = new KeyboardEvent('keydown', { key: 'e' });
        service.handleKeyDown(event);
        expect(toolsServiceSpy.setTool).not.toHaveBeenCalled();
    });

    it('should process shortcut if dialog not opened and path is editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(true);
        const event = new KeyboardEvent('keydown', { key: 'e' });
        service.handleKeyDown(event);
        expect(toolsServiceSpy.setTool).toHaveBeenCalledWith(ToolName.ERASER);
    });

    it('should not inverse magnet activeMode if m key is pressed and path is not editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(false);
        const event = new KeyboardEvent('keydown', { key: 'g' });
        service.handleKeyDown(event);
        expect(magnetServiceSpy.inverseMode).not.toHaveBeenCalled();
    });

    it('should inverse magnet activeMode if m key is pressed and path is editor', () => {
        spyOn(service, 'isEditorPath').and.returnValue(true);
        const event = new KeyboardEvent('keydown', { key: 'm' });
        service.handleKeyDown(event);
        expect(magnetServiceSpy.inverseMode).toHaveBeenCalled();
    });
});
