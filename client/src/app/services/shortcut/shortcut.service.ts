import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { FileAction } from '@app/classes/shortcuts/file-action';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { KeyValues } from '@app/classes/tools-utility';
import { DialogService } from '@app/services/dialog/dialog.service';
import { FileService } from '@app/services/file/file.service';
import { ToolsService } from '@app/services/tools/tools.service';
import { GridService } from '@app/tools/grid/grid.service';
import { MagnetService } from '@app/tools/selection/magnet/magnet.service';

@Injectable({
    providedIn: 'root',
})
export class ShortcutService {
    // Map from shortcut to a Tool
    tools: Map<string, ToolName> = new Map();
    // Map from shortcut to an Action
    fileActions: Map<string, FileAction> = new Map();

    constructor(
        private toolsService: ToolsService,
        private fileService: FileService,
        private dialogService: DialogService,
        private gridService: GridService,

        private location: Location,
        private magnetService: MagnetService,
    ) {
        this.init();
    }

    init(): void {
        this.tools = this.tools
            .set('e', ToolName.ERASER)
            .set('c', ToolName.PENCIL)
            .set('w', ToolName.BRUSH)
            .set('l', ToolName.LINE)
            .set('1', ToolName.RECTANGLE)
            .set('2', ToolName.ELLIPSE)
            .set('r', ToolName.SELECT_RECTANGLE)
            .set('s', ToolName.SELECT_ELLIPSE)
            .set('b', ToolName.BUCKET)
            .set('g', ToolName.GRID)
            .set('3', ToolName.POLYGON)
            .set('p', ToolName.FEATHER)
            .set('i', ToolName.EYE_DROPPER)
            .set('v', ToolName.MAGIC_WAND)
            .set('a', ToolName.SPRAYPAINT)
            .set('t', ToolName.TEXT)
            .set('d', ToolName.STAMP);
        // Add UPPERCASE for tools
        this.tools.forEach((name: ToolName, key: string) => {
            this.tools.set(key.toUpperCase(), name);
        });
        this.fileActions = this.fileActions
            .set('o', FileAction.CREATE)
            .set('s', FileAction.SAVE)
            .set('g', FileAction.CAROUSEL)
            .set('e', FileAction.EXPORT);
        // Add UPPERCASE for FileActions
        this.fileActions.forEach((action: FileAction, key: string) => {
            this.fileActions.set(key.toUpperCase(), action);
        });
    }

    isShortcutEnabled(): boolean {
        return !this.dialogService.isDialogOpened() && !this.toolsService.disableShortcuts;
    }

    isEditorPath(): boolean {
        return this.location.isCurrentPathEqualTo('/editor');
    }

    handleFileAction(fileAction: FileAction): void {
        this.fileService.setAction(fileAction);
        if (fileAction === FileAction.CAROUSEL) this.dialogService.openCarousel();
        if (this.isEditorPath() && fileAction === FileAction.EXPORT) this.dialogService.openExportPanel();
        if (this.isEditorPath() && fileAction === FileAction.SAVE) this.dialogService.openSavePanel();
    }

    handleKeyDown(event: KeyboardEvent): void {
        if (this.isShortcutEnabled()) {
            const fileAction = this.fileActions.get(event.key);
            if (event.ctrlKey && fileAction) {
                event.preventDefault();
                this.handleFileAction(fileAction);
                return;
            }
            if (this.isEditorPath() && !event.ctrlKey) {
                switch (event.key) {
                    case KeyValues.GridMode:
                        this.gridService.inverseViewMode();
                        break;
                    case KeyValues.MagnetMode:
                        this.magnetService.inverseMode();
                        break;
                    case KeyValues.Plus:
                        event.preventDefault();
                        this.gridService.incrementThickness();
                        break;
                    case KeyValues.Minus:
                        event.preventDefault();
                        this.gridService.decrementThickness();
                        break;
                    default:
                        const toolName = this.tools.get(event.key);
                        if (toolName) this.toolsService.setTool(toolName);
                        break;
                }
            }
        }
    }
}
