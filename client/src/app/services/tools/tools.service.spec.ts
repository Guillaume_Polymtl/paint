import { TestBed } from '@angular/core/testing';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { BrushService } from '@app/tools/brush/brush.service';
import { PencilService } from '@app/tools/pencil/pencil.service';
import { isObservable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToolsService } from './tools.service';

class ToolStub extends Tool {}

// tslint:disable: no-string-literal
describe('ToolsService', () => {
    let service: ToolsService;
    let pencilMock: PencilService;
    let brushMock: BrushService;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;

    beforeEach(() => {
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['isPristine', 'clearCanvas'], ['baseCtx, previewCtx, canvas']);
        TestBed.configureTestingModule({
            providers: [
                { provide: PencilService, BrushService },
                { provide: DrawingService, useValue: drawingServiceSpy },
            ],
        });
        service = TestBed.inject(ToolsService);
        pencilMock = TestBed.inject(PencilService);
        brushMock = TestBed.inject(BrushService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return an Observable<Tool> from getTool()', () => {
        expect(isObservable(service.getTool())).toBe(true);
    });

    it('should be pencilService by default', () => {
        const obs = service.getTool().pipe(map((tool) => tool));
        obs.subscribe((tool: Tool) => {
            expect(tool).toEqual(pencilMock);
        }).unsubscribe();
    });

    it('get blockShortcuts should return false by default', () => {
        expect(service.disableShortcuts).toBeFalse();
    });

    it('should change the tool if given a valid ToolName', () => {
        const obs = service.getTool().pipe(map((tool) => tool));
        service.setTool(ToolName.BRUSH);
        obs.subscribe((tool: Tool) => {
            expect(tool).toEqual(brushMock);
        }).unsubscribe();
    });

    it('should reset tool before changing', () => {
        const tool: ToolStub = new ToolStub(drawingServiceSpy, pencilMock['colorService']);
        service['tool'].next(tool);
        const resetSpy = spyOn(tool, 'onToolChange').and.callThrough();
        service.setTool(ToolName.BRUSH);
        expect(resetSpy).toHaveBeenCalled();
    });

    it('should not change tool if the map is empty', () => {
        const obs = service.getTool().pipe(map((tool) => tool));
        // set to Brush
        service.setTool(ToolName.BRUSH);
        // empty map
        service.tools = new Map();
        // set to pencil
        service.setTool(ToolName.PENCIL);
        obs.subscribe((tool: Tool) => {
            // did not actually change
            expect(tool).toEqual(brushMock);
        }).unsubscribe();
    });
});
