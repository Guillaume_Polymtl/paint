import { Injectable } from '@angular/core';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { BrushService } from '@app/tools/brush/brush.service';
import { BucketService } from '@app/tools/bucket/bucket.service';
import { EllipseService } from '@app/tools/ellipse/ellipse.service';
import { EraserService } from '@app/tools/eraser/eraser.service';
import { EyeDropperService } from '@app/tools/eye-dropper/eye-dropper.service';
import { FeatherPenService } from '@app/tools/feather-pen/feather-pen.service';
import { GridService } from '@app/tools/grid/grid.service';
import { LineService } from '@app/tools/line/line.service';
import { PencilService } from '@app/tools/pencil/pencil.service';
import { PolygonService } from '@app/tools/polygon/polygon.service';
import { RectangleService } from '@app/tools/rectangle/rectangle.service';
import { EllipseSelectionService } from '@app/tools/selection/ellipse-selection/ellipse-selection.service';
import { MagicWandService } from '@app/tools/selection/magic-wand/magic-wand.service';
import { RectangleSelectionService } from '@app/tools/selection/rectangle-selection/rectangle-selection.service';
import { SpraypaintService } from '@app/tools/spraypaint/spraypaint.service';
import { StampService } from '@app/tools/stamp/stamp.service';
import { TextService } from '@app/tools/text/text.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ToolsService {
    constructor(
        private pencilService: PencilService,
        private eraserService: EraserService,
        private brushService: BrushService,
        private lineService: LineService,
        private rectangleService: RectangleService,
        private ellipseService: EllipseService,
        private rectangleSelectionService: RectangleSelectionService,
        private ellipseSelectionService: EllipseSelectionService,
        private polygonService: PolygonService,
        private eyeDropperService: EyeDropperService,
        private bucketService: BucketService,
        private magicWandService: MagicWandService,
        private gridService: GridService,
        private spraypaintService: SpraypaintService,
        private textService: TextService,
        private stampService: StampService,
        private featherPenService: FeatherPenService,
    ) {
        this.init();
    }

    private privateDisableShortcuts: boolean = false;

    get disableShortcuts(): boolean {
        return this.privateDisableShortcuts;
    }

    // tools: BehaviorSubject<ToolButton[]> = new BehaviorSubject<ToolButton[]>(toolButtons);
    tools: Map<ToolName, Tool> = new Map();
    private tool: BehaviorSubject<Tool> = new BehaviorSubject<Tool>(this.pencilService);

    init(): void {
        this.textService.getDisableShortcuts().subscribe((value: boolean) => {
            this.privateDisableShortcuts = value;
        });

        this.tools = this.tools
            .set(ToolName.ERASER, this.eraserService)
            .set(ToolName.RECTANGLE, this.rectangleService)
            .set(ToolName.PENCIL, this.pencilService)
            .set(ToolName.BRUSH, this.brushService)
            .set(ToolName.LINE, this.lineService)
            .set(ToolName.ELLIPSE, this.ellipseService)
            .set(ToolName.SELECT_RECTANGLE, this.rectangleSelectionService)
            .set(ToolName.SELECT_ELLIPSE, this.ellipseSelectionService)
            .set(ToolName.POLYGON, this.polygonService)
            .set(ToolName.EYE_DROPPER, this.eyeDropperService)
            .set(ToolName.FEATHER, this.featherPenService)
            .set(ToolName.BUCKET, this.bucketService)
            .set(ToolName.MAGIC_WAND, this.magicWandService)
            .set(ToolName.GRID, this.gridService)
            .set(ToolName.SPRAYPAINT, this.spraypaintService)
            .set(ToolName.TEXT, this.textService)
            .set(ToolName.STAMP, this.stampService)
            .set(ToolName.BUCKET, this.bucketService);
    }

    getTool(): Observable<Tool> {
        return this.tool;
    }

    setTool(toolName: ToolName): void {
        const service = this.tools.get(toolName);
        if (service) {
            // Reset the previous tool before changing it
            this.tool.getValue().onToolChange();
            this.tool.next(service);
        }
    }
}
