import { Injectable } from '@angular/core';
import { Command } from '@app/classes/commands/command';
import { DrawingService } from '@app/services/drawing/drawing.service';

@Injectable({
    providedIn: 'root',
})
export class UndoRedoService {
    doneCommands: Command[] = []; // change detection and if undone not empty , clear it
    undoneCommands: Command[] = [];

    constructor(private drawingService: DrawingService) {}

    undoLastCommand(): void {
        const length = this.doneCommands.length;
        if (length > 1) {
            this.doneCommands[length - 1].undo();
            this.undoneCommands.push(this.doneCommands[length - 1]);
            this.doneCommands.pop();
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
        } else if (length === 1) {
            this.doneCommands.pop();
            this.drawingService.clearCanvas(this.drawingService.baseCtx);
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
        }
    }
}
