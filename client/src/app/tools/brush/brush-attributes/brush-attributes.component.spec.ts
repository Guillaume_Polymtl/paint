import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrushService } from '@app/tools/brush/brush.service';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';
import { TextureComponent } from '@app/tools/shared/components/texture/texture.component';
import { BrushAttributesComponent } from './brush-attributes.component';

describe('BrushAttributesComponent', () => {
    let component: BrushAttributesComponent;
    let fixture: ComponentFixture<BrushAttributesComponent>;
    let serviceSpy: jasmine.SpyObj<BrushService>;

    beforeEach(async(() => {
        serviceSpy = jasmine.createSpyObj('BrushService', ['setWidth', 'getWidth', 'setTexture', 'getTexture']);
        serviceSpy.setWidth.and.returnValue();
        // serviceSpy.setTexture.and.returnValue();
        TestBed.configureTestingModule({
            declarations: [BrushAttributesComponent, SliderComponent, TextureComponent],
            providers: [{ provide: BrushService, useValue: serviceSpy }],
            imports: [MatSliderModule, MatButtonToggleModule, MatTooltipModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BrushAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call service.setWidth()', () => {
        component.setWidth(0);
        expect(serviceSpy.setWidth).toHaveBeenCalledWith(0);
    });

    it('should call service.setTexture()', () => {
        component.setTexture(0);
        expect(serviceSpy.setTexture).toHaveBeenCalledWith(0);
    });
});
