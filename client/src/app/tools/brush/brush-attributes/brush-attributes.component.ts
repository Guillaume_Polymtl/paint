import { Component, OnInit } from '@angular/core';
import { BRUSH_MAX_WIDTH, BRUSH_MIN_WIDTH } from '@app/classes/tools-utility';
import { BrushService } from '@app/tools/brush/brush.service';

@Component({
    selector: 'app-brush-attributes',
    templateUrl: './brush-attributes.component.html',
    styleUrls: ['./brush-attributes.component.scss'],
})
export class BrushAttributesComponent implements OnInit {
    title: string = 'Pinceau';

    width: number;
    texture: number;

    readonly widthTitle: string = 'Épaisseur';

    readonly maxWidth: number = BRUSH_MAX_WIDTH;
    readonly minWidth: number = BRUSH_MIN_WIDTH;

    constructor(private brushService: BrushService) {}

    ngOnInit(): void {
        this.width = this.brushService.getWidth();
        this.texture = this.brushService.getTexture();
    }

    setWidth(width: number): void {
        this.brushService.setWidth(width);
    }

    setTexture(texture: number): void {
        this.brushService.setTexture(Number(texture));
    }
}
