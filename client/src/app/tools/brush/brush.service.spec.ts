import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { MouseButton } from '@app/classes/mouse-buttons';
import { BRUSH_MAX_WIDTH, BRUSH_MIN_WIDTH } from '@app/classes/tools-utility';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { BrushService } from './brush.service';

// tslint:disable:no-any
// tslint:disable:no-magic-numbers

describe('BrushService', () => {
    let service: BrushService;
    let mouseEvent: MouseEvent;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;
    let textureFunctionSpy: jasmine.Spy<any>;

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);
        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawServiceSpy }],
        });
        service = TestBed.inject(BrushService);
        textureFunctionSpy = spyOn<any>(service, 'textureFunction').and.callThrough();

        // Configuration du spy du service
        // tslint:disable:no-string-literal
        service['drawingService'].baseCtx = baseCtxStub; // Jasmine doesnt copy properties with underlying data
        service['drawingService'].previewCtx = previewCtxStub;
        service['pathData'].push({ x: 348, y: 139 });
        service['pathData'].push({ x: 345, y: 139 });
        service['pathData'].push({ x: 348, y: 146 });
        service['pathData'].push({ x: 345, y: 146 });
        service['pathData'].push({ x: 344, y: 160 });

        mouseEvent = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Left,
        } as MouseEvent;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('setWidth should change the width if correct', () => {
        const currentWidth = service.getWidth();
        const expectedWidth = BRUSH_MAX_WIDTH - 1;
        service.setWidth(expectedWidth);
        expect(service.getWidth()).not.toEqual(currentWidth);
        expect(service.getWidth()).toEqual(expectedWidth);
    });

    it('setWidth should set width to min if entry is smaller', () => {
        const width = BRUSH_MIN_WIDTH - 1;
        service.setWidth(width);
        expect(service.getWidth()).toEqual(BRUSH_MIN_WIDTH);
    });

    it('setWidth should set width to max if entry is greater', () => {
        const width = BRUSH_MAX_WIDTH + 1;
        service.setWidth(width);
        expect(service.getWidth()).toEqual(BRUSH_MAX_WIDTH);
    });

    it('should update texture if the texture to set is binded', () => {
        service.setTexture(2);
        expect(service.getTexture()).toEqual(2);
    });

    it('should not update texture if the texture to set is not binded', () => {
        const texture: number = service.getTexture();
        service.setTexture(0);
        expect(service.getTexture()).not.toEqual(0);
        expect(service.getTexture()).toEqual(texture);
    });

    it(' mouseDown should set mouseDown property to true on left click', () => {
        service.onMouseDown(mouseEvent);
        expect(service.mouseDown).toEqual(true);
    });

    it(' mouseDown should set mouseDown property to false on right click', () => {
        const mouseEventRClick = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;
        service.onMouseDown(mouseEventRClick);
        expect(service.mouseDown).toEqual(false);
    });

    it(' onMouseUp should call textureFunction if mouse was already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseUp(mouseEvent);
        expect(textureFunctionSpy).toHaveBeenCalled();
    });

    it(' onMouseUp should not call textureFunction if mouse was not already down', () => {
        service.mouseDown = false;
        service.mouseDownCoord = { x: 0, y: 0 };
        service.onMouseUp(mouseEvent);
        expect(textureFunctionSpy).not.toHaveBeenCalled();
    });

    it(' onMouseMove should call textureFunction if mouse was already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
        expect(textureFunctionSpy).toHaveBeenCalled();
    });

    it(' onMouseMove should not call textureFunction if mouse was not already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = false;
        service.onMouseMove(mouseEvent);
        expect(textureFunctionSpy).not.toHaveBeenCalled();
    });

    it('onMousemove should translate, rotate and stroke when line texture is 2', () => {
        service.setTexture(2);
        const rotateSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'rotate');
        const translateSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'translate');
        const strokeSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'stroke');
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(translateSpy).toHaveBeenCalled();
        expect(rotateSpy).toHaveBeenCalled();
        expect(strokeSpy).toHaveBeenCalled();
    });
    it('on Mouse move should moveTo lineTo and stroke when line texture is 3', () => {
        service.setTexture(3);
        const moveToSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'moveTo');
        const lineToSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'lineTo');
        const strokeSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'stroke');
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(moveToSpy).toHaveBeenCalled();
        expect(lineToSpy).toHaveBeenCalled();
        expect(strokeSpy).toHaveBeenCalled();
    });
    it('on Mouse move should moveTo lineTo and stroke when line texture is 3 testcase path.length < 1', () => {
        service.setTexture(3);
        service['pathData'].length = 0;
        const moveToSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'moveTo');
        const lineToSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'lineTo');
        const strokeSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'stroke');
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(moveToSpy).not.toHaveBeenCalled();
        expect(lineToSpy).not.toHaveBeenCalled();
        expect(strokeSpy).not.toHaveBeenCalled();
    });
    it('on Mouse move should fillRect 30 times when line texture is 4', () => {
        service.setTexture(4);
        const fillRectSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'fillRect');
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(fillRectSpy).toHaveBeenCalled();
    });

    it('on Mouse move should moveTo lineTo and stroke when line texture is 5', () => {
        service.setTexture(5);
        const fillRectSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'fillRect');
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(fillRectSpy).toHaveBeenCalled();
    });
    it('on Mouse move should moveTo lineTo and stroke when line texture is 5 testcase of path.length', () => {
        service.setTexture(5);
        service['pathData'].length = 0;
        const fillRectSpy: jasmine.Spy<any> = spyOn<any>(baseCtxStub, 'fillRect');
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(fillRectSpy).toHaveBeenCalled();
    });
});
