import { Injectable } from '@angular/core';
import { Color } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { BrushTextures, BRUSH_DEFAULT_WIDTH, BRUSH_MAX_WIDTH, BRUSH_MIN_WIDTH } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';

type TextureFunction = (ctx: CanvasRenderingContext2D, path: Vec2[]) => void;

// tslint:disable:no-magic-numbers
@Injectable({
    providedIn: 'root',
})
export class BrushService extends Tool {
    private pathData: Vec2[];
    readonly name: ToolName = ToolName.BRUSH;
    private width: number = BRUSH_DEFAULT_WIDTH;
    readonly MIN_WIDTH: number = 20;
    readonly MAX_WIDTH: number = 100;
    private textureFunction: TextureFunction;
    readonly textureBindings: Map<BrushTextures, TextureFunction> = new Map();

    constructor(drawingService: DrawingService, colorService: ColorService) {
        super(drawingService, colorService);
        this.clearPath();
        this.bindKeys();
        this.textureFunction = this.lineTexture1;
    }

    private bindKeys(): void {
        this.textureBindings
            .set(BrushTextures.Texture1, this.lineTexture1)
            .set(BrushTextures.Texture2, this.lineTexture2)
            .set(BrushTextures.Texture3, this.lineTexture3)
            .set(BrushTextures.Texture4, this.lineTexture4)
            .set(BrushTextures.Texture5, this.lineTexture5);
    }

    getTexture(): number {
        for (const [key, value] of this.textureBindings.entries()) {
            if (value === this.textureFunction) return key.valueOf();
        }
        return BrushTextures.Texture1.valueOf();
    }

    setTexture(texture: number): void {
        if (this.textureBindings.has(texture)) {
            const textureFunction: TextureFunction = this.textureBindings.get(texture) as TextureFunction;
            this.textureFunction = textureFunction;
        }
    }

    getWidth(): number {
        return this.width;
    }

    setWidth(width: number): void {
        if (width >= BRUSH_MIN_WIDTH && width <= BRUSH_MAX_WIDTH) {
            this.width = width;
        } else if (width < BRUSH_MIN_WIDTH) {
            this.width = BRUSH_MIN_WIDTH;
        } else {
            this.width = BRUSH_MAX_WIDTH;
        }
    }

    onMouseDown(event: MouseEvent): void {
        this.mouseDown = event.button === MouseButton.Left;
        if (this.mouseDown) {
            this.clearPath();

            this.mouseDownCoord = this.getPositionFromMouse(event);
            this.pathData.push(this.mouseDownCoord);
            this.drawingService.autoSave();
        }
    }

    onMouseUp(event: MouseEvent): void {
        if (this.mouseDown) {
            const mousePosition = this.getPositionFromMouse(event);
            this.pathData.push(mousePosition);
            this.textureFunction(this.drawingService.baseCtx, this.pathData);
        }
        this.mouseDown = false;
        this.clearPath();
    }

    onMouseMove(event: MouseEvent): void {
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        if (this.mouseDown) {
            const mousePosition = this.getPositionFromMouse(event);
            this.pathData.push(mousePosition);
            this.textureFunction(this.drawingService.baseCtx, this.pathData);
        }
    }

    private getRandomInt(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private distanceBetween(point1: Vec2, point2: Vec2): number {
        return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
    }

    private angleBetween(point1: Vec2, point2: Vec2): number {
        const angle1 = Math.atan2(point1.x, point1.y);
        const angle2 = Math.atan2(point2.x, point2.y);
        return angle2 - angle1;
    }

    // texture 1 Lissage des bords avec gradient radial
    private lineTexture1(ctx: CanvasRenderingContext2D, path: Vec2[]): void {
        const length: number = path.length;
        const radgrad = ctx.createRadialGradient(
            path[length - 1].x,
            path[length - 1].y,
            this.width / 4,
            path[length - 1].x,
            path[length - 1].y,
            this.width / 2,
        );
        const firstColorStop: Color = new Color(this.color.red, this.color.green, this.color.blue, 0.2);
        const secondColorStop: Color = new Color(this.color.red, this.color.green, this.color.blue, 0.1);
        const thirdColorStop: Color = new Color(this.color.red, this.color.green, this.color.blue, 0);
        radgrad.addColorStop(0, firstColorStop.toString());
        radgrad.addColorStop(0.5, secondColorStop.toString());
        radgrad.addColorStop(1, thirdColorStop.toString());
        ctx.fillStyle = radgrad;
        ctx.fillRect(path[length - 1].x - this.width / 2, path[length - 1].y - this.width / 2, this.width, this.width);
    }

    // texture 2 en forme d'étoile
    private lineTexture2(ctx: CanvasRenderingContext2D, path: Vec2[]): void {
        ctx.strokeStyle = this.color.toString();
        for (const point of path) {
            const length = this.width / 3;
            ctx.save();
            ctx.translate(point.x, point.y);
            ctx.beginPath();
            ctx.rotate((Math.PI * 1) / 10);
            for (let i = 5; i--; ) {
                ctx.lineTo(0, length);
                ctx.translate(0, length);
                ctx.rotate((Math.PI * 2) / 10);
                ctx.lineTo(0, -length);
                ctx.translate(0, -length);
                ctx.rotate(-((Math.PI * 6) / 10));
            }
            ctx.lineTo(0, length);
            ctx.closePath();
            ctx.stroke();
            ctx.restore();
        }
    }

    private lineTexture3(ctx: CanvasRenderingContext2D, path: Vec2[]): void {
        if (path.length > 1) {
            const length: number = path.length;
            const shadowBlurBackup: number = ctx.shadowBlur;
            const shadowColorBackup: string = ctx.shadowColor;
            ctx.shadowBlur = 10;
            ctx.shadowColor = ctx.strokeStyle = this.color.toString();
            ctx.lineWidth = this.width;
            ctx.beginPath();
            ctx.moveTo(path[length - 2].x, path[length - 2].y);
            ctx.lineTo(path[length - 1].x, path[length - 1].y);
            ctx.stroke();
            ctx.shadowBlur = shadowBlurBackup;
            ctx.shadowColor = shadowColorBackup;
        }
    }

    private lineTexture4(ctx: CanvasRenderingContext2D, path: Vec2[]): void {
        const density = 30;
        ctx.fillStyle = this.color.toString();
        ctx.lineJoin = 'round';
        for (let i = density; i >= 0; --i) {
            const angle = this.getRandomInt(0, Math.PI * 2);
            const radius = this.getRandomInt(0, this.width / 2);
            const x: number = path[path.length - 1].x + radius * Math.cos(angle);
            const y: number = path[path.length - 1].y + radius * Math.sin(angle);
            ctx.fillRect(x, y, 1, 1);
        }
    }

    private lineTexture5(ctx: CanvasRenderingContext2D, path: Vec2[]): void {
        const previousMousePosition: Vec2 = path.length < 2 ? this.mouseDownCoord : path[path.length - 2];
        const distance = this.distanceBetween(previousMousePosition, path[path.length - 1]);
        const angle = this.angleBetween(previousMousePosition, path[path.length - 1]);
        ctx.fillStyle = this.color.toString();

        for (let i = 0; i < distance; i += 5) {
            const x: number = previousMousePosition.x + Math.sin(angle) * i;
            const y: number = previousMousePosition.y + Math.cos(angle) * i;

            for (let j = 2 * this.width; j >= 0; --j) {
                const radius: number = this.width / 2;
                const r: number = radius * Math.sqrt(Math.random());
                const theta: number = Math.random() * 2 * Math.PI;

                const startPositionX: number = x + r * Math.cos(theta);
                const startPositionY: number = y + r * Math.sin(theta);

                ctx.fillRect(startPositionX, startPositionY, 1, 1);
            }
        }
    }

    private clearPath(): void {
        this.pathData = [];
    }
}
