import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSliderModule } from '@angular/material/slider';
import { BucketService } from '@app/tools/bucket/bucket.service';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';
import { of } from 'rxjs';

import { BucketAttributesComponent } from './bucket-attributes.component';

describe('BucketAttributesComponent', () => {
    let component: BucketAttributesComponent;
    let fixture: ComponentFixture<BucketAttributesComponent>;
    let bucketServiceSpy: jasmine.SpyObj<BucketService>;
    const newTolerance = 0;

    beforeEach(async(() => {
        bucketServiceSpy = jasmine.createSpyObj(BucketService, ['setTolerance', 'getTolerance']);
        bucketServiceSpy.getTolerance.and.returnValue(of(0));

        TestBed.configureTestingModule({
            declarations: [BucketAttributesComponent, SliderComponent],
            imports: [MatSliderModule],
            providers: [{ provide: BucketService, useValue: bucketServiceSpy }],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BucketAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call the bucketService.setTolerance() method on slider change', () => {
        component.onToleranceChange(newTolerance);
        expect(bucketServiceSpy.setTolerance).toHaveBeenCalledWith(newTolerance);
    });
});
