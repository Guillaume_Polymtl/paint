import { Component } from '@angular/core';
import { BucketService } from '@app/tools/bucket/bucket.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-bucket-attributes',
    templateUrl: './bucket-attributes.component.html',
    styleUrls: ['./bucket-attributes.component.scss'],
})
export class BucketAttributesComponent {
    readonly title: string = 'Sceau de peinture';
    readonly tolerance: string = 'Tolérance';
    tolerance$: Observable<number> = this.bucketService.getTolerance();

    constructor(public bucketService: BucketService) {}

    onToleranceChange(tolerance: number): void {
        this.bucketService.setTolerance(tolerance);
    }
}
