import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { BLUE, ORANGE, PURPLE, RED, WHITE } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { of } from 'rxjs';
import { BucketService } from './bucket.service';

describe('BucketService', () => {
    let service: BucketService;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;
    let colorServiceSpy: jasmine.SpyObj<ColorService>;
    const nStripes = 3;
    const mockContext = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
    const thirdOfTheWidth = Math.floor(mockContext.canvas.width / nStripes);

    const zone1 = {
        startX: 0,
        finishX: thirdOfTheWidth,
        middleX: thirdOfTheWidth / 2,
        startY: 0,
        finishY: mockContext.canvas.height,
        middleY: mockContext.canvas.height / 2,
    };

    const zone2 = {
        startX: thirdOfTheWidth + 1,
        finishX: thirdOfTheWidth * 2,
        middleX: thirdOfTheWidth + thirdOfTheWidth / 2,
        startY: 0,
        finishY: mockContext.canvas.height,
        middleY: mockContext.canvas.height / 2,
    };

    const zone3 = {
        startX: thirdOfTheWidth * 2 + 1,
        finishX: mockContext.canvas.width,
        middleX: 2 * thirdOfTheWidth + thirdOfTheWidth / 2,
        startY: 0,
        finishY: mockContext.canvas.height,
        middleY: mockContext.canvas.height / 2,
    };

    const zone1MiddleClick = new MouseEvent('click', {
        button: MouseButton.Left,
        clientX: zone1.middleX,
        clientY: zone1.middleY,
    });

    const zone1MiddleRightClick = new MouseEvent('click', {
        button: MouseButton.Right,
        clientX: zone1.middleX,
        clientY: zone1.middleY,
    });

    const zone2MiddleClick = new MouseEvent('click', {
        button: MouseButton.Left,
        clientX: zone2.middleX,
        clientY: zone2.middleY,
    });

    const zone3MiddleClick = new MouseEvent('click', {
        button: MouseButton.Left,
        clientX: zone3.middleX,
        clientY: zone3.middleY,
    });

    const zone2InvalidClick = new MouseEvent('click', {
        button: MouseButton.Middle,
        clientX: zone2.middleX,
        clientY: zone2.middleY,
    });

    beforeEach(() => {
        CanvasTestHelper.clearCanvas(mockContext);
        // Reset the canvas to a WHITE/BLACK/WHITE flag
        mockContext.fillStyle = BLUE.toString();
        mockContext.fillRect(0, 0, thirdOfTheWidth, mockContext.canvas.height);
        mockContext.fillStyle = WHITE.toString();
        mockContext.fillRect(thirdOfTheWidth + 1, 0, thirdOfTheWidth, mockContext.canvas.height);
        mockContext.fillStyle = RED.toString();
        mockContext.fillRect(2 * thirdOfTheWidth + 1, 0, thirdOfTheWidth, mockContext.canvas.height);

        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);
        drawServiceSpy.canvas = mockContext.canvas;
        drawServiceSpy.baseCtx = mockContext;
        colorServiceSpy = jasmine.createSpyObj('ColorService', ['getColor']);
        colorServiceSpy.getColor.and.returnValue(of(ORANGE));
        TestBed.configureTestingModule({
            providers: [
                { provide: DrawingService, useValue: drawServiceSpy },
                { provide: ColorService, useValue: colorServiceSpy },
            ],
        });
        service = TestBed.inject(BucketService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should set the tolerance', () => {
        // get the initial tolerance
        let initial = 0;
        service
            .getTolerance()
            .subscribe((value: number) => {
                initial = value;
            })
            .unsubscribe();
        // add one
        service.setTolerance(initial + 1);
        // make sure it was updated
        let final = 0;
        service
            .getTolerance()
            .subscribe((value: number) => {
                final = value;
            })
            .unsubscribe();
        expect(initial).not.toEqual(final);
    });

    it('should set the tolerance to max for an invalid max+1 value', () => {
        service.setTolerance(service.maxTolerance + 1);
        let tolerance = 0;
        service
            .getTolerance()
            .subscribe((value: number) => {
                tolerance = value;
            })
            .unsubscribe();
        expect(tolerance).toEqual(service.maxTolerance);
    });

    it('should set to tolerance for an invalid min-1 value', () => {
        service.setTolerance(service.minTolerance - 1);
        let tolerance = 0;
        service
            .getTolerance()
            .subscribe((value: number) => {
                tolerance = value;
            })
            .unsubscribe();
        expect(tolerance).toEqual(service.minTolerance);
    });

    it('should call the paint method onMouseDown', () => {
        spyOn(service, 'paint');
        let calls = 0;
        service.onMouseDown(zone1MiddleClick);
        calls++;
        service.onMouseDown(zone1MiddleRightClick);
        calls++;
        service.onMouseDown(zone2InvalidClick);
        calls++;
        expect(service.paint).toHaveBeenCalledTimes(calls);
    });

    it('should not do anything on invalid click', () => {
        const initialColor = service.getColorAtPosition(zone2InvalidClick);
        service.onMouseDown(zone2InvalidClick);
        const finalColor = service.getColorAtPosition(zone2InvalidClick);
        expect(initialColor).toEqual(finalColor);
    });

    it('should not try to color the pixel if it is already the correct color', () => {
        drawServiceSpy.clearCanvas.and.callThrough();
        const spy = spyOn(service, 'updateCanvas');
        service.color = BLUE;
        service.onMouseDown(zone1MiddleClick);
        expect(spy).not.toHaveBeenCalled();
    });

    it('should not try to color the pixel if mouse button is neither left nor right', () => {
        const spy = spyOn(service, 'updateCanvas');
        const event = { button: MouseButton.Middle, offsetX: 0, offsetY: 0 } as MouseEvent;
        service.onMouseDown(event);
        expect(spy).not.toHaveBeenCalled();
    });

    it('should call the flood function on left click', () => {
        spyOn(service, 'flood');
        service.onMouseDown(zone1MiddleClick);
        expect(service.flood).toHaveBeenCalled();
    });

    it('should call the colorNonContinuous function on right click', () => {
        spyOn(service, 'colorNonContinuous');
        service.onMouseDown(zone1MiddleRightClick);
        expect(service.colorNonContinuous).toHaveBeenCalled();
    });

    it('should only flood continuous pixels (zone1 and zone3)', () => {
        service.color = ORANGE;

        service.onMouseDown(zone1MiddleClick);
        const zone1Color = service.getColorAtPosition(zone1MiddleClick);
        expect(zone1Color).toEqual(service.color);

        const zone2Color = service.getColorAtPosition(zone2MiddleClick);
        expect(zone2Color).not.toEqual(service.color);

        let zone3Color = service.getColorAtPosition(zone3MiddleClick);
        expect(zone3Color).not.toEqual(service.color);

        service.onMouseDown(zone3MiddleClick);
        zone3Color = service.getColorAtPosition(zone3MiddleClick);
        expect(zone3Color).toEqual(service.color);
    });

    it('it should flood continuous pixels (zone2)', () => {
        service.onMouseDown(zone2MiddleClick);
        const zone2Color = service.getColorAtPosition(zone2MiddleClick);
        expect(zone2Color).toEqual(service.color);

        const zone1Color = service.getColorAtPosition(zone1MiddleClick);
        expect(zone1Color).not.toEqual(service.color);

        const zone3Color = service.getColorAtPosition(zone3MiddleClick);
        expect(zone3Color).not.toEqual(service.color);
    });

    it('should color all non continuous pixels (zone 1 and zone 3)', () => {
        drawServiceSpy.clearCanvas.and.callThrough();
        service.onMouseDown(zone1MiddleClick);
        service.onMouseDown(zone3MiddleClick);
        service.color = BLUE;
        service.onMouseDown(zone1MiddleRightClick);
        const zone1Color = service.getColorAtPosition(zone1MiddleClick);
        expect(zone1Color).toEqual(service.color);
        const zone3Color = service.getColorAtPosition(zone3MiddleClick);
        expect(zone3Color).toEqual(service.color);
    });

    /*it('given a tolerance of 100, it should color the whole canvas', () => {
        service.setTolerance(MAX_TOLRERANCE);
        service.onMouseDown(zone1MiddleClick);
        const zone1Color = service.getColorAtPosition(zone1MiddleClick);
        const zone2Color = service.getColorAtPosition(zone2MiddleClick);
        const zone3Color = service.getColorAtPosition(zone3MiddleClick);
        expect(zone1Color).toEqual(service.color);
        expect(zone2Color).toEqual(service.color);
        expect(zone3Color).toEqual(service.color);
    });*/

    it('should only floor continuous pixels for a concave shape', () => {
        const length = 10;
        mockContext.fillStyle = PURPLE.toString();
        mockContext.fillText('J', zone2MiddleClick.clientX - length / 2, zone2MiddleClick.clientY - length / 2, length);
        service.onMouseDown(zone2MiddleClick);
        const zone2Color = service.getColorAtPosition(zone2MiddleClick);
        expect(zone2Color).toEqual(service.color);
    });
});
