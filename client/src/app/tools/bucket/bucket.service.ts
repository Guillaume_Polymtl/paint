import { Injectable } from '@angular/core';
import {
    Color,
    MAX_COLOR_INTENSITY,
    PIXEL_ALPHA_OFFSET,
    PIXEL_BLUE_OFFSET,
    PIXEL_DATA_SIZE,
    PIXEL_GREEN_OFFSET,
    PIXEL_RED_OFFSET,
} from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { FloodService } from '@app/tools/shared/services/flood/flood.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class BucketService extends Tool {
    readonly defaultTolerance: number = 0;
    readonly maxTolerance: number = 100;
    readonly minTolerance: number = 0;
    readonly name: ToolName = ToolName.BUCKET;
    private colorToReplace: Color;
    private tolerance: BehaviorSubject<number> = new BehaviorSubject<number>(this.defaultTolerance);

    constructor(drawingService: DrawingService, colorService: ColorService, public floodService: FloodService) {
        super(drawingService, colorService);
    }

    getTolerance(): Observable<number> {
        return this.tolerance.asObservable();
    }

    setTolerance(tolerance: number): void {
        if (tolerance > this.maxTolerance) {
            tolerance = this.maxTolerance;
        } else if (tolerance < this.minTolerance) {
            tolerance = this.minTolerance;
        } else {
            tolerance = tolerance;
        }
        this.tolerance.next(tolerance);
    }

    onMouseDown(event: MouseEvent): void {
        const imageData = this.drawingService.baseCtx.getImageData(0, 0, this.drawingService.canvas.width, this.drawingService.canvas.height);
        this.paint(event, imageData);
    }

    paint(event: MouseEvent, imageData: ImageData): void {
        const position = this.getPositionFromMouse(event);
        this.colorToReplace = this.getColorAtPosition(position);
        if (this.colorToReplace.equals(this.color)) {
            return;
        }
        if (event.button === MouseButton.Left) {
            this.flood(position, imageData);
        } else if (event.button === MouseButton.Right) {
            this.colorNonContinuous(position, imageData);
        } else {
            // nothing
        }
    }

    // Function inspired by William Malone at http://www.williammalone.com/articles/create-html5-canvas-javascript-drawing-app/
    flood(clickedPosition: Vec2, imageData: ImageData): void {
        const positions = this.floodService.getContinuousPositions(clickedPosition, this.tolerance.getValue());
        this.colorPixels(positions, imageData);
    }

    colorNonContinuous(clickedPosition: Vec2, imageData: ImageData): void {
        const positions = this.floodService.getNonContinuousPositions(clickedPosition, this.tolerance.getValue());
        this.colorPixels(positions, imageData);
    }

    private colorPixels(positions: Vec2[], imageData: ImageData): void {
        const width = imageData.width;
        for (const position of positions) {
            const offset = (position.y * width + position.x) * PIXEL_DATA_SIZE;
            this.colorPixel(offset, this.color, imageData);
        }
        this.updateCanvas(this.drawingService.baseCtx, imageData);
    }

    private colorPixel(offset: number, color: Color, imageData: ImageData): void {
        imageData.data[offset + PIXEL_RED_OFFSET] = color.red;
        imageData.data[offset + PIXEL_GREEN_OFFSET] = color.green;
        imageData.data[offset + PIXEL_BLUE_OFFSET] = color.blue;
        imageData.data[offset + PIXEL_ALPHA_OFFSET] = color.alpha * MAX_COLOR_INTENSITY;
    }

    getColorAtPosition(position: Vec2): Color {
        const imageData = this.drawingService.baseCtx.getImageData(position.x, position.y, 1, 1).data;
        return new Color(
            imageData[PIXEL_RED_OFFSET],
            imageData[PIXEL_GREEN_OFFSET],
            imageData[PIXEL_BLUE_OFFSET],
            +(imageData[PIXEL_ALPHA_OFFSET] / MAX_COLOR_INTENSITY).toFixed(2),
        );
    }

    updateCanvas(context: CanvasRenderingContext2D, imageData: ImageData): void {
        context.putImageData(imageData, 0, 0);
        this.drawingService.autoSave();
    }
}
