import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { DrawTypes } from '@app/classes/tools-utility';
import { EllipseService } from '@app/tools/ellipse/ellipse.service';
import { DrawTypeComponent } from '@app/tools/shared/components/draw-type/draw-type.component';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';

import { EllipseAttributesComponent } from './ellipse-attributes.component';

describe('EllipseAttributesComponent', () => {
    let component: EllipseAttributesComponent;
    let fixture: ComponentFixture<EllipseAttributesComponent>;
    let serviceSpy: jasmine.SpyObj<EllipseService>;

    beforeEach(async(() => {
        serviceSpy = jasmine.createSpyObj('EllipseService', ['setContourWidth', 'setDrawType', 'getContourWidth', 'getDrawType']);
        serviceSpy.setContourWidth.and.returnValue();
        serviceSpy.setDrawType.and.returnValue();
        TestBed.configureTestingModule({
            declarations: [EllipseAttributesComponent, SliderComponent, DrawTypeComponent],
            providers: [{ provide: EllipseService, useValue: serviceSpy }],
            imports: [MatSliderModule, MatButtonToggleModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EllipseAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call EllipseService.getContourWidth() on init', () => {
        expect(serviceSpy.getContourWidth).toHaveBeenCalled();
    });

    it('should call EllipseService.getDrawType() on init', () => {
        expect(serviceSpy.getDrawType).toHaveBeenCalled();
    });

    it('should call EllipseService.setContourWidth()', () => {
        const width = 10;
        component.setContourWidth(width);
        expect(serviceSpy.setContourWidth).toHaveBeenCalledWith(width);
    });

    it('should call EllipseService.setDrawType()', () => {
        component.setDrawType(DrawTypes.Fill);
        expect(serviceSpy.setDrawType).toHaveBeenCalledWith(DrawTypes.Fill);
    });
});
