import { Component, OnInit } from '@angular/core';
import { DrawTypes, ELLIPSE_MAX_CONTOUR_WIDTH, ELLIPSE_MIN_CONTOUR_WIDTH } from '@app/classes/tools-utility';
import { EllipseService } from '@app/tools/ellipse/ellipse.service';

@Component({
    selector: 'app-ellipse-attributes',
    templateUrl: './ellipse-attributes.component.html',
    styleUrls: ['./ellipse-attributes.component.scss'],
})
export class EllipseAttributesComponent implements OnInit {
    readonly title: string = 'Ellipse';

    contourWidth: number;
    drawType: DrawTypes;
    readonly minWidth: number = ELLIPSE_MIN_CONTOUR_WIDTH;
    readonly maxWidth: number = ELLIPSE_MAX_CONTOUR_WIDTH;

    constructor(public ellipseService: EllipseService) {}

    ngOnInit(): void {
        this.contourWidth = this.ellipseService.getContourWidth();
        this.drawType = this.ellipseService.getDrawType();
    }

    setContourWidth(width: number): void {
        this.ellipseService.setContourWidth(width);
    }

    setDrawType(drawType: DrawTypes): void {
        this.ellipseService.setDrawType(drawType);
    }
}
