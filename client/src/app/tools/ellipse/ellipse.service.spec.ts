import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { BLUE } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { DrawTypes, ELLIPSE_MAX_CONTOUR_WIDTH, ELLIPSE_MIN_CONTOUR_WIDTH } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { EllipseService } from '@app/tools/ellipse/ellipse.service';

// tslint:disable:no-any
// tslint:disable:max-file-line-count
describe('EllipseService', () => {
    let service: EllipseService;
    let mouseEvent: MouseEvent;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;
    let drawEllipseSpy: jasmine.Spy<any>;
    let drawDashRectangleSpy: jasmine.Spy<any>;

    let fillSpy: jasmine.Spy<any>;
    let strokeSpy: jasmine.Spy<any>;
    let ellipseSpy: jasmine.Spy<any>;

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);

        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawServiceSpy }],
        });
        service = TestBed.inject(EllipseService);
        drawEllipseSpy = spyOn<any>(service, 'drawEllipse').and.callThrough();
        drawDashRectangleSpy = spyOn<any>(service, 'drawDashRectangle').and.callThrough();

        fillSpy = spyOn<any>(previewCtxStub, 'fill');
        strokeSpy = spyOn<any>(previewCtxStub, 'stroke');
        ellipseSpy = spyOn<any>(previewCtxStub, 'ellipse');

        // Configuration du spy du service
        // tslint:disable:no-string-literal
        service['drawingService'].baseCtx = baseCtxStub; // Jasmine doesnt copy properties with underlying data
        service['drawingService'].previewCtx = previewCtxStub;

        mouseEvent = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Left,
        } as MouseEvent;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('init should reset service properties', () => {
        service['shiftPressed'] = true;
        service['mouseDown'] = true;
        service.init();
        expect(service['shiftPressed']).toEqual(false);
        expect(service['mouseDown']).toEqual(false);
    });

    it('setDrawType should change draw type if correct', () => {
        const currentDrawType = (service['drawType'] = DrawTypes.Contour);
        const expectedDrawType = DrawTypes.FillWithContour;
        service.setDrawType(expectedDrawType);
        expect(service.getDrawType()).not.toEqual(currentDrawType);
        expect(service.getDrawType()).toEqual(expectedDrawType);
    });

    it('setDrawType should not change draw type if incorrect', () => {
        const currentDrawType = service.getDrawType();
        const drawType = 'DrawType' as DrawTypes;
        service.setDrawType(drawType);
        expect(service.getDrawType()).toEqual(currentDrawType);
    });

    it('setContourWidth should change the contour width if correct', () => {
        const currentContourWidth = service.getContourWidth();
        const expectedContourWidth = ELLIPSE_MAX_CONTOUR_WIDTH - 1;
        service.setContourWidth(expectedContourWidth);
        expect(service.getContourWidth()).not.toEqual(currentContourWidth);
        expect(service.getContourWidth()).toEqual(expectedContourWidth);
    });

    it('setContourWidth should set contour width to min if entry is smaller', () => {
        const contourWidth = ELLIPSE_MIN_CONTOUR_WIDTH - 1;
        service.setContourWidth(contourWidth);
        expect(service.getContourWidth()).toEqual(ELLIPSE_MIN_CONTOUR_WIDTH);
    });

    it('setContourWidth should set contour width to max if entry is greater', () => {
        const contourWidth = ELLIPSE_MAX_CONTOUR_WIDTH + 1;
        service.setContourWidth(contourWidth);
        expect(service.getContourWidth()).toEqual(ELLIPSE_MAX_CONTOUR_WIDTH);
    });

    it(' mouseDown should set mouseDownCoord to correct position', () => {
        const expectedResult: Vec2 = service.getPositionFromMouse(mouseEvent);
        service.onMouseDown(mouseEvent);
        expect(service.mouseDownCoord).toEqual(expectedResult);
    });

    it(' mouseDown should set properties correctly to true on left click', () => {
        service.onMouseDown(mouseEvent);
        expect(service.mouseDown).toEqual(true);
        expect(service['hasLeft']).toBe(false);
        expect(service['drawPerimeter']).toBe(true);
    });

    it(' mouseDown should set mouseDown property to false on right click', () => {
        const mouseEventRClick = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;
        service.onMouseDown(mouseEventRClick);
        expect(service.mouseDown).toEqual(false);
    });

    it(' onMouseUp should call drawEllipse if mouse was already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;

        service.onMouseUp(mouseEvent);
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
        expect(drawEllipseSpy).toHaveBeenCalled();
    });

    it(' onMouseUp should not call drawEllipse if mouse was not already down', () => {
        service.mouseDown = false;
        service.onMouseUp(mouseEvent);
        expect(drawServiceSpy.clearCanvas).not.toHaveBeenCalled();
        expect(drawEllipseSpy).not.toHaveBeenCalled();
    });

    it('onMouseUp should set mousePosition to last position before mouse leaves', () => {
        service.mouseDown = true;
        service.mouseDownCoord = { x: 0, y: 0 };
        const mouseLeaveEvent = {
            offsetX: 0,
            offsetY: 0,
        } as MouseEvent;
        service.onMouseLeave(mouseLeaveEvent);
        service.onMouseUp(mouseEvent);
        expect(drawEllipseSpy).toHaveBeenCalledWith(baseCtxStub, service['positionBeforeLeave']);
    });

    it(' onMouseMove should call drawEllipse if mouse was already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
        expect(drawEllipseSpy).toHaveBeenCalled();
    });

    it(' onMouseMove should not call drawEllipse if mouse was not already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = false;
        service.onMouseMove(mouseEvent);
        expect(drawServiceSpy.clearCanvas).not.toHaveBeenCalled();
        expect(drawEllipseSpy).not.toHaveBeenCalled();
    });

    it('onMouseLeave should set hasLeft to true', () => {
        service['hasLeft'] = false;
        service.onMouseLeave(mouseEvent);
        expect(service['hasLeft']).toBe(true);
    });

    it('onKeyDown should set shiftPressed to true when shift is pressed', () => {
        service['shiftPressed'] = false;
        service['mouseDownCoord'] = { x: 3, y: 0 };
        service['lastMousePosition'] = { x: 0, y: 3 };
        const simulatedShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        service.onKeyDown(simulatedShiftKey);
        expect(service['shiftPressed']).toBe(true);
    });

    it('Ellipse must be automatically drawn when shift is released and mouse down', () => {
        service['shiftPressed'] = true;
        service['lastMousePosition'] = { x: 0, y: 0 };
        service['mouseDownCoord'] = { x: 0, y: 0 };
        service['mouseDown'] = true;
        const simulatedShiftKey = new KeyboardEvent('keyup', { key: 'Shift' });
        service.onKeyUp(simulatedShiftKey);
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
        expect(drawEllipseSpy).toHaveBeenCalled();
    });

    it('onKeyUp should set shiftPressed to false when shift was being pressed', () => {
        service['shiftPressed'] = false;
        service['lastMousePosition'] = { x: 0, y: 0 };
        service['mouseDownCoord'] = { x: 0, y: 0 };
        const simulatedDownShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedUpKey = new KeyboardEvent('keyup', { key: 'Shift' });
        service.onKeyDown(simulatedDownShiftKey);
        service.onKeyUp(simulatedUpKey);
        expect(service['shiftPressed']).toBe(false);
    });

    it('drawEllipse should draw only contour when draw type is Contour', () => {
        service['drawType'] = DrawTypes.Contour;
        service['mouseDownCoord'] = { x: 0, y: 3 };
        service['mouseDown'] = true;

        service.onMouseMove(mouseEvent);
        expect(ellipseSpy).toHaveBeenCalled();
        expect(strokeSpy).toHaveBeenCalled();
    });

    it('drawEllipse should only fill Ellipse when drawType is Fill', () => {
        service['drawType'] = DrawTypes.Fill;
        service['mouseDownCoord'] = { x: 3, y: 3 };
        service['mouseDown'] = true;

        service.onMouseMove(mouseEvent);
        expect(ellipseSpy).toHaveBeenCalled();
        expect(fillSpy).toHaveBeenCalled();
        expect(strokeSpy).not.toHaveBeenCalled();
    });

    it('drawEllipse should fill Ellipse and draw contour when drawType is FillWithContour', () => {
        service['drawType'] = DrawTypes.FillWithContour;
        service['mouseDownCoord'] = { x: 3, y: 3 };
        service['mouseDown'] = true;

        service.onMouseMove(mouseEvent);
        expect(ellipseSpy).toHaveBeenCalled();
        expect(fillSpy).toHaveBeenCalled();
        expect(strokeSpy).toHaveBeenCalled();
    });

    it('rectangle perimeter top left coordinates are well computed from bottom-right to top-left', () => {
        service.mouseDownCoord = { x: 5, y: 4 };
        service['shiftPressed'] = false;
        service.mouseDown = true;
        mouseEvent = { offsetX: 1, offsetY: 1 } as MouseEvent;
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        const expectedCoordinates: Vec2 = { x: 1, y: 1 };
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        const computedCoordinates: Vec2 = service.computeRectStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('rectangle perimeter top left coordinates are well computed from bottom-left to top-right', () => {
        service.mouseDownCoord = { x: 1, y: 6 };
        service['shiftPressed'] = false;
        service.mouseDown = true;
        mouseEvent = { offsetX: 4, offsetY: 0 } as MouseEvent;
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        const expectedCoordinates: Vec2 = { x: 1, y: 0 };
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        const computedCoordinates: Vec2 = service.computeRectStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('rectangle perimeter top left coordinates are well computed from top-right to bottom-left', () => {
        service.mouseDownCoord = { x: 8, y: 0 };
        service['shiftPressed'] = false;
        service.mouseDown = true;
        mouseEvent = { offsetX: 3, offsetY: 4 } as MouseEvent;
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        const expectedCoordinates: Vec2 = { x: 3, y: 0 };
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        const computedCoordinates: Vec2 = service.computeRectStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('square perimeter top left coordinates are well computed from bottom-right to top-left', () => {
        service['mouseDownCoord'] = { x: 5, y: 4 };
        service['shiftPressed'] = true;
        service['mouseDown'] = true;
        service.color = BLUE;
        mouseEvent = { offsetX: 1, offsetY: 1 } as MouseEvent;
        const expectedCoordinates: Vec2 = { x: 1, y: 0 };
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        const computedCoordinates: Vec2 = service.computeRectStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('square perimeter top left coordinates are well computed from bottom-left to top-right', () => {
        service['mouseDownCoord'] = { x: 1, y: 6 };
        service['shiftPressed'] = true;
        service['mouseDown'] = true;
        service.color = BLUE;
        mouseEvent = { offsetX: 4, offsetY: 0 } as MouseEvent;
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        const expectedCoordinates: Vec2 = { x: 1, y: 2 };
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        const computedCoordinates: Vec2 = service.computeRectStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('square perimeter top left coordinates are well computed from top-right to bottom-left', () => {
        service['mouseDownCoord'] = { x: 8, y: 0 };
        service['shiftPressed'] = true;
        service['mouseDown'] = true;
        service.color = BLUE;
        mouseEvent = { offsetX: 3, offsetY: 4 } as MouseEvent;
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        const expectedCoordinates: Vec2 = { x: 3, y: 0 };
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        const computedCoordinates: Vec2 = service.computeRectStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('Escape Key should erase currrently drawn shape', () => {
        service.mouseDown = true;
        service.mouseDownCoord = { x: 3, y: 3 };
        mouseEvent = { offsetX: 0, offsetY: 0 } as MouseEvent;
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Escape' });
        service.onMouseMove(mouseEvent);

        service.onKeyDown(simulatedDownKey);

        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
        expect(service.mouseDown).toEqual(false);
    });

    it('Perimeter should not be drawn when mouse goes up', () => {
        service.mouseDown = true;
        service.mouseDownCoord = { x: 3, y: 3 };
        service.onMouseMove(mouseEvent);

        service.onMouseUp(mouseEvent);

        expect(drawDashRectangleSpy).not.toHaveBeenCalled();
    });

    it('Perimeter should be drawn when mouse is down', () => {
        service.onMouseDown(mouseEvent);
        service.onMouseMove(mouseEvent);

        expect(service['drawPerimeter']).toBe(true);
        expect(drawDashRectangleSpy).toHaveBeenCalled();
    });
});

const computeWidth = (x1: number, x2: number) => {
    let width: number = x1 - x2;
    width = width < 0 ? --width : ++width;
    return width;
};

const computeHeigth = (y1: number, y2: number) => {
    let heigth: number = y1 - y2;
    heigth = heigth < 0 ? --heigth : ++heigth;
    return heigth;
};
