import { Injectable } from '@angular/core';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import {
    DrawTypes,
    ELLIPSE_DEFAULT_CONTOUR_WIDTH,
    ELLIPSE_DEFAULT_RECT_COLOR,
    ELLIPSE_DEFAULT_RECT_WIDTH,
    ELLIPSE_MAX_CONTOUR_WIDTH,
    ELLIPSE_MIN_CONTOUR_WIDTH,
    ELLIPSE_PERIMETER_DASH,
    KeyValues,
} from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';

@Injectable({
    providedIn: 'root',
})
export class EllipseService extends Tool {
    readonly name: ToolName = ToolName.ELLIPSE;
    private drawType: DrawTypes = DrawTypes.Contour;
    private drawPerimeter: boolean = false;
    private hasLeft: boolean = false;
    private positionBeforeLeave: Vec2;

    private contourWidth: number = ELLIPSE_DEFAULT_CONTOUR_WIDTH;
    readonly RECT_LINE_WIDTH: number = ELLIPSE_DEFAULT_RECT_WIDTH;

    shiftPressed: boolean = false;
    private lastMousePosition: Vec2 = { x: 0, y: 0 };

    constructor(drawingService: DrawingService, colorService: ColorService) {
        super(drawingService, colorService);
    }

    init(): void {
        this.shiftPressed = false;
        this.mouseDown = false;
    }

    getDrawType(): DrawTypes {
        return this.drawType;
    }

    setDrawType(drawType: DrawTypes): void {
        if (drawType in DrawTypes) {
            this.drawType = drawType;
        }
    }

    getContourWidth(): number {
        return this.contourWidth;
    }

    setContourWidth(width: number): void {
        if (width >= ELLIPSE_MIN_CONTOUR_WIDTH && width <= ELLIPSE_MAX_CONTOUR_WIDTH) {
            this.contourWidth = width;
        } else if (width < ELLIPSE_MIN_CONTOUR_WIDTH) {
            this.contourWidth = ELLIPSE_MIN_CONTOUR_WIDTH;
        } else {
            this.contourWidth = ELLIPSE_MAX_CONTOUR_WIDTH;
        }
    }

    onMouseDown(event: MouseEvent): void {
        this.mouseDown = event.button === MouseButton.Left;
        if (this.mouseDown) {
            this.hasLeft = false;
            this.drawPerimeter = true;
            this.mouseDownCoord = this.getPositionFromMouse(event);
        }
    }

    onMouseUp(event: MouseEvent): void {
        if (this.mouseDown && event.button === MouseButton.Left) {
            this.mouseDown = false;
            this.drawPerimeter = false;
            const mousePosition = this.hasLeft ? this.positionBeforeLeave : this.getPositionFromMouse(event);

            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.drawEllipse(this.drawingService.baseCtx, mousePosition);
            this.drawingService.autoSave();
        }
    }

    onMouseMove(event: MouseEvent): void {
        if (this.mouseDown) {
            const mousePosition = this.getPositionFromMouse(event);
            this.lastMousePosition = mousePosition;

            // On dessine sur le canvas de prévisualisation et on l'efface à chaque déplacement de la souris
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.drawEllipse(this.drawingService.previewCtx, mousePosition);
        }
    }

    onMouseLeave(event: MouseEvent): void {
        this.hasLeft = true;
        this.positionBeforeLeave = this.getPositionFromMouse(event);
    }

    onKeyDown(event: KeyboardEvent): void {
        const key: string = event.key;
        /* istanbul ignore else*/
        if (key === KeyValues.Shift) {
            this.shiftPressed = true;
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.drawEllipse(this.drawingService.previewCtx, this.lastMousePosition);
        } else if (key === KeyValues.Escape && this.mouseDown) {
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.mouseDown = false;
        }
    }

    onKeyUp(event: KeyboardEvent): void {
        /* istanbul ignore else*/
        if (this.shiftPressed && event.key === KeyValues.Shift) {
            this.shiftPressed = false;
            if (this.mouseDown) {
                this.drawingService.clearCanvas(this.drawingService.previewCtx);
                this.drawEllipse(this.drawingService.previewCtx, this.lastMousePosition);
            }
        }
    }

    computeRectStartPosition(currentMousePosition: Vec2, width: number, heigth: number): Vec2 {
        const rectStartPosition: Vec2 = { x: this.mouseDownCoord.x, y: this.mouseDownCoord.y };
        const positiveWidth = Math.abs(width);
        const positiveHeigth = Math.abs(heigth);
        const min: number = positiveWidth < positiveHeigth ? positiveWidth : positiveHeigth;
        // Du bas à droite à gauche en haut
        if (width > 0 && heigth > 0) {
            if (!this.shiftPressed) {
                rectStartPosition.x = currentMousePosition.x;
                rectStartPosition.y = currentMousePosition.y;
            } else {
                rectStartPosition.x = this.mouseDownCoord.x - min;
                rectStartPosition.y = this.mouseDownCoord.y - min;
            }

            // Du bas à gauche à droite en haut
        } else if (width < 0 && heigth > 0) {
            if (!this.shiftPressed) {
                rectStartPosition.y = currentMousePosition.y;
            } else {
                rectStartPosition.y = this.mouseDownCoord.y - min;
            }

            // Du haut à droite à gauche en bas
        } else if (width > 0 && heigth < 0) {
            if (!this.shiftPressed) {
                rectStartPosition.x = currentMousePosition.x;
            } else {
                rectStartPosition.x = this.mouseDownCoord.x - min;
            }
        } // Sinon du haut à gauche à droite en bas

        return rectStartPosition;
    }

    computeEllipseCenter(rectStartPosition: Vec2, rectWidth: number, rectHeigth: number): Vec2 {
        const ellipseCenter: Vec2 = { x: rectStartPosition.x + rectWidth / 2, y: rectStartPosition.y + rectHeigth / 2 };
        return ellipseCenter;
    }

    private drawDashRectangle(ctx: CanvasRenderingContext2D, rectStartPosition: Vec2, dimensions: Vec2): void {
        ctx.setLineDash([ELLIPSE_PERIMETER_DASH, ELLIPSE_PERIMETER_DASH]);
        ctx.strokeStyle = ELLIPSE_DEFAULT_RECT_COLOR;
        ctx.lineWidth = ELLIPSE_DEFAULT_RECT_WIDTH;
        ctx.strokeRect(rectStartPosition.x, rectStartPosition.y, dimensions.x, dimensions.y);
        ctx.setLineDash([]);
    }

    private drawEllipse(ctx: CanvasRenderingContext2D, currentMousePosition: Vec2): void {
        let rectStartPosition: Vec2 = { x: this.mouseDownCoord.x, y: this.mouseDownCoord.y };
        let rectWidth = this.mouseDownCoord.x - currentMousePosition.x;
        let rectHeigth = this.mouseDownCoord.y - currentMousePosition.y;

        // Pour avoir les valeurs exactes du nombre de pixels
        rectWidth = rectWidth < 0 ? --rectWidth : ++rectWidth;
        rectHeigth = rectHeigth < 0 ? --rectHeigth : ++rectHeigth;

        rectStartPosition = this.computeRectStartPosition(currentMousePosition, rectWidth, rectHeigth);
        rectWidth = Math.abs(rectWidth);
        rectHeigth = Math.abs(rectHeigth);
        if (this.shiftPressed) {
            const circleRadius = Math.min(rectWidth, rectHeigth);
            rectWidth = circleRadius;
            rectHeigth = circleRadius;
        }

        const ellipseCenter: Vec2 = this.computeEllipseCenter(rectStartPosition, rectWidth, rectHeigth);

        if (this.drawPerimeter) this.drawDashRectangle(ctx, rectStartPosition, { x: rectWidth, y: rectHeigth });

        ctx.beginPath();
        ctx.lineWidth = this.contourWidth;
        ctx.ellipse(ellipseCenter.x, ellipseCenter.y, rectWidth / 2, rectHeigth / 2, Math.PI, 0, 2 * Math.PI);
        ctx.strokeStyle = this.secondaryColor.toString();
        ctx.fillStyle = this.color.toString();

        switch (this.drawType) {
            case DrawTypes.Fill:
                ctx.fill();
                break;
            case DrawTypes.FillWithContour:
                ctx.fill();
            case DrawTypes.Contour:
                ctx.stroke();
                break;
        }
    }
}
