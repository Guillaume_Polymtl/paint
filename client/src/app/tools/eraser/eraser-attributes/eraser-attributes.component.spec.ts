import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSliderModule } from '@angular/material/slider';
import { EraserService } from '@app/tools/eraser/eraser.service';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';
import { EraserAttributesComponent } from './eraser-attributes.component';

describe('EraserAttributesComponent', () => {
    const WIDTH = 0;
    let component: EraserAttributesComponent;
    let fixture: ComponentFixture<EraserAttributesComponent>;
    let serviceSpy: jasmine.SpyObj<EraserService>;

    beforeEach(async(() => {
        serviceSpy = jasmine.createSpyObj('EraserService', ['setWidth', 'getWidth']);
        TestBed.configureTestingModule({
            declarations: [EraserAttributesComponent, SliderComponent],
            providers: [{ provide: EraserService, useValue: serviceSpy }],
            imports: [MatSliderModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EraserAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call EraserService.setWidth()', () => {
        component.setWidth(WIDTH);
        expect(serviceSpy.setWidth).toHaveBeenCalled();
    });

    it('should call EraserService.setWidth(val) with val', () => {
        component.setWidth(WIDTH);
        expect(serviceSpy.setWidth).toHaveBeenCalledWith(WIDTH);
    });
});
