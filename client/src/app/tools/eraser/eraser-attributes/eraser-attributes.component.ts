import { Component, OnInit } from '@angular/core';
import { ERASER_MAX_WIDTH, ERASER_MIN_WIDTH } from '@app/classes/tools-utility';
import { EraserService } from '@app/tools/eraser/eraser.service';

@Component({
    selector: 'app-eraser-attributes',
    templateUrl: './eraser-attributes.component.html',
    styleUrls: ['./eraser-attributes.component.scss'],
})
export class EraserAttributesComponent implements OnInit {
    readonly title: string = 'Efface';

    readonly width: string = 'Épaisseur';
    defaultWidth: number;
    readonly minWidth: number = ERASER_MIN_WIDTH;
    readonly maxWidth: number = ERASER_MAX_WIDTH;

    constructor(private eraserService: EraserService) {}

    ngOnInit(): void {
        this.defaultWidth = this.eraserService.getWidth();
    }

    setWidth(width: number): void {
        this.eraserService.setWidth(width);
    }
}
