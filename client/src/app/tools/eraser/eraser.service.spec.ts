import { TestBed } from '@angular/core/testing';
import { MatSliderModule } from '@angular/material/slider';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { MouseButton } from '@app/classes/mouse-buttons';
import { ERASER_MAX_WIDTH, ERASER_MIN_WIDTH } from '@app/classes/tools-utility';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { EraserService } from './eraser.service';

// tslint:disable:no-any
describe('EraserService', () => {
    let service: EraserService;
    let mouseEvent: MouseEvent;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;

    let fillRectSpy: jasmine.Spy<any>;
    let strokeRectSpy: jasmine.Spy<any>;
    let clearRectSpy: jasmine.Spy<any>;

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);

        fillRectSpy = spyOn<any>(previewCtxStub, 'fillRect');
        strokeRectSpy = spyOn<any>(previewCtxStub, 'strokeRect');
        clearRectSpy = spyOn<any>(baseCtxStub, 'clearRect');

        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawServiceSpy }],
            imports: [MatSliderModule],
        });
        service = TestBed.inject(EraserService);

        // Configuration du spy du service
        // tslint:disable:no-string-literal
        service['drawingService'].baseCtx = baseCtxStub; // Jasmine doesnt copy properties with underlying data
        service['drawingService'].previewCtx = previewCtxStub;

        mouseEvent = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Left,
        } as MouseEvent;
    });
    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('setWidth should change the width if correct', () => {
        const currentWidth = service.getWidth();
        const expectedWidth = ERASER_MAX_WIDTH - 1;
        service.setWidth(expectedWidth);
        expect(service.getWidth()).not.toEqual(currentWidth);
        expect(service.getWidth()).toEqual(expectedWidth);
    });

    it('setWidth should set width to min if entry is smaller', () => {
        const width = ERASER_MIN_WIDTH - 1;
        service.setWidth(width);
        expect(service.getWidth()).toEqual(ERASER_MIN_WIDTH);
    });

    it('setWidth should set width to max if entry is greater', () => {
        const width = ERASER_MAX_WIDTH + 1;
        service.setWidth(width);
        expect(service.getWidth()).toEqual(ERASER_MAX_WIDTH);
    });

    it(' mouseDown should set mouseDown property to true on left click', () => {
        service.onMouseDown(mouseEvent);
        expect(service.mouseDown).toEqual(true);
    });

    it(' mouseDown should set mouseDown property to false on right click', () => {
        const mouseEventRClick = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;
        service.onMouseDown(mouseEventRClick);
        expect(service.mouseDown).toEqual(false);
    });

    it(' mouseUp should set mouseDown property to false on left mouse button up', () => {
        service.mouseDown = true;
        service.onMouseUp(mouseEvent);
        expect(service.mouseDown).toEqual(false);
    });

    it(' mouseUp should let mouseDown property to true on right mouse button up', () => {
        const mouseEventRUp = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;
        service.mouseDown = true;
        service.onMouseUp(mouseEventRUp);
        expect(service.mouseDown).toEqual(true);
    });

    it('onMouseMove should draw the a filled square with contour when mouse move', () => {
        service.onMouseMove(mouseEvent);
        expect(fillRectSpy).toHaveBeenCalled();
        expect(strokeRectSpy).toHaveBeenCalled();
    });

    it('onMouseMove should clear surface under square when mouse down and mouse move', () => {
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(clearRectSpy).toHaveBeenCalled();
    });

    it('onMouseMove should not clear surface under square when mouse not down', () => {
        service.mouseDown = false;
        service.onMouseMove(mouseEvent);
        expect(clearRectSpy).not.toHaveBeenCalled();
    });

    it('onMouseClick should clear surface under square', () => {
        service.onMouseClick(mouseEvent);
        expect(clearRectSpy).toHaveBeenCalled();
    });
});
