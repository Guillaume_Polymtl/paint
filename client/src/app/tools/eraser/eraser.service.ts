import { Injectable } from '@angular/core';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { ERASER_DEFAULT_WIDTH, ERASER_LINE_WIDTH, ERASER_MAX_WIDTH, ERASER_MIN_WIDTH } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';

@Injectable({
    providedIn: 'root',
})
export class EraserService extends Tool {
    readonly name: ToolName = ToolName.ERASER;
    private width: number = ERASER_DEFAULT_WIDTH;

    constructor(drawingService: DrawingService, colorService: ColorService) {
        super(drawingService, colorService);
    }

    getWidth(): number {
        return this.width;
    }

    // epaisseur de l'efface
    setWidth(width: number): void {
        if (width >= ERASER_MIN_WIDTH && width <= ERASER_MAX_WIDTH) {
            this.width = width;
        } else if (width < ERASER_MIN_WIDTH) {
            this.width = ERASER_MIN_WIDTH;
        } else {
            this.width = ERASER_MAX_WIDTH;
        }
    }

    onMouseDown(event: MouseEvent): void {
        this.mouseDown = event.button === MouseButton.Left;
    }

    onMouseUp(event: MouseEvent): void {
        this.mouseDown = !(event.button === MouseButton.Left);
    }

    onMouseMove(event: MouseEvent): void {
        const mousePosition = this.getPositionFromMouse(event);
        // sommet en haut à gauche du carré de l'efface
        const topLeftPosition: Vec2 = { x: mousePosition.x - this.width / 2, y: mousePosition.y - this.width / 2 };

        this.drawEraser(topLeftPosition);

        if (this.mouseDown) {
            this.drawingService.baseCtx.clearRect(topLeftPosition.x, topLeftPosition.y, this.width, this.width);
            this.drawingService.autoSave();
        }
    }

    onMouseClick(event: MouseEvent): void {
        const mousePosition = this.getPositionFromMouse(event);
        // sommet en haut à gauche du carré de l'efface
        const topLeftPosition: Vec2 = { x: mousePosition.x - this.width / 2, y: mousePosition.y - this.width / 2 };
        this.drawEraser(topLeftPosition);

        this.drawingService.baseCtx.clearRect(topLeftPosition.x, topLeftPosition.y, this.width, this.width);
    }

    drawEraser(topLeftPosition: Vec2): void {
        const ctx = this.drawingService.previewCtx;
        this.drawingService.clearCanvas(ctx);

        ctx.fillStyle = 'white';
        ctx.fillRect(topLeftPosition.x, topLeftPosition.y, this.width, this.width);

        ctx.strokeStyle = 'black';
        ctx.lineWidth = ERASER_LINE_WIDTH;
        ctx.strokeRect(topLeftPosition.x, topLeftPosition.y, this.width, this.width);
    }
}
