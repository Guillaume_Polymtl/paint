import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSliderModule } from '@angular/material/slider';
import { Router } from '@angular/router';
import { EyeDropperService } from '@app/tools/eye-dropper/eye-dropper.service';
import { EyeDropperAttributesComponent } from './eye-dropper-attributes.component';

describe('EyeDropperAttributesComponent', () => {
    let component: EyeDropperAttributesComponent;
    let fixture: ComponentFixture<EyeDropperAttributesComponent>;
    let routerSpy: jasmine.SpyObj<Router>;

    beforeEach(async(() => {
        routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
        TestBed.configureTestingModule({
            declarations: [EyeDropperAttributesComponent],
            providers: [{ provide: EyeDropperService }, { provide: Router, useValue: routerSpy }],
            imports: [MatSliderModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EyeDropperAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
