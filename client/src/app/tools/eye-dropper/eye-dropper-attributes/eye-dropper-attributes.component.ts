import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { EYE_DROPPER_CANVAS_SIZE } from '@app/classes/tools-utility';
import { EyeDropperService } from '@app/tools/eye-dropper/eye-dropper.service';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-eye-dropper-attributes',
    templateUrl: './eye-dropper-attributes.component.html',
    styleUrls: ['./eye-dropper-attributes.component.scss'],
})
export class EyeDropperAttributesComponent implements OnDestroy, AfterViewInit {
    readonly title: string = 'Pipette';
    @ViewChild('previewCanvas', { static: false }) previewCanvas: ElementRef<HTMLCanvasElement>;
    previewCtxLoaded: Subject<boolean> = new Subject<boolean>();
    destroyed: Subject<boolean> = new Subject<boolean>();

    constructor(public eyeDropperService: EyeDropperService) {}

    ngAfterViewInit(): void {
        const loadedChangeObservable = this.previewCtxLoaded.pipe(
            map((ctxLoaded) => ctxLoaded),
            takeUntil(this.destroyed),
        );
        loadedChangeObservable.subscribe((ctxLoaded: boolean) => (this.eyeDropperService.previewCtxLoaded = ctxLoaded));
        this.eyeDropperService.previewCtx = this.previewCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.previewCtxLoaded.next(true);
    }

    ngOnDestroy(): void {
        this.destroyed.next(true);
        this.destroyed.complete();
    }

    get previewCanvasWidth(): number {
        return EYE_DROPPER_CANVAS_SIZE;
    }

    get previewCanvasHeight(): number {
        return EYE_DROPPER_CANVAS_SIZE;
    }
}
