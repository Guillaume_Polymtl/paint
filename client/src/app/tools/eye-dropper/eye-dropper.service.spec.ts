import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { Color, ColorType, RED } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { EYE_DROPPER_IMAGE_DATA_HALF_SIZE } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { EyeDropperService } from '@app/tools/eye-dropper/eye-dropper.service';
import { of } from 'rxjs';

// tslint:disable:no-any
describe('EyeDropperService', () => {
    let service: EyeDropperService;
    let mouseEvent: MouseEvent;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;
    let colorServiceSpy: jasmine.SpyObj<ColorService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas']);
        colorServiceSpy = jasmine.createSpyObj('ColorService', ['setColor', 'getColor']);
        colorServiceSpy.getColor.and.returnValue(of(RED));

        TestBed.configureTestingModule({
            providers: [
                { provide: DrawingService, useValue: drawServiceSpy },
                { provide: ColorService, useValue: colorServiceSpy },
            ],
        });
        service = TestBed.inject(EyeDropperService);
        service.previewCtx = previewCtxStub;

        // Configuration du spy du service
        // tslint:disable:no-string-literal
        service['drawingService'].baseCtx = baseCtxStub;

        mouseEvent = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Left,
        } as MouseEvent;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it(' mouseDown should set colorService primary color when left button', () => {
        const color: Color = service.getPixelColor(service.getPositionFromMouse(mouseEvent));
        service.onMouseDown(mouseEvent);
        expect(colorServiceSpy.setColor).toHaveBeenCalledWith(ColorType.PRIMARY, color);
    });

    it(' mouseDown should set colorService secondary color when right button', () => {
        const rightMouseEvent = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;

        const color: Color = service.getPixelColor(service.getPositionFromMouse(rightMouseEvent));
        service.onMouseDown(rightMouseEvent);
        expect(colorServiceSpy.setColor).toHaveBeenCalledWith(ColorType.SECONDARY, color);
    });

    it('should clear previewCanvas when mouseleave', () => {
        const clearPreviewCanvasSpy: jasmine.Spy<any> = spyOn<any>(service, 'clearPreviewCanvas');
        service.onMouseLeave(mouseEvent);
        expect(clearPreviewCanvasSpy).toHaveBeenCalled();
    });

    it(' onMouseMove should call drawPreview', () => {
        const drawPreviewSpy: jasmine.Spy<any> = spyOn<any>(service, 'drawPreview');
        service.onMouseMove(mouseEvent);
        expect(drawPreviewSpy).toHaveBeenCalled();
    });

    it(' should read the pixel of the canvas ', () => {
        // drawServiceSpy.clearCanvas.and.callThrough();
        drawServiceSpy.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(baseCtxStub);
        baseCtxStub.fillStyle = RED.toString();
        const position: Vec2 = { x: baseCtxStub.canvas.width - 1, y: baseCtxStub.canvas.height - 1 };
        baseCtxStub.fillRect(position.x, position.y, 1, 1);
        // mouseEvent = { offsetX: 0, offsetY: 0, button: 0 } as MouseEvent;
        const color: Color = service.getPixelColor(position);

        expect(color).toEqual(RED);
        service.clearPreviewCanvas();
    });

    it('should clear the preview circle canvas', () => {
        const clearRectSpy: jasmine.Spy<any> = spyOn<any>(service.previewCtx, 'clearRect');
        service.clearPreviewCanvas();
        expect(clearRectSpy).toHaveBeenCalled();
    });

    it('drawPreviewBorder should draw the circle preview', () => {
        const arcSpy: jasmine.Spy<any> = spyOn<any>(service.previewCtx, 'arc');
        const strokeSpy: jasmine.Spy<any> = spyOn<any>(service.previewCtx, 'stroke');
        service.drawPreviewBorder();
        expect(arcSpy).toHaveBeenCalled();
        expect(strokeSpy).toHaveBeenCalled();
    });

    it('computeCornerOffset should return 0 for coordinate which is greater than EYE_DROPPER_IMAGE_DATA_HALF_SIZE', () => {
        const position: Vec2 = { x: EYE_DROPPER_IMAGE_DATA_HALF_SIZE, y: EYE_DROPPER_IMAGE_DATA_HALF_SIZE + 1 };
        const expectedCornerOffset: Vec2 = { x: 0, y: 0 };
        const cornerOffset: Vec2 = service.computeCornerOffset(position);
        expect(cornerOffset).toEqual(expectedCornerOffset);
    });

    it('computeCornerOffset should return difference for coordinate which is less than EYE_DROPPER_IMAGE_DATA_HALF_SIZE', () => {
        const position: Vec2 = { x: EYE_DROPPER_IMAGE_DATA_HALF_SIZE - 1, y: EYE_DROPPER_IMAGE_DATA_HALF_SIZE - 1 };
        const expectedCornerOffset: Vec2 = { x: 1, y: 1 };
        const cornerOffset: Vec2 = service.computeCornerOffset(position);
        expect(cornerOffset).toEqual(expectedCornerOffset);
    });

    it('should not draw preview image if previewCtx is not loaded', () => {
        const clearPreviewCanvasSpy: jasmine.Spy<any> = spyOn<any>(service, 'clearPreviewCanvas');
        service.previewCtxLoaded = false;
        const topLeftCorner: Vec2 = { x: 0, y: 0 };
        const cornerOffset: Vec2 = { x: 0, y: 0 };
        service.drawPreview(topLeftCorner, cornerOffset);
        expect(clearPreviewCanvasSpy).not.toHaveBeenCalled();
    });

    it('drawPreview should draw preview image on previewCtx if loaded', () => {
        const drawImagepy: jasmine.Spy<any> = spyOn<any>(service.previewCtx, 'drawImage');
        const clearPreviewCanvasSpy: jasmine.Spy<any> = spyOn<any>(service, 'clearPreviewCanvas');
        service.previewCtxLoaded = true;
        const topLeftCorner: Vec2 = { x: 0, y: 0 };
        const cornerOffset: Vec2 = { x: 0, y: 0 };
        service.drawPreview(topLeftCorner, cornerOffset);
        expect(clearPreviewCanvasSpy).toHaveBeenCalled();
        expect(drawImagepy).toHaveBeenCalled();
    });

    it('drawPreview should draw preview circle', () => {
        const drawPreviewBorderpy: jasmine.Spy<any> = spyOn<any>(service, 'drawPreviewBorder');
        service.previewCtxLoaded = true;
        const topLeftCorner: Vec2 = { x: 0, y: 0 };
        const cornerOffset: Vec2 = { x: 0, y: 0 };
        service.drawPreview(topLeftCorner, cornerOffset);
        expect(drawPreviewBorderpy).toHaveBeenCalled();
    });

    it('drawPreview should only show preview circle not the whole previewCanvas', () => {
        const arcSpy: jasmine.Spy<any> = spyOn<any>(service.previewCtx, 'arc');
        const clipSpy: jasmine.Spy<any> = spyOn<any>(service.previewCtx, 'clip');
        service.previewCtxLoaded = true;
        const topLeftCorner: Vec2 = { x: 0, y: 0 };
        const cornerOffset: Vec2 = { x: 0, y: 0 };
        service.drawPreview(topLeftCorner, cornerOffset);
        expect(arcSpy).toHaveBeenCalled();
        expect(clipSpy).toHaveBeenCalled();
    });

    it('drawPreview should draw a square around the pixel under the mouse', () => {
        const strokeRectSpy: jasmine.Spy<any> = spyOn<any>(service.previewCtx, 'strokeRect');
        service.previewCtxLoaded = true;
        const topLeftCorner: Vec2 = { x: 0, y: 0 };
        const cornerOffset: Vec2 = { x: 0, y: 0 };
        service.drawPreview(topLeftCorner, cornerOffset);
        expect(strokeRectSpy).toHaveBeenCalled();
    });
});
