import { Injectable } from '@angular/core';
import { Color, ColorType, SILVER } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { EYE_DROPPER_CANVAS_SIZE, EYE_DROPPER_IMAGE_DATA_HALF_SIZE, EYE_DROPPER_SCALE_FACTOR } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';

@Injectable({
    providedIn: 'root',
})
export class EyeDropperService extends Tool {
    readonly name: ToolName = ToolName.EYE_DROPPER;
    previewCtx: CanvasRenderingContext2D;
    previewCtxLoaded: boolean;

    constructor(drawingService: DrawingService, colorService: ColorService) {
        super(drawingService, colorService);
        this.previewCtxLoaded = false;
    }

    onMouseDown(event: MouseEvent): void {
        const mousePosition = this.getPositionFromMouse(event);
        /* istanbul ignore else */
        if (event.button === MouseButton.Left) {
            this.colorService.setColor(ColorType.PRIMARY, this.getPixelColor(mousePosition));
        } else if (event.button === MouseButton.Right) {
            this.colorService.setColor(ColorType.SECONDARY, this.getPixelColor(mousePosition));
        }
    }

    onMouseMove(event: MouseEvent): void {
        const mousePosition: Vec2 = this.getPositionFromMouse(event);
        const topLeftCorner: Vec2 = this.getTopLeftCorner(mousePosition);
        const cornerOffset: Vec2 = this.computeCornerOffset(mousePosition);
        this.drawPreview(topLeftCorner, cornerOffset);
    }

    onMouseLeave(event: MouseEvent): void {
        this.clearPreviewCanvas();
    }

    getPixelColor(position: Vec2): Color {
        const pixelData = this.drawingService.baseCtx.getImageData(position.x, position.y, 1, 1).data;
        // tslint:disable-next-line: no-magic-numbers
        return new Color(pixelData[0], pixelData[1], pixelData[2], pixelData[3]);
    }

    computeCornerOffset(position: Vec2): Vec2 {
        const cornerOffset: Vec2 = { x: 0, y: 0 };
        cornerOffset.x = EYE_DROPPER_IMAGE_DATA_HALF_SIZE <= position.x ? 0 : EYE_DROPPER_IMAGE_DATA_HALF_SIZE - position.x;
        cornerOffset.y = EYE_DROPPER_IMAGE_DATA_HALF_SIZE <= position.y ? 0 : EYE_DROPPER_IMAGE_DATA_HALF_SIZE - position.y;
        return cornerOffset;
    }

    getTopLeftCorner(position: Vec2): Vec2 {
        const topLeftCornerX: number = Math.max(0, position.x - EYE_DROPPER_IMAGE_DATA_HALF_SIZE);
        const topLeftCornerY: number = Math.max(0, position.y - EYE_DROPPER_IMAGE_DATA_HALF_SIZE);

        return { x: topLeftCornerX, y: topLeftCornerY };
    }

    drawPreviewBorder(): void {
        this.previewCtx.beginPath();
        this.previewCtx.arc(EYE_DROPPER_CANVAS_SIZE / 2, EYE_DROPPER_CANVAS_SIZE / 2, EYE_DROPPER_CANVAS_SIZE / 2, 0, Math.PI * 2);
        this.previewCtx.strokeStyle = SILVER.toString();
        this.previewCtx.lineWidth = 2;
        this.previewCtx.stroke();
    }

    drawPreview(topLeftCorner: Vec2, cornerOffset: Vec2): void {
        if (this.previewCtxLoaded) {
            this.clearPreviewCanvas();

            this.drawPreviewBorder();

            this.previewCtx.beginPath();
            this.previewCtx.arc(EYE_DROPPER_CANVAS_SIZE / 2, EYE_DROPPER_CANVAS_SIZE / 2, EYE_DROPPER_CANVAS_SIZE / 2, 0, Math.PI * 2);
            this.previewCtx.clip();

            this.previewCtx.imageSmoothingEnabled = false;
            this.previewCtx.drawImage(
                this.drawingService.baseCtx.canvas,
                topLeftCorner.x,
                topLeftCorner.y,
                EYE_DROPPER_IMAGE_DATA_HALF_SIZE * 2,
                EYE_DROPPER_IMAGE_DATA_HALF_SIZE * 2,
                (0 + cornerOffset.x) * EYE_DROPPER_SCALE_FACTOR,
                (0 + cornerOffset.y) * EYE_DROPPER_SCALE_FACTOR,
                this.previewCtx.canvas.width,
                this.previewCtx.canvas.height,
            );

            const evidencePixelCorner: number = EYE_DROPPER_CANVAS_SIZE / 2;
            this.previewCtx.strokeRect(evidencePixelCorner, evidencePixelCorner, EYE_DROPPER_SCALE_FACTOR, EYE_DROPPER_SCALE_FACTOR);
        }
    }

    clearPreviewCanvas(): void {
        this.previewCtx.clearRect(0, 0, this.previewCtx.canvas.width, this.previewCtx.canvas.height);
    }
}
