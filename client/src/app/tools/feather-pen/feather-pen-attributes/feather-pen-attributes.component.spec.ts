import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { FEATHER_DEFAULT_ANGLE, FEATHER_DEFAULT_LENGTH } from '@app/classes/tools-utility';
import { FeatherPenService } from '@app/tools/feather-pen/feather-pen.service';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';
import { of } from 'rxjs/internal/observable/of';
import { FeatherPenAttributesComponent } from './feather-pen-attributes.component';

describe('FeatherPenAttributesComponent', () => {
    let component: FeatherPenAttributesComponent;
    let fixture: ComponentFixture<FeatherPenAttributesComponent>;
    let serviceSpy: jasmine.SpyObj<FeatherPenService>;

    beforeEach(async(() => {
        serviceSpy = jasmine.createSpyObj('FeatherPenService', ['setLength', 'getLength', 'setAngle', 'getAngle']);
        serviceSpy.setLength.and.returnValue();
        serviceSpy.setAngle.and.returnValue();
        serviceSpy.getAngle.and.returnValue(of(0));
        TestBed.configureTestingModule({
            declarations: [FeatherPenAttributesComponent, SliderComponent],
            providers: [{ provide: FeatherPenService, useValue: serviceSpy }],
            imports: [MatSliderModule, FormsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FeatherPenAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call getLength() on init', () => {
        expect(serviceSpy.getLength).toHaveBeenCalled();
    });

    it('should call FeatherPenService.setLength() on widthChange', () => {
        component.setLength(FEATHER_DEFAULT_LENGTH);
        expect(serviceSpy.setLength).toHaveBeenCalledWith(FEATHER_DEFAULT_LENGTH);
    });
    it('should call getAngle() on init', () => {
        expect(serviceSpy.getAngle).toHaveBeenCalled();
    });

    it('should call FeatherPenService.setLength() on widthChange', () => {
        component.setAngle(FEATHER_DEFAULT_ANGLE);
        expect(serviceSpy.setAngle).toHaveBeenCalledWith(FEATHER_DEFAULT_ANGLE);
    });
});
