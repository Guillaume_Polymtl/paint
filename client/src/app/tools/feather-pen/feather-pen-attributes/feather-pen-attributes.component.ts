import { Component, OnDestroy, OnInit } from '@angular/core';
import { FEATHER_MAX_LENGTH, FEATHER_MIN_LENGTH, MAX_ANGLE, MIN_ANGLE } from '@app/classes/tools-utility';
import { FeatherPenService } from '@app/tools/feather-pen/feather-pen.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-feather-pen-attributes',
    templateUrl: './feather-pen-attributes.component.html',
    styleUrls: ['./feather-pen-attributes.component.scss'],
})
export class FeatherPenAttributesComponent implements OnInit, OnDestroy {
    readonly title: string = 'Plume';
    readonly lengthTitle: string = 'Longueur';
    readonly angleTitle: string = 'Angle';

    length: number;
    angle: number;
    private destroyed: Subject<boolean> = new Subject();

    readonly minWidth: number = FEATHER_MIN_LENGTH;
    readonly maxWidth: number = FEATHER_MAX_LENGTH;
    readonly minAngle: number = MIN_ANGLE;
    readonly maxAngle: number = MAX_ANGLE;

    constructor(private featherPenService: FeatherPenService) {}

    ngOnInit(): void {
        this.length = this.featherPenService.getLength();
        this.featherPenService
            .getAngle()
            .pipe(takeUntil(this.destroyed))
            .subscribe((angle: number) => {
                this.angle = angle;
            });
    }
    ngOnDestroy(): void {
        this.destroyed.next();
        this.destroyed.complete();
    }

    setLength(width: number): void {
        this.featherPenService.setLength(width);
    }
    setAngle(angle: number): void {
        this.featherPenService.setAngle(angle);
    }
}
