import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { MouseButton } from '@app/classes/mouse-buttons';
import { FEATHER_DEFAULT_ANGLE, FEATHER_MAX_LENGTH, FEATHER_MIN_LENGTH, MAX_ANGLE, MIN_ANGLE } from '@app/classes/tools-utility';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { FeatherPenService } from '@app/tools/feather-pen/feather-pen.service';

// tslint:disable:no-any
// tslint:disable:no-magic-numbers

describe('FeatherPenService', () => {
    let service: FeatherPenService;
    let mouseEvent: MouseEvent;

    let drawServiceSpy: jasmine.SpyObj<DrawingService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;
    let drawLineSpy: jasmine.Spy<any>;

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);

        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawServiceSpy }],
        });
        service = TestBed.inject(FeatherPenService);
        drawLineSpy = spyOn<any>(service, 'drawLine').and.callThrough();

        // Configuration du spy du service
        // tslint:disable:no-string-literal
        service['drawingService'].baseCtx = baseCtxStub; // Jasmine doesnt copy properties with underlying data
        service['drawingService'].previewCtx = previewCtxStub;
        service['pathData'].push({ x: 348, y: 139 });
        service['pathData'].push({ x: 346, y: 139 });
        service['pathData'].push({ x: 348, y: 146 });
        service['pathData'].push({ x: 346, y: 146 });
        service['pathData'].push({ x: 344, y: 160 });

        mouseEvent = {
            offsetX: 85,
            offsetY: 35,
            button: MouseButton.Left,
        } as MouseEvent;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should clear the canvas', () => {
        service.onToolChange();
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
    });

    it('setLength should change the length if correct', () => {
        const currentLength = service.getLength();
        const expectedLength = FEATHER_DEFAULT_ANGLE - 1;
        service.setLength(expectedLength);
        expect(service.getLength()).not.toEqual(currentLength);
        expect(service.getLength()).toEqual(expectedLength);
    });

    it('setAngle should change the angle if correct', () => {
        service.setAngle(MAX_ANGLE);
        let angle = 0;
        service.getAngle().subscribe((value: number) => {
            angle = value;
        });
        expect(angle).toEqual(MAX_ANGLE);
    });

    it('setLength should set length to min if entry is smaller', () => {
        const length = FEATHER_MIN_LENGTH - 1;
        service.setLength(length);
        expect(service.getLength()).toEqual(FEATHER_MIN_LENGTH);
    });

    it('setLength should set length to max if entry is greater', () => {
        const length = FEATHER_MAX_LENGTH + 1;
        service.setLength(length);
        expect(service.getLength()).toEqual(FEATHER_MAX_LENGTH);
    });

    it('setAngle should set angle to min if entry is smaller', () => {
        const angle = MIN_ANGLE - 1;
        service.setAngle(angle);
        expect(service['angle'].getValue()).toEqual(angle + MAX_ANGLE);
    });

    it('setAngle should set angle to max if entry is greater', () => {
        const angle = MAX_ANGLE + 1;
        service.setAngle(angle);
        expect(service['angle'].getValue()).toEqual(angle - MAX_ANGLE);
    });

    it(' mouseDown should set mouseDown property to true on left click', () => {
        service.onMouseDown(mouseEvent);
        expect(service.mouseDown).toEqual(true);
    });

    it(' mouseDown should set mouseDown property to false on right click', () => {
        const mouseEventRClick = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;
        service.onMouseDown(mouseEventRClick);
        expect(service.mouseDown).toEqual(false);
    });

    it(' onMouseUp should call drawLine if mouse was already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseUp(mouseEvent);
        expect(drawLineSpy).toHaveBeenCalled();
    });

    it(' onMouseUp should not call drawLine if mouse was not already down', () => {
        service.mouseDown = false;
        service.mouseDownCoord = { x: 0, y: 0 };
        service.onMouseUp(mouseEvent);
        expect(drawLineSpy).not.toHaveBeenCalled();
    });

    it(' onMouseMove should call drawLine if mouse was already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
        expect(drawLineSpy).toHaveBeenCalled();
    });

    it(' onMouseMove should not call drawLine if mouse was not already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = false;
        service.onMouseMove(mouseEvent);
        expect(drawLineSpy).not.toHaveBeenCalled();
    });

    it(' mouseWheel should set update the angle correctly', () => {
        const wheel = {
            deltaY: 5,
            deltaX: 0,
        } as WheelEvent;
        const setAnglespy = spyOn(service, 'setAngle');
        service.onMouseWheel(wheel);
        expect(setAnglespy).toHaveBeenCalled();
    });

    it(' mouseWheel should set update on MIN_ANGLE the angle correctly', () => {
        const wheel = {
            altKey: true,
        } as WheelEvent;
        const setAnglespy = spyOn(service, 'setAngle');
        service.onMouseWheel(wheel);
        expect(setAnglespy).toHaveBeenCalled();
    });
});
