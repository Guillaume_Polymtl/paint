import { Injectable } from '@angular/core';
import { Color } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import {
    FEATHER_DEFAULT_ANGLE,
    FEATHER_DEFAULT_LENGTH,
    FEATHER_DEFAULT_WIDTH,
    FEATHER_MAX_LENGTH,
    FEATHER_MIN_LENGTH,
    MAX_ANGLE,
    MAX_ANGLE_STEP,
    MIN_ANGLE,
    MIN_ANGLE_STEP,
} from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { BehaviorSubject, Observable } from 'rxjs';

// tslint:disable:no-magic-numbers

@Injectable({
    providedIn: 'root',
})
export class FeatherPenService extends Tool {
    readonly name: ToolName = ToolName.FEATHER;
    private pathData: Vec2[];

    private length: number;
    private angle: BehaviorSubject<number>;

    color: Color;

    constructor(drawingService: DrawingService, public colorService: ColorService) {
        super(drawingService, colorService);
        this.length = FEATHER_DEFAULT_LENGTH;
        this.angle = new BehaviorSubject(FEATHER_DEFAULT_ANGLE);
        this.clearPath();
        this.onInit();
    }

    private clearPath(): void {
        this.pathData = [];
    }

    onToolChange(): void {
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        this.clearPath();
    }

    getLength(): number {
        return this.length;
    }
    getAngle(): Observable<number> {
        return this.angle;
    }

    setLength(length: number): void {
        if (length >= FEATHER_MIN_LENGTH && length <= FEATHER_MAX_LENGTH) {
            this.length = length;
        } else if (length < FEATHER_MIN_LENGTH) {
            this.length = FEATHER_MIN_LENGTH;
        } else {
            this.length = FEATHER_MAX_LENGTH;
        }
    }

    setAngle(angle: number): void {
        if (angle >= MIN_ANGLE && angle <= MAX_ANGLE) {
            this.angle.next(angle);
        } else if (angle < MIN_ANGLE) {
            this.angle.next(angle + MAX_ANGLE);
        } else {
            this.angle.next(angle - MAX_ANGLE);
        }
    }

    onMouseDown(event: MouseEvent): void {
        this.mouseDown = event.button === MouseButton.Left;
        if (this.mouseDown) {
            this.clearPath();
            this.mouseDownCoord = this.getPositionFromMouse(event);
            this.pathData.push(this.mouseDownCoord);
            this.drawingService.autoSave();
        }
    }

    onMouseUp(event: MouseEvent): void {
        if (this.mouseDown) {
            const mousePosition = this.getPositionFromMouse(event);
            this.pathData.push(mousePosition);
            this.drawLine(this.drawingService.baseCtx, this.pathData);
        }
        this.mouseDown = false;
        this.clearPath();
    }

    onMouseMove(event: MouseEvent): void {
        const mousePosition = this.getPositionFromMouse(event);
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        if (this.mouseDown) {
            this.pathData.push(mousePosition);
            this.drawLine(this.drawingService.previewCtx, this.pathData);
        }
    }

    onMouseWheel(event: WheelEvent): void {
        const changeAngle = event.altKey ? MIN_ANGLE_STEP : MIN_ANGLE_STEP * MAX_ANGLE_STEP;
        const newAngle = this.angle.getValue() + Math.sign(event.deltaY) * changeAngle;
        this.setAngle(newAngle);
    }

    private setAngles(point: Vec2, length: number): Vec2 {
        const newAngle = -this.angle.getValue() * (Math.PI / 180);
        return { x: point.x + length * Math.cos(newAngle), y: point.y + length * Math.sin(newAngle) };
    }

    private drawLine(ctx: CanvasRenderingContext2D, pathData: Vec2[]): void {
        ctx.beginPath();
        ctx.lineWidth = FEATHER_DEFAULT_WIDTH;
        ctx.lineCap = 'square';
        ctx.fillStyle = this.color.toString();
        ctx.strokeStyle = this.color.toString();
        for (let i = 0; i < pathData.length - 5; i++) {
            ctx.moveTo(this.setAngles(pathData[i], this.getLength()).x, this.setAngles(pathData[i], this.getLength()).y);
            ctx.lineTo(this.setAngles(pathData[i], -this.getLength()).x, this.setAngles(pathData[i], -this.getLength()).y);
            ctx.lineTo(this.setAngles(pathData[i + 1], -this.getLength()).x, this.setAngles(pathData[i + 1], -this.getLength()).y);
            ctx.lineTo(this.setAngles(pathData[i + 1], this.getLength()).x, this.setAngles(pathData[i + 1], this.getLength()).y);
            ctx.fill();
            ctx.closePath();
            ctx.stroke();
        }
    }
}
