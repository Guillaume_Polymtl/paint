import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatRadioModule } from '@angular/material/radio';
import { MatSliderModule } from '@angular/material/slider';
import { GridViewModes } from '@app/classes/grid-view';
import { GridService } from '@app/tools/grid/grid.service';
import { BehaviorSubject } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { GridAttributesComponent } from './grid-attributes.component';

// tslint:disable: no-string-literal
describe('GridAttributesComponent', () => {
    let component: GridAttributesComponent;
    let fixture: ComponentFixture<GridAttributesComponent>;
    let gridServiceSpy: jasmine.SpyObj<GridService>;

    beforeEach(async(() => {
        gridServiceSpy = jasmine.createSpyObj('GridService', [
            'getThickness',
            'setCurrentViewMode',
            'getCurrentViewMode',
            'getOpacity',
            'setThickness',
            'setOpacity',
            'handleGrid',
        ]);
        gridServiceSpy.getCurrentViewMode.and.returnValue(of(GridViewModes.Invisible));
        gridServiceSpy.setCurrentViewMode(GridViewModes.Visible);
        gridServiceSpy.setThickness.and.returnValue();
        gridServiceSpy.setOpacity.and.returnValue();
        gridServiceSpy.getThickness.and.returnValue(new BehaviorSubject<number>(0));
        TestBed.configureTestingModule({
            declarations: [GridAttributesComponent],
            providers: [
                {
                    provide: GridService,
                    useValue: gridServiceSpy,
                },
            ],
            imports: [MatRadioModule, MatGridListModule, MatSliderModule],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GridAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(' should update valid correct view mode for grid', () => {
        component.onGridViewUpdate('Visible');
        let currentMode = GridViewModes.Visible;
        component.gridService.setCurrentViewMode(GridViewModes.Invisible);
        component.gridService.getCurrentViewMode().subscribe((mode: GridViewModes) => {
            currentMode = mode;
        });
        expect(currentMode).toEqual(GridViewModes.Invisible);
    });
    it(' should not update format if get invalid format', () => {
        component.onGridViewUpdate('none');
        let currentMode = GridViewModes.Visible;
        component.gridService.setCurrentViewMode(GridViewModes.Invisible);
        component.gridService.getCurrentViewMode().subscribe((mode: GridViewModes) => {
            currentMode = mode;
        });
        expect(currentMode).toEqual(GridViewModes.Invisible);
    });
    it('setThickness should set new thickness value', () => {
        component.setThickness(0);
        expect(gridServiceSpy.setThickness).toHaveBeenCalledWith(0);
    });

    it('setThickness should NOT set new thickness value', () => {
        component.setOpacity(0);
        expect(gridServiceSpy.setOpacity).toHaveBeenCalledWith(0);
    });
});
