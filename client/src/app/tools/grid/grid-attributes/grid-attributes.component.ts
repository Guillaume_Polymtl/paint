import { Component, OnDestroy, OnInit } from '@angular/core';
import { GridViewModes } from '@app/classes/grid-view';
import { MAX_OPACITY, MAX_THICKNESS, MIN_OPACITY, MIN_THICKNESS } from '@app/classes/tools-utility';
import { GridService } from '@app/tools/grid/grid.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-grid-attributes',
    templateUrl: './grid-attributes.component.html',
    styleUrls: ['./grid-attributes.component.scss'],
})
export class GridAttributesComponent implements OnInit, OnDestroy {
    readonly title: string = 'Grille';

    thickness: number;
    opacity: number;
    gridModes: string[];
    gridViewMode: string;
    private gridViewMap: Map<string, GridViewModes>;
    private destroyed: Subject<boolean> = new Subject();

    readonly THICKNESS_SLIDER_MINIMUM: number = MIN_THICKNESS;
    readonly THICKNESS_SLIDER_MAXIMUM: number = MAX_THICKNESS;
    readonly OPACITY_SLIDER_MINIMUM: number = MIN_OPACITY;
    readonly OPACITY_SLIDER_MAXIMUM: number = MAX_OPACITY;

    constructor(public gridService: GridService) {}

    private initializeMaps(): void {
        this.gridViewMap = new Map();
        this.gridViewMap.set('Invisible', GridViewModes.Invisible);
        this.gridViewMap.set('Visible', GridViewModes.Visible);
    }

    ngOnInit(): void {
        this.initializeMaps();
        this.gridModes = Object.keys(GridViewModes);
        this.gridService
            .getCurrentViewMode()
            .pipe(takeUntil(this.destroyed))
            .subscribe((mode: GridViewModes) => {
                this.gridViewMode = mode;
            });

        this.gridService
            .getThickness()
            .pipe(takeUntil(this.destroyed))
            .subscribe((thickness: number) => {
                this.thickness = thickness;
            });
        this.opacity = this.gridService.getOpacity();
    }

    ngOnDestroy(): void {
        this.destroyed.next();
        this.destroyed.complete();
    }

    onGridViewUpdate(newGrid: string): void {
        const newMode = this.gridViewMap.get(newGrid);
        if (newMode) {
            this.gridService.setCurrentViewMode(newMode);
            this.gridService.handleGrid();
        }
    }

    setOpacity(opacity: number): void {
        this.gridService.setOpacity(opacity);
    }

    setThickness(thickness: number): void {
        this.gridService.setThickness(thickness);
    }
}
