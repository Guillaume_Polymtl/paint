import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { GridViewModes } from '@app/classes/grid-view';
import {
    DEFAULT_OPACITY,
    DEFAULT_THICKNESS,
    MAX_OPACITY,
    MAX_THICKNESS,
    MIN_THICKNESS,
    OPACITY_STEP,
    THICKNESS_STEP,
} from '@app/classes/tools-utility';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { GridService } from './grid.service';

// tslint:disable: no-string-literal
// tslint:disable: no-any

describe('GridService', () => {
    let service: GridService;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;
    let previewCtxStub: CanvasRenderingContext2D;

    beforeEach(() => {
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas']);
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawServiceSpy }],
        });
        service = TestBed.inject(GridService);
        service['drawingService'].grid = previewCtxStub;
        service['drawingService'].previewCtx = previewCtxStub;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('setThickness should not update the canvas if thickness is incorrect', () => {
        const spy = spyOn<any>(service, 'updateCanvas');
        service.setThickness(MAX_THICKNESS + THICKNESS_STEP);
        expect(service['thickness'].getValue()).toEqual(DEFAULT_THICKNESS);
        expect(spy).not.toHaveBeenCalled();
    });

    it('setThickness should update the canvas if thickness is correct', () => {
        const thickness = MAX_THICKNESS - THICKNESS_STEP;
        const spy = spyOn<any>(service, 'updateCanvas');
        service.setThickness(thickness);
        expect(service['thickness'].getValue()).toEqual(thickness);
        expect(spy).toHaveBeenCalled();
    });

    it('setOpacity should not update the canvas if opacity incorrect', () => {
        const spy = spyOn<any>(service, 'updateCanvas');
        service.setOpacity(MAX_OPACITY + OPACITY_STEP);
        expect(spy).not.toHaveBeenCalled();
        expect(service.getOpacity()).toEqual(DEFAULT_OPACITY);
    });

    it('setOpacity should update the canvas if opacity is correct ', () => {
        const opacity = MAX_OPACITY - OPACITY_STEP;
        const spy = spyOn<any>(service, 'updateCanvas');
        service.setOpacity(opacity);
        expect(service.getOpacity()).toEqual(opacity);
        expect(spy).toHaveBeenCalled();
    });

    it('handleGrid should clear canvas if current view is Invisible ', () => {
        service.setCurrentViewMode(GridViewModes.Invisible);
        service.handleGrid();
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
    });

    it('incrementThickness should increment thickness', () => {
        service.incrementThickness();
        expect(service['thickness'].getValue()).toEqual(DEFAULT_THICKNESS + THICKNESS_STEP);
    });

    it('incrementThickness should NOT increment thickness if already max', () => {
        service.setThickness(MAX_THICKNESS);
        service.incrementThickness();
        expect(service['thickness'].getValue()).toEqual(MAX_THICKNESS);
    });

    it('decrementThickness should decrement thickness', () => {
        service.decrementThickness();
        expect(service['thickness'].getValue()).toEqual(DEFAULT_THICKNESS - THICKNESS_STEP);
    });

    it('decrementThickness should not decrement thickness if already min', () => {
        service.setThickness(MIN_THICKNESS);
        service.decrementThickness();
        expect(service['thickness'].getValue()).toEqual(MIN_THICKNESS);
    });

    it('inverseMode should switch Mode Invisible to Visible', () => {
        service.setCurrentViewMode(GridViewModes.Invisible);
        service.inverseViewMode();
        expect(service['currentViewMode'].getValue()).toEqual(GridViewModes.Visible);
    });

    it('inverseMode should switch Mode Visible to Invisible', () => {
        service.setCurrentViewMode(GridViewModes.Visible);
        service.inverseViewMode();
        expect(service['currentViewMode'].getValue()).toEqual(GridViewModes.Invisible);
    });

    it('updateCanvas should call drawGrid if mode is visible', () => {
        service.setCurrentViewMode(GridViewModes.Visible);
        const drawGridSpy = spyOn<any>(service, 'drawGrid');
        service['updateCanvas']();
        expect(drawGridSpy).toHaveBeenCalled();
    });

    it('getThickness should return the thickness', () => {
        service.setThickness(MAX_THICKNESS);
        let thickness = 0;
        service.getThickness().subscribe((value: number) => {
            thickness = value;
        });
        expect(thickness).toEqual(MAX_THICKNESS);
    });

    it('getCurrentViewMode should return the current view mode', () => {
        service.setCurrentViewMode(GridViewModes.Invisible);
        let mode = GridViewModes.Visible;
        service.getCurrentViewMode().subscribe((value: GridViewModes) => {
            mode = value;
        });
        expect(mode).toEqual(GridViewModes.Invisible);
    });
});
