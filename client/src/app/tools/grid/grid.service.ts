import { Injectable } from '@angular/core';
import { GridViewModes } from '@app/classes/grid-view';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import {
    DEFAULT_OPACITY,
    DEFAULT_THICKNESS,
    MAX_OPACITY,
    MAX_THICKNESS,
    MIN_OPACITY,
    MIN_THICKNESS,
    THICKNESS_STEP,
} from '@app/classes/tools-utility';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class GridService extends Tool {
    readonly name: ToolName = ToolName.GRID;

    private thickness: BehaviorSubject<number> = new BehaviorSubject(DEFAULT_THICKNESS);
    private opacity: number = DEFAULT_OPACITY;
    private currentViewMode: BehaviorSubject<GridViewModes> = new BehaviorSubject(GridViewModes.Invisible);

    constructor(drawingService: DrawingService, colorService: ColorService) {
        super(drawingService, colorService);
    }
    getThickness(): BehaviorSubject<number> {
        return this.thickness;
    }

    getOpacity(): number {
        return this.opacity;
    }

    getCurrentViewMode(): Observable<GridViewModes> {
        return this.currentViewMode;
    }

    setCurrentViewMode(mode: GridViewModes): void {
        this.currentViewMode.next(mode);
    }

    setThickness(thickness: number): void {
        if (thickness >= MIN_THICKNESS && thickness <= MAX_THICKNESS) {
            this.thickness.next(thickness);
            this.updateCanvas();
        }
    }

    setOpacity(opacity: number): void {
        if (opacity >= MIN_OPACITY && opacity <= MAX_OPACITY) {
            this.opacity = opacity;
            this.drawingService.grid.globalAlpha = this.opacity;
            this.updateCanvas();
        }
    }

    inverseViewMode(): void {
        const currentViewMode = this.currentViewMode.getValue();
        const newViewMode = currentViewMode === GridViewModes.Visible ? GridViewModes.Invisible : GridViewModes.Visible;
        this.currentViewMode.next(newViewMode);
        this.handleGrid();
    }

    handleGrid(): void {
        if (this.currentViewMode.getValue() === GridViewModes.Visible) {
            this.drawGrid();
        } else {
            this.drawingService.clearCanvas(this.drawingService.grid);
        }
    }

    incrementThickness(): void {
        const currentThickness = this.thickness.getValue();
        if (currentThickness <= MAX_THICKNESS - THICKNESS_STEP) {
            this.thickness.next(currentThickness + THICKNESS_STEP);
            this.updateCanvas();
        }
    }

    decrementThickness(): void {
        const currentThickness = this.thickness.getValue();
        if (currentThickness >= MIN_THICKNESS + THICKNESS_STEP) {
            this.thickness.next(currentThickness - THICKNESS_STEP);
            this.updateCanvas();
        }
    }

    private updateCanvas(): void {
        this.drawingService.clearCanvas(this.drawingService.grid);
        if (this.currentViewMode.getValue() === GridViewModes.Visible) {
            this.drawGrid();
        }
    }

    private drawGrid(): void {
        const currentThickness = this.thickness.getValue();
        for (let i = 1; i < this.drawingService.previewCtx.canvas.width / currentThickness; i++) {
            this.drawingService.grid.beginPath();
            this.drawingService.grid.globalAlpha = this.opacity;
            this.drawingService.grid.moveTo(i * currentThickness, 0);
            this.drawingService.grid.lineTo(i * currentThickness, this.drawingService.previewCtx.canvas.height);
            this.drawingService.grid.stroke();
        }
        for (let j = 1; j < this.drawingService.previewCtx.canvas.height / currentThickness; j++) {
            this.drawingService.grid.beginPath();
            this.drawingService.grid.globalAlpha = this.opacity;
            this.drawingService.grid.moveTo(0, j * currentThickness);
            this.drawingService.grid.lineTo(this.drawingService.previewCtx.canvas.width, j * currentThickness);
            this.drawingService.grid.stroke();
        }
    }
}
