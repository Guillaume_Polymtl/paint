import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { LineService } from '@app/tools/line/line.service';
import { HasJunctionComponent } from '@app/tools/shared/components/has-junction/has-junction.component';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';
import { LineAttributesComponent } from './line-attributes.component';

describe('LineAttributesComponent', () => {
    const WIDTH = 0;
    let component: LineAttributesComponent;
    let fixture: ComponentFixture<LineAttributesComponent>;
    let serviceSpy: jasmine.SpyObj<LineService>;

    beforeEach(async(() => {
        serviceSpy = jasmine.createSpyObj('LineService', [
            'getHasJunction',
            'setHasJunction',
            'getWidth',
            'setWidth',
            'setJunctionDiameter',
            'getJunctionDiameter',
        ]);
        TestBed.configureTestingModule({
            declarations: [LineAttributesComponent, HasJunctionComponent, SliderComponent],
            imports: [MatSliderModule, MatSlideToggleModule],
            providers: [{ provide: LineService, useValue: serviceSpy }],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LineAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call lineService.setWidth()', () => {
        component.setWidth(WIDTH);
        expect(serviceSpy.setWidth).toHaveBeenCalled();
    });

    it('should call lineService.setJunctionDiameter', () => {
        component.setDiameter(0);
        expect(serviceSpy.setJunctionDiameter).toHaveBeenCalled();
    });

    it('should set service.hasJunction', () => {
        serviceSpy.hasJunction = false;
        component.setHasJunction(true);
        expect(serviceSpy.hasJunction).toEqual(true);
    });
});
