import { Component, OnInit } from '@angular/core';
import { LINE_MAX_JUNCTION_DIAMETER, LINE_MAX_WIDTH, LINE_MIN_JUNCTION_DIAMETER, LINE_MIN_WIDTH } from '@app/classes/tools-utility';
import { LineService } from '@app/tools/line/line.service';

@Component({
    selector: 'app-line-attributes',
    templateUrl: './line-attributes.component.html',
    styleUrls: ['./line-attributes.component.scss'],
})
export class LineAttributesComponent implements OnInit {
    readonly title: string = 'Ligne';

    width: number;
    readonly minWidth: number = LINE_MIN_WIDTH;
    readonly maxWidth: number = LINE_MAX_WIDTH;
    diameter: number;
    readonly minDiameter: number = LINE_MIN_JUNCTION_DIAMETER;
    readonly maxDiameter: number = LINE_MAX_JUNCTION_DIAMETER;
    hasJunction: boolean;

    constructor(private lineService: LineService) {}

    ngOnInit(): void {
        this.width = this.lineService.getWidth();
        this.diameter = this.lineService.getJunctionDiameter();
        this.hasJunction = this.lineService.hasJunction;
    }

    setWidth(width: number): void {
        this.lineService.setWidth(width);
    }

    setDiameter(diameter: number): void {
        this.lineService.setJunctionDiameter(diameter);
    }

    setHasJunction(hasJunction: boolean): void {
        this.lineService.hasJunction = hasJunction;
        this.hasJunction = hasJunction;
    }
}
