import { async, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { BLACK, RED } from '@app/classes/colors/color';
import { Command } from '@app/classes/commands/command';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { UndoRedoService } from '@app/services/undo-redo/undo-redo.service';
import { LineService } from './line.service';

class CommandStub extends Command {
    update(): void {
        return;
    }
    undo(): void {
        return;
    }
    redo(): void {
        return;
    }
}

// tslint:disable: no-magic-numbers
describe('LineService', () => {
    let service: LineService;
    let drawingServiceStub: DrawingService;
    let undoRedoServiceStub: UndoRedoService;
    let colorServiceStub: ColorService;
    let routerSpy: jasmine.SpyObj<Router>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [{ provide: Router, useValue: routerSpy }],
        });
    }));

    beforeEach(() => {
        routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
        colorServiceStub = new ColorService();
        drawingServiceStub = TestBed.inject(DrawingService);
        drawingServiceStub.canvas = CanvasTestHelper.canvas();
        drawingServiceStub.baseCtx = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawingServiceStub.previewCtx = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawingServiceStub.clearCanvas(drawingServiceStub.baseCtx);
        drawingServiceStub.clearCanvas(drawingServiceStub.previewCtx);

        undoRedoServiceStub = new UndoRedoService(drawingServiceStub);

        service = new LineService(undoRedoServiceStub, drawingServiceStub, colorServiceStub);
        service.color = BLACK;
    });

    it('onKeyDown should set shiftPressed to true when shift is pressed', () => {
        // Arrange
        service.shiftPressed = false;
        const simulatedShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });

        // Act
        service.onKeyDown(simulatedShiftKey);

        // Assert
        expect(service.shiftPressed).toBe(true);
    });

    it('onKeyUp should set shiftPressed to false when shift was being pressed', () => {
        // Arrange
        service.shiftPressed = false;
        const simulatedDownShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedUpKey = new KeyboardEvent('keyup', { key: 'Shift' });

        // Act
        service.onKeyDown(simulatedDownShiftKey);
        service.onKeyUp(simulatedUpKey);

        // Assert
        expect(service.shiftPressed).toBe(false);
    });

    // tslint:disable:no-any
    it('Segment is automatically drawn to current mouse position when shift key is released and path not empty', () => {
        service.shiftPressed = false;
        service.pathData = [{ x: 0, y: 0 }];
        const mouseMoveSpy = spyOn<any>(service, 'onMouseMove').and.callThrough();
        const simulatedDownShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedUpKey = new KeyboardEvent('keyup', { key: 'Shift' });
        service.onKeyDown(simulatedDownShiftKey);

        // Act
        service.onKeyUp(simulatedUpKey);

        // Assert
        expect(mouseMoveSpy).toHaveBeenCalled();
    });

    it('backspace key should do nothing when no line is drawn', () => {
        // Arrange
        undoRedoServiceStub.doneCommands = [new CommandStub(drawingServiceStub.previewCtx)];
        const simulatedDownShiftKey = new KeyboardEvent('keydown', { key: 'Backspace' });

        // Act
        service.onKeyDown(simulatedDownShiftKey);

        // Assert
        expect(undoRedoServiceStub.doneCommands.length).toBe(1);
    });

    it('backspace key should undo last segment of line when one has been drawn', () => {
        // Arrange
        service.pathData = [];
        const simulatedMouseClickEvent1: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 0,
        });
        const simulatedMouseClickEvent2: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 3,
        });
        service.onMouseClick(simulatedMouseClickEvent1);
        service.onMouseClick(simulatedMouseClickEvent2);
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Backspace' });

        // Act
        service.onKeyDown(simulatedDownKey);

        // Assert
        expect(undoRedoServiceStub.doneCommands.length).toBe(1);
        expect(undoRedoServiceStub.undoneCommands.length).toBe(1);
        expect(service.pathData.length).toBe(1);
        for (let i = 0; i < 3; ++i) {
            const imageData: ImageData = drawingServiceStub.baseCtx.getImageData(0, i, 1, 1);
            expect(imageData.data[0]).toEqual(0); // R
            expect(imageData.data[1]).toEqual(0); // G
            expect(imageData.data[2]).toEqual(0); // B
        }
    });

    it('backspace key should not undo first point', () => {
        // Arrange
        service.pathData = [{ x: 0, y: 0 }];
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Backspace' });

        // Act
        service.onKeyDown(simulatedDownKey);

        // Assert
        expect(service.pathData.length).toBe(1);
        expect(service.pathData[0]).toEqual({ x: 0, y: 0 });
    });

    it('Escape key should do nothing when line is not drawn', () => {
        // Arrange
        service.hasJunction = false;
        drawingServiceStub.clearCanvas(drawingServiceStub.baseCtx);
        drawingServiceStub.clearCanvas(drawingServiceStub.previewCtx);
        service.color = RED;
        service.pathData = [];
        service.pathData = [
            { x: 0, y: 0 },
            { x: 1, y: 0 },
        ];
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 5,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        const simulatedMouseDblClickEvent: MouseEvent = new MouseEvent('mousedblclick', {
            clientX: 0,
            clientY: 3,
        });
        service.onMouseDblClick(simulatedMouseDblClickEvent);
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Escape' });

        // Act
        service.onKeyDown(simulatedDownKey);

        // Assert
        const imageData: ImageData = drawingServiceStub.baseCtx.getImageData(0, 2, 1, 1);
        expect(imageData.data[0]).toEqual(255); // R
        expect(imageData.data[1]).toEqual(0); // G
        expect(imageData.data[2]).toEqual(0); // B
    });

    it('Escape key should erase entire line when in construction', () => {
        // Arrange
        service.hasJunction = false;
        drawingServiceStub.clearCanvas(drawingServiceStub.baseCtx);
        drawingServiceStub.clearCanvas(drawingServiceStub.previewCtx);
        service.color = RED;
        service.pathData = [];
        const simulatedMouseClickEvent1: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 0,
        });
        const simulatedMouseClickEvent2: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 3,
        });
        const simulatedMouseClickEvent3: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 5,
        });
        service.onMouseClick(simulatedMouseClickEvent1);
        service.onMouseClick(simulatedMouseClickEvent2);
        service.onMouseClick(simulatedMouseClickEvent3);
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Escape' });

        // Act
        service.onKeyDown(simulatedDownKey);

        // Assert
        for (let i = 0; i < 5; ++i) {
            const imageData: ImageData = drawingServiceStub.baseCtx.getImageData(0, i, 1, 1);
            expect(imageData.data[0]).toEqual(0); // R
            expect(imageData.data[1]).toEqual(0); // G
            expect(imageData.data[2]).toEqual(0); // B
        }
    });

    it('Escape key should only delete the line in construction and no other line', () => {
        // Arrange
        service.hasJunction = false;
        drawingServiceStub.clearCanvas(drawingServiceStub.baseCtx);
        drawingServiceStub.clearCanvas(drawingServiceStub.previewCtx);
        service.color = RED;
        service.pathData = [];
        service.pathData = [
            { x: 0, y: 0 },
            { x: 1, y: 0 },
        ];
        const simulatedMouseClickEvent1: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 5,
        });
        const simulatedMouseDblClickEvent: MouseEvent = new MouseEvent('mousedblclick', {
            clientX: 0,
            clientY: 3,
        });
        const simulatedMouseClickEvent2: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 6,
        });
        const simulatedMouseClickEvent3: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 10,
        });
        service.onMouseClick(simulatedMouseClickEvent1);
        service.onMouseDblClick(simulatedMouseDblClickEvent);
        service.onMouseClick(simulatedMouseClickEvent2);
        service.onMouseClick(simulatedMouseClickEvent3);
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Escape' });

        // Act
        service.onKeyDown(simulatedDownKey);

        // Assert
        for (let i = 0; i < 5; ++i) {
            const imageData: ImageData = drawingServiceStub.baseCtx.getImageData(0, i, 1, 1);
            expect(imageData.data[0]).toEqual(255); // R
            expect(imageData.data[1]).toEqual(0); // G
            expect(imageData.data[2]).toEqual(0); // B
        }
    });
});
