import { async, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { BLACK, RED } from '@app/classes/colors/color';
import { LineCommand } from '@app/classes/commands/line-command';
import { LINE_MAX_JUNCTION_DIAMETER, LINE_MAX_WIDTH, LINE_MIN_JUNCTION_DIAMETER, LINE_MIN_WIDTH } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { UndoRedoService } from '@app/services/undo-redo/undo-redo.service';
import { LineService } from './line.service';

// tslint:disable: no-magic-numbers
// tslint:disable:no-any
describe('LineService', () => {
    let service: LineService;
    let drawingServiceStub: DrawingService;
    let undoRedoServiceStub: UndoRedoService;
    let arcSpy: jasmine.Spy<any>;
    let colorService: ColorService;
    let routerSpy: jasmine.SpyObj<Router>;

    beforeEach(async(() => {
        routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
        TestBed.configureTestingModule({
            providers: [{ provide: Router, useValue: routerSpy }],
        });
    }));

    beforeEach(() => {
        colorService = new ColorService();
        drawingServiceStub = TestBed.inject(DrawingService);
        drawingServiceStub.canvas = CanvasTestHelper.canvas();
        drawingServiceStub.baseCtx = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawingServiceStub.previewCtx = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawingServiceStub.clearCanvas(drawingServiceStub.baseCtx);
        drawingServiceStub.clearCanvas(drawingServiceStub.previewCtx);

        undoRedoServiceStub = new UndoRedoService(drawingServiceStub);

        arcSpy = spyOn<any>(drawingServiceStub.baseCtx, 'arc');
        service = new LineService(undoRedoServiceStub, drawingServiceStub, colorService);
        service.color = BLACK;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('init should reset service properties', () => {
        // Arrange
        service.firstPointSet = true;
        service.lastMousePositions = { prevNormal: randomVec2(), prevAligned: randomVec2() };
        service.shiftPressed = true;
        service.pathData = [randomVec2(), randomVec2()];

        // Act
        service.init();

        // Assert
        expect(service.firstPointSet).toBe(false);
        expect(service.lastMousePositions.prevNormal).toEqual({ x: 0, y: 0 });
        expect(service.lastMousePositions.prevAligned).toEqual({ x: 0, y: 0 });
        expect(service.shiftPressed).toBe(false);
        expect(service.pathData.length).toBe(0);
    });

    it('setWidth should change width if correct', () => {
        // Arrange

        // Act
        service.setWidth(LINE_MAX_WIDTH - 1);

        // Assert
        expect(service.getWidth()).toBe(LINE_MAX_WIDTH - 1);
    });

    it('setWidth should change width to max possible value if entry is greater', () => {
        // Arrange

        // Act
        service.setWidth(LINE_MAX_WIDTH + 10);

        // Assert
        expect(service.getWidth()).toBe(LINE_MAX_WIDTH);
    });

    it('setWidth should change width to min possible value if entry is smaller', () => {
        // Arrange

        // Act
        service.setWidth(LINE_MIN_WIDTH - 2);

        // Assert
        expect(service.getWidth()).toBe(LINE_MIN_WIDTH);
    });

    it('setJunctionDiameter should change junction diameter if correct', () => {
        // Arrange

        // Act
        service.setJunctionDiameter(LINE_MAX_JUNCTION_DIAMETER - 1);

        // Assert
        expect(service.getJunctionDiameter()).toBe(LINE_MAX_JUNCTION_DIAMETER - 1);
    });

    it('setJunctionDiameter should change junction diameter to max possible value if entry is greater', () => {
        // Arrange

        // Act
        service.setJunctionDiameter(LINE_MAX_JUNCTION_DIAMETER + 3);

        // Assert
        expect(service.getJunctionDiameter()).toBe(LINE_MAX_JUNCTION_DIAMETER);
    });

    it('setJunctionDiameter should change junction to min possible value if entry is smaller', () => {
        // Arrange

        // Act
        service.setJunctionDiameter(LINE_MIN_JUNCTION_DIAMETER - 1);

        // Assert
        expect(service.getJunctionDiameter()).toBe(LINE_MIN_JUNCTION_DIAMETER);
    });

    it('onMouseMove should not draw anything until first point is set', () => {
        // Arrange
        service.firstPointSet = false;
        service.pathData = [];
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: randomInt(),
            clientY: randomInt(),
        });

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.pathData.length).toBe(0);
    });

    it('onMouseMove should record correct last mouse position', () => {
        // Arrange
        service.firstPointSet = true;
        service.lastMousePositions.prevNormal = { x: 0, y: 0 };
        const expectedPosition = { x: randomInt(), y: randomInt() };
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: expectedPosition.x,
            clientY: expectedPosition.y,
        });

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.lastMousePositions.prevNormal).toEqual(expectedPosition);
    });

    it('onMouseMove should not definetely add a point to pathData', () => {
        // Arrange
        service.firstPointSet = true;
        const expectedPosition = { x: randomInt(), y: randomInt() };
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: expectedPosition.x,
            clientY: expectedPosition.y,
        });

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.pathData.length).toBe(0);
    });

    it('onMouseClick should add current mousePosition to pathData', () => {
        // Arrange
        service.pathData = [];
        const expectedPosition = { x: randomInt(), y: randomInt() };
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: expectedPosition.x,
            clientY: expectedPosition.y,
        });

        // Act
        service.onMouseClick(simulatedMouseClickEvent);

        // Assert
        expect(service.pathData.length).toBe(1);
        expect(service.pathData[0]).toEqual(expectedPosition);
    });

    it('firstPointSet should be true after first Mouse click', () => {
        // Arrange
        service.firstPointSet = false;
        const expectedPosition = { x: randomInt(), y: randomInt() };
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: expectedPosition.x,
            clientY: expectedPosition.y,
        });

        // Act
        service.onMouseClick(simulatedMouseClickEvent);

        // Assert
        expect(service.firstPointSet).toBe(true);
    });

    it('onMouseClick should add a LineCommand to doneCommands of undoRedoService', () => {
        // Arrange
        undoRedoServiceStub.doneCommands = [];
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: randomInt(),
            clientY: randomInt(),
        });

        // Act
        service.onMouseClick(simulatedMouseClickEvent);

        // Assert
        expect(undoRedoServiceStub.doneCommands.length).toBe(1);
        expect(undoRedoServiceStub.doneCommands[0]).toBeInstanceOf(LineCommand);
    });

    it('two times mouse click should draw a segment', () => {
        service.hasJunction = false;
        drawingServiceStub.clearCanvas(drawingServiceStub.baseCtx);
        drawingServiceStub.clearCanvas(drawingServiceStub.previewCtx);
        undoRedoServiceStub.doneCommands = [];
        const simulatedMouseClickEvent1: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 0,
        });
        const simulatedMouseClickEvent2: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 5,
        });
        service.color = RED;

        // Act
        service.onMouseClick(simulatedMouseClickEvent1);
        service.onMouseClick(simulatedMouseClickEvent2);

        // Assert
        for (let i = 0; i < 5; ++i) {
            const imageData: ImageData = drawingServiceStub.baseCtx.getImageData(0, i, 1, 1);
            expect(imageData.data[0]).toEqual(255); // R
            expect(imageData.data[1]).toEqual(0); // G
            expect(imageData.data[2]).toEqual(0); // B
        }
    });

    it('onMouseDblClick should close the line if distance <= 20 pixels', () => {
        // Arrange
        service.hasJunction = false;
        drawingServiceStub.clearCanvas(drawingServiceStub.baseCtx);
        drawingServiceStub.clearCanvas(drawingServiceStub.previewCtx);
        service.pathData = [
            { x: 0, y: 0 },
            { x: 1, y: 0 },
        ];
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 5,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        const simulatedMouseDblClickEvent: MouseEvent = new MouseEvent('mousedblclick', {
            clientX: 0,
            clientY: 3,
        });
        service.color = RED;

        // Act
        service.onMouseDblClick(simulatedMouseDblClickEvent);

        // Assert
        const imageData: ImageData = drawingServiceStub.baseCtx.getImageData(0, 2, 1, 1);
        expect(imageData.data[0]).toEqual(255); // R
        expect(imageData.data[1]).toEqual(0); // G
        expect(imageData.data[2]).toEqual(0); // B
    });

    it('pathData should be empty after double click', () => {
        // Arrange
        service.pathData = [
            { x: 0, y: 0 },
            { x: 0, y: 1 },
        ];
        const simulatedMouseDblClickEvent: MouseEvent = new MouseEvent('mousedblclick', {
            clientX: 0,
            clientY: 3,
        });

        // Act
        service.onMouseDblClick(simulatedMouseDblClickEvent);

        // Assert
        expect(service.pathData.length).toBe(0);
    });

    it('arc method of ctx should be called to draw circle when hasJunction is true', () => {
        // Arrange
        service.hasJunction = true;
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 0,
            clientY: 3,
        });

        // Act
        service.onMouseClick(simulatedMouseClickEvent);

        // Assert
        expect(arcSpy).toHaveBeenCalled();
    });
});

const randomInt = () => {
    const max = 250;
    const min = 0;
    return Math.floor(Math.random() * (max - min + 1) + min);
};

const randomVec2 = () => {
    return { x: randomInt(), y: randomInt() } as Vec2;
};
