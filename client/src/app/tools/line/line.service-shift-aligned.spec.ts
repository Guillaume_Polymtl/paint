import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { BLACK } from '@app/classes/colors/color';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { UndoRedoService } from '@app/services/undo-redo/undo-redo.service';
import { LineService } from './line.service';

// tslint:disable: no-magic-numbers
describe('LineService', () => {
    let service: LineService;
    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;
    let undoRedoServiceSpy: UndoRedoService;

    beforeEach(() => {
        TestBed.configureTestingModule({});

        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);
        undoRedoServiceSpy = new UndoRedoService(drawServiceSpy);
        drawServiceSpy.clearCanvas(drawServiceSpy.baseCtx);
        drawServiceSpy.clearCanvas(drawServiceSpy.previewCtx);

        TestBed.configureTestingModule({
            providers: [
                { provide: DrawingService, useValue: drawServiceSpy },
                { provide: UndoRedoService, useValue: undoRedoServiceSpy },
            ],
        });

        service = TestBed.inject(LineService);
        // tslint:disable:no-string-literal
        service['drawingService'].baseCtx = baseCtxStub; // Jasmine doesnt copy properties with underlying data
        service['drawingService'].previewCtx = previewCtxStub;

        service.color = BLACK;
    });

    it('Coordinates are computed correctly for angle 0 when shift is pressed', () => {
        // Arrange
        service.pathData = [];
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 2,
            clientY: 2,
        });
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: 4,
            clientY: 2,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        service.onKeyDown(simulatedDownKey);

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.lastMousePositions.prevAligned.x).toBe(4);
        expect(service.lastMousePositions.prevAligned.y).toBe(2);
        expect(service.pathData.length).toBe(1);
    });

    it('Coordinates are computed correctly for angle 180 when shift is pressed', () => {
        // Arrange
        service.pathData = [];
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 4,
            clientY: 2,
        });
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: 2,
            clientY: 2,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        service.onKeyDown(simulatedDownKey);

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.lastMousePositions.prevAligned.x).toBe(2);
        expect(service.lastMousePositions.prevAligned.y).toBe(2);
        expect(service.pathData.length).toBe(1);
    });

    it('Coordinates are computed correctly for angle 90 when shift is pressed', () => {
        // Arrange
        service.pathData = [];
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 2,
            clientY: 4,
        });
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: 2,
            clientY: 2,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        service.onKeyDown(simulatedDownKey);

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.lastMousePositions.prevAligned.x).toBe(2);
        expect(service.lastMousePositions.prevAligned.y).toBe(2);
        expect(service.pathData.length).toBe(1);
    });

    it('Coordinates are computed correctly for angle -90 when shift is pressed', () => {
        // Arrange
        service.pathData = [];
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 2,
            clientY: 4,
        });
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: 2,
            clientY: 6,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        service.onKeyDown(simulatedDownKey);

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.lastMousePositions.prevAligned.x).toBe(2);
        expect(service.lastMousePositions.prevAligned.y).toBe(6);
        expect(service.pathData.length).toBe(1);
    });

    it('Coordinates are computed correctly for angle 45 when shift is pressed', () => {
        // Arrange
        service.pathData = [];
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 4,
            clientY: 4,
        });
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: 6,
            clientY: 2,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        service.onKeyDown(simulatedDownKey);

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.lastMousePositions.prevAligned.x).toBe(6);
        expect(service.lastMousePositions.prevAligned.y).toBe(2);
        expect(service.pathData.length).toBe(1);
    });

    it('Coordinates are computed correctly for angle -45 when shift is pressed', () => {
        // Arrange
        service.pathData = [];
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 4,
            clientY: 4,
        });
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: 6,
            clientY: 6,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        service.onKeyDown(simulatedDownKey);

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.lastMousePositions.prevAligned.x).toBe(6);
        expect(service.lastMousePositions.prevAligned.y).toBe(6);
        expect(service.pathData.length).toBe(1);
    });

    it('Coordinates are computed correctly for angle 135 when shift is pressed', () => {
        // Arrange
        service.pathData = [];
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 4,
            clientY: 4,
        });
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: 2,
            clientY: 2,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        service.onKeyDown(simulatedDownKey);

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.lastMousePositions.prevAligned.x).toBe(2);
        expect(service.lastMousePositions.prevAligned.y).toBe(2);
        expect(service.pathData.length).toBe(1);
    });

    it('Coordinates are computed correctly for angle -135 when shift is pressed', () => {
        // Arrange
        service.pathData = [];
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 4,
            clientY: 4,
        });
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: 2,
            clientY: 5,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        service.onKeyDown(simulatedDownKey);

        // Act
        service.onMouseMove(simulatedMouseMoveEvent);

        // Assert
        expect(service.lastMousePositions.prevAligned.x).toBe(2);
        expect(service.lastMousePositions.prevAligned.y).toBe(6);
        expect(service.pathData.length).toBe(1);
    });

    it('aligned position is added to path when shift key is pressed and mouse clicked', () => {
        // Arrange
        service.pathData = [];
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedMouseClickEvent: MouseEvent = new MouseEvent('mouseclick', {
            clientX: 4,
            clientY: 4,
        });
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: 2,
            clientY: 5,
        });
        service.onMouseClick(simulatedMouseClickEvent);
        service.onKeyDown(simulatedDownKey);
        service.onMouseMove(simulatedMouseMoveEvent);

        // Act
        service.onMouseClick(simulatedMouseMoveEvent);

        // Assert
        expect(service.pathData[service.pathData.length - 1]).toEqual({ x: 2, y: 6 });
    });
});
