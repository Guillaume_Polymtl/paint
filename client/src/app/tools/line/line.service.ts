import { Injectable } from '@angular/core';
import { LineCommand } from '@app/classes/commands/line-command';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import {
    Angles,
    KeyValues,
    LINE_DEFAULT_JUNCTION_DIAMETER,
    LINE_DEFAULT_WIDTH,
    LINE_DELTA_ANGLE,
    LINE_MAX_JUNCTION_DIAMETER,
    LINE_MAX_WIDTH,
    LINE_MIN_DIST_FOR_CLOSURE,
    LINE_MIN_JUNCTION_DIAMETER,
    LINE_MIN_WIDTH,
} from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { UndoRedoService } from '@app/services/undo-redo/undo-redo.service';

@Injectable({
    providedIn: 'root',
})
export class LineService extends Tool {
    readonly name: ToolName = ToolName.LINE;
    pathData: Vec2[] = [];
    firstPointSet: boolean = false;

    private widthPx: number = LINE_DEFAULT_WIDTH;

    hasJunction: boolean = true;
    private junctionPointDiameter: number = LINE_DEFAULT_JUNCTION_DIAMETER;

    shiftPressed: boolean = false;

    lastMousePositions: { prevNormal: Vec2; prevAligned: Vec2 } = { prevNormal: { x: 0, y: 0 }, prevAligned: { x: 0, y: 0 } };

    constructor(private undoRedoService: UndoRedoService, drawingService: DrawingService, colorService: ColorService) {
        super(drawingService, colorService);
    }

    init(): void {
        this.clearPath();
        this.firstPointSet = false;
        this.shiftPressed = false;
        this.lastMousePositions.prevNormal = { x: 0, y: 0 };
        this.lastMousePositions.prevAligned = { x: 0, y: 0 };
    }

    getWidth(): number {
        return this.widthPx;
    }

    setWidth(width: number): void {
        if (width >= LINE_MIN_WIDTH && width <= LINE_MAX_WIDTH) {
            this.widthPx = width;
        } else if (width < LINE_MIN_WIDTH) {
            this.widthPx = LINE_MIN_WIDTH;
        } else {
            this.widthPx = LINE_MAX_WIDTH;
        }
    }

    getJunctionDiameter(): number {
        return this.junctionPointDiameter;
    }

    setJunctionDiameter(diameter: number): void {
        if (diameter >= LINE_MIN_JUNCTION_DIAMETER && diameter <= LINE_MAX_JUNCTION_DIAMETER) {
            this.junctionPointDiameter = diameter;
        } else if (diameter < LINE_MIN_JUNCTION_DIAMETER) {
            this.junctionPointDiameter = LINE_MIN_JUNCTION_DIAMETER;
        } else {
            this.junctionPointDiameter = LINE_MAX_JUNCTION_DIAMETER;
        }
    }

    onMouseMove(event: MouseEvent): void {
        if (this.firstPointSet) {
            const previousPoint = this.pathData[this.pathData.length - 1];
            const mousePosition = this.getPositionFromMouse(event);
            this.lastMousePositions.prevNormal = mousePosition;
            if (this.shiftPressed) {
                // Trancription dans le repère centré au dernier point de clic
                const newX = mousePosition.x - previousPoint.x;
                let newY = previousPoint.y - mousePosition.y;
                let angle = Math.atan2(newY, newX);
                // Pour trouver le multiple le plus proche de 45
                angle = Math.round((angle * Angles._180) / Math.PI);
                const isNegative = angle < 0;
                angle = Math.abs(angle);
                angle = angle + LINE_DELTA_ANGLE / 2;
                angle = angle - (angle % LINE_DELTA_ANGLE);
                angle = isNegative ? -angle : angle;
                let position: Vec2 = mousePosition;
                newY = previousPoint.y - Math.round(newX * Math.tan((angle * Math.PI) / Angles._180));
                switch (angle) {
                    case Angles._0:
                    case Angles._180:
                        position = { x: mousePosition.x, y: previousPoint.y };
                        break;
                    case Angles._90:
                    case Angles._Neg90:
                        position = { x: previousPoint.x, y: mousePosition.y };
                        break;
                    case Angles._45:
                    case Angles._Neg45:
                    case Angles._135:
                    case Angles._Neg135:
                        position = { x: mousePosition.x, y: newY };
                        break;
                }
                this.lastMousePositions.prevAligned = position;
                this.pathData.push(position);
            } else {
                this.pathData.push(mousePosition);
            }
            // On dessine sur le canvas de prévisualisation et on l'efface à chaque déplacement de la souris
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.drawLine(this.drawingService.previewCtx, this.pathData);
            this.pathData.pop();
        }
    }

    onMouseClick(event: MouseEvent): void {
        const mousePosition = this.getPositionFromMouse(event);
        this.lastMousePositions.prevNormal = mousePosition;
        // Le path ne conserve que les points de clics ou les points d'alignement
        if (!this.shiftPressed) {
            this.pathData.push(mousePosition);
        } else {
            this.pathData.push(this.lastMousePositions.prevAligned);
        }
        const lineCommand = new LineCommand(this, this.drawingService);
        const start: boolean = !this.firstPointSet;
        if (start) {
            this.firstPointSet = true;
            lineCommand.isStart = true;
        }
        this.undoRedoService.doneCommands.push(lineCommand);
        this.drawLine(this.drawingService.baseCtx, this.pathData);
        this.drawingService.autoSave();
    }

    onMouseDblClick(event: MouseEvent): void {
        const mousePosition = this.getPositionFromMouse(event);
        const length = this.pathData.length;
        /* istanbul ignore else*/
        if (length > 1) {
            const initialPoint: Vec2 = this.pathData[0];
            const xDistance = Math.abs(mousePosition.x - initialPoint.x);
            const yDistance = Math.abs(mousePosition.y - initialPoint.y);
            /* istanbul ignore else*/
            if (xDistance <= LINE_MIN_DIST_FOR_CLOSURE && yDistance <= LINE_MIN_DIST_FOR_CLOSURE) {
                // Pour annuler les deux points ajoutés par les deux derniers clics
                // ayant mené à un double clic
                // Ensuite on relie directement au point intial pour former la boucle
                this.undoRedoService.undoLastCommand();
                this.undoRedoService.undoLastCommand();
                this.pathData.push(initialPoint);
                const lineCommand = new LineCommand(this, this.drawingService);
                this.undoRedoService.doneCommands.push(lineCommand);
                this.drawLine(this.drawingService.baseCtx, this.pathData);
                this.drawingService.autoSave();
            }
            this.clearPath();
            this.firstPointSet = false;
        }
    }

    // Simuler le mouvement de la souris afin de tracer automatiquement un segment vers le curseur
    simulateMouseMove(): void {
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
            clientX: this.lastMousePositions.prevNormal.x,
            clientY: this.lastMousePositions.prevNormal.y,
        });
        if (this.pathData.length > 0) this.onMouseMove(simulatedMouseMoveEvent);
    }

    actForBackspaceKey(): void {
        /* istanbul ignore else*/
        if (this.undoRedoService.doneCommands[this.undoRedoService.doneCommands.length - 1] instanceof LineCommand) {
            const lastIndex1 = this.undoRedoService.doneCommands.length - 1;
            /* istanbul ignore else*/
            if (this.undoRedoService.doneCommands[lastIndex1] instanceof LineCommand) {
                const lineCommand: LineCommand = this.undoRedoService.doneCommands[lastIndex1] as LineCommand;
                /* istanbul ignore else*/
                if (this.firstPointSet && !lineCommand.isStart) {
                    this.undoRedoService.undoLastCommand();
                    this.simulateMouseMove();
                }
            }
        }
    }

    acctForEscapeKey(): void {
        let lastIndex = this.undoRedoService.doneCommands.length - 1;
        /* istanbul ignore else*/
        if (this.firstPointSet && this.undoRedoService.doneCommands[lastIndex] instanceof LineCommand) {
            while (this.undoRedoService.doneCommands[lastIndex] instanceof LineCommand) {
                const lineCommand: LineCommand = this.undoRedoService.doneCommands[lastIndex] as LineCommand;
                this.undoRedoService.undoLastCommand();
                --lastIndex;
                if (lineCommand.isStart) {
                    this.firstPointSet = false;
                    break;
                }
            }
            this.simulateMouseMove();
        }
    }

    onKeyDown(event: KeyboardEvent): void {
        const key: string = event.key;
        switch (key) {
            case KeyValues.Shift:
                this.shiftPressed = true;
                break;
            case KeyValues.Backspace:
                this.actForBackspaceKey();
                break;
            case KeyValues.Escape:
                this.acctForEscapeKey();
                break;
        }
    }

    onKeyUp(event: KeyboardEvent): void {
        /* istanbul ignore else*/
        if (this.shiftPressed && event.key === KeyValues.Shift) {
            this.shiftPressed = false;
            const simulatedMouseMoveEvent: MouseEvent = new MouseEvent('mousemove', {
                clientX: this.lastMousePositions.prevNormal.x,
                clientY: this.lastMousePositions.prevNormal.y,
            });
            if (this.pathData.length > 0) this.onMouseMove(simulatedMouseMoveEvent);
        }
    }

    private putJunction(ctx: CanvasRenderingContext2D, position: Vec2): void {
        if (this.hasJunction) {
            ctx.beginPath();
            ctx.arc(position.x, position.y, this.junctionPointDiameter / 2, 0, 2 * Math.PI);
            ctx.fillStyle = this.color.toString();
            ctx.fill();
        }
    }

    private drawLine(ctx: CanvasRenderingContext2D, path: Vec2[]): void {
        ctx.lineWidth = this.widthPx;
        ctx.beginPath();
        ctx.strokeStyle = this.color.toString();
        const numberOfPoints: number = this.pathData.length;
        const mousePosition = this.pathData[numberOfPoints - 1];
        if (numberOfPoints > 1) {
            ctx.moveTo(this.pathData[numberOfPoints - 2].x, this.pathData[numberOfPoints - 2].y);
            ctx.lineTo(mousePosition.x, mousePosition.y);
            ctx.stroke();
        }
        this.putJunction(ctx, mousePosition);
    }

    private clearPath(): void {
        this.pathData = [];
    }
}
