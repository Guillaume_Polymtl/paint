import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSliderModule } from '@angular/material/slider';
import { PencilService } from '@app/tools/pencil/pencil.service';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';
import { PencilAttributesComponent } from './pencil-attributes.component';

const VALID_WIDTH = 10;

describe('PencilAttributesComponent', () => {
    let component: PencilAttributesComponent;
    let fixture: ComponentFixture<PencilAttributesComponent>;
    let serviceSpy: jasmine.SpyObj<PencilService>;

    beforeEach(async(() => {
        serviceSpy = jasmine.createSpyObj('PencilService', ['setWidth', 'getWidth']);
        serviceSpy.setWidth.and.returnValue();
        TestBed.configureTestingModule({
            declarations: [PencilAttributesComponent, SliderComponent],
            providers: [{ provide: PencilService, useValue: serviceSpy }],
            imports: [MatSliderModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PencilAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call getWidth() on init', () => {
        expect(serviceSpy.getWidth).toHaveBeenCalled();
    });

    it('should call PencilService.setWidth() on widthChange', () => {
        component.setWidth(VALID_WIDTH);
        expect(serviceSpy.setWidth).toHaveBeenCalledWith(VALID_WIDTH);
    });
});
