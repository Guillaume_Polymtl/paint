import { Component, OnInit } from '@angular/core';
import { PENCIL_MAX_WIDTH, PENCIL_MIN_WIDTH } from '@app/classes/tools-utility';
import { PencilService } from '@app/tools/pencil/pencil.service';

@Component({
    selector: 'app-pencil-attributes',
    templateUrl: './pencil-attributes.component.html',
    styleUrls: ['./pencil-attributes.component.scss'],
})
export class PencilAttributesComponent implements OnInit {
    readonly title: string = 'Crayon';

    defaultWidth: number;
    readonly minWidth: number = PENCIL_MIN_WIDTH;
    readonly maxWidth: number = PENCIL_MAX_WIDTH;
    readonly width: string = 'Épaisseur';

    constructor(private pencilService: PencilService) {}

    ngOnInit(): void {
        this.defaultWidth = this.pencilService.getWidth();
    }

    setWidth(width: number): void {
        this.pencilService.setWidth(width);
    }
}
