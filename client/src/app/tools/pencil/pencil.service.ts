import { Injectable } from '@angular/core';
import { Color } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { PENCIL_DEFAULT_WIDTH, PENCIL_MAX_WIDTH, PENCIL_MIN_WIDTH } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';

@Injectable({
    providedIn: 'root',
})
export class PencilService extends Tool {
    readonly name: ToolName = ToolName.PENCIL;
    private pathData: Vec2[];
    private width: number = PENCIL_DEFAULT_WIDTH;
    color: Color;

    constructor(drawingService: DrawingService, public colorService: ColorService) {
        super(drawingService, colorService);
        this.clearPath();
        this.onInit();
    }

    private clearPath(): void {
        this.pathData = [];
    }

    reset(): void {
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        this.clearPath();
    }

    getWidth(): number {
        return this.width;
    }

    setWidth(width: number): void {
        if (width >= PENCIL_MIN_WIDTH && width <= PENCIL_MAX_WIDTH) {
            this.width = width;
        } else if (width < PENCIL_MIN_WIDTH) {
            this.width = PENCIL_MIN_WIDTH;
        } else {
            this.width = PENCIL_MAX_WIDTH;
        }
    }

    onMouseDown(event: MouseEvent): void {
        this.mouseDown = event.button === MouseButton.Left;
        if (this.mouseDown) {
            this.clearPath();
            this.mouseDownCoord = this.getPositionFromMouse(event);
            this.pathData.push(this.mouseDownCoord);
        }
    }

    onMouseUp(event: MouseEvent): void {
        if (this.mouseDown) {
            const mousePosition = this.getPositionFromMouse(event);
            this.pathData.push(mousePosition);
            this.drawLine(this.drawingService.baseCtx, this.pathData);
            this.drawingService.autoSave();
        }
        this.mouseDown = false;
        this.clearPath();
    }

    onMouseMove(event: MouseEvent): void {
        const mousePosition = this.getPositionFromMouse(event);
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        if (this.mouseDown) {
            this.pathData.push(mousePosition);
            // We draw on the previsualisation canvas
            this.drawLine(this.drawingService.previewCtx, this.pathData);
        }
        // Pencil round point
        const radius = this.width / 2;
        const startAngle = 0;
        const endAngle = 2 * Math.PI;
        const ctx = this.drawingService.previewCtx;
        ctx.beginPath();
        ctx.arc(mousePosition.x, mousePosition.y, radius, startAngle, endAngle);
        ctx.fillStyle = this.color.toString();
        ctx.fill();
    }

    private drawLine(ctx: CanvasRenderingContext2D, path: Vec2[]): void {
        // change size
        ctx.strokeStyle = this.color.toString();
        ctx.lineWidth = this.width;
        ctx.lineCap = 'round';
        ctx.lineJoin = 'round';
        for (let i = 0; i < path.length; i++) {
            ctx.beginPath();
            if (i) {
                ctx.moveTo(path[i - 1].x, path[i - 1].y);
            } else {
                ctx.moveTo(path[i].x - 1, path[i].y);
            }
            ctx.lineTo(path[i].x, path[i].y);
            ctx.closePath();
            ctx.stroke();
        }
    }
}
