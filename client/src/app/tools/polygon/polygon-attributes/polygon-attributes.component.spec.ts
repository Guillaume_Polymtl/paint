import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DrawTypes } from '@app/classes/tools-utility';
import { PolygonService } from '@app/tools/polygon/polygon.service';
import { DrawTypeComponent } from '@app/tools/shared/components/draw-type/draw-type.component';
import { SidesNumberComponent } from '@app/tools/shared/components/sides-number/sides-number.component';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';
import { PolygonAttributesComponent } from './polygon-attributes.component';

describe('PolygonAttributesComponent', () => {
    let component: PolygonAttributesComponent;
    let fixture: ComponentFixture<PolygonAttributesComponent>;
    let serviceSpy: jasmine.SpyObj<PolygonService>;

    beforeEach(async(() => {
        serviceSpy = jasmine.createSpyObj('PolygonService', [
            'setContourWidth',
            'setDrawType',
            'setSidesNumber',
            'getContourWidth',
            'getDrawType',
            'getSidesNumber',
        ]);
        serviceSpy.setContourWidth.and.returnValue();
        serviceSpy.setDrawType.and.returnValue();
        TestBed.configureTestingModule({
            declarations: [PolygonAttributesComponent, SliderComponent, DrawTypeComponent, SidesNumberComponent],
            providers: [{ provide: PolygonService, useValue: serviceSpy }],
            imports: [BrowserAnimationsModule, MatSliderModule, MatButtonToggleModule, MatSelectModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PolygonAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call polygonService.getContourWidth() on init', () => {
        expect(serviceSpy.getContourWidth).toHaveBeenCalled();
    });

    it('should call polygonService.getDrawType() on init', () => {
        expect(serviceSpy.getDrawType).toHaveBeenCalled();
    });

    it('should call polygonService.getSidesNumber() on init', () => {
        expect(serviceSpy.getSidesNumber).toHaveBeenCalled();
    });

    it('should call polygonService.setContourWidth()', () => {
        const width = 2;
        component.setContourWidth(width);
        expect(serviceSpy.setContourWidth).toHaveBeenCalledWith(width);
    });

    it('should call polygonService.setDrawType()', () => {
        component.setDrawType(DrawTypes.Fill);
        expect(serviceSpy.setDrawType).toHaveBeenCalledWith(DrawTypes.Fill);
    });

    it('should call polygonService.setSidesNumber()', () => {
        component.setSidesNumber(2);
        expect(serviceSpy.setSidesNumber).toHaveBeenCalledWith(2);
    });
});
