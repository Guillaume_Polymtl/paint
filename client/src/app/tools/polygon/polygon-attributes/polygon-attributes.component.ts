import { Component, OnInit } from '@angular/core';
import { DrawTypes, POLYGON_MAX_CONTOUR_WIDTH, POLYGON_MIN_CONTOUR_WIDTH } from '@app/classes/tools-utility';
import { PolygonService } from '@app/tools/polygon/polygon.service';

@Component({
    selector: 'app-polygon-attributes',
    templateUrl: './polygon-attributes.component.html',
    styleUrls: ['./polygon-attributes.component.scss'],
})
export class PolygonAttributesComponent implements OnInit {
    readonly title: string = 'Polygon';

    contourWidth: number;
    drawType: DrawTypes;
    sidesNumber: number;
    readonly minWidth: number = POLYGON_MIN_CONTOUR_WIDTH;
    readonly maxWidth: number = POLYGON_MAX_CONTOUR_WIDTH;

    constructor(public polygonService: PolygonService) {}

    ngOnInit(): void {
        this.contourWidth = this.polygonService.getContourWidth();
        this.drawType = this.polygonService.getDrawType();
        this.sidesNumber = this.polygonService.getSidesNumber();
    }

    setContourWidth(width: number): void {
        this.polygonService.setContourWidth(width);
    }

    setDrawType(drawType: DrawTypes): void {
        this.polygonService.setDrawType(drawType);
    }

    setSidesNumber(sidesNumber: number): void {
        this.polygonService.setSidesNumber(sidesNumber as number);
    }
}
