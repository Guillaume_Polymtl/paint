import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { BLACK, BLUE } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { DrawTypes, POLYGON_MAX_CONTOUR_WIDTH, POLYGON_MAX_SIDES, POLYGON_MIN_CONTOUR_WIDTH, POLYGON_MIN_SIDES } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { PolygonService } from '@app/tools/polygon/polygon.service';

// tslint:disable:no-any
// tslint:disable:max-file-line-count
describe('PolygonService', () => {
    let service: PolygonService;
    let mouseEvent: MouseEvent;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;

    let drawDashCircleSpy: jasmine.Spy<any>;
    let drawPolygonSpy: jasmine.Spy<any>;
    let polygonSpy: jasmine.Spy<any>;

    let fillSpy: jasmine.Spy<any>;
    let strokeSpy: jasmine.Spy<any>;

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);

        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawServiceSpy }],
        });
        service = TestBed.inject(PolygonService);
        drawDashCircleSpy = spyOn<any>(service, 'drawDashCircle').and.callThrough();
        drawPolygonSpy = spyOn<any>(service, 'drawPolygon').and.callThrough();
        polygonSpy = spyOn<any>(service, 'polygon').and.callThrough();

        fillSpy = spyOn<any>(previewCtxStub, 'fill');
        strokeSpy = spyOn<any>(previewCtxStub, 'stroke');

        // Configuration du spy du service
        // tslint:disable:no-string-literal
        service['drawingService'].baseCtx = baseCtxStub; // Jasmine doesnt copy properties with underlying data
        service['drawingService'].previewCtx = previewCtxStub;

        mouseEvent = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Left,
        } as MouseEvent;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('init should reset service properties', () => {
        service['mouseDown'] = true;
        service.init();
        expect(service['mouseDown']).toEqual(false);
    });

    // Setter and Getter tests

    it('setDrawType should change draw type if correct', () => {
        const currentDrawType = (service['drawType'] = DrawTypes.Contour);
        const expectedDrawType = DrawTypes.FillWithContour;
        service.setDrawType(expectedDrawType);
        expect(service.getDrawType()).not.toEqual(currentDrawType);
        expect(service.getDrawType()).toEqual(expectedDrawType);
    });

    it('setDrawType should not change draw type if incorrect', () => {
        const currentDrawType = service.getDrawType();
        const drawType = 'DrawType' as DrawTypes;
        service.setDrawType(drawType);
        expect(service.getDrawType()).toEqual(currentDrawType);
    });

    it('setContourWidth should change the contour width if correct', () => {
        const currentContourWidth = service.getContourWidth();
        const expectedContourWidth = POLYGON_MAX_CONTOUR_WIDTH - 1;
        service.setContourWidth(expectedContourWidth);
        expect(service.getContourWidth()).not.toEqual(currentContourWidth);
        expect(service.getContourWidth()).toEqual(expectedContourWidth);
    });

    it('setContourWidth should set contour width to min if entry is smaller', () => {
        const contourWidth = POLYGON_MIN_CONTOUR_WIDTH - 1;
        service.setContourWidth(contourWidth);
        expect(service.getContourWidth()).toEqual(POLYGON_MIN_CONTOUR_WIDTH);
    });

    it('setContourWidth should set contour width to max if entry is greater', () => {
        const contourWidth = POLYGON_MAX_CONTOUR_WIDTH + 1;
        service.setContourWidth(contourWidth);
        expect(service.getContourWidth()).toEqual(POLYGON_MAX_CONTOUR_WIDTH);
    });

    it('setSidesNumber should change the contour width if correct', () => {
        const currentSidesNumber = service.getSidesNumber();
        const expectedSidesNumber = POLYGON_MAX_SIDES - 1;
        service.setSidesNumber(expectedSidesNumber);
        expect(service.getSidesNumber()).not.toEqual(currentSidesNumber);
        expect(service.getSidesNumber()).toEqual(expectedSidesNumber);
    });

    it('setSidesNumber should set sides number to min if entry is smaller', () => {
        const sidesNumber = POLYGON_MIN_SIDES - 1;
        service.setSidesNumber(sidesNumber);
        expect(service.getSidesNumber()).toEqual(POLYGON_MIN_SIDES);
    });

    it('setSidesNumber should set contour sides number to max if entry is greater', () => {
        const sidesNumber = POLYGON_MAX_SIDES + 1;
        service.setSidesNumber(sidesNumber);
        expect(service.getSidesNumber()).toEqual(POLYGON_MAX_SIDES);
    });

    // Mouse events functions tests

    it(' mouseDown should set mouseDownCoord to correct position', () => {
        const expectedResult: Vec2 = service.getPositionFromMouse(mouseEvent);
        service.onMouseDown(mouseEvent);
        expect(service.mouseDownCoord).toEqual(expectedResult);
    });

    it(' mouseDown should set properties correctly to true on left click', () => {
        service.onMouseDown(mouseEvent);
        expect(service.mouseDown).toEqual(true);
    });

    it(' mouseDown should set mouseDown property to false on right click', () => {
        const mouseEventRClick = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;
        service.onMouseDown(mouseEventRClick);
        expect(service.mouseDown).toEqual(false);
    });

    it(' onMouseUp should call polygon if mouse was already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseUp(mouseEvent);
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
        expect(drawPolygonSpy).toHaveBeenCalled();
    });

    it(' onMouseUp should not call polygon if mouse was not already down', () => {
        service.mouseDown = false;
        service.onMouseUp(mouseEvent);
        expect(drawServiceSpy.clearCanvas).not.toHaveBeenCalled();
        expect(polygonSpy).not.toHaveBeenCalled();
    });

    it('perimeter should not be drawn when mouse goes up', () => {
        service.mouseDown = true;
        service.mouseDownCoord = { x: 3, y: 3 };

        service.onMouseUp(mouseEvent);

        expect(drawDashCircleSpy).not.toHaveBeenCalled();
    });

    it('Perimeter should be drawn when mouse is down', () => {
        service.onMouseDown(mouseEvent);
        service.onMouseMove(mouseEvent);
        expect(drawDashCircleSpy).toHaveBeenCalled();
    });

    it('onMouseMove should call polygon if mouse was already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        service.onMouseMove(mouseEvent);
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
        expect(polygonSpy).toHaveBeenCalled();
    });

    it('onMouseMove should not call polygon if mouse was not already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = false;
        service.onMouseMove(mouseEvent);
        expect(drawServiceSpy.clearCanvas).not.toHaveBeenCalled();
        expect(polygonSpy).not.toHaveBeenCalled();
    });

    // drawing functions

    it('framing rectangle top left coordinate should be well computed', () => {
        service.mouseDownCoord = { x: 0, y: 2 };
        const currentMousePosition = { x: 2, y: 0 };
        const expectedResult = { x: 0, y: 0 };
        const topLeftPoint = service['framingRectTopLeftPoint'](currentMousePosition);
        expect(topLeftPoint).toEqual(expectedResult);
    });

    it('framing rectangle Bottom Right coordinate should be well computed', () => {
        service.mouseDownCoord = { x: 0, y: 2 };
        const currentMousePosition = { x: 2, y: 0 };
        const expectedResult = { x: 2, y: 2 };
        const bottomRightPoint = service['framingRectBottomRightPoint'](currentMousePosition);
        expect(bottomRightPoint).toEqual(expectedResult);
    });

    it('compute Circle diameter should return the minimum between the rectangle heigth and width', () => {
        const rectTopLeftPoint = { x: 0, y: 0 };
        const rectBottomRightPoint = { x: 1, y: 2 };
        const expectedDiameter = 1;
        const diameter = service['computeCircleDiameter'](rectTopLeftPoint, rectBottomRightPoint);
        expect(diameter).toEqual(expectedDiameter);
    });

    it('Should return the dash circle center coordinates, going from top left point', () => {
        const rectTopLeftPoint = { x: 2, y: 2 };
        const radius = 1;
        const expectedCenter = { x: 3, y: 3 };
        const computedCenter: Vec2 = service['computeCircleCenterFromTopLeft'](rectTopLeftPoint, radius);
        expect(computedCenter).toEqual(expectedCenter);
    });

    it('Should return the dash circle center coordinates, going from top left point', () => {
        const rectBottomRightPoint = { x: 2, y: 2 };
        const radius = 1;
        const expectedCenter = { x: 1, y: 1 };
        const computedCenter: Vec2 = service['computeCircleCenterFromBottomRight'](rectBottomRightPoint, radius);
        expect(computedCenter).toEqual(expectedCenter);
    });

    it('Polygon should draw only contour when draw type is Contour', () => {
        service['drawType'] = DrawTypes.Contour;
        service.mouseDownCoord = { x: 0, y: 3 };
        service['mouseDown'] = true;

        service.onMouseMove(mouseEvent);
        expect(drawPolygonSpy).toHaveBeenCalled();
        expect(fillSpy).not.toHaveBeenCalled();
        expect(strokeSpy).toHaveBeenCalled();
    });

    it('Polygon should only fill polygon when drawType is Fill', () => {
        service['drawType'] = DrawTypes.Fill;
        service.mouseDownCoord = { x: 3, y: 3 };
        service['mouseDown'] = true;
        const numberCallsStroke = 1; // just for dashed circle

        service.onMouseMove(mouseEvent);
        expect(drawPolygonSpy).toHaveBeenCalled();
        expect(fillSpy).toHaveBeenCalled();
        expect(strokeSpy).toHaveBeenCalledTimes(numberCallsStroke);
    });

    it('Polygon should fill polygon and draw contour when drawType is FillWithContour', () => {
        service['drawType'] = DrawTypes.FillWithContour;
        service.mouseDownCoord = { x: 3, y: 0 };
        service['mouseDown'] = true;

        service.onMouseMove(mouseEvent);
        expect(drawPolygonSpy).toHaveBeenCalled();
        expect(fillSpy).toHaveBeenCalled();
        expect(strokeSpy).toHaveBeenCalled();
    });

    it('drawPolygon should turn adaptCircle to true if the drawtype is contour', () => {
        const ctx: CanvasRenderingContext2D = baseCtxStub;
        const center: Vec2 = { x: 2, y: 2 };
        const radius = 1;
        const sidesNumber = 3;
        const startAngle = 0;
        const anticlockwise = false;
        service['drawType'] = DrawTypes.Contour;
        service.color = BLACK;
        service.secondaryColor = BLUE;
        service['drawPolygon'](ctx, center, radius, sidesNumber, startAngle, anticlockwise);
        expect(service['adaptCircle']).toEqual(true);
    });

    it('drawPolygon should turn adaptCircle to true if the drawtype is FillWithContour', () => {
        const ctx: CanvasRenderingContext2D = baseCtxStub;
        const center: Vec2 = { x: 2, y: 2 };
        const radius = 1;
        const sidesNumber = 3;
        const startAngle = 0;
        const anticlockwise = true;
        service['drawType'] = DrawTypes.FillWithContour;
        service.color = BLACK;
        service.secondaryColor = BLUE;
        service['drawPolygon'](ctx, center, radius, sidesNumber, startAngle, anticlockwise);
        expect(service['adaptCircle']).toEqual(true);
    });

    it('Polygon should reset adaptCircle to false', () => {
        service['drawType'] = DrawTypes.Contour;
        service.onMouseMove(mouseEvent);
        expect(service['adaptCircle']).toEqual(false);
    });

    it('drawPolygon should not draw anything if sides < 3', () => {
        const ctx: CanvasRenderingContext2D = baseCtxStub;
        const saveSpy: jasmine.Spy<any> = spyOn<any>(ctx, 'save');
        const center: Vec2 = { x: 2, y: 2 };
        const radius = 1;
        const sidesNumber = 2;
        const startAngle = 0;
        const anticlockwise = false;
        service['drawPolygon'](ctx, center, radius, sidesNumber, startAngle, anticlockwise);
        expect(saveSpy).not.toHaveBeenCalled();
    });
});
