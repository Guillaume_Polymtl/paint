import { Injectable } from '@angular/core';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import {
    DrawTypes,
    POLYGON_DEFAULT_CONTOUR_WIDTH,
    POLYGON_DEFAULT_PERIMETER_COLOR,
    POLYGON_DEFAULT_PERIMETER_DASH,
    POLYGON_DEFAULT_PERIMETER_WIDTH,
    POLYGON_DEFAULT_SIDES,
    POLYGON_MAX_CONTOUR_WIDTH,
    POLYGON_MAX_SIDES,
    POLYGON_MIN_CONTOUR_WIDTH,
    POLYGON_MIN_SIDES,
} from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';

@Injectable({
    providedIn: 'root',
})
export class PolygonService extends Tool {
    readonly name: ToolName = ToolName.POLYGON;
    private drawType: DrawTypes = DrawTypes.Contour;
    private sidesNumber: number = POLYGON_DEFAULT_SIDES;
    private contourWidth: number = POLYGON_DEFAULT_CONTOUR_WIDTH;
    private perimeterWidth: number = POLYGON_DEFAULT_PERIMETER_WIDTH;
    private perimeterColor: string = POLYGON_DEFAULT_PERIMETER_COLOR;
    private perimeterDash: number = POLYGON_DEFAULT_PERIMETER_DASH;
    private radius: number = 0;
    private adaptCircle: boolean = false;

    shiftPressed: boolean = false;

    constructor(drawingService: DrawingService, colorService: ColorService) {
        super(drawingService, colorService);
    }

    init(): void {
        this.mouseDown = false;
    }

    // Getters and setters

    getDrawType(): DrawTypes {
        return this.drawType;
    }

    setDrawType(drawType: DrawTypes): void {
        if (drawType in DrawTypes) {
            this.drawType = drawType;
        }
    }

    getContourWidth(): number {
        return this.contourWidth;
    }

    setContourWidth(width: number): void {
        if (width >= POLYGON_MIN_CONTOUR_WIDTH && width <= POLYGON_MAX_CONTOUR_WIDTH) {
            this.contourWidth = width;
        } else if (width < POLYGON_MIN_CONTOUR_WIDTH) {
            this.contourWidth = POLYGON_MIN_CONTOUR_WIDTH;
        } else {
            this.contourWidth = POLYGON_MAX_CONTOUR_WIDTH;
        }
    }

    getSidesNumber(): number {
        return this.sidesNumber;
    }

    setSidesNumber(sidesNumber: number): void {
        if (sidesNumber >= POLYGON_MIN_SIDES && sidesNumber <= POLYGON_MAX_SIDES) {
            this.sidesNumber = sidesNumber;
        } else if (sidesNumber < POLYGON_MIN_SIDES) {
            this.sidesNumber = POLYGON_MIN_SIDES;
        } else {
            this.sidesNumber = POLYGON_MAX_SIDES;
        }
    }

    // Mouse events functions
    onMouseDown(event: MouseEvent): void {
        this.mouseDown = event.button === MouseButton.Left;
        if (this.mouseDown) {
            this.mouseDownCoord = this.getPositionFromMouse(event);
        }
    }

    onMouseUp(event: MouseEvent): void {
        if (this.mouseDown && event.button === MouseButton.Left) {
            this.mouseDown = false;
            const mousePosition = this.getPositionFromMouse(event);
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.polygon(this.drawingService.baseCtx, mousePosition);
            this.drawingService.autoSave();
        }
    }

    onMouseMove(event: MouseEvent): void {
        if (this.mouseDown) {
            const mousePosition = this.getPositionFromMouse(event);
            // On dessine sur le canvas de prévisualisation et on l'efface à chaque déplacement de la souris
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.polygon(this.drawingService.previewCtx, mousePosition);
        }
    }

    // drawing functions

    private framingRectTopLeftPoint(currentMousePosition: Vec2): Vec2 {
        const topLeftPoint: Vec2 = { x: 0, y: 0 };
        topLeftPoint.x = Math.min(this.mouseDownCoord.x, currentMousePosition.x);
        topLeftPoint.y = Math.min(this.mouseDownCoord.y, currentMousePosition.y);
        return topLeftPoint;
    }

    private framingRectBottomRightPoint(currentMousePosition: Vec2): Vec2 {
        const bottomRightPoint: Vec2 = { x: 0, y: 0 };
        bottomRightPoint.x = Math.max(this.mouseDownCoord.x, currentMousePosition.x);
        bottomRightPoint.y = Math.max(this.mouseDownCoord.y, currentMousePosition.y);
        return bottomRightPoint;
    }

    private computeCircleDiameter(rectTopLeftPoint: Vec2, rectBottomRightPoint: Vec2): number {
        const heigth = rectBottomRightPoint.y - rectTopLeftPoint.y;
        const width = rectBottomRightPoint.x - rectTopLeftPoint.x;
        const diameter = Math.min(heigth, width);
        return diameter;
    }

    private computeCircleCenterFromTopLeft(topLeftPoint: Vec2, radius: number): Vec2 {
        const center: Vec2 = { x: 0, y: 0 };
        center.x = topLeftPoint.x + radius;
        center.y = topLeftPoint.y + radius;
        return center;
    }

    private computeCircleCenterFromBottomRight(bottomRightPoint: Vec2, radius: number): Vec2 {
        const center: Vec2 = { x: 0, y: 0 };
        center.x = bottomRightPoint.x - radius;
        center.y = bottomRightPoint.y - radius;
        return center;
    }

    private drawDashCircle(ctx: CanvasRenderingContext2D, centerPosition: Vec2, radius: number): void {
        ctx.beginPath();
        ctx.setLineDash([this.perimeterDash, this.perimeterDash]);
        ctx.strokeStyle = this.perimeterColor;
        ctx.lineWidth = this.perimeterWidth;
        ctx.arc(centerPosition.x, centerPosition.y, radius, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.setLineDash([]);
    }

    private drawPolygon(
        ctx: CanvasRenderingContext2D,
        center: Vec2,
        radius: number,
        sidesNumber: number,
        startAngle: number,
        anticlockwise: boolean,
    ): void {
        // radius correspond à la moitié du diamètre calculateCircleDiameter
        // sides est le nombre de cotés, donné par l'utilisateur
        ctx.beginPath();
        ctx.strokeStyle = this.secondaryColor.toString();
        ctx.fillStyle = this.color.toString();
        ctx.lineWidth = this.contourWidth;

        if (sidesNumber < POLYGON_MIN_SIDES) return;
        let a = (Math.PI * 2) / sidesNumber;
        a = anticlockwise ? -a : a;
        ctx.save();
        ctx.translate(center.x, center.y);
        ctx.rotate(startAngle);
        ctx.moveTo(radius, 0);
        for (let i = 1; i < sidesNumber; i++) {
            ctx.lineTo(radius * Math.cos(a * i), radius * Math.sin(a * i));
        }
        // source: https://wp.storminthecastle.com/2013/07/24/how-you-can-draw-regular-polygons-with-the-html5-canvas-api/
        ctx.closePath();
        ctx.restore();

        switch (this.drawType) {
            case DrawTypes.Fill:
                ctx.fill();
                break;
            case DrawTypes.FillWithContour:
                ctx.fill();
            case DrawTypes.Contour:
                ctx.stroke();
                this.adaptCircle = true;
                break;
        }
    }

    private polygon(ctx: CanvasRenderingContext2D, currentMousePosition: Vec2): void {
        const framingRectTopLeftPoint: Vec2 = this.framingRectTopLeftPoint(currentMousePosition);
        const framingRectBottomRightPoint: Vec2 = this.framingRectBottomRightPoint(currentMousePosition);
        const diameter: number = this.computeCircleDiameter(framingRectTopLeftPoint, framingRectBottomRightPoint);
        this.radius = diameter / 2;

        let center: Vec2 = { x: 0, y: 0 };
        const heigth = framingRectBottomRightPoint.y - framingRectTopLeftPoint.y;
        const width = framingRectBottomRightPoint.x - framingRectTopLeftPoint.x;
        /* if the mouse is going from left to right with a horizontal framing rectangle,
           or the mouse is going from up to down with a vertical framing rectangle,
           then polygon center will be computed from the rectangle topLeftPoint coordinates. */
        if (
            (heigth < width && currentMousePosition.x > this.mouseDownCoord.x) ||
            (width < heigth && currentMousePosition.y > this.mouseDownCoord.y)
        ) {
            center = this.computeCircleCenterFromTopLeft(framingRectTopLeftPoint, this.radius);
        } else {
            /* But if the mouse is going from right to left with a horizontal framing rectangle,
           or the mouse is going from down to up with a vertical framing rectangle,
           then polygon center will be computed from the rectangle bottomRighttPoint coordinates. */
            center = this.computeCircleCenterFromBottomRight(framingRectBottomRightPoint, this.radius);
        }
        this.drawPolygon(ctx, center, this.radius, this.sidesNumber, 0, false);
        if (this.mouseDown) {
            // Circle adaptation according to polygon contour width variation
            if (this.adaptCircle) {
                // tslint:disable: no-magic-numbers
                const variation = (3 * this.contourWidth) / 5 + 1;
                this.radius += variation;
                // this.adaptRadius(variation);
                this.adaptCircle = false;
            }
            this.drawDashCircle(ctx, center, this.radius);
        }
    }
}
