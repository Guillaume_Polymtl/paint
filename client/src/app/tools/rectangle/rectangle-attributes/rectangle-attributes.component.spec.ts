import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { DrawTypes } from '@app/classes/tools-utility';
import { RectangleService } from '@app/tools/rectangle/rectangle.service';
import { DrawTypeComponent } from '@app/tools/shared/components/draw-type/draw-type.component';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';

import { RectangleAttributesComponent } from './rectangle-attributes.component';

describe('RectangleAttributesComponent', () => {
    let component: RectangleAttributesComponent;
    let fixture: ComponentFixture<RectangleAttributesComponent>;
    let serviceSpy: jasmine.SpyObj<RectangleService>;

    beforeEach(async(() => {
        serviceSpy = jasmine.createSpyObj('RectangleService', ['setContourWidth', 'setDrawType', 'getContourWidth', 'getDrawType']);
        TestBed.configureTestingModule({
            declarations: [RectangleAttributesComponent, SliderComponent, DrawTypeComponent],
            imports: [MatSliderModule, MatButtonToggleModule],
            providers: [{ provide: RectangleService, useValue: serviceSpy }],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RectangleAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call RectangleService.getContourWidth() on init', () => {
        expect(serviceSpy.getContourWidth).toHaveBeenCalled();
    });

    it('should call RectangleService.getDrawType() on init', () => {
        expect(serviceSpy.getDrawType).toHaveBeenCalled();
    });

    it('should call RectangleService.setWidth()', () => {
        const width = 5;
        component.setContourWidth(width);
        expect(serviceSpy.setContourWidth).toHaveBeenCalledWith(width);
    });

    it('should call RectangleService.setDrawType()', () => {
        component.setDrawType(DrawTypes.Fill);
        expect(serviceSpy.setDrawType).toHaveBeenCalledWith(DrawTypes.Fill);
    });
});
