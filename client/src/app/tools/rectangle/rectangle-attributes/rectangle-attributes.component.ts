import { Component, OnInit } from '@angular/core';
import { DrawTypes, RECT_MAX_CONTOUR_WIDTH, RECT_MIN_CONTOUR_WIDTH } from '@app/classes/tools-utility';
import { RectangleService } from '@app/tools/rectangle/rectangle.service';
@Component({
    selector: 'app-rectangle-attributes',
    templateUrl: './rectangle-attributes.component.html',
    styleUrls: ['./rectangle-attributes.component.scss'],
})
export class RectangleAttributesComponent implements OnInit {
    readonly title: string = 'Rectangle';

    contourWidth: number;
    drawType: DrawTypes;
    readonly minWidth: number = RECT_MIN_CONTOUR_WIDTH;
    readonly maxWidth: number = RECT_MAX_CONTOUR_WIDTH;
    readonly width: string = 'Épaisseur';

    constructor(private rectangleService: RectangleService) {}

    ngOnInit(): void {
        this.contourWidth = this.rectangleService.getContourWidth();
        this.drawType = this.rectangleService.getDrawType();
    }

    setContourWidth(width: number): void {
        this.rectangleService.setContourWidth(width);
    }

    setDrawType(drawType: DrawTypes): void {
        this.rectangleService.setDrawType(drawType);
    }
}
