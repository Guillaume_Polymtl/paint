import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { BLACK } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { DrawTypes, RECT_MAX_CONTOUR_WIDTH, RECT_MIN_CONTOUR_WIDTH } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { RectangleService } from '@app/tools/rectangle/rectangle.service';
import { of } from 'rxjs';

// tslint:disable:no-any
// tslint:disable:max-file-line-count
describe('RectangleService', () => {
    let service: RectangleService;
    let mouseEvent: MouseEvent;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;
    let colorServiceSpy: jasmine.SpyObj<ColorService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;
    let drawRectangleSpy: jasmine.Spy<any>;

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);
        drawServiceSpy.clearCanvas.and.callThrough();

        colorServiceSpy = jasmine.createSpyObj('ColorService', ['getColor', 'getSecondaryColor']);
        colorServiceSpy.getColor.and.returnValue(of(BLACK));

        TestBed.configureTestingModule({
            providers: [
                { provide: DrawingService, useValue: drawServiceSpy },
                {
                    provide: ColorService,
                    useValue: colorServiceSpy,
                },
            ],
        });
        service = TestBed.inject(RectangleService);
        drawRectangleSpy = spyOn<any>(service, 'drawRectangle').and.callThrough();

        // Configuration du spy du service
        // tslint:disable:no-string-literal
        service['drawingService'].canvas = CanvasTestHelper.canvas();
        service['drawingService'].baseCtx = baseCtxStub;
        service['drawingService'].previewCtx = previewCtxStub;

        mouseEvent = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Left,
        } as MouseEvent;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('init should reset service properties', () => {
        // Arrange
        service['shiftPressed'] = true;
        service.mouseDown = true;

        // Act
        service.init();

        // Assert
        expect(service['shiftPressed']).toEqual(false);
        expect(service.mouseDown).toEqual(false);
    });

    it('setDrawType should change draw type if correct', () => {
        // Arrange
        const currentDrawType = service.getDrawType();
        const expectedDrawType = DrawTypes.FillWithContour;

        // Act
        service.setDrawType(expectedDrawType);

        // Assert
        expect(service.getDrawType()).not.toEqual(currentDrawType);
        expect(service.getDrawType()).toEqual(expectedDrawType);
    });

    it('setDrawType should not change draw type if incorrect', () => {
        // Arrange
        const currentDrawType = service.getDrawType();
        const drawType = 'DrawTypes' as DrawTypes;

        // Act
        service.setDrawType(drawType);

        // Assert
        expect(service.getDrawType()).toEqual(currentDrawType);
    });

    it('setContourWidth should change the contour width if correct', () => {
        // Arrange
        const currentContourWidth = service.getContourWidth();
        const expectedContourWidth = RECT_MAX_CONTOUR_WIDTH - 1;

        // Act
        service.setContourWidth(expectedContourWidth);

        // Assert
        expect(service.getContourWidth()).not.toEqual(currentContourWidth);
        expect(service.getContourWidth()).toEqual(expectedContourWidth);
    });

    it('setContourWidth should set contour width to min if entry is smaller', () => {
        // Arrange
        const contourWidth = RECT_MIN_CONTOUR_WIDTH - 1;

        // Act
        service.setContourWidth(contourWidth);

        // Assert
        expect(service.getContourWidth()).toEqual(RECT_MIN_CONTOUR_WIDTH);
    });

    it('setContourWidth should set contour width to max if entry is greater', () => {
        // Arrange
        const contourWidth = RECT_MAX_CONTOUR_WIDTH + 1;

        // Act
        service.setContourWidth(contourWidth);

        // Assert
        expect(service.getContourWidth()).toEqual(RECT_MAX_CONTOUR_WIDTH);
    });

    it(' mouseDown should set mouseDownCoord to correct position', () => {
        const expectedResult: Vec2 = { x: 25, y: 25 };
        service.onMouseDown(mouseEvent);
        expect(service.mouseDownCoord).toEqual(expectedResult);
    });

    it(' mouseDown should set mouseDown property to true on left click', () => {
        service.onMouseDown(mouseEvent);
        expect(service.mouseDown).toEqual(true);
    });

    it(' mouseDown should set mouseDown property to false on right click', () => {
        const mouseEventRClick = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;
        service.onMouseDown(mouseEventRClick);
        expect(service.mouseDown).toEqual(false);
    });

    it(' onMouseUp should call drawRectangle if mouse was already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;

        service.onMouseUp(mouseEvent);
        expect(drawRectangleSpy).toHaveBeenCalled();
    });

    it(' onMouseUp should not call drawRectangle if mouse was not already down', () => {
        service.mouseDown = false;
        service.mouseDownCoord = { x: 0, y: 0 };

        service.onMouseUp(mouseEvent);
        expect(drawRectangleSpy).not.toHaveBeenCalled();
    });

    it(' onMouseMove should call drawRectangle if mouse was already down', () => {
        service['drawType'] = DrawTypes.FillWithContour;
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;

        service.onMouseMove(mouseEvent);
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
        expect(drawRectangleSpy).toHaveBeenCalled();
    });

    it(' onMouseMove should not call drawRectangle if mouse was not already down', () => {
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = false;

        service.onMouseMove(mouseEvent);
        expect(drawServiceSpy.clearCanvas).not.toHaveBeenCalled();
        expect(drawRectangleSpy).not.toHaveBeenCalled();
    });

    it('onKeyDown should set shiftPressed to true when shift is pressed', () => {
        // Arrange
        service['shiftPressed'] = false;
        service.mouseDownCoord = { x: 3, y: 0 };
        service['lastMousePosition'] = { x: 0, y: 3 };
        const simulatedShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });

        // Act
        service.onKeyDown(simulatedShiftKey);

        // Assert
        expect(service['shiftPressed']).toBe(true);
    });

    it('rectangle must be automatically drawn when shift is released and mouse down', () => {
        // Arrange
        service['shiftPressed'] = true;
        service['lastMousePosition'] = { x: 0, y: 0 };
        service.mouseDownCoord = { x: 0, y: 0 };
        service.mouseDown = true;
        const simulatedShiftKey = new KeyboardEvent('keyup', { key: 'Shift' });

        // Act
        service.onKeyUp(simulatedShiftKey);

        // Assert
        expect(drawRectangleSpy).toHaveBeenCalled();
    });

    it('onKeyUp should set shiftPressed to false when shift was being pressed', () => {
        // Arrange
        service['shiftPressed'] = false;
        service['lastMousePosition'] = { x: 0, y: 0 };
        service.mouseDownCoord = { x: 0, y: 0 };
        const simulatedDownShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const simulatedUpKey = new KeyboardEvent('keyup', { key: 'Shift' });

        // Act
        service.onKeyDown(simulatedDownShiftKey);
        service.onKeyUp(simulatedUpKey);

        // Assert
        expect(service['shiftPressed']).toBe(false);
    });

    it('drawRectangle should only fill rectangle when drawType is Fill', () => {
        // Arrange
        service['drawType'] = DrawTypes.Fill;
        service.mouseDownCoord = { x: 3, y: 3 };
        service.mouseDown = true;
        service.color = BLACK;
        mouseEvent = { offsetX: 0, offsetY: 0 } as MouseEvent;
        const width = 4;
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        // Act
        service.onMouseMove(mouseEvent);

        // Assert
        for (let i = 0; i < width; ++i) {
            for (let j = 0; j < width; ++j) {
                const imageData: ImageData = previewCtxStub.getImageData(i, j, 1, 1);
                checkBlackColor(imageData);
            }
        }
    });

    it('square top left coordinates are well computed from bottom-right to top-left', () => {
        // Arrange
        service.mouseDownCoord = { x: 5, y: 4 };
        service['shiftPressed'] = true;
        service.mouseDown = true;
        mouseEvent = { offsetX: 1, offsetY: 1 } as MouseEvent;
        const expectedCoordinates: Vec2 = { x: 1, y: 0 };
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        // Act
        const computedCoordinates: Vec2 = service.computeStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        // Assert
        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('square top left coordinates are well computed from bottom-left to top-right', () => {
        // Arrange
        service.mouseDownCoord = { x: 1, y: 6 };
        service['shiftPressed'] = true;
        service.mouseDown = true;
        mouseEvent = { offsetX: 4, offsetY: 0 } as MouseEvent;
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        const expectedCoordinates: Vec2 = { x: 1, y: 2 };
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        // Act
        const computedCoordinates: Vec2 = service.computeStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        // Assert
        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('square top left coordinates are well computed from top-right to bottom-left', () => {
        // Arrange
        service.mouseDownCoord = { x: 8, y: 0 };
        service['shiftPressed'] = true;
        service.mouseDown = true;
        mouseEvent = { offsetX: 3, offsetY: 4 } as MouseEvent;
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        const expectedCoordinates: Vec2 = { x: 3, y: 0 };
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        // Act
        const computedCoordinates: Vec2 = service.computeStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        // Assert
        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('rectangle top left coordinates are well computed from bottom-right to top-left', () => {
        // Arrange
        service.mouseDownCoord = { x: 5, y: 4 };
        service['shiftPressed'] = false;
        service.mouseDown = true;
        mouseEvent = { offsetX: 1, offsetY: 1 } as MouseEvent;
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        const expectedCoordinates: Vec2 = { x: 1, y: 1 };
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        // Act
        const computedCoordinates: Vec2 = service.computeStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        // Assert
        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('rectangle top left coordinates are well computed from bottom-left to top-right', () => {
        // Arrange
        service.mouseDownCoord = { x: 1, y: 6 };
        service['shiftPressed'] = false;
        service.mouseDown = true;
        mouseEvent = { offsetX: 4, offsetY: 0 } as MouseEvent;
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        const expectedCoordinates: Vec2 = { x: 1, y: 0 };
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        // Act
        const computedCoordinates: Vec2 = service.computeStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        // Assert
        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('rectangle top left coordinates are well computed from top-right to bottom-left', () => {
        // Arrange
        service.mouseDownCoord = { x: 8, y: 0 };
        service['shiftPressed'] = false;
        service.mouseDown = true;
        mouseEvent = { offsetX: 3, offsetY: 4 } as MouseEvent;
        const width = computeWidth(service.mouseDownCoord.x, mouseEvent.offsetX);
        const heigth = computeHeigth(service.mouseDownCoord.y, mouseEvent.offsetY);
        const expectedCoordinates: Vec2 = { x: 3, y: 0 };
        CanvasTestHelper.clearCanvas(baseCtxStub);
        CanvasTestHelper.clearCanvas(previewCtxStub);

        // Act
        const computedCoordinates: Vec2 = service.computeStartPosition(service.getPositionFromMouse(mouseEvent), width, heigth);

        // Assert
        expect(computedCoordinates).toEqual(expectedCoordinates);
    });

    it('Escape Key should erase currrently drawn shape', () => {
        // Arrange
        service.mouseDown = true;
        service.mouseDownCoord = { x: 3, y: 3 };
        mouseEvent = { offsetX: 0, offsetY: 0 } as MouseEvent;
        const simulatedDownKey = new KeyboardEvent('keydown', { key: 'Escape' });
        service.onMouseMove(mouseEvent);

        // Act
        service.onKeyDown(simulatedDownKey);

        // Assert
        expect(drawServiceSpy.clearCanvas).toHaveBeenCalled();
        expect(service.mouseDown).toEqual(false);
    });
});

// tslint:disable:no-magic-numbers
const checkBlackColor = (imageData: ImageData) => {
    const MIN_VALUE = 0;
    expect(imageData.data[0]).toEqual(MIN_VALUE); // R
    expect(imageData.data[1]).toEqual(MIN_VALUE); // G
    expect(imageData.data[2]).toEqual(MIN_VALUE); // B
    // expect(imageData.data[3]).not.toEqual(MIN_VALUE); // A
};

const computeWidth = (x1: number, x2: number) => {
    let width: number = x1 - x2;
    width = width < 0 ? --width : ++width;
    return width;
};

const computeHeigth = (y1: number, y2: number) => {
    let heigth: number = y1 - y2;
    heigth = heigth < 0 ? --heigth : ++heigth;
    return heigth;
};
