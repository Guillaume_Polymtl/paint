import { Injectable } from '@angular/core';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { DrawTypes, KeyValues, RECT_DEFAULT_CONTOUR_WIDTH, RECT_MAX_CONTOUR_WIDTH, RECT_MIN_CONTOUR_WIDTH } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';

@Injectable({
    providedIn: 'root',
})
export class RectangleService extends Tool {
    readonly name: ToolName = ToolName.RECTANGLE;
    private drawType: DrawTypes = DrawTypes.Contour;

    private contourWidth: number = RECT_DEFAULT_CONTOUR_WIDTH;

    private shiftPressed: boolean = false;
    private lastMousePosition: Vec2 = { x: 0, y: 0 };

    constructor(drawingService: DrawingService, colorService: ColorService) {
        super(drawingService, colorService);
    }

    init(): void {
        this.shiftPressed = false;
        this.mouseDown = false;
        // beep b
    }

    getDrawType(): DrawTypes {
        return this.drawType;
    }

    setDrawType(drawType: DrawTypes): void {
        this.drawType = drawType in DrawTypes ? drawType : this.drawType;
    }

    getContourWidth(): number {
        return this.contourWidth;
    }

    setContourWidth(width: number): void {
        if (width >= RECT_MIN_CONTOUR_WIDTH && width <= RECT_MAX_CONTOUR_WIDTH) {
            this.contourWidth = width;
        } else if (width < RECT_MIN_CONTOUR_WIDTH) {
            this.contourWidth = RECT_MIN_CONTOUR_WIDTH;
        } else {
            this.contourWidth = RECT_MAX_CONTOUR_WIDTH;
        }
    }

    onMouseDown(event: MouseEvent): void {
        this.mouseDown = event.button === MouseButton.Left;
        if (this.mouseDown) {
            this.mouseDownCoord = this.getPositionFromMouse(event);
        }
    }

    onMouseUp(event: MouseEvent): void {
        if (this.mouseDown) {
            const mousePosition = this.getPositionFromMouse(event);
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.drawRectangle(this.drawingService.baseCtx, mousePosition);
            this.drawingService.autoSave();
        }
        this.mouseDown = false;
    }

    onMouseMove(event: MouseEvent): void {
        if (this.mouseDown) {
            const mousePosition = this.getPositionFromMouse(event);
            this.lastMousePosition = mousePosition;

            // On dessine sur le canvas de prévisualisation et on l'efface à chaque déplacement de la souris
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.drawRectangle(this.drawingService.previewCtx, mousePosition);
        }
    }

    onKeyDown(event: KeyboardEvent): void {
        const key: string = event.key;
        /* istanbul ignore else*/
        if (key === KeyValues.Shift) {
            this.shiftPressed = true;
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.drawRectangle(this.drawingService.previewCtx, this.lastMousePosition);
        } else if (key === KeyValues.Escape && this.mouseDown) {
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            this.mouseDown = false;
        }
    }

    onKeyUp(event: KeyboardEvent): void {
        /* istanbul ignore else*/
        if (this.shiftPressed && event.key === KeyValues.Shift) {
            this.shiftPressed = false;
            if (this.mouseDown) {
                this.drawingService.clearCanvas(this.drawingService.previewCtx);
                this.drawRectangle(this.drawingService.previewCtx, this.lastMousePosition);
            }
        }
    }

    computeStartPosition(currentMousePosition: Vec2, width: number, heigth: number): Vec2 {
        const startPosition: Vec2 = { x: this.mouseDownCoord.x, y: this.mouseDownCoord.y };
        const positiveWidth = width < 0 ? -width : width;
        const positiveHeigth = heigth < 0 ? -heigth : heigth;
        const min: number = positiveWidth < positiveHeigth ? positiveWidth : positiveHeigth;
        // Du bas à droite à gauche en haut
        if (width > 0 && heigth > 0) {
            if (!this.shiftPressed) {
                startPosition.x = currentMousePosition.x;
                startPosition.y = currentMousePosition.y;
            } else {
                startPosition.x = this.mouseDownCoord.x - min;
                startPosition.y = this.mouseDownCoord.y - min;
            }

            // Du bas à gauche à droite en haut
        } else if (width < 0 && heigth > 0) {
            if (!this.shiftPressed) {
                startPosition.y = currentMousePosition.y;
            } else {
                startPosition.y = this.mouseDownCoord.y - min;
            }

            // Du haut à droite à gauche en bas
        } else if (width > 0 && heigth < 0) {
            if (!this.shiftPressed) {
                startPosition.x = currentMousePosition.x;
            } else {
                startPosition.x = this.mouseDownCoord.x - min;
            }
        } // Sinon du haut à gauche à droite en bas

        return startPosition;
    }

    private drawRectangle(ctx: CanvasRenderingContext2D, currentMousePosition: Vec2): void {
        let startPosition: Vec2 = { x: this.mouseDownCoord.x, y: this.mouseDownCoord.y };
        let width = this.mouseDownCoord.x - currentMousePosition.x;
        let heigth = this.mouseDownCoord.y - currentMousePosition.y;

        startPosition = this.computeStartPosition(currentMousePosition, width, heigth);
        width = width < 0 ? -width : width;
        heigth = heigth < 0 ? -heigth : heigth;
        if (this.shiftPressed) {
            const squareSize = Math.min(width, heigth);
            width = squareSize;
            heigth = squareSize;
        }
        ++width;
        ++heigth;
        ctx.strokeStyle = this.secondaryColor.toString();
        ctx.fillStyle = this.color.toString();
        ctx.lineWidth = this.contourWidth;

        switch (this.drawType) {
            case DrawTypes.Fill:
                ctx.fillRect(startPosition.x, startPosition.y, width, heigth);
                break;
            case DrawTypes.FillWithContour:
                ctx.fillRect(startPosition.x, startPosition.y, width, heigth);
            case DrawTypes.Contour:
                ctx.strokeRect(startPosition.x, startPosition.y, width, heigth);
                break;
        }
    }
}
