import { Injectable } from '@angular/core';
import { Cardinal } from '@app/classes/anchors/cardinal';
import { Color, WHITE } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { SelectionState } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { SelectionManipulationsService } from '@app/tools/selection/selection-manipulations/selection-manipulations.service';
import { SelectionService } from '@app/tools/selection/selection.service';

@Injectable({
    providedIn: 'root',
})
export class EllipseSelectionService extends SelectionService {
    name: ToolName = ToolName.SELECT_ELLIPSE;
    constructor(drawingService: DrawingService, colorService: ColorService, manipulationsService: SelectionManipulationsService) {
        super(drawingService, colorService, manipulationsService);
    }

    onMouseUp(event: MouseEvent): void {
        if (event.button === MouseButton.Left) {
            this.mouseDown = false;
            this.currentMousePosition = this.getPositionFromMouse(event);

            const northWest = this.anchors.get(Cardinal.NorthWest) as Vec2;
            const southEast = this.anchors.get(Cardinal.SouthEast) as Vec2;

            const height = southEast.y - northWest.y + 1;
            const width = southEast.x - northWest.x + 1;

            if (this.isState(SelectionState.Selecting)) {
                this.initialLeftCorner = { x: northWest.x, y: northWest.y };
                const ghostCanvas = document.createElement('canvas');
                ghostCanvas.height = height;
                ghostCanvas.width = width;
                this.dataCtx = ghostCanvas.getContext('2d') as CanvasRenderingContext2D;

                this.dataCtx.clearRect(0, 0, width, height);
                this.clipEllipse(this.dataCtx, { x: 0, y: 0 }, { x: width, y: height });
                this.dataCtx.fillStyle = WHITE.toString();
                this.dataCtx.fillRect(0, 0, width, height);
                this.dataCtx.drawImage(this.drawingService.canvas, northWest.x, northWest.y, width, height, 0, 0, width, height);

                this.finalizePreviewSelection();

                this.selectionState = SelectionState.Idle;
                this.hasSelected = true;
            } else {
                super.onMouseUp(event);
            }
        }
    }

    // fillWhite (ctx: CanvasRenderingContext2D, width: number, height: number): void {
    //     ctx.fillStyle = WHITE.toString();
    //     ctx.beginPath();
    //     ctx.ellipse(
    //         this.initialLeftCorner.x + width / 2,
    //         this.initialLeftCorner.y + height / 2,
    //         width / 2,
    //         height / 2,
    //         0,
    //         0,
    //         2 * Math.PI,
    //     );
    //     this.drawingService.baseCtx.fill();
    // }

    fillWhite(width: number, height: number): void {
        this.drawingService.baseCtx.fillStyle = WHITE.toString();
        this.drawingService.baseCtx.beginPath();
        this.drawingService.baseCtx.ellipse(
            this.initialLeftCorner.x + width / 2,
            this.initialLeftCorner.y + height / 2,
            width / 2,
            height / 2,
            0,
            0,
            2 * Math.PI,
        );
        this.drawingService.baseCtx.fill();
    }

    drawSelection(startPosition: Vec2, endPosition: Vec2, lineDash: number, lineWidth: number, color: Color): void {
        this.drawingService.previewCtx.setLineDash([lineDash]);
        this.drawingService.previewCtx.lineWidth = lineWidth;
        this.drawingService.previewCtx.strokeStyle = color.toString();

        const x = startPosition.x;
        const y = startPosition.y;
        const width = endPosition.x - startPosition.x;
        const height = endPosition.y - startPosition.y;

        this.drawingService.previewCtx.beginPath();
        this.drawingService.previewCtx.ellipse(x + width / 2, y + height / 2, width / 2, height / 2, 0, 0, 2 * Math.PI);
        this.drawingService.previewCtx.stroke();
    }

    clipEllipse(ctx: CanvasRenderingContext2D, startPosition: Vec2, endPosition: Vec2): void {
        const x = startPosition.x;
        const y = startPosition.y;
        const width = endPosition.x - startPosition.x;
        const height = endPosition.y - startPosition.y;
        ctx.beginPath();
        ctx.ellipse(x + width / 2, y + height / 2, width / 2, height / 2, 0, 0, 2 * Math.PI);
        ctx.clip();
    }
}
