import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { BLACK } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { SelectionState } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { FloodService } from '@app/tools/shared/services/flood/flood.service';
import { MagicWandService } from './magic-wand.service';

// tslint:disable: no-string-literal
// tslint:disable: no-any
describe('MagicWandService', () => {
    let service: MagicWandService;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let floodServiceSpy: jasmine.SpyObj<FloodService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;
    let width: number;
    let height: number;
    const leftClick = new MouseEvent('', { clientX: 0, clientY: 0, button: MouseButton.Left });
    const rightClick = new MouseEvent('', { clientX: 0, clientY: 0, button: MouseButton.Right });
    const middleBtnClick = new MouseEvent('', { clientX: 0, clientY: 0, button: MouseButton.Middle });

    const squareSideLength = 20;
    const squarePositions: Vec2[] = [];
    for (let x = 0; x < squareSideLength; x++) {
        for (let y = 0; y < squareSideLength; y++) {
            squarePositions.push({ x, y });
        }
    }

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        width = baseCtxStub.canvas.width;
        height = baseCtxStub.canvas.height;
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['isPristine', 'clearCanvas', 'autoSave']);
        drawingServiceSpy.baseCtx = baseCtxStub;
        drawingServiceSpy.canvas = baseCtxStub.canvas;
        drawingServiceSpy.previewCtx = previewCtxStub;
        floodServiceSpy = jasmine.createSpyObj('FloodService', [
            'getColorAtPosition',
            'getContinuousPositions',
            'getNonContinuousPositions',
            'getContinuousContourPositions',
            'getNonContinuousContourPositions',
            'colorsMatch',
        ]);
        floodServiceSpy.getColorAtPosition.and.returnValue([0, 0, 0, 0]);
        floodServiceSpy.getContinuousPositions.and.returnValue(squarePositions);
        floodServiceSpy.getNonContinuousPositions.and.returnValue([{ x: 0, y: 0 }]);
        floodServiceSpy.getContinuousContourPositions.and.returnValue([]);
        floodServiceSpy.getNonContinuousContourPositions.and.returnValue([]);
        floodServiceSpy.colorsMatch.and.returnValue(false);
        TestBed.configureTestingModule({
            providers: [
                { provide: DrawingService, useValue: drawingServiceSpy },
                { provide: FloodService, useValue: floodServiceSpy },
            ],
        });
        service = TestBed.inject(MagicWandService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('fillWhite should fill the baseCtx with white', () => {
        const position = { x: 0, y: 0 };
        service['positions'] = [position];
        drawingServiceSpy.baseCtx.fillStyle = BLACK.toString();
        drawingServiceSpy.baseCtx.fillRect(0, 0, width, height);
        let imageData = drawingServiceSpy.baseCtx.getImageData(0, 0, width, height);
        const beforeColor = service.floodService.getColorAtPosition(position, imageData);
        service.fillWhite(0, 0);
        imageData = drawingServiceSpy.baseCtx.getImageData(0, 0, width, height);
        const afterColor = service.floodService.getColorAtPosition(position, imageData);
        const result = service.floodService.colorsMatch(beforeColor, afterColor, 0);
        expect(result).toBeFalse();
    });

    it('on mouseUp, should set mouseDown to false', () => {
        service.onMouseUp(leftClick);
        expect(service.mouseDown).toBeFalse();
    });

    it('on mouseUp, if state is selecting, should call the correct flood service function to get pixels', () => {
        service['selectionState'] = SelectionState.Selecting;
        service.onMouseUp(leftClick);
        expect(floodServiceSpy.getContinuousPositions).toHaveBeenCalledTimes(1);
        service['selectionState'] = SelectionState.Selecting;
        service.onMouseUp(rightClick);
        expect(floodServiceSpy.getNonContinuousPositions).toHaveBeenCalledTimes(1);
        service['selectionState'] = SelectionState.Selecting;
        service.onMouseUp(middleBtnClick);
        expect(floodServiceSpy.getContinuousPositions).toHaveBeenCalledTimes(1);
        expect(floodServiceSpy.getNonContinuousPositions).toHaveBeenCalledTimes(1);
    });

    it('on mouseUp, if state is selecting, should call this.finalizePreviewSelectiong', () => {
        const spy = spyOn<any>(service, 'finalizePreviewSelection');
        service['selectionState'] = SelectionState.Selecting;
        service.onMouseUp(leftClick);
        expect(spy).toHaveBeenCalled();
    });

    it('on mouseUp, if state is selecting, should set state to idle', () => {
        service['selectionState'] = SelectionState.Selecting;
        service.onMouseUp(leftClick);
        const finalState = service['selectionState'];
        expect(finalState).toEqual(SelectionState.Idle);
    });

    it('on mouseUp, if state is selecting, should set hasSelected to true', () => {
        service['selectionState'] = SelectionState.Selecting;
        service.onMouseUp(leftClick);
        expect(service['hasSelected']).toBeTrue();
    });

    it("on mouseUp, if state is selecting, then it should copy the data from the baseCtx onto the service's dataCtx", () => {
        // Draw a black square
        drawingServiceSpy.baseCtx.fillStyle = BLACK.toString();
        drawingServiceSpy.baseCtx.fillRect(0, 0, squareSideLength, squareSideLength);
        // click in the middle of the square
        service['selectionState'] = SelectionState.Selecting;
        const middleLeftClick = new MouseEvent('', { clientX: squareSideLength / 2, clientY: squareSideLength / 2, button: MouseButton.Left });
        service.onMouseUp(middleLeftClick);
        expect(service['dataCtx'].canvas.height).toEqual(squareSideLength);
        expect(service['dataCtx'].canvas.width).toEqual(squareSideLength);
    });

    it('draw contour should actually draw a contour', () => {
        service['contourPositions'] = [
            { x: 0, y: 0 },
            { x: 1, y: 0 },
        ];
        const fillRectSpy = spyOn<any>(drawingServiceSpy.previewCtx, 'fillRect');
        service.drawContour();
        expect(fillRectSpy).toHaveBeenCalledTimes(2);
    });

    it('draw selection should not drawContour if the SelectionState is not Selecting', () => {
        service['selectionState'] = SelectionState.Idle;
        const spy = spyOn<any>(service, 'drawContour');
        service.drawSelection(service['currentMousePosition'], service['currentMousePosition'], 0, 0, BLACK);
        expect(spy).not.toHaveBeenCalled();
    });
});
