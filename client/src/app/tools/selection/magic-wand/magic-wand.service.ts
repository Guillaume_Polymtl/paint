import { Injectable } from '@angular/core';
import { BLACK, Color, PIXEL_ALPHA_OFFSET, PIXEL_BLUE_OFFSET, PIXEL_GREEN_OFFSET, PIXEL_RED_OFFSET, WHITE } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { SelectionState } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { MagnetService } from '@app/tools/selection/magnet/magnet.service';
import { SelectionManipulationsService } from '@app/tools/selection/selection-manipulations/selection-manipulations.service';
import { SelectionService } from '@app/tools/selection/selection.service';
import { FloodService } from '@app/tools/shared/services/flood/flood.service';
import { ShapeService } from '@app/tools/shared/services/shape/shape.service';

@Injectable({
    providedIn: 'root',
})
export class MagicWandService extends SelectionService {
    readonly name: ToolName = ToolName.MAGIC_WAND;
    private positions: Vec2[] = [];
    private contourPositions: Vec2[] = [];

    constructor(
        magnetService: MagnetService,
        drawingService: DrawingService,
        colorService: ColorService,
        public shapeService: ShapeService,
        manipulationsService: SelectionManipulationsService,
        public floodService: FloodService,
    ) {
        super(drawingService, colorService, manipulationsService);
    }

    onMouseUp(event: MouseEvent): void {
        if (event.button === MouseButton.Left || event.button === MouseButton.Right) {
            this.mouseDown = false;

            const mousePosition = this.getPositionFromMouse(event);
            this.currentMousePosition = mousePosition;

            if (this.isState(SelectionState.Selecting)) {
                this.contourPositions = [];
                if (event.button === MouseButton.Left) {
                    this.positions = this.floodService.getContinuousPositions(mousePosition, 0);
                    this.contourPositions = this.floodService.getContinuousContourPositions(mousePosition, 0);
                } else {
                    this.positions = this.floodService.getNonContinuousPositions(mousePosition, 0);
                    this.contourPositions = this.floodService.getNonContinuousContourPositions(mousePosition, 0);
                }

                const baseCtx = this.drawingService.baseCtx;
                const canvasWidth = baseCtx.canvas.width;
                const canvasHeight = baseCtx.canvas.height;
                const canvasData = this.drawingService.baseCtx.getImageData(0, 0, canvasWidth, canvasHeight);
                const clicked = this.floodService.getColorAtPosition(mousePosition, canvasData);
                const clickedColor = new Color(
                    clicked[PIXEL_RED_OFFSET],
                    clicked[PIXEL_GREEN_OFFSET],
                    clicked[PIXEL_BLUE_OFFSET],
                    clicked[PIXEL_ALPHA_OFFSET],
                );
                const northWest: Vec2 = { x: canvasWidth, y: canvasHeight };
                const southEast: Vec2 = { x: 0, y: 0 };
                for (const position of this.positions) {
                    northWest.x = Math.min(position.x, northWest.x);
                    southEast.x = Math.max(position.x, southEast.x);
                    northWest.y = Math.min(position.y, northWest.y);
                    southEast.y = Math.max(position.y, southEast.y);
                }

                this.anchors = ShapeService.getAnchors(northWest, southEast, false);
                this.initialLeftCorner = { x: northWest.x, y: northWest.y };

                const height = southEast.y - northWest.y + 1;
                const width = southEast.x - northWest.x + 1;
                const ghostCanvas = document.createElement('canvas');
                ghostCanvas.height = height;
                ghostCanvas.width = width;
                this.dataCtx = ghostCanvas.getContext('2d') as CanvasRenderingContext2D;
                for (const position of this.positions) {
                    const x = position.x - northWest.x;
                    const y = position.y - northWest.y;
                    this.dataCtx.fillStyle = clickedColor.toString();
                    this.dataCtx.fillRect(x, y, 1, 1);
                }

                this.finalizePreviewSelection();

                this.selectionState = SelectionState.Idle;
                this.hasSelected = true;
            } else {
                super.onMouseUp(event);
            }
            this.drawingService.autoSave();
        }
    }

    fillWhite(width: number, height: number): void {
        this.drawingService.baseCtx.fillStyle = WHITE.toString();
        for (const position of this.positions) {
            this.drawingService.baseCtx.fillRect(position.x, position.y, 1, 1);
        }
    }

    drawSelection(startPosition: Vec2, endPosition: Vec2, lineDash: number, lineWidth: number, color: Color): void {
        const previewCtx = this.drawingService.previewCtx;
        previewCtx.setLineDash([lineDash]);
        previewCtx.lineWidth = lineWidth;
        previewCtx.strokeStyle = color.toString();
        const width = endPosition.x - startPosition.x;
        const height = endPosition.y - startPosition.y;
        previewCtx.strokeRect(startPosition.x, startPosition.y, width, height);
        if (this.isState(SelectionState.Selecting)) {
            this.drawContour();
        }
    }

    drawContour(): void {
        const previewCtx = this.drawingService.previewCtx;
        for (let i = 0; i < this.contourPositions.length - 1; i += 2) {
            previewCtx.fillStyle = BLACK.toString();
            const position = this.contourPositions[i];
            previewCtx.fillRect(position.x, position.y, 2, 2);
            previewCtx.fillStyle = WHITE.toString();
            const nextPostion = this.contourPositions[i + 1];
            previewCtx.fillRect(nextPostion.x, nextPostion.y, 2, 2);
        }
    }
}
