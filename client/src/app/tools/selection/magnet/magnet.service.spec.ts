import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { MagnetModes } from '@app/classes/magnet-view';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { MagnetService } from './magnet.service';

// tslint:disable: no-string-literal
// tslint:disable: no-any

describe('MagnetService', () => {
    let service: MagnetService;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;

    beforeEach(() => {
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas']);
        drawServiceSpy.canvas = CanvasTestHelper.canvas();
        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawServiceSpy }],
        });
        service = TestBed.inject(MagnetService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('inverseMode should switch Mode Inactive to Active', () => {
        service.inverseMode();
        expect(service.currentMode).toEqual(MagnetModes.Active);
    });

    it('inverseMode should switch Mode Active to Inactive', () => {
        service.currentMode = MagnetModes.Active;
        service.inverseMode();
        expect(service.currentMode).toEqual(MagnetModes.Inactive);
    });

    it('FindNearestIntersection should return a correct value ', () => {
        const point = { x: 130, y: 520 };
        const expectedPoint = { x: 150, y: 500 };
        service.currentMode = MagnetModes.Active;
        const result = service.findNearestIntersection(point, false, { x: 0, y: 0 });
        expect(result).toEqual(expectedPoint);
    });

    it('FindNearestIntersection should return a correct value ', () => {
        const point = { x: 110, y: 540 };
        const expectedPoint = { x: 100, y: 550 };
        service.currentMode = MagnetModes.Active;
        const result = service.findNearestIntersection(point, false, { x: 0, y: 0 });
        expect(result).toEqual(expectedPoint);
    });

    it('FindNearestIntersection should return a correct value ', () => {
        const point = { x: 130, y: 520 };
        const expectedPoint = { x: 100, y: 520 };
        service.currentMode = MagnetModes.Active;
        const result = service.findNearestIntersection(point, true, { x: -1, y: 0 });
        expect(result).toEqual(expectedPoint);
    });

    it('FindNearestIntersection should return a correct value ', () => {
        const point = { x: 130, y: 520 };
        const expectedPoint = { x: 130, y: 500 };
        service.currentMode = MagnetModes.Active;
        const result = service.findNearestIntersection(point, true, { x: 0, y: -1 });
        expect(result).toEqual(expectedPoint);
    });

    it('FindNearestIntersection should return a correct value ', () => {
        const point = { x: 50, y: 520 };
        const expectedPoint = { x: 100, y: 520 };
        service.currentMode = MagnetModes.Active;
        const result = service.findNearestIntersection(point, true, { x: 1, y: 0 });
        expect(result).toEqual(expectedPoint);
    });

    it('FindNearestIntersection should return a correct value ', () => {
        const point = { x: 100, y: 520 };
        const expectedPoint = { x: 50, y: 520 };
        service.currentMode = MagnetModes.Active;
        const result = service.findNearestIntersection(point, true, { x: -1, y: 0 });
        expect(result).toEqual(expectedPoint);
    });

    it('FindNearestIntersection should return a correct value ', () => {
        const point = { x: 108, y: 50 };
        const expectedPoint = { x: 108, y: 100 };
        service.currentMode = MagnetModes.Active;
        const result = service.findNearestIntersection(point, true, { x: 0, y: 1 });
        expect(result).toEqual(expectedPoint);
    });

    it('FindNearestIntersection should return a correct value ', () => {
        const point = { x: 109, y: 500 };
        const expectedPoint = { x: 109, y: 450 };
        service.currentMode = MagnetModes.Active;
        const result = service.findNearestIntersection(point, true, { x: 0, y: -1 });
        expect(result).toEqual(expectedPoint);
    });
});
