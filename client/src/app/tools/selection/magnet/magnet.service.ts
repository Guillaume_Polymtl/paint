import { Injectable } from '@angular/core';
import { Cardinal } from '@app/classes/anchors/cardinal';
import { MagnetModes } from '@app/classes/magnet-view';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { GridService } from '@app/tools/grid/grid.service';

export const MAX_POINTS_PER_DIMENSION = 3;
@Injectable({
    providedIn: 'root',
})
export class MagnetService {
    anchor: Cardinal;
    currentMode: MagnetModes;

    constructor(private gridService: GridService, private drawingService: DrawingService) {
        this.anchor = Cardinal.Center;
        this.currentMode = MagnetModes.Inactive;
    }

    inverseMode(): void {
        this.currentMode = this.currentMode === MagnetModes.Active ? MagnetModes.Inactive : MagnetModes.Active;
    }

    findNearestIntersection(point: Vec2, forArrow: boolean, arrowDirection: Vec2): Vec2 {
        const thicknessSize = this.gridService.getThickness().value;
        const nearestIntersection: Vec2 = { x: 0, y: 0 };

        const leftLineX = Math.floor(point.x / thicknessSize) * thicknessSize;
        if (forArrow && arrowDirection.x === 0) nearestIntersection.x = point.x;
        else if (forArrow && leftLineX === point.x)
            nearestIntersection.x =
                arrowDirection.x < 0 ? Math.max(0, point.x - thicknessSize) : Math.min(point.x + thicknessSize, this.drawingService.canvas.width);
        else {
            const takeLeftX = forArrow ? arrowDirection.x < 0 : Math.abs(leftLineX - point.x) < thicknessSize / 2;
            nearestIntersection.x = takeLeftX ? leftLineX : leftLineX + thicknessSize;
        }

        const upLineY = Math.floor(point.y / thicknessSize) * thicknessSize;
        if (forArrow && arrowDirection.y === 0) nearestIntersection.y = point.y;
        else if (forArrow && upLineY === point.y)
            nearestIntersection.y =
                arrowDirection.y < 0 ? Math.max(0, point.y - thicknessSize) : Math.min(point.y + thicknessSize, this.drawingService.canvas.height);
        else {
            const takeUpY = forArrow ? arrowDirection.y < 0 : Math.abs(upLineY - point.y) < thicknessSize / 2;
            nearestIntersection.y = takeUpY ? upLineY : upLineY + thicknessSize;
        }

        return nearestIntersection;
    }
}
