import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { BLACK } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { SelectionState } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { SelectionService } from '@app/tools/selection/selection.service';
import { ShapeService } from '@app/tools/shared/services/shape/shape.service';
import { RectangleSelectionService } from './rectangle-selection.service';

// tslint:disable:no-any
// tslint:disable: no-string-literal
describe('RectangleSelectionService', () => {
    let service: RectangleSelectionService;
    let mouseEvent: MouseEvent;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;

    const defaultVector: Vec2 = { x: 0, y: 0 };

    beforeEach(() => {
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);

        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawingServiceSpy }],
        });
        service = TestBed.inject(RectangleSelectionService);

        // Configuration du spy du service
        mouseEvent = { offsetX: 25, offsetY: 25, button: MouseButton.Left } as MouseEvent;
        service['drawingService'].canvas = CanvasTestHelper.canvas();
        service['drawingService'].baseCtx = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        service['drawingService'].previewCtx = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        service.mouseDownCoord = defaultVector;
        service['currentMousePosition'] = service.getPositionFromMouse(mouseEvent);
        service['anchors'] = ShapeService.getAnchors(service.mouseDownCoord, service['currentMousePosition'], false);
        service['dataCtx'] = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should set mousedown and selecting to false if left mouse button goes up', () => {
        service.mouseDown = true;
        service.onMouseUp(mouseEvent);
        expect(service.mouseDown).toEqual(false);
    });

    it('should draw anchors when selecting and mouseup', () => {
        const drawAnchorsSpy: jasmine.Spy<any> = spyOn<any>(service, 'drawAnchors');
        service['selectionState'] = SelectionState.Selecting;
        service.onMouseUp(mouseEvent);
        expect(drawAnchorsSpy).toHaveBeenCalled();
    });

    it('should set to idle state and hasSelected to true when selecting and mouseUp', () => {
        service['selectionState'] = SelectionState.Selecting;
        service.onMouseUp(mouseEvent);
        expect(service['selectionState']).toEqual(SelectionState.Idle);
        expect(service['hasSelected']).toEqual(true);
    });

    it('should not set mouseDown to false if mouse up button is not left', () => {
        service.mouseDown = true;
        const mouseUpEvent = { offsetX: 0, offsetY: 0, button: MouseButton.Right } as MouseEvent;
        service.onMouseUp(mouseUpEvent);
        expect(service.mouseDown).toEqual(true);
    });

    it('should call selection mouseup if moving and mouse button goes up', () => {
        const superMouseUpSpy: jasmine.Spy = spyOn(SelectionService.prototype, 'onMouseUp').and.callThrough();
        service['selectionState'] = SelectionState.Moving;
        service.onMouseUp(mouseEvent);
        expect(superMouseUpSpy).toHaveBeenCalledWith(mouseEvent);
        expect(service['selectionState']).toEqual(SelectionState.Idle);
    });

    it('should fill initial rectangle selection with white color', () => {
        const fillRectSpy: jasmine.Spy<any> = spyOn<any>(service['drawingService'].baseCtx, 'fillRect');
        service['initialLeftCorner'] = { x: 54, y: 15 };
        const width = 2;
        const height = 1;
        service.fillWhite(width, height);
        expect(fillRectSpy).toHaveBeenCalledWith(service['initialLeftCorner'].x, service['initialLeftCorner'].y, width, height);
    });

    it('drawSelection should draw rectangle selection', () => {
        const strokeRectSpy: jasmine.Spy<any> = spyOn<any>(service['drawingService'].previewCtx, 'strokeRect');
        const startPosition: Vec2 = defaultVector;
        const endPosition: Vec2 = { x: 2, y: 2 };
        const lineDash = 2;
        const lineWidth = 2;
        const width = endPosition.x - startPosition.x;
        const height = endPosition.y - startPosition.y;
        service.drawSelection(startPosition, endPosition, lineDash, lineWidth, BLACK);
        expect(strokeRectSpy).toHaveBeenCalledWith(startPosition.x, startPosition.y, width, height);
    });
});
