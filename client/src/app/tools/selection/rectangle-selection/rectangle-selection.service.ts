import { Injectable } from '@angular/core';
import { Cardinal } from '@app/classes/anchors/cardinal';
import { Color, WHITE } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { ToolName } from '@app/classes/toolbar/tool-name';
import { SelectionState } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { SelectionManipulationsService } from '@app/tools/selection/selection-manipulations/selection-manipulations.service';
import { SelectionService } from '@app/tools/selection/selection.service';

@Injectable({
    providedIn: 'root',
})
export class RectangleSelectionService extends SelectionService {
    name: ToolName = ToolName.SELECT_RECTANGLE;
    constructor(drawingService: DrawingService, colorService: ColorService, manipulationsService: SelectionManipulationsService) {
        super(drawingService, colorService, manipulationsService);
    }

    onMouseUp(event: MouseEvent): void {
        if (event.button === MouseButton.Left) {
            this.mouseDown = false;
            this.currentMousePosition = this.getPositionFromMouse(event);

            const northWest = this.anchors.get(Cardinal.NorthWest) as Vec2;
            const southEast = this.anchors.get(Cardinal.SouthEast) as Vec2;

            const height = southEast.y - northWest.y + 1;
            const width = southEast.x - northWest.x + 1;

            if (this.isState(SelectionState.Selecting)) {
                this.initialLeftCorner = { x: northWest.x, y: northWest.y };

                const ghostCanvas = document.createElement('canvas');
                ghostCanvas.height = height;
                ghostCanvas.width = width;
                this.dataCtx = ghostCanvas.getContext('2d') as CanvasRenderingContext2D;

                this.dataCtx.clearRect(0, 0, width, height);
                this.dataCtx.fillStyle = WHITE.toString();
                this.dataCtx.fillRect(0, 0, width, height);
                this.dataCtx.drawImage(this.drawingService.canvas, northWest.x, northWest.y, width, height, 0, 0, width, height);

                this.finalizePreviewSelection();

                this.selectionState = SelectionState.Idle;
                this.hasSelected = true;
            } else {
                super.onMouseUp(event);
            }
        }
    }

    fillWhite(width: number, height: number): void {
        this.drawingService.baseCtx.fillStyle = WHITE.toString();
        this.drawingService.baseCtx.fillRect(this.initialLeftCorner.x, this.initialLeftCorner.y, width, height);
    }

    drawSelection(startPosition: Vec2, endPosition: Vec2, lineDash: number, lineWidth: number, color: Color): void {
        this.drawingService.previewCtx.setLineDash([lineDash]);
        this.drawingService.previewCtx.lineWidth = lineWidth;
        this.drawingService.previewCtx.strokeStyle = color.toString();
        const width = endPosition.x - startPosition.x;
        const height = endPosition.y - startPosition.y;
        this.drawingService.previewCtx.strokeRect(startPosition.x, startPosition.y, width, height);
    }
}
