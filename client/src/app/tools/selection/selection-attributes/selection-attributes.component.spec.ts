import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgModel } from '@angular/forms';
import { MatRippleModule } from '@angular/material/core';
import { MatGridList, MatGridTile } from '@angular/material/grid-list';
import { MatRadioButton, MatRadioGroup } from '@angular/material/radio';
import { ToolsService } from '@app/services/tools/tools.service';
import { MagnetService } from '@app/tools/selection/magnet/magnet.service';
import { RectangleSelectionService } from '@app/tools/selection/rectangle-selection/rectangle-selection.service';
import { of } from 'rxjs';
import { SelectionAttributesComponent } from './selection-attributes.component';

describe('SelectionAttributesComponent', () => {
    let component: SelectionAttributesComponent;
    let fixture: ComponentFixture<SelectionAttributesComponent>;
    let toolsServiceSpy: jasmine.SpyObj<ToolsService>;
    let rectangleSelectionServiceSpy: jasmine.SpyObj<RectangleSelectionService>;
    let magnetServiceSpy: jasmine.SpyObj<MagnetService>;

    beforeEach(async(() => {
        toolsServiceSpy = jasmine.createSpyObj('ToolsService', ['getTool']);
        rectangleSelectionServiceSpy = jasmine.createSpyObj('RectangleSelectionService', ['selectAll', 'copy', 'paste', 'cut', 'delete']);
        magnetServiceSpy = jasmine.createSpyObj('MagnetService', ['inverseMode']);
        toolsServiceSpy.getTool.and.returnValue(of(rectangleSelectionServiceSpy));
        TestBed.configureTestingModule({
            imports: [MatRippleModule],
            declarations: [SelectionAttributesComponent, MatRadioButton, MatRadioGroup, MatGridList, MatGridTile, NgModel],
            providers: [
                { provide: ToolsService, useValue: toolsServiceSpy },
                {
                    provide: MagnetService,
                    useValue: magnetServiceSpy,
                },
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SelectionAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('get tool should call toolsServiceSpy.getTool', () => {
        component.selectAll();
        expect(toolsServiceSpy.getTool).toHaveBeenCalled();
    });

    it('select all should call selectionService.selectAll', () => {
        component.selectAll();
        expect(rectangleSelectionServiceSpy.selectAll).toHaveBeenCalled();
    });

    it('copy should call selectionService.copy()', () => {
        component.copy();
        expect(rectangleSelectionServiceSpy.copy).toHaveBeenCalled();
    });

    it('paste should call selectionService.copy()', () => {
        component.paste();
        expect(rectangleSelectionServiceSpy.paste).toHaveBeenCalled();
    });

    it('cut should call selectionService.cut()', () => {
        component.cut();
        expect(rectangleSelectionServiceSpy.cut).toHaveBeenCalled();
    });

    it('delete should call selectionService.delete()', () => {
        component.delete();
        expect(rectangleSelectionServiceSpy.delete).toHaveBeenCalled();
    });
});
