import { Component, OnInit } from '@angular/core';
import { Cardinal } from '@app/classes/anchors/cardinal';
import { MagnetModes } from '@app/classes/magnet-view';
import { ToolsService } from '@app/services/tools/tools.service';
import { MagnetService } from '@app/tools/selection/magnet/magnet.service';
import { SelectionService } from '@app/tools/selection/selection.service';

@Component({
    selector: 'app-selection-attributes',
    templateUrl: './selection-attributes.component.html',
    styleUrls: ['./selection-attributes.component.scss'],
})
export class SelectionAttributesComponent implements OnInit {
    readonly title: string = 'Sélection';
    readonly magnetTitle: string = 'Magnétisme';

    anchor: Cardinal;
    anchors: string[];
    magnetModes: string[];

    constructor(public toolsService: ToolsService, public magnetService: MagnetService) {}

    ngOnInit(): void {
        this.anchor = Cardinal.Center;
        this.magnetModes = Object.values(MagnetModes);
        this.anchors = Object.values(Cardinal);
    }

    selectAll(): void {
        this.toolsService
            .getTool()
            .subscribe((tool: SelectionService) => {
                tool.selectAll();
            })
            .unsubscribe();
    }

    copy(): void {
        this.toolsService
            .getTool()
            .subscribe((tool: SelectionService) => {
                tool.copy();
            })
            .unsubscribe();
    }

    paste(): void {
        this.toolsService
            .getTool()
            .subscribe((tool: SelectionService) => {
                tool.paste();
            })
            .unsubscribe();
    }

    delete(): void {
        this.toolsService
            .getTool()
            .subscribe((tool: SelectionService) => {
                tool.delete();
            })
            .unsubscribe();
    }

    cut(): void {
        this.toolsService
            .getTool()
            .subscribe((tool: SelectionService) => {
                tool.cut();
            })
            .unsubscribe();
    }
}
