import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Cardinal } from '@app/classes/anchors/cardinal';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { MagnetModes } from '@app/classes/magnet-view';
import {
    ALT_DIVIDER_FOR_ROTATION_ANGLE,
    DEFAULT_TIMEOUT_ID,
    DIVIDER_FOR_ROTATION_ANGLE,
    KeyValues,
    SELECTION_REPEAT_DELAY,
    SELECTION_REPEAT_START_TIME,
} from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { MagnetService } from '@app/tools/selection/magnet/magnet.service';
import { ShapeService } from '@app/tools/shared/services/shape/shape.service';
import { SelectionManipulationsService } from './selection-manipulations.service';

// tslint:disable:no-any
// tslint:disable:no-string-literal
describe('SelectionManipulationsService', () => {
    let service: SelectionManipulationsService;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let magnetServiceSpy: jasmine.SpyObj<MagnetService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;
    let dataCtxStub: CanvasRenderingContext2D;

    const defaultVector: Vec2 = { x: 0, y: 0 };

    beforeEach(() => {
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas']);
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        dataCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        magnetServiceSpy = jasmine.createSpyObj('MagnetService', ['findNearestIntersection']);

        TestBed.configureTestingModule({
            providers: [
                { provide: DrawingService, useValue: drawingServiceSpy },
                { provide: MagnetService, useValue: magnetServiceSpy },
            ],
        });
        service = TestBed.inject(SelectionManipulationsService);
        service['drawingService'].canvas = CanvasTestHelper.canvas();
        service['drawingService'].baseCtx = baseCtxStub;
        service['drawingService'].previewCtx = previewCtxStub;
        service.anchors = ShapeService.getAnchors(defaultVector, { x: 25, y: 25 }, false);
    });

    it('reset should stopTimer', () => {
        const stopTimerSpy: jasmine.Spy<any> = spyOn<any>(service, 'stopTimer');
        service.reset();
        expect(stopTimerSpy).toHaveBeenCalled();
    });

    it('should compute translation vector correctly if directionPressed is false', async () => {
        const currentMousePosition: Vec2 = { x: 5, y: 9 };
        const previousMousePosition: Vec2 = { x: 12, y: 4 };
        const expectedResult: Vec2 = { x: -7, y: 5 };
        const result = service.computeTranslationVector(previousMousePosition, currentMousePosition);
        expect(result).toEqual(expectedResult);
    });

    it('should compute translation vector correctly if directionPressed is true', async () => {
        service.arrows.set(KeyValues.ArrowLeft, true);
        service.arrows.set(KeyValues.ArrowRight, true);
        service.arrows.set(KeyValues.ArrowDown, true);
        const expectedResult: Vec2 = { x: 0, y: 3 };
        const result = service.computeTranslationVector(defaultVector, defaultVector);
        expect(result).toEqual(expectedResult);
    });

    it('should set moveSequenceRepeat to true after 500 ms', fakeAsync(() => {
        service.startTimer();
        tick(SELECTION_REPEAT_START_TIME);
        expect(service['moveSequenceRepeat']).toEqual(true);
    }));

    it('move should call delay if arrowPressed and moveSequenceRepeat is true', async () => {
        const delaySpy: jasmine.Spy<any> = spyOn<any>(service, 'delay');
        service['moveSequenceRepeat'] = true;
        service.arrows.set(KeyValues.ArrowUp, true);
        await service.move(dataCtxStub, defaultVector, defaultVector);
        expect(delaySpy).toHaveBeenCalledWith(SELECTION_REPEAT_DELAY);
    });

    it('move should not call delay if moveSequenceRepeat is false', async () => {
        const delaySpy: jasmine.Spy<any> = spyOn<any>(service, 'delay');
        service['moveSequenceRepeat'] = false;
        service.arrows.set(KeyValues.ArrowUp, true);
        await service.move(dataCtxStub, defaultVector, defaultVector);
        expect(delaySpy).not.toHaveBeenCalled();
    });

    it('delay should call setTimeout', () => {
        const setTimeoutSpy: jasmine.Spy = spyOn(window, 'setTimeout');
        const duration = 1;
        service.delay(duration);
        expect(setTimeoutSpy).toHaveBeenCalled();
    });

    it('should not restart timer if already started', () => {
        service['timeoutID'] = 1;
        const setTimeoutSpy: jasmine.Spy = spyOn(window, 'setTimeout');
        service.startTimer();
        expect(setTimeoutSpy).not.toHaveBeenCalled();
    });

    it('should not clearTimeout if timeoutID is not strictly positive', () => {
        const clearSpy = spyOn(window, 'clearTimeout');
        service['timeoutID'] = DEFAULT_TIMEOUT_ID;
        service.stopTimer();
        expect(clearSpy).not.toHaveBeenCalled();
    });

    it('should clearTimeout if timeoutID is strictly positive', () => {
        const clearSpy = spyOn(window, 'clearTimeout');
        service['timeoutID'] = 1;
        service.stopTimer();
        expect(clearSpy).toHaveBeenCalled();
    });

    it('stopTimer should set moveSequenceRepeat to false', () => {
        service['moveSequenceRepeat'] = true;
        service.stopTimer();
        expect(service['moveSequenceRepeat']).toEqual(false);
    });

    it('computeResizeTranslationVector should resize on x axe if pointedAnchor.x is same as anchorToTranslate.x', () => {
        const anchorToTranslate: Vec2 = service.anchors.get(Cardinal.NorthWest) as Vec2;
        const currentMousePosition: Vec2 = defaultVector;
        service.resizingAnchor = service.anchors.get(Cardinal.West) as Vec2;
        const expectedResizeVector: Vec2 = { x: currentMousePosition.x - service.resizingAnchor.x, y: 0 };
        const resizeVector: Vec2 = service.computeResizeTranslationVector(service.resizingAnchor, anchorToTranslate, currentMousePosition);
        expect(resizeVector).toEqual(expectedResizeVector);
    });

    it('computeResizeTranslationVector should resize on y axe if pointedAnchor.y is same as anchorToTranslate.y', () => {
        const anchorToTranslate: Vec2 = service.anchors.get(Cardinal.NorthWest) as Vec2;
        service.resizingAnchor = service.anchors.get(Cardinal.North) as Vec2;
        const currentMousePosition: Vec2 = defaultVector;
        const expectedResizeVector: Vec2 = { x: 0, y: currentMousePosition.y - service.resizingAnchor.y };
        const resizeVector: Vec2 = service.computeResizeTranslationVector(service.resizingAnchor, anchorToTranslate, currentMousePosition);
        expect(resizeVector).toEqual(expectedResizeVector);
    });

    it('computeResizeTranslationVector should resize on both x and y axes if pointedAnchor is same as anchorToTranslate', () => {
        const anchorToTranslate: Vec2 = service.anchors.get(Cardinal.NorthWest) as Vec2;
        service.resizingAnchor = service.anchors.get(Cardinal.NorthWest) as Vec2;
        const currentMousePosition: Vec2 = defaultVector;
        const expectedResizeVector: Vec2 = {
            x: currentMousePosition.x - service.resizingAnchor.x,
            y: currentMousePosition.y - service.resizingAnchor.y,
        };
        const resizeVector: Vec2 = service.computeResizeTranslationVector(service.resizingAnchor, anchorToTranslate, currentMousePosition);
        expect(resizeVector).toEqual(expectedResizeVector);
    });

    it('resizeTranslationVector x should equals y if shiftKey is pressed and pointedAnchor is a corner anchor', () => {
        const anchorToTranslate: Vec2 = service.anchors.get(Cardinal.NorthWest) as Vec2;
        service.resizingAnchor = service.anchors.get(Cardinal.NorthWest) as Vec2;
        const currentMousePosition: Vec2 = defaultVector;
        const isACornerAnchor: boolean = service.isACornerAnchor(service.resizingAnchor);
        service.shiftPressed = true;
        const minDistance: number = Math.min(currentMousePosition.x - service.resizingAnchor.x, currentMousePosition.y - service.resizingAnchor.y);
        const expectedResizeVector: Vec2 = { x: minDistance, y: minDistance };
        const resizeVector: Vec2 = service.computeResizeTranslationVector(service.resizingAnchor, anchorToTranslate, currentMousePosition);
        expect(isACornerAnchor).toEqual(true);
        expect(resizeVector).toEqual(expectedResizeVector);
    });

    it('isACornerAnchor should return true if we are on a corner anchor', () => {
        const anchor: Vec2 = service.anchors.get(Cardinal.NorthWest) as Vec2;
        const result = service.isACornerAnchor(anchor);
        expect(result).toEqual(true);
    });

    it('isACornerAnchor should return true if we are on a corner anchor', () => {
        const anchor: Vec2 = service.anchors.get(Cardinal.SouthEast) as Vec2;
        const result = service.isACornerAnchor(anchor);
        expect(result).toEqual(true);
    });

    it('mirrorScale should scale correctly when southEast.x is smaller than northEast.x', () => {
        const northWest: Vec2 = { x: 2, y: 0 };
        const southEast: Vec2 = { x: 0, y: 2 };
        const scaleFactor = -1;
        const scaleSpy: jasmine.Spy = spyOn(dataCtxStub, 'scale');
        service.mirrorScale(dataCtxStub, northWest, southEast);
        expect(scaleSpy).toHaveBeenCalledWith(scaleFactor, 1);
    });

    it('mirrorScale should scale correctly when southEast.y is smaller than northEast.y', () => {
        const northWest: Vec2 = { x: 0, y: 2 };
        const southEast: Vec2 = { x: 2, y: 0 };
        const scaleFactor = -1;
        const scaleSpy: jasmine.Spy = spyOn(dataCtxStub, 'scale');
        service.mirrorScale(dataCtxStub, northWest, southEast);
        expect(scaleSpy).toHaveBeenCalledWith(1, scaleFactor);
    });

    it('mirrorScale should scale correctly when southEast is smaller than northEast both on x and y', () => {
        const northWest: Vec2 = { x: 2, y: 2 };
        const southEast: Vec2 = { x: 0, y: 0 };
        const scaleFactor = -1;
        const scaleSpy: jasmine.Spy = spyOn(dataCtxStub, 'scale');
        service.mirrorScale(dataCtxStub, northWest, southEast);
        expect(scaleSpy).toHaveBeenCalledWith(scaleFactor, 1);
        expect(scaleSpy).toHaveBeenCalledWith(1, scaleFactor);
    });

    it('resize should call computeResizeTranslationVector, and drawImage', () => {
        service.resizingAnchor = service.anchors.get(Cardinal.NorthWest) as Vec2;
        const currentMousePosition: Vec2 = { x: 20, y: 20 };
        const computeSpy: jasmine.Spy = spyOn(service, 'computeResizeTranslationVector').and.callThrough();
        const drawImageSpy: jasmine.Spy = spyOn(previewCtxStub, 'drawImage');
        service.resize(dataCtxStub, currentMousePosition);
        expect(computeSpy).toHaveBeenCalled();
        expect(drawImageSpy).toHaveBeenCalled();
    });

    it('rotate should clearPreviewCtx and drawImage', () => {
        const drawImageSpy: jasmine.Spy<any> = spyOn<any>(previewCtxStub, 'drawImage');
        const clearPreviewCtxSpy: jasmine.Spy = spyOn(service, 'clearPreviewCtx');
        const wheelRolledUp = true;
        service.rotate(dataCtxStub, wheelRolledUp);
        expect(clearPreviewCtxSpy).toHaveBeenCalled();
        expect(drawImageSpy).toHaveBeenCalled();
    });

    it('rotate should set the angle to Math.PI/12 when wheel is up and alt is not pressed', () => {
        const wheelRolledUp = true;
        service.altPressed = false;
        service.rotate(dataCtxStub, wheelRolledUp);
        const expectedAngle: number = Math.PI / DIVIDER_FOR_ROTATION_ANGLE;
        expect(service.angle).toEqual(expectedAngle);
    });

    it('rotate should set the angle to -Math.PI/12 when wheel is down and alt is pressed', () => {
        const wheelRolledUp = false;
        service.altPressed = true;
        service.rotate(dataCtxStub, wheelRolledUp);
        const expectedAngle: number = -Math.PI / ALT_DIVIDER_FOR_ROTATION_ANGLE;
        expect(service.angle).toEqual(expectedAngle);
    });

    it('should call magnetService findNearestIntersection if mode active', () => {
        magnetServiceSpy.findNearestIntersection.and.returnValue(defaultVector);
        magnetServiceSpy.anchor = Cardinal.SouthWest;
        magnetServiceSpy.currentMode = MagnetModes.Active;
        service.computeTranslationVector(defaultVector, defaultVector);
        expect(magnetServiceSpy.findNearestIntersection).toHaveBeenCalled();
    });

    it('should call magnetService findNearestIntersection if mode active and arrow pressed', () => {
        magnetServiceSpy.findNearestIntersection.and.returnValue(defaultVector);
        magnetServiceSpy.anchor = Cardinal.Center;
        magnetServiceSpy.currentMode = MagnetModes.Active;
        service.arrows.set(KeyValues.ArrowRight, true);
        service.computeTranslationVector(defaultVector, defaultVector);
        expect(magnetServiceSpy.findNearestIntersection).toHaveBeenCalled();
    });

    it('pendingTranslation shoulsd store pendind translations if magnet mode is active', () => {
        magnetServiceSpy.anchor = Cardinal.Center;
        service.anchors.set(Cardinal.NorthWest, { x: 0, y: 0 });
        service.anchors.set(Cardinal.SouthEast, { x: 3, y: 3 });
        const centerPoint = { x: 2, y: 2 };
        const currentMousePosition = { x: 0, y: 1 };
        magnetServiceSpy.findNearestIntersection.and.returnValue(centerPoint);
        magnetServiceSpy.currentMode = MagnetModes.Active;
        const result = service.computeTranslationVector(defaultVector, currentMousePosition);
        expect(service['pendingTranslation']).toEqual(currentMousePosition);
        expect(result).toEqual(defaultVector);
    });
});
