import { Injectable } from '@angular/core';
import { Cardinal } from '@app/classes/anchors/cardinal';
import { MagnetModes } from '@app/classes/magnet-view';
import {
    ALT_DIVIDER_FOR_ROTATION_ANGLE,
    DEFAULT_TIMEOUT_ID,
    DIVIDER_FOR_ROTATION_ANGLE,
    KeyValues,
    NUMBER_PIXELS_OFFSET,
    SELECTION_REPEAT_DELAY,
    SELECTION_REPEAT_START_TIME,
    SELECTION_TRANSLATION_DIRECTIONS,
} from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { MagnetService } from '@app/tools/selection/magnet/magnet.service';
import { ShapeService } from '@app/tools/shared/services/shape/shape.service';

@Injectable({
    providedIn: 'root',
})
export class SelectionManipulationsService {
    shiftPressed: boolean = false;
    altPressed: boolean = false;
    angle: number = 0;
    anchors: Map<Cardinal, Vec2> = new Map<Cardinal, Vec2>();
    arrows: Map<KeyValues, boolean> = new Map<KeyValues, boolean>()
        .set(KeyValues.ArrowUp, false)
        .set(KeyValues.ArrowDown, false)
        .set(KeyValues.ArrowLeft, false)
        .set(KeyValues.ArrowRight, false);
    resizingAnchor: Vec2 = { x: 0, y: 0 };
    private timeoutID: number = DEFAULT_TIMEOUT_ID;
    private moveSequenceRepeat: boolean = false;
    private pendingTranslation: Vec2 = { x: 0, y: 0 };

    constructor(private drawingService: DrawingService, private magnetService: MagnetService) {}

    reset(): void {
        this.stopTimer();
    }

    isArrowPressed(): boolean {
        return [...this.arrows.values()].some((x) => x === true);
    }

    startTimer(): void {
        if (this.timeoutID < 0) {
            this.timeoutID = window.setTimeout(() => {
                this.moveSequenceRepeat = true;
            }, SELECTION_REPEAT_START_TIME);
        }
    }

    stopTimer(): void {
        this.moveSequenceRepeat = false;
        if (this.timeoutID > 0) {
            clearTimeout(this.timeoutID);
            this.timeoutID = DEFAULT_TIMEOUT_ID;
        }
    }

    async move(dataCtx: CanvasRenderingContext2D, previousMousePosition: Vec2, currentMousePosition: Vec2): Promise<Map<Cardinal, Vec2>> {
        if (this.isArrowPressed() && this.moveSequenceRepeat) {
            await this.delay(SELECTION_REPEAT_DELAY);
        }

        const translationVector = this.computeTranslationVector(previousMousePosition, currentMousePosition);
        const northWest: Vec2 = this.vec2Sum(this.anchors.get(Cardinal.NorthWest) as Vec2, translationVector);
        const southEast: Vec2 = this.vec2Sum(this.anchors.get(Cardinal.SouthEast) as Vec2, translationVector);

        const height = southEast.y - northWest.y + 1;
        const width = southEast.x - northWest.x + 1;

        this.clearPreviewCtx();
        this.drawingService.previewCtx.drawImage(dataCtx.canvas, northWest.x, northWest.y, width, height);

        return ShapeService.getAnchors(northWest, southEast, false);
    }

    computeTranslationVector(previousMousePosition: Vec2, currentMousePosition: Vec2): Vec2 {
        const translationVector = { x: 0, y: 0 };
        const northWest: Vec2 = this.anchors.get(Cardinal.NorthWest) as Vec2;
        const southEast: Vec2 = this.anchors.get(Cardinal.SouthEast) as Vec2;
        const halfWidth = Math.round((southEast.x - northWest.x + 1) / 2);
        const halfHeight = Math.round((southEast.y - northWest.y + 1) / 2);

        const magnetAnchorPosition =
            this.magnetService.anchor !== Cardinal.Center
                ? (this.anchors.get(this.magnetService.anchor) as Vec2)
                : { x: northWest.x + halfWidth, y: northWest.y + halfHeight };

        if (!this.isArrowPressed()) {
            translationVector.x = currentMousePosition.x - previousMousePosition.x;
            translationVector.y = currentMousePosition.y - previousMousePosition.y;

            if (this.magnetService.currentMode === MagnetModes.Active) {
                this.pendingTranslation = this.vec2Sum(this.pendingTranslation, translationVector);
                const previewPosition = this.vec2Sum(magnetAnchorPosition, this.pendingTranslation);
                const nearestIntersection = this.magnetService.findNearestIntersection(previewPosition, false, { x: 0, y: 0 });
                translationVector.x = nearestIntersection.x - magnetAnchorPosition.x;
                translationVector.y = nearestIntersection.y - magnetAnchorPosition.y;

                this.pendingTranslation = translationVector.x === 0 && translationVector.y === 0 ? this.pendingTranslation : { x: 0, y: 0 };
            } else {
                this.pendingTranslation = { x: 0, y: 0 };
            }
        } else {
            let translationDirection = { x: 0, y: 0 };
            [...this.arrows.keys()].forEach((key) => {
                if (this.arrows.get(key) === true) {
                    translationDirection = this.vec2Sum(translationDirection, SELECTION_TRANSLATION_DIRECTIONS.get(key) as Vec2);
                }
            });
            if (this.magnetService.currentMode !== MagnetModes.Active) {
                translationVector.x = translationDirection.x * NUMBER_PIXELS_OFFSET;
                translationVector.y = translationDirection.y * NUMBER_PIXELS_OFFSET;
            } else {
                const nearestIntersection = this.magnetService.findNearestIntersection(magnetAnchorPosition, true, translationDirection);
                translationVector.x = nearestIntersection.x - magnetAnchorPosition.x;
                translationVector.y = nearestIntersection.y - magnetAnchorPosition.y;
            }
        }
        return translationVector;
    }

    computeResizeTranslationVector(resizingAnchor: Vec2, anchorToTranslate: Vec2, currentMousePosition: Vec2): Vec2 {
        let resizingVector: Vec2 = { x: 0, y: 0 };
        if (resizingAnchor.x === anchorToTranslate.x || resizingAnchor.y === anchorToTranslate.y) {
            // if the pointedAnchor is on a vertical side of the selection, then the resizing is done on horizontal axe.
            if (resizingAnchor.x === anchorToTranslate.x) {
                resizingVector.x = currentMousePosition.x - resizingAnchor.x;
            }

            // if the pointedAnchor is on a horizontal side of the selection, then the resizing is done on vertical axe.
            if (resizingAnchor.y === anchorToTranslate.y) {
                resizingVector.y = currentMousePosition.y - resizingAnchor.y;
            }

            if (this.shiftPressed && this.isACornerAnchor(resizingAnchor)) {
                const minDistance = Math.min(resizingVector.x, resizingVector.y);
                resizingVector = { x: minDistance, y: minDistance };
            }
        }
        return resizingVector;
    }

    isACornerAnchor(anchor: Vec2): boolean {
        const northWest: Vec2 = this.anchors.get(Cardinal.NorthWest) as Vec2;
        const southEast: Vec2 = this.anchors.get(Cardinal.SouthEast) as Vec2;
        return (anchor.x === northWest.x || anchor.x === southEast.x) && (anchor.y === northWest.y || anchor.y === southEast.y);
    }

    mirrorScale(dataCtx: CanvasRenderingContext2D, northWest: Vec2, southEast: Vec2): void {
        const scaleFactor = -1;
        const compositeOperation = dataCtx.globalCompositeOperation;
        dataCtx.globalCompositeOperation = 'copy';
        if (northWest.x > southEast.x) {
            dataCtx.translate(dataCtx.canvas.width, 0);
            dataCtx.scale(scaleFactor, 1);
            dataCtx.drawImage(dataCtx.canvas, 0, 0);
        }
        if (northWest.y > southEast.y) {
            dataCtx.translate(0, dataCtx.canvas.height);
            dataCtx.scale(1, scaleFactor);
            dataCtx.drawImage(dataCtx.canvas, 0, 0);
        }
        dataCtx.globalCompositeOperation = compositeOperation;
        dataCtx.resetTransform();
    }

    async delay(msDuration: number): Promise<void> {
        return new Promise((resolve) => setTimeout(resolve, msDuration));
    }

    vec2Sum(vector1: Vec2, vector2: Vec2): Vec2 {
        return { x: vector1.x + vector2.x, y: vector1.y + vector2.y };
    }

    resize(dataCtx: CanvasRenderingContext2D, currentMousePosition: Vec2): Map<Cardinal, Vec2> {
        let northWest: Vec2 = this.anchors.get(Cardinal.NorthWest) as Vec2;
        let southEast: Vec2 = this.anchors.get(Cardinal.SouthEast) as Vec2;

        // if the pointed anchor is on the same side as northWest anchor, then the northWest anchor is updated.
        const resizingVectorNW = this.computeResizeTranslationVector(this.resizingAnchor, northWest, currentMousePosition);
        northWest = this.vec2Sum(northWest, resizingVectorNW);
        this.resizingAnchor = this.vec2Sum(this.resizingAnchor, resizingVectorNW);

        // if the pointed anchor is on the same side as southEast anchor, then the southeast anchor is updated.
        const resizingVectorSE = this.computeResizeTranslationVector(this.resizingAnchor, southEast, currentMousePosition);
        southEast = this.vec2Sum(southEast, resizingVectorSE);
        this.resizingAnchor = this.vec2Sum(this.resizingAnchor, resizingVectorSE);

        this.mirrorScale(dataCtx, northWest, southEast);

        const height = southEast.y - northWest.y + 1;
        const width = southEast.x - northWest.x + 1;

        this.clearPreviewCtx();
        this.drawingService.previewCtx.drawImage(dataCtx.canvas, northWest.x, northWest.y, width, height);

        return ShapeService.getAnchors(northWest, southEast, false);
    }

    rotate(dataCtx: CanvasRenderingContext2D, wheelRolledUp: boolean): void {
        const northWest: Vec2 = this.anchors.get(Cardinal.NorthWest) as Vec2;
        const southEast: Vec2 = this.anchors.get(Cardinal.SouthEast) as Vec2;

        const angleAbs = this.altPressed ? Math.PI / ALT_DIVIDER_FOR_ROTATION_ANGLE : Math.PI / DIVIDER_FOR_ROTATION_ANGLE;
        this.angle = wheelRolledUp === true ? angleAbs : -angleAbs;

        const height = southEast.y - northWest.y + 1;
        const width = southEast.x - northWest.x + 1;
        const center: Vec2 = { x: northWest.x + width / 2, y: northWest.y + height / 2 };

        this.clearPreviewCtx();
        this.drawingService.previewCtx.translate(center.x, center.y);
        this.drawingService.previewCtx.rotate(this.angle);
        this.drawingService.previewCtx.translate(-center.x, -center.y);
        this.drawingService.previewCtx.drawImage(dataCtx.canvas, northWest.x, northWest.y, width, height);
    }

    clearPreviewCtx(): void {
        const transformMatrix = this.drawingService.previewCtx.getTransform();
        this.drawingService.previewCtx.resetTransform();
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        this.drawingService.previewCtx.setTransform(transformMatrix);
    }
}
