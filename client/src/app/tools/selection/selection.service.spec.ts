import { TestBed } from '@angular/core/testing';
import { Cardinal } from '@app/classes/anchors/cardinal';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { MouseButton } from '@app/classes/mouse-buttons';
import { KeyValues, SelectionState } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { SelectionManipulationsService } from '@app/tools/selection/selection-manipulations/selection-manipulations.service';
import { SelectionService } from '@app/tools/selection/selection.service';
import { ShapeService } from '@app/tools/shared/services/shape/shape.service';

// tslint:disable:no-any
// tslint:disable:no-string-literal
// tslint:disable: max-file-line-count
describe('SelectionService', () => {
    let service: SelectionService;
    let mouseEvent: MouseEvent;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let manipulationsSpy: jasmine.SpyObj<SelectionManipulationsService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;
    let dataCtxStub: CanvasRenderingContext2D;

    const defaultVector: Vec2 = { x: 0, y: 0 };

    beforeEach(() => {
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        dataCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        manipulationsSpy = jasmine.createSpyObj('SelectionManipulationsService', [
            'move',
            'resize',
            'rotate',
            'startTimer',
            'stopTimer',
            'isArrowPressed',
            'reset',
            'clearPreviewCtx',
        ]);
        const anchors = ShapeService.getAnchors(defaultVector, { x: 25, y: 25 }, false);
        manipulationsSpy.move.and.callFake(async () => {
            return anchors;
        });
        manipulationsSpy.resize.and.returnValue(anchors);
        TestBed.configureTestingModule({
            providers: [
                { provide: DrawingService, useValue: drawingServiceSpy },
                { provide: SelectionManipulationsService, useValue: manipulationsSpy },
            ],
        });
        service = TestBed.inject(SelectionService);
        mouseEvent = { offsetX: 25, offsetY: 25, button: MouseButton.Left } as MouseEvent;
        service['drawingService'].canvas = CanvasTestHelper.canvas();
        service['drawingService'].baseCtx = baseCtxStub;
        service['drawingService'].previewCtx = previewCtxStub;
        service['manipulationsService'].arrows = new Map<KeyValues, boolean>()
            .set(KeyValues.ArrowUp, false)
            .set(KeyValues.ArrowDown, false)
            .set(KeyValues.ArrowLeft, false)
            .set(KeyValues.ArrowRight, false);
        service['manipulationsService'].anchors = new Map<Cardinal, Vec2>();
        service['dataCtx'] = dataCtxStub;
        service['anchors'] = anchors;
        service.drawSelection = () => {
            return;
        };
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('reset should clear selection and reset the manipulationsService', () => {
        const clearSelectionSpy: jasmine.Spy<any> = spyOn<any>(service, 'clearSelection');
        service['hasSelected'] = true;
        service.reset();
        expect(clearSelectionSpy).toHaveBeenCalled();
        expect(service['manipulationsService'].reset).toHaveBeenCalled();
    });

    it('should update current mouse position and mousedowncoord to correct values when left mouse button is down', () => {
        service.onMouseDown(mouseEvent);
        expect(service.mouseDown).toEqual(true);
        const expectedCoord: Vec2 = service.getPositionFromMouse(mouseEvent);
        expect(service.mouseDownCoord).toEqual(expectedCoord);
        expect(service['currentMousePosition']).toEqual(expectedCoord);
    });

    it('should put to moving state if isOnAnAnchor is false, hasSelected and isOnSelection are true, when left mouse button is down', () => {
        spyOn<any>(service, 'isOnAnAnchor').and.returnValue(false);
        service['hasSelected'] = true;
        spyOn<any>(service, 'isOnSelection').and.returnValue(true);
        service.onMouseDown(mouseEvent);
        expect(service['selectionState']).toEqual(SelectionState.Moving);
    });

    it('should put to selecting state if hasSelected is false when left mouse button is down', () => {
        service['hasSelected'] = false;
        // spyOn<any>(service['manipulationsService'], 'clearPreviewCtx');
        service.onMouseDown(mouseEvent);
        expect(service['selectionState']).toEqual(SelectionState.Selecting);
    });

    it('should put to resizing state if hasSelected and isOnAnAnchor are true when left mouse button is down', () => {
        service['hasSelected'] = true;
        spyOn<any>(service, 'isOnAnAnchor').and.returnValue(true);
        service.onMouseDown(mouseEvent);
        expect(service['selectionState']).toEqual(SelectionState.Resizing);
    });

    it('should call resize and put to idle state if left mouse button goes up and resizing is true', () => {
        const resizeSpy: jasmine.Spy<any> = spyOn<any>(service, 'resize');
        service['selectionState'] = SelectionState.Resizing;
        service.onMouseUp(mouseEvent);
        expect(resizeSpy).toHaveBeenCalled();
        expect(service['selectionState']).toEqual(SelectionState.Idle);
    });

    it('should drawImage on baseCtx if hasSelected is true and call reset, in all other cases of mouseDown', () => {
        const drawImageSpy: jasmine.Spy<any> = spyOn<any>(service['drawingService'].baseCtx, 'drawImage');
        service['hasSelected'] = true;
        service['dataCtx'] = document.createElement('canvas').getContext('2d') as CanvasRenderingContext2D;
        spyOn<any>(service, 'isOnAnAnchor').and.returnValue(false);
        spyOn<any>(service, 'isOnSelection').and.returnValue(false);
        const resetSpy: jasmine.Spy<any> = spyOn<any>(service, 'reset');

        service.onMouseDown(mouseEvent);
        expect(resetSpy).toHaveBeenCalled();
        expect(drawImageSpy).toHaveBeenCalled();
    });

    it('mouseDown should be false if mouse button is not Left or Right', () => {
        const event: MouseEvent = { offsetX: 25, offsetY: 25, button: MouseButton.Middle } as MouseEvent;
        service.onMouseDown(event);
        expect(service.mouseDown).toEqual(false);
    });

    it('should put to Idle state if left mouse button goes up and moving is true', () => {
        service['selectionState'] = SelectionState.Moving;
        service.onMouseUp(mouseEvent);
        expect(service['selectionState']).toEqual(SelectionState.Idle);
    });

    it('should call previewSelection on mousemove if mouse already down and selecting is true', () => {
        service['currentMousePosition'] = defaultVector;
        service.mouseDown = true;
        service['selectionState'] = SelectionState.Selecting;
        const selectionSpy: jasmine.Spy<any> = spyOn<any>(service, 'previewSelection');
        service.onMouseMove(mouseEvent);
        expect(selectionSpy).toHaveBeenCalled();
    });

    it('should call move on mousemove if mouse already down, moving is true', () => {
        service['currentMousePosition'] = defaultVector;
        const moveSpy: jasmine.Spy<any> = spyOn<any>(service, 'move');
        service.mouseDown = true;
        service['selectionState'] = SelectionState.Moving;
        service.onMouseMove(mouseEvent);
        expect(moveSpy).toHaveBeenCalled();
    });

    it('should call resize on mousemove if mouse already down and resizing is true', () => {
        service['currentMousePosition'] = defaultVector;
        service.mouseDown = true;
        service['selectionState'] = SelectionState.Resizing;
        service.onMouseMove(mouseEvent);
        expect(manipulationsSpy.resize).toHaveBeenCalled();
    });

    it('onWheel should call rotate if haSelected is true', () => {
        const wheelEvent = new WheelEvent('syntheticWheel', { altKey: false, deltaY: 2, deltaMode: 0 });
        service['hasSelected'] = true;
        const rotateSpy: jasmine.Spy<any> = spyOn<any>(service, 'rotate').and.callThrough();
        service.onMouseWheel(wheelEvent);
        expect(rotateSpy).toHaveBeenCalled();
    });

    it('onWheel should set altPressed to false if alt is not pressed', () => {
        const wheelEvent = new WheelEvent('syntheticWheel', { altKey: false, deltaY: 2, deltaMode: 0 });
        service['hasSelected'] = true;
        const rotateSpy: jasmine.Spy<any> = spyOn<any>(service, 'rotate').and.callThrough();
        service.onMouseWheel(wheelEvent);
        expect(rotateSpy).toHaveBeenCalledWith(true);
        expect(service['altPressed']).toEqual(false);
    });

    it('onWheel should set altPressed to true if alt is pressed', () => {
        const wheelEvent = new WheelEvent('syntheticWheel', { altKey: true, deltaY: -2, deltaMode: 0 });
        service['hasSelected'] = true;
        const rotateSpy: jasmine.Spy<any> = spyOn<any>(service, 'rotate').and.callThrough();
        service.onMouseWheel(wheelEvent);
        expect(rotateSpy).toHaveBeenCalledWith(false);
        expect(service['altPressed']).toEqual(true);
    });

    it('onWheel should not call rotate if haSelected is false', () => {
        const wheelEvent = new WheelEvent('syntheticWheel', { altKey: false, deltaY: 2, deltaMode: 0 });
        service['hasSelected'] = false;
        const rotateSpy: jasmine.Spy<any> = spyOn<any>(service, 'rotate');
        service.onMouseWheel(wheelEvent);
        expect(rotateSpy).not.toHaveBeenCalled();
    });

    it('onKeyDown should put shiftPressed to true and call selection if mouse down and selecting state', () => {
        const simulatedShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const selectionSpy: jasmine.Spy<any> = spyOn<any>(service, 'previewSelection');
        service['shiftPressed'] = false;
        service.mouseDown = true;
        service['selectionState'] = SelectionState.Selecting;
        service.onKeyDown(simulatedShiftKey);
        expect(service['shiftPressed']).toEqual(true);
        expect(selectionSpy).toHaveBeenCalled();
    });

    it('onKeyDown should not do anything if mouse down and both selecting and resizing are false', () => {
        const simulatedShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const selectionSpy: jasmine.Spy<any> = spyOn<any>(service, 'previewSelection');
        const resizeSpy: jasmine.Spy<any> = spyOn<any>(service, 'resize');
        service['shiftPressed'] = false;
        service.mouseDown = true;
        service.onKeyDown(simulatedShiftKey);
        expect(service['shiftPressed']).toEqual(false);
        expect(selectionSpy).not.toHaveBeenCalled();
        expect(resizeSpy).not.toHaveBeenCalled();
    });

    it('onKeyDown should not do anything if mouse is not down and selecting is true', () => {
        const simulatedShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const selectionSpy: jasmine.Spy<any> = spyOn<any>(service, 'previewSelection');
        const resizeSpy: jasmine.Spy<any> = spyOn<any>(service, 'resize');
        service['shiftPressed'] = false;
        service.mouseDown = false;
        service['selectionState'] = SelectionState.Selecting;
        service.onKeyDown(simulatedShiftKey);
        expect(service['shiftPressed']).toEqual(false);
        expect(selectionSpy).not.toHaveBeenCalled();
        expect(resizeSpy).not.toHaveBeenCalled();
    });

    it('onKeyDown should put shifPressed to true and call resize if mouse down and selecting true', () => {
        const simulatedShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        const resizeSpy: jasmine.Spy<any> = spyOn<any>(service, 'resize');
        service['shiftPressed'] = false;
        service.mouseDown = true;
        service['selectionState'] = SelectionState.Resizing;
        service.onKeyDown(simulatedShiftKey);
        expect(service['shiftPressed']).toEqual(true);
        expect(resizeSpy).toHaveBeenCalled();
    });

    it('onKeyDown should finalizeSelection if escape key is down', () => {
        const simulatedKey = new KeyboardEvent('keydown', { key: 'Escape' });
        const finalizeSelectionSpy: jasmine.Spy = spyOn(service, 'finalizeSelection');
        service.onKeyDown(simulatedKey);
        expect(finalizeSelectionSpy).toHaveBeenCalled();
    });

    it('should select whole canvas if ctrl + a is pressed and selecting state', () => {
        service['selectionState'] = SelectionState.Selecting;
        const simulatedKey = new KeyboardEvent('keydown', { key: 'a', ctrlKey: true });
        const selectAllSpy: jasmine.Spy<any> = spyOn<any>(service, 'selectAll');
        const preventSpy: jasmine.Spy<any> = spyOn<any>(simulatedKey, 'preventDefault');
        service.onKeyDown(simulatedKey);
        expect(preventSpy).toHaveBeenCalled();
        expect(selectAllSpy).toHaveBeenCalled();
    });

    it('should set correct direction to true if arrow key is pressed', () => {
        const simulatedKey = new KeyboardEvent('keydown', { key: 'ArrowUp' });
        service['hasSelected'] = true;
        service.onKeyDown(simulatedKey);
        for (const key of [...service['manipulationsService'].arrows.keys()]) {
            if (key === (simulatedKey.key as KeyValues)) {
                expect(service['manipulationsService'].arrows.get(key)).toEqual(true);
            } else {
                expect(service['manipulationsService'].arrows.get(key)).toEqual(false);
            }
        }
    });

    it('should call startTimer and move if arrow key pressed, hasSelected true', () => {
        const simulatedKey = new KeyboardEvent('keydown', { key: 'ArrowUp' });
        service['hasSelected'] = true;
        const moveSpy: jasmine.Spy<any> = spyOn<any>(service, 'move');
        const preventSpy: jasmine.Spy<any> = spyOn<any>(simulatedKey, 'preventDefault');
        service.onKeyDown(simulatedKey);
        expect(service['selectionState']).toEqual(SelectionState.Moving);
        expect(preventSpy).toHaveBeenCalled();
        expect(manipulationsSpy.startTimer).toHaveBeenCalled();
        expect(moveSpy).toHaveBeenCalled();
    });

    it('should put shiftPressed to false if shift key up', () => {
        const simulatedShiftKey = new KeyboardEvent('keyup', { key: 'Shift' });
        service['shiftPressed'] = true;
        service.onKeyUp(simulatedShiftKey);
        expect(service['shiftPressed']).toEqual(false);
    });

    it('onKeyUp should call previewSelection if shift key up, selecting true and mouse down', () => {
        const simulatedShiftKey = new KeyboardEvent('keyup', { key: 'Shift' });
        const selectionSpy: jasmine.Spy<any> = spyOn<any>(service, 'previewSelection');
        service['selectionState'] = SelectionState.Selecting;
        service.mouseDown = true;
        service.onKeyUp(simulatedShiftKey);
        expect(selectionSpy).toHaveBeenCalled();
    });

    it('onKeyUp should call resize if shift key up,resizing true and mouse down', () => {
        const simulatedShiftKey = new KeyboardEvent('keyup', { key: 'Shift' });
        const resizeSpy: jasmine.Spy<any> = spyOn<any>(service, 'resize');
        service['selectionState'] = SelectionState.Resizing;
        service.mouseDown = true;
        service.onKeyUp(simulatedShiftKey);
        expect(resizeSpy).toHaveBeenCalled();
    });

    it('should set correct direction to false if arrow key up', () => {
        const simulatedKey = new KeyboardEvent('keyup', { key: 'ArrowUp' });
        const preventSpy: jasmine.Spy<any> = spyOn<any>(simulatedKey, 'preventDefault');
        for (const key of [...service['manipulationsService'].arrows.keys()]) {
            service['manipulationsService'].arrows.set(key, true);
        }
        service.onKeyUp(simulatedKey);
        for (const key of [...service['manipulationsService'].arrows.keys()]) {
            if (key === (simulatedKey.key as KeyValues)) {
                expect(service['manipulationsService'].arrows.get(key)).toEqual(false);
            } else {
                expect(service['manipulationsService'].arrows.get(key)).toEqual(true);
            }
        }
        expect(preventSpy).toHaveBeenCalled();
    });

    it('should call stopTimer if no arrow key still down', () => {
        const simulatedKey = new KeyboardEvent('keyup', { key: 'ArrowUp' });
        service['manipulationsService'].arrows.set(KeyValues.ArrowUp, true);
        service.onKeyUp(simulatedKey);
        expect(service['manipulationsService'].arrows.get(KeyValues.ArrowUp)).toEqual(false);
        expect(manipulationsSpy.stopTimer).toHaveBeenCalled();
    });

    it('should not call stopTimer if an arrow key is still down', () => {
        manipulationsSpy.isArrowPressed.and.returnValue(true);
        const simulatedKey = new KeyboardEvent('keyup', { key: 'ArrowUp' });
        service['manipulationsService'].arrows.set(KeyValues.ArrowUp, true);
        service['manipulationsService'].arrows.set(KeyValues.ArrowDown, true);
        service.onKeyUp(simulatedKey);
        expect(manipulationsSpy.stopTimer).not.toHaveBeenCalled();
    });

    it('should select entire canvas and previewSelection when selectAll', () => {
        const selectionSpy: jasmine.Spy<any> = spyOn<any>(service, 'previewSelection');
        const topLeft: Vec2 = { x: 0, y: 0 };
        const bottomRight = {
            x: service['drawingService'].baseCtx.canvas.width,
            y: service['drawingService'].baseCtx.canvas.height,
        };
        service.selectAll();
        expect(selectionSpy).toHaveBeenCalledWith(topLeft, bottomRight, false);
    });

    it('previewSelection should call drawSelection', () => {
        const drawSelectionSpy: jasmine.Spy<any> = spyOn<any>(service, 'drawSelection');
        service.mouseDownCoord = defaultVector;
        service['currentMousePosition'] = defaultVector;
        service['previewSelection'](defaultVector, defaultVector, false);
        expect(drawSelectionSpy).toHaveBeenCalled();
    });

    it('isOnSelection should return false if hasSelected is false', () => {
        const getPostionSpy: jasmine.Spy<any> = spyOn<any>(service, 'getPositionFromMouse');
        service['hasSelected'] = false;
        const result = service['isOnSelection'](mouseEvent);
        expect(result).toEqual(false);
        expect(getPostionSpy).not.toHaveBeenCalled();
    });

    it('isOnSelection should return false if mouse is not inside selection', () => {
        const position: Vec2 = { x: 30, y: 30 };
        service['hasSelected'] = true;
        const result = service['isOnSelection'](position);
        expect(result).toEqual(false);
    });

    it('isOnSelection should return true if mouse is inside selection', () => {
        // const event: MouseEvent = { offsetX: 10, offsetY: 10 } as MouseEvent;
        const position: Vec2 = { x: 10, y: 10 };
        service['hasSelected'] = true;
        const result = service['isOnSelection'](position);
        expect(result).toEqual(true);
    });

    it('isOnAnAnchor should return false if mouse is not on an anchor', () => {
        const currentMousePosition: Vec2 = { x: 40, y: 40 };
        const result = service['isOnAnAnchor'](currentMousePosition);
        expect(result).toEqual(false);
    });

    it('isOnAnAnchor should return true if mouse is on an anchor', () => {
        const currentMousePosition: Vec2 = service['anchors'].get(Cardinal.NorthWest) as MouseEvent;
        const result = service['isOnAnAnchor'](currentMousePosition);
        expect(result).toEqual(true);
    });

    it('isOnAnchor should return false if mouse is not on the given an anchor', () => {
        const anchor: Vec2 = service['anchors'].get(Cardinal.NorthWest) as Vec2;
        const currentMousePosition: Vec2 = { x: 40, y: 40 };
        const result = service['isOnAnchor'](currentMousePosition, anchor);
        expect(result).toEqual(false);
    });

    it('isOnAnchor should return true if mouse is on the given an anchor', () => {
        const anchor: Vec2 = service['anchors'].get(Cardinal.NorthWest) as Vec2;
        const currentMousePosition: Vec2 = { x: anchor.x, y: anchor.y };
        const result = service['isOnAnchor'](currentMousePosition, anchor);
        expect(result).toEqual(true);
    });

    it('onMouseMove should not do anything if mouseDown is false', () => {
        const getPostionSpy: jasmine.Spy = spyOn(service, 'getPositionFromMouse');
        service.mouseDown = false;
        service.onMouseMove(mouseEvent);
        expect(getPostionSpy).not.toHaveBeenCalled();
    });

    it('should not select all if ctrl key not pressed', () => {
        const simulatedKey = new KeyboardEvent('keydown', { key: 'a', ctrlKey: false });
        const selectAllSpy: jasmine.Spy<any> = spyOn<any>(service, 'selectAll');
        service.onKeyDown(simulatedKey);
        expect(selectAllSpy).not.toHaveBeenCalled();
    });

    it('should not call move if arrow key pressed and hasSelected is false', () => {
        const moveSpy: jasmine.Spy<any> = spyOn<any>(service, 'move');
        const simulatedKey = new KeyboardEvent('keydown', { key: 'ArrowUp' });
        service['hasSelected'] = false;
        service.onKeyDown(simulatedKey);
        expect(moveSpy).not.toHaveBeenCalled();
    });

    it('onToolChange should call reset and call drawImageSelection if hasSelected', () => {
        const resetSpy = spyOn(service, 'reset');
        const drawSpy = spyOn(service, 'drawImageSelection');
        service.onToolChange();
        expect(resetSpy).toHaveBeenCalled();

        service['hasSelected'] = true;
        service.onToolChange();
        expect(resetSpy).toHaveBeenCalled();
        expect(drawSpy).toHaveBeenCalled();
    });

    it('draw image selection should not try to draw something if anchors are not set', () => {
        service['anchors'] = new Map();
        const spy = spyOn<any>(drawingServiceSpy.baseCtx, 'drawImage');
        service.drawImageSelection(drawingServiceSpy.baseCtx);
        expect(spy).not.toHaveBeenCalled();
    });

    it('[c,C,v,V,x,X,Delete] should all call this.handleClipoard on keyDown', () => {
        const handleClipboardSpy = spyOn<any>(service, 'handleClipboard');
        const keys: string[] = ['c', 'C', 'v', 'V', 'x', 'X', 'Delete'];
        let callCount = 0;
        for (const key of keys) {
            const keyDownEvent = new KeyboardEvent('', { key });
            service.onKeyDown(keyDownEvent);
            callCount++;
        }
        expect(handleClipboardSpy).toHaveBeenCalledTimes(callCount);
    });

    it('handleClipboard should call this.delete() if keyDown is delete', () => {
        const deleteSpy = spyOn<any>(service, 'delete');
        const deleteKeyDown = new KeyboardEvent('', { key: KeyValues.Delete });
        service['handleClipboard'](deleteKeyDown);
        expect(deleteSpy).toHaveBeenCalled();
    });

    it('handleClipboard should call this.copy() if keyDown is c or C only if ctrl is down', () => {
        const copySpy = spyOn<any>(service, 'copy');
        const keyDownc = new KeyboardEvent('', { key: 'c' });
        const keyDownC = new KeyboardEvent('', { key: 'C' });
        service['handleClipboard'](keyDownc);
        service['handleClipboard'](keyDownC);
        expect(copySpy).not.toHaveBeenCalled();
        const keyDownCtrlc = new KeyboardEvent('', { key: 'c', ctrlKey: true });
        const keyDownCtrlC = new KeyboardEvent('', { key: 'C', ctrlKey: true });
        service['handleClipboard'](keyDownCtrlc);
        service['handleClipboard'](keyDownCtrlC);
        expect(copySpy).toHaveBeenCalledTimes(2);
    });

    it('handleClipboard should call this.cut() if keyDown is x or X only if ctrl is down', () => {
        const cutSpy = spyOn<any>(service, 'cut');
        const keyDownx = new KeyboardEvent('', { key: 'x' });
        const keyDownX = new KeyboardEvent('', { key: 'X' });
        service['handleClipboard'](keyDownx);
        service['handleClipboard'](keyDownX);
        expect(cutSpy).not.toHaveBeenCalled();
        const keyDownCtrlx = new KeyboardEvent('', { key: 'x', ctrlKey: true });
        const keyDownCtrlX = new KeyboardEvent('', { key: 'X', ctrlKey: true });
        service['handleClipboard'](keyDownCtrlx);
        service['handleClipboard'](keyDownCtrlX);
        expect(cutSpy).toHaveBeenCalledTimes(2);
    });

    it('handleClipboard should call this.paste() if keyDown is v or V only if ctrl is down', () => {
        const pasteSpy = spyOn<any>(service, 'paste');
        const keyDownv = new KeyboardEvent('', { key: 'v' });
        const keyDownV = new KeyboardEvent('', { key: 'V' });
        service['handleClipboard'](keyDownv);
        service['handleClipboard'](keyDownV);
        expect(pasteSpy).not.toHaveBeenCalled();
        const keyDownCtrlv = new KeyboardEvent('', { key: 'v', ctrlKey: true });
        const keyDownCtrlV = new KeyboardEvent('', { key: 'V', ctrlKey: true });
        service['handleClipboard'](keyDownCtrlv);
        service['handleClipboard'](keyDownCtrlV);
        expect(pasteSpy).toHaveBeenCalledTimes(2);
    });

    it('copy should not do anything if user has not selected anything', () => {
        const ghostCanvas = document.createElement('canvas');
        service['clipboard'] = ghostCanvas.getContext('2d') as CanvasRenderingContext2D;
        const drawImageSpy = spyOn<any>(service['clipboard'], 'drawImage');
        service['hasSelected'] = false;
        service.copy();
        expect(drawImageSpy).not.toHaveBeenCalled();
    });

    it('copy should copy the selection if the user has selected something', () => {
        service['hasSelected'] = true;
        service.copy();
        expect(service['clipboard'].canvas.height).toEqual(service['dataCtx'].canvas.height);
    });

    it('copy should do nothing if the clipboard is empty', () => {
        const toolChangeSpy = spyOn<any>(service, 'onToolChange');
        const drawImageSelectionSpy = spyOn<any>(service, 'drawImageSelection');
        service.paste();
        expect(toolChangeSpy).not.toHaveBeenCalled();
        expect(drawImageSelectionSpy).not.toHaveBeenCalled();
        expect(service['selectionState']).toEqual(SelectionState.Idle);
    });

    it('paste should paste the clipboard to the topLeft corner', () => {
        const ghostCanvas = document.createElement('canvas');
        service['clipboard'] = ghostCanvas.getContext('2d') as CanvasRenderingContext2D;
        const toolChangeSpy = spyOn<any>(service, 'onToolChange');
        const drawImageSelectionSpy = spyOn<any>(service, 'drawImageSelection');
        service.paste();
        expect(toolChangeSpy).toHaveBeenCalled();
        expect(drawImageSelectionSpy).toHaveBeenCalled();
        expect(service['selectionState']).toEqual(SelectionState.Idle);
    });

    it('delete should clear the dataCtx and draw it back on the canvas', () => {
        const width = service['dataCtx'].canvas.width;
        const height = service['dataCtx'].canvas.height;
        const clearRectSpy = spyOn<any>(service['dataCtx'], 'clearRect');
        const drawImageSelectionSpy = spyOn<any>(service, 'drawImageSelection');
        service.delete();
        expect(clearRectSpy).toHaveBeenCalledWith(0, 0, width, height);
        expect(drawImageSelectionSpy).toHaveBeenCalledWith(drawingServiceSpy.baseCtx);
    });

    it('cut should only work if user has selected something', () => {
        const copySpy = spyOn<any>(service, 'copy');
        const deleteSpy = spyOn<any>(service, 'delete');
        service['hasSelected'] = false;
        service.cut();
        expect(copySpy).not.toHaveBeenCalled();
        expect(deleteSpy).not.toHaveBeenCalled();
        service['hasSelected'] = true;
        service.cut();
        expect(copySpy).toHaveBeenCalled();
        expect(deleteSpy).toHaveBeenCalled();
    });

    it('on mouseUp, it should not do anything if mouse button is not Left or Right, even if state is moving or resizing', () => {
        const middleClick = new MouseEvent('', { button: MouseButton.Middle });
        service['selectionState'] = SelectionState.Moving;
        service.onMouseUp(middleClick);
        let finalState = service['selectionState'] as SelectionState;
        expect(finalState).toEqual(SelectionState.Moving);
        service['selectionState'] = SelectionState.Resizing;
        service.onMouseUp(middleClick);
        finalState = service['selectionState'];
        expect(finalState).toEqual(SelectionState.Resizing);
    });
});
