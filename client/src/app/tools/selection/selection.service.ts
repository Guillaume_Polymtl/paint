// tslint:disable:max-file-line-count
import { Injectable } from '@angular/core';
import { Cardinal } from '@app/classes/anchors/cardinal';
import { BLACK, Color, WHITE } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { KeyValues, SelectionState, SELECTION_ANCHOR_SIZE, SELECTION_DASH_LINE } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { SelectionManipulationsService } from '@app/tools/selection/selection-manipulations/selection-manipulations.service';
import { ShapeService } from '@app/tools/shared/services/shape/shape.service';

@Injectable({
    providedIn: 'root',
})
export abstract class SelectionService extends Tool {
    protected shiftPressed: boolean = false;
    protected altPressed: boolean = false;
    protected angle: number = 0;
    protected dataCtx: CanvasRenderingContext2D;
    protected selectionState: SelectionState = SelectionState.Idle;
    protected currentMousePosition: Vec2 = { x: 0, y: 0 };
    protected anchors: Map<Cardinal, Vec2> = new Map<Cardinal, Vec2>();
    protected hasSelected: boolean = false;
    protected initialLeftCorner: Vec2 = { x: 0, y: 0 };
    protected clipboard: CanvasRenderingContext2D;
    constructor(drawingService: DrawingService, colorService: ColorService, private manipulationsService: SelectionManipulationsService) {
        super(drawingService, colorService);
    }
    onToolChange(): void {
        this.finalizeSelection();
    }
    reset(): void {
        this.angle = 0;
        this.clearSelection();
        this.drawingService.previewCtx.resetTransform();
        this.mouseDown = false;
        this.manipulationsService.arrows = new Map<KeyValues, boolean>()
            .set(KeyValues.ArrowUp, false)
            .set(KeyValues.ArrowDown, false)
            .set(KeyValues.ArrowLeft, false)
            .set(KeyValues.ArrowRight, false);
        this.manipulationsService.reset();
    }
    isState(state: SelectionState): boolean {
        return this.selectionState === state;
    }
    private isOnAnAnchor(currentMousePosition: Vec2): boolean {
        return [...this.anchors.values()].some((anchor) => this.isOnAnchor(currentMousePosition, anchor));
    }
    private isOnAnchor(currentMousePosition: Vec2, anchor: Vec2): boolean {
        if (
            Math.abs(currentMousePosition.x - anchor.x) <= SELECTION_ANCHOR_SIZE / 2 &&
            Math.abs(currentMousePosition.y - anchor.y) <= SELECTION_ANCHOR_SIZE / 2
        ) {
            this.manipulationsService.resizingAnchor = anchor;
            return true;
        }
        return false;
    }

    private isOnSelection(position: Vec2): boolean {
        if (!this.hasSelected) {
            return false;
        }
        const northWest = this.anchors.get(Cardinal.NorthWest) as Vec2;
        const southEast = this.anchors.get(Cardinal.SouthEast) as Vec2;
        if (position.x > northWest.x && position.x < southEast.x && position.y > northWest.y && position.y < southEast.y) {
            return true;
        }
        return false;
    }
    onMouseDown(event: MouseEvent): void {
        if (event.button === MouseButton.Left || event.button === MouseButton.Right) {
            this.selectionState = SelectionState.Idle;
            this.mouseDown = true;
            this.mouseDownCoord = this.getPositionFromMouse(event);
            this.currentMousePosition = this.mouseDownCoord;
            if (this.hasSelected && this.isOnAnAnchor(this.mouseDownCoord)) {
                this.selectionState = SelectionState.Resizing;
            } else if (this.hasSelected && this.isOnSelection(this.mouseDownCoord) && !this.isOnAnAnchor(this.mouseDownCoord)) {
                this.selectionState = SelectionState.Moving;
            } else if (!this.hasSelected) {
                this.clearSelection();
                this.selectionState = SelectionState.Selecting;
            } else {
                this.drawingService.baseCtx.setTransform(this.drawingService.previewCtx.getTransform());
                this.drawImageSelection(this.drawingService.baseCtx);
                this.drawingService.baseCtx.resetTransform();
                this.reset();
            }
        }
    }
    onMouseUp(event: MouseEvent): void {
        if ((event.button === MouseButton.Left || event.button === MouseButton.Right) && this.isState(SelectionState.Moving)) {
            this.selectionState = SelectionState.Idle;
        } else if ((event.button === MouseButton.Left || event.button === MouseButton.Right) && this.isState(SelectionState.Resizing)) {
            this.resize();
            this.selectionState = SelectionState.Idle;
        }
    }
    onMouseMove(event: MouseEvent): void {
        if (this.mouseDown) {
            const previousMousePosition: Vec2 = { x: this.currentMousePosition.x, y: this.currentMousePosition.y };
            this.currentMousePosition = this.getPositionFromMouse(event);
            /* istanbul ignore else */
            if (this.isState(SelectionState.Selecting)) {
                this.drawingService.clearCanvas(this.drawingService.previewCtx);
                this.previewSelection(this.mouseDownCoord, this.currentMousePosition, this.shiftPressed);
            } else if (this.isState(SelectionState.Resizing)) {
                this.resize();
            } else if (this.isState(SelectionState.Moving)) {
                this.move(previousMousePosition);
            }
        }
    }

    onMouseWheel(event: WheelEvent): void {
        event.preventDefault();
        this.altPressed = event.altKey ? true : false;
        const wheelRolledUp: boolean = event.deltaY > 0 ? true : false;
        if (this.hasSelected) this.rotate(wheelRolledUp);
    }

    onKeyDown(event: KeyboardEvent): void {
        switch (event.key) {
            case KeyValues.Shift: {
                if ((this.isState(SelectionState.Selecting) || this.isState(SelectionState.Resizing)) && this.mouseDown) {
                    this.shiftPressed = true;
                    this.drawingService.clearCanvas(this.drawingService.previewCtx);
                    this.isState(SelectionState.Selecting)
                        ? this.previewSelection(this.mouseDownCoord, this.currentMousePosition, true)
                        : this.resize();
                }
                break;
            }
            case KeyValues.Escape: {
                this.finalizeSelection();
                break;
            }
            case 'a':
            case 'A': {
                if (event.ctrlKey && this.isState(SelectionState.Selecting)) {
                    event.preventDefault();
                    this.selectAll();
                }
                break;
            }
            case KeyValues.ArrowUp:
            case KeyValues.ArrowDown:
            case KeyValues.ArrowLeft:
            case KeyValues.ArrowRight: {
                this.handleManipulation(event);
                break;
            }
            case 'c':
            case 'C':
            case 'v':
            case 'V':
            case 'x':
            case 'X':
            case KeyValues.Delete: {
                this.handleClipboard(event);
                break;
            }
        }
    }

    private handleClipboard(event: KeyboardEvent): void {
        if (event.key === KeyValues.Delete) {
            this.delete();
        } else if (event.ctrlKey) {
            switch (event.key.toLowerCase()) {
                case 'c': {
                    this.copy();
                    break;
                }
                case 'v': {
                    this.paste();
                    break;
                }
                case 'x': {
                    this.cut();
                    break;
                }
            }
        }
    }

    copy(): void {
        if (this.hasSelected) {
            const ghostCanvas = document.createElement('canvas');
            ghostCanvas.height = this.dataCtx.canvas.height;
            ghostCanvas.width = this.dataCtx.canvas.width;
            this.clipboard = ghostCanvas.getContext('2d') as CanvasRenderingContext2D;
            this.clipboard.drawImage(this.dataCtx.canvas, 0, 0);
        }
    }

    paste(): void {
        if (this.clipboard) {
            this.onToolChange();
            this.dataCtx = this.clipboard;
            const northWest = this.anchors.get(Cardinal.NorthWest) as Vec2;
            const southEast = this.anchors.get(Cardinal.SouthEast) as Vec2;
            const topLeft = { x: 0, y: 0 };
            const width = southEast.x - northWest.x;
            const height = southEast.y - northWest.y;
            const bottomRight = { x: width, y: height };
            this.anchors = ShapeService.getAnchors(topLeft, bottomRight, false);
            this.previewSelection(topLeft, bottomRight, false);
            this.drawImageSelection(this.drawingService.previewCtx);
            this.drawSelection(topLeft, bottomRight, SELECTION_DASH_LINE, 2, BLACK);
            this.drawAnchors();
            this.selectionState = SelectionState.Idle;
            this.hasSelected = true;
        }
    }

    delete(): void {
        this.dataCtx.clearRect(0, 0, this.dataCtx.canvas.width, this.dataCtx.canvas.height);
        this.drawImageSelection(this.drawingService.baseCtx);
    }

    cut(): void {
        if (this.hasSelected) {
            this.copy();
            this.delete();
        }
    }

    private handleManipulation(event: KeyboardEvent): void {
        if (!this.isState(SelectionState.Selecting) && !this.isState(SelectionState.Resizing) && this.hasSelected) {
            event.preventDefault();
            this.selectionState = SelectionState.Moving;
            this.manipulationsService.arrows.set(event.key as KeyValues, true);
            this.manipulationsService.startTimer();
            this.move(this.currentMousePosition);
        }
    }

    onKeyUp(event: KeyboardEvent): void {
        switch (event.key) {
            case KeyValues.Shift: {
                this.shiftPressed = false;
                if ((this.isState(SelectionState.Selecting) || this.isState(SelectionState.Resizing)) && this.mouseDown) {
                    this.drawingService.clearCanvas(this.drawingService.previewCtx);
                    this.isState(SelectionState.Selecting)
                        ? this.previewSelection(this.mouseDownCoord, this.currentMousePosition, false)
                        : this.resize();
                }
                break;
            }
            case KeyValues.ArrowUp:
            case KeyValues.ArrowDown:
            case KeyValues.ArrowLeft:
            case KeyValues.ArrowRight: {
                event.preventDefault();
                this.manipulationsService.arrows.set(event.key as KeyValues, false);
                if (!this.manipulationsService.isArrowPressed()) {
                    this.manipulationsService.stopTimer();
                }
                break;
            }
        }
    }

    private drawAnchor(position: Vec2, size: number): void {
        this.drawingService.previewCtx.strokeStyle = BLACK.toString();
        this.drawingService.previewCtx.fillStyle = WHITE.toString();
        this.drawingService.previewCtx.setLineDash([0]);
        this.drawingService.previewCtx.fillRect(position.x - size / 2, position.y - size / 2, size, size);
        this.drawingService.previewCtx.strokeRect(position.x - size / 2, position.y - size / 2, size, size);
    }

    drawAnchors(): void {
        const northWest = this.anchors.get(Cardinal.NorthWest) as Vec2;
        const southEast = this.anchors.get(Cardinal.SouthEast) as Vec2;
        const distance = ShapeService.getDistance(northWest, southEast);
        this.drawingService.previewCtx.setLineDash([SELECTION_DASH_LINE]);
        this.drawingService.previewCtx.lineWidth = 2;
        this.drawingService.previewCtx.strokeRect(northWest.x, northWest.y, distance.x, distance.y);
        for (const anchorPosition of this.anchors.values()) {
            this.drawAnchor(anchorPosition, SELECTION_ANCHOR_SIZE);
        }
    }

    protected previewSelection(corner: Vec2, oppositeCorner: Vec2, forceToSquare: boolean): void {
        this.anchors = ShapeService.getAnchors(corner, oppositeCorner, forceToSquare);
        const northWest = this.anchors.get(Cardinal.NorthWest) as Vec2;
        const southEast = this.anchors.get(Cardinal.SouthEast) as Vec2;
        this.drawSelection(northWest, southEast, SELECTION_DASH_LINE, 2, BLACK);
        this.drawAnchors();
        return;
    }

    finalizeSelection(): void {
        if (this.hasSelected) {
            this.drawingService.baseCtx.setTransform(this.drawingService.previewCtx.getTransform());
            this.drawImageSelection(this.drawingService.baseCtx);
            this.drawingService.baseCtx.resetTransform();
        }
        this.reset();
    }

    protected finalizePreviewSelection(): void {
        const northWest = this.anchors.get(Cardinal.NorthWest) as Vec2;
        const southEast = this.anchors.get(Cardinal.SouthEast) as Vec2;
        const height = southEast.y - northWest.y + 1;
        const width = southEast.x - northWest.x + 1;
        this.fillWhite(width, height);
        this.drawImageSelection(this.drawingService.previewCtx);
        this.drawSelection(northWest, southEast, SELECTION_DASH_LINE, 2, BLACK);
        this.drawAnchors();
    }

    selectAll(): void {
        const topLeftCorner: Vec2 = { x: 0, y: 0 };
        const bottomRightCorner: Vec2 = { x: this.drawingService.baseCtx.canvas.width, y: this.drawingService.baseCtx.canvas.height };
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        this.previewSelection(topLeftCorner, bottomRightCorner, false);
        this.hasSelected = true;
    }

    private clearSelection(): void {
        this.selectionState = SelectionState.Idle;
        this.hasSelected = false;
        this.manipulationsService.clearPreviewCtx();
    }
    drawImageSelection(ctx: CanvasRenderingContext2D): void {
        const northWest: Vec2 = this.anchors.get(Cardinal.NorthWest) as Vec2;
        const southEast: Vec2 = this.anchors.get(Cardinal.SouthEast) as Vec2;
        if (northWest && southEast) {
            const height = southEast.y - northWest.y + 1;
            const width = southEast.x - northWest.x + 1;
            this.drawingService.clearCanvas(this.drawingService.previewCtx);
            ctx.drawImage(this.dataCtx.canvas, northWest.x, northWest.y, width, height);
            if (ctx === this.drawingService.baseCtx) this.drawingService.autoSave();
        }
    }
    private async move(previousMousePosition: Vec2): Promise<void> {
        this.manipulationsService.anchors = this.anchors;
        const movedAnchors = await this.manipulationsService.move(this.dataCtx, previousMousePosition, this.currentMousePosition);
        const northWest = movedAnchors.get(Cardinal.NorthWest) as Vec2;
        const southEast = movedAnchors.get(Cardinal.SouthEast) as Vec2;
        this.previewSelection(northWest, southEast, false);
    }
    protected resize(): void {
        this.manipulationsService.anchors = this.anchors;
        this.manipulationsService.shiftPressed = this.shiftPressed;
        const resizedAnchors = this.manipulationsService.resize(this.dataCtx, this.currentMousePosition);
        const northWest = resizedAnchors.get(Cardinal.NorthWest) as Vec2;
        const southEast = resizedAnchors.get(Cardinal.SouthEast) as Vec2;
        this.previewSelection(northWest, southEast, false);
    }

    protected rotate(wheelRolledUp: boolean): void {
        this.manipulationsService.anchors = this.anchors;
        this.manipulationsService.altPressed = this.altPressed;
        this.manipulationsService.rotate(this.dataCtx, wheelRolledUp);
        const northWest = this.anchors.get(Cardinal.NorthWest) as Vec2;
        const southEast = this.anchors.get(Cardinal.SouthEast) as Vec2;
        this.previewSelection(northWest, southEast, false);
    }
    abstract drawSelection(startPosition: Vec2, endPosition: Vec2, lineDash: number, lineWidth: number, color: Color): void;
    abstract fillWhite(width: number, height: number): void;
}
