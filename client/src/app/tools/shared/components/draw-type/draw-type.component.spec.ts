import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonToggleGroupHarness, MatButtonToggleHarness } from '@angular/material/button-toggle/testing';
import { DrawTypes } from '@app/classes/tools-utility';
import { DrawTypeComponent } from './draw-type.component';

describe('FillTypeComponent', () => {
    let component: DrawTypeComponent;
    let fixture: ComponentFixture<DrawTypeComponent>;
    let loader: HarnessLoader;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DrawTypeComponent],
            imports: [MatButtonToggleModule, MatButtonModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DrawTypeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        loader = TestbedHarnessEnvironment.loader(fixture);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should be able to get the button toggle harness', async () => {
        const toggleGroup = await loader.getHarness(MatButtonToggleGroupHarness);
        expect(toggleGroup).not.toBeNull();
    });

    it('chosing a new toggle value should call onInputChange', async () => {
        spyOn(component, 'onChange');
        const toggleButton = await loader.getHarness(MatButtonToggleHarness);
        await toggleButton.check();
        await toggleButton.uncheck();
        expect(component.onChange).toHaveBeenCalled();
    });

    it('toggling a button should emit', async () => {
        spyOn(component.valueChange, 'emit');
        const toggleButton = await loader.getHarness(MatButtonToggleHarness);
        await toggleButton.check();
        await toggleButton.uncheck();
        expect(component.valueChange.emit).toHaveBeenCalled();
    });

    it('toggling a button should emit the correct value', async () => {
        spyOn(component.valueChange, 'emit');
        const toggleButton = await loader.getHarness(MatButtonToggleHarness);
        await toggleButton.check();
        await toggleButton.uncheck();
        const value: unknown = await toggleButton.getText();
        expect(component.valueChange.emit).toHaveBeenCalledWith(value as DrawTypes);
    });
});
