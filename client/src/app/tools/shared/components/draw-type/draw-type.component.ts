import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { DrawTypes } from '@app/classes/tools-utility';

@Component({
    selector: 'app-draw-type',
    templateUrl: './draw-type.component.html',
    styleUrls: ['./draw-type.component.scss'],
})
export class DrawTypeComponent {
    title: string = 'Type de tracé';

    @Input() value: DrawTypes;
    @Output() valueChange: EventEmitter<DrawTypes> = new EventEmitter<DrawTypes>();

    onChange(event: MatButtonToggleChange): void {
        this.valueChange.emit(event.value);
    }
}
