import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectHarness } from '@angular/material/select/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DropDownComponent } from './drop-down.component';

describe('DropDownComponent', () => {
    let component: DropDownComponent;
    let fixture: ComponentFixture<DropDownComponent>;
    let loader: HarnessLoader;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DropDownComponent],
            imports: [MatFormFieldModule, MatSelectModule, BrowserAnimationsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DropDownComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.choices = ['0', '1', '2'];
        loader = TestbedHarnessEnvironment.loader(fixture);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('selecting value should emit the correct value', async () => {
        spyOn(component.valueChange, 'emit');
        const select = await loader.getHarness(MatSelectHarness);
        const choice = component.choices[0];
        await select.clickOptions();
        expect(component.valueChange.emit).toHaveBeenCalledWith(choice);
    });
});
