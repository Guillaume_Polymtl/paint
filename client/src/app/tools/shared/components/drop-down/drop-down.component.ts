import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';

@Component({
    selector: 'app-drop-down',
    templateUrl: './drop-down.component.html',
    styleUrls: ['./drop-down.component.scss'],
})
export class DropDownComponent {
    title: string = 'Police';

    @Input() defaultChoice: string;
    @Input() choices: string[];
    @Output() valueChange: EventEmitter<string> = new EventEmitter<string>();

    onChange(event: MatSelectChange): void {
        this.valueChange.emit(event.value);
    }
}
