import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FontStyleComponent } from './font-style.component';

describe('FontStyleComponent', () => {
    let component: FontStyleComponent;
    let fixture: ComponentFixture<FontStyleComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FontStyleComponent],
            imports: [BrowserAnimationsModule, MatFormFieldModule, MatButtonModule, MatButtonToggleModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FontStyleComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('setting the italic should next the correct value', async () => {
        spyOn(component.italicChange, 'next');
        component.setItalic(true);
        expect(component.italicChange.next).toHaveBeenCalledWith(true);
    });

    it('setting the bold should next the correct value', async () => {
        spyOn(component.boldChange, 'next');
        component.setBold(true);
        expect(component.boldChange.next).toHaveBeenCalledWith(true);
    });
});
