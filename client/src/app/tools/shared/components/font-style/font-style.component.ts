import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-font-style',
    templateUrl: './font-style.component.html',
    styleUrls: ['./font-style.component.scss'],
})
export class FontStyleComponent {
    readonly title: string = 'Style de police';
    @Input() isItalic: boolean;
    @Input() isBold: boolean;

    @Output() italicChange: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() boldChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    setBold(value: boolean): void {
        this.boldChange.next(value);
        this.isBold = value;
    }

    setItalic(value: boolean): void {
        this.italicChange.next(value);
        this.isItalic = value;
    }
}
