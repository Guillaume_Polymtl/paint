import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSlideToggleHarness } from '@angular/material/slide-toggle/testing';
import { HasJunctionComponent } from './has-junction.component';

describe('HasJunctionComponent', () => {
    let component: HasJunctionComponent;
    let fixture: ComponentFixture<HasJunctionComponent>;
    let loader: HarnessLoader;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HasJunctionComponent],
            imports: [MatSlideToggleModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HasJunctionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        loader = TestbedHarnessEnvironment.loader(fixture);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should be able to get the toggle harness', async () => {
        const toggle = await loader.getHarness(MatSlideToggleHarness);
        expect(toggle).not.toBeNull();
    });

    it('toggling the toggle should call onChange()', async () => {
        spyOn(component, 'onChange');
        const toggle = await loader.getHarness(MatSlideToggleHarness);
        await toggle.toggle();
        expect(component.onChange).toHaveBeenCalled();
    });

    it('toggling the toggle should emit', async () => {
        spyOn(component.valueChange, 'emit');
        const toggle = await loader.getHarness(MatSlideToggleHarness);
        await toggle.toggle();
        expect(component.valueChange.emit).toHaveBeenCalled();
    });

    it('toggling the toggle should emit the correct value', async () => {
        spyOn(component.valueChange, 'emit');
        const toggle = await loader.getHarness(MatSlideToggleHarness);
        await toggle.toggle();
        expect(component.valueChange.emit).toHaveBeenCalledWith(true);
    });
});
