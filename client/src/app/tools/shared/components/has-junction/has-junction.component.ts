import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
    selector: 'app-has-junction',
    templateUrl: './has-junction.component.html',
    styleUrls: ['./has-junction.component.scss'],
})
export class HasJunctionComponent {
    title: string = 'Jonction';
    @Input() value: boolean;
    @Output() valueChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    onChange(event: MatSlideToggleChange): void {
        this.valueChange.emit(event.checked);
    }
}
