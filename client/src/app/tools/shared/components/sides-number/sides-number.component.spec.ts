import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectHarness } from '@angular/material/select/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidesNumberComponent } from './sides-number.component';

describe('SidesNumberComponent', () => {
    let component: SidesNumberComponent;
    let fixture: ComponentFixture<SidesNumberComponent>;
    let loader: HarnessLoader;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SidesNumberComponent],
            imports: [MatSelectModule, BrowserAnimationsModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SidesNumberComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        loader = TestbedHarnessEnvironment.loader(fixture);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('selecting value should emit the correct value', async () => {
        spyOn(component.valueChange, 'emit');
        const select = await loader.getHarness(MatSelectHarness);
        const side = component.sidesChoices[0];
        await select.clickOptions();
        expect(component.valueChange.emit).toHaveBeenCalledWith(side);
    });
});
