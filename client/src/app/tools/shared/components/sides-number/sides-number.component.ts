import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { POLYGON_MAX_SIDES, POLYGON_MIN_SIDES } from '@app/classes/tools-utility';

@Component({
    selector: 'app-sides-number',
    templateUrl: './sides-number.component.html',
    styleUrls: ['./sides-number.component.scss'],
})
export class SidesNumberComponent {
    title: string = 'Nombre de côtés';

    @Input() value: number;
    @Output() valueChange: EventEmitter<number> = new EventEmitter<number>();
    sidesChoices: number[];

    constructor() {
        this.sidesChoices = [...Array(POLYGON_MAX_SIDES - POLYGON_MIN_SIDES + 1).keys()].map((i) => i + POLYGON_MIN_SIDES);
    }

    onChange(event: MatSelectChange): void {
        this.valueChange.emit(event.value);
    }
}
