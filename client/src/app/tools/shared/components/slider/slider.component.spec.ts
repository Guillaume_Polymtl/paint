import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSliderModule } from '@angular/material/slider';
import { MatSliderHarness } from '@angular/material/slider/testing';
import { SliderComponent } from './slider.component';

describe('SliderComponent', () => {
    let component: SliderComponent;
    let fixture: ComponentFixture<SliderComponent>;
    let loader: HarnessLoader;
    const NEW_VALUE = 1;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SliderComponent],
            imports: [MatSliderModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SliderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        loader = TestbedHarnessEnvironment.loader(fixture);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should be able to get the slider harness', async () => {
        const slider = await loader.getHarness(MatSliderHarness);
        expect(slider).not.toBeNull();
    });

    it('setting a value on the slider should call onInputChange()', async () => {
        spyOn(component, 'onChange');
        const slider = await loader.getHarness(MatSliderHarness);
        await slider.setValue(NEW_VALUE);
        expect(component.onChange).toHaveBeenCalled();
    });

    it('setting the value on the slider should emit', async () => {
        spyOn(component.valueChange, 'emit');
        const slider = await loader.getHarness(MatSliderHarness);
        expect(slider).not.toBeNull();
        await slider.setValue(NEW_VALUE);
        expect(component.valueChange.emit).toHaveBeenCalled();
    });

    it('setting the value on the slider should emit the correct value', async () => {
        spyOn(component.valueChange, 'emit');
        const slider = await loader.getHarness(MatSliderHarness);
        expect(slider).not.toBeNull();
        await slider.setValue(NEW_VALUE);
        expect(component.valueChange.emit).toHaveBeenCalledWith(NEW_VALUE);
    });
});
