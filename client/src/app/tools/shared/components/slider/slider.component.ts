import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';

@Component({
    selector: 'app-slider',
    templateUrl: './slider.component.html',
    styleUrls: ['./slider.component.scss'],
})
export class SliderComponent {
    @Input() title: string;

    @Input() value: number;
    @Input() min: number;
    @Input() max: number;
    @Input() step: number;

    @Output() valueChange: EventEmitter<number | null> = new EventEmitter<number | null>();
    @Output() inputChange: EventEmitter<number | null> = new EventEmitter<number | null>();

    onChange(event: MatSliderChange): void {
        this.valueChange.emit(event.value);
    }

    onInput(event: MatSliderChange): void {
        this.inputChange.emit(event.value);
    }
}
