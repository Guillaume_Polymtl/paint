import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { StampChoices, StampIconsPath } from '@app/classes/tools-utility';

@Component({
    selector: 'app-stamp-choice',
    templateUrl: './stamp-choice.component.html',
    styleUrls: ['./stamp-choice.component.scss'],
})
export class StampChoiceComponent {
    title: string = "choix d'étampe";

    @Input() value: StampChoices;
    @Output() valueChange: EventEmitter<StampChoices> = new EventEmitter<StampChoices>();
    iconsPath: string[];
    choices: string[];

    constructor() {
        this.iconsPath = Object.values(StampIconsPath);
        this.choices = Object.values(StampChoices);
    }

    onChange(event: MatButtonToggleChange): void {
        this.valueChange.emit(event.value as StampChoices);
    }
}
