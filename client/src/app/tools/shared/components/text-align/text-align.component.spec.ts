import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { TEXT_ALIGN } from '@app/classes/tools-utility';

import { TextAlignComponent } from './text-align.component';

describe('TextAlignComponent', () => {
    let component: TextAlignComponent;
    let fixture: ComponentFixture<TextAlignComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TextAlignComponent],
            imports: [MatButtonToggleModule, MatIconModule, MatFormFieldModule, MatButtonModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextAlignComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('toggling a button should emit the correct value', async () => {
        spyOn(component.alignmentChange, 'next');
        component.setAlignment(TEXT_ALIGN.LEFT);
        expect(component.alignmentChange.next).toHaveBeenCalled();
    });
});
