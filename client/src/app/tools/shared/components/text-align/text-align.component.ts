import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TEXT_ALIGN } from '@app/classes/tools-utility';

@Component({
    selector: 'app-text-align',
    templateUrl: './text-align.component.html',
    styleUrls: ['./text-align.component.scss'],
})
export class TextAlignComponent {
    readonly title: string = 'Alignement du texte';

    @Input() alignment: TEXT_ALIGN;

    @Output() alignmentChange: EventEmitter<TEXT_ALIGN> = new EventEmitter<TEXT_ALIGN>();

    setAlignment(alignment: TEXT_ALIGN): void {
        this.alignmentChange.next(alignment);
    }
}
