import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatButtonToggleChange } from '@angular/material/button-toggle';

@Component({
    selector: 'app-texture',
    templateUrl: './texture.component.html',
    styleUrls: ['./texture.component.scss'],
})
export class TextureComponent {
    title: string = 'Texture';

    @Input() value: number;
    @Output() valueChange: EventEmitter<number> = new EventEmitter<number>();

    onChange(event: MatButtonToggleChange): void {
        this.valueChange.emit(event.value);
    }
}
