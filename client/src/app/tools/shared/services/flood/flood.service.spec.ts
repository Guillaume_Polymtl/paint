import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { BLACK } from '@app/classes/colors/color';
import { MouseButton } from '@app/classes/mouse-buttons';
import { DrawingService } from '@app/services/drawing/drawing.service';

import { FloodService } from './flood.service';

// tslint:disable:no-any
// tslint:disable:no-string-literal
describe('FloodService', () => {
    let service: FloodService;

    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let height: number;
    let width: number;

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        height = baseCtxStub.canvas.height;
        width = baseCtxStub.canvas.width;
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['isPristine', 'clearCanvas']);
        drawingServiceSpy.canvas = baseCtxStub.canvas;
        drawingServiceSpy.baseCtx = baseCtxStub;
        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawingServiceSpy }],
        });
        service = TestBed.inject(FloodService);
        service['drawingService'].baseCtx = baseCtxStub;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('getContinuousPositions should get all continuous positions', () => {
        drawingServiceSpy.baseCtx.fillStyle = BLACK.toString();
        drawingServiceSpy.baseCtx.fillRect(0, 0, width / 2, height);
        const click = new MouseEvent('', { button: MouseButton.Left, clientX: 0, clientY: 0 });
        const positions = service.getContinuousPositions(click, 0);
        const expectedPositionsCount = (width / 2) * height;
        expect(positions.length).toEqual(expectedPositionsCount);
    });

    it('getNonContinuousPositions should get all non-continuous positions', () => {
        const lineWidth = 10;
        drawingServiceSpy.baseCtx.fillStyle = BLACK.toString();
        drawingServiceSpy.baseCtx.fillRect(0, 0, lineWidth, height);
        drawingServiceSpy.baseCtx.fillRect(width - lineWidth, 0, lineWidth, height);
        const click = new MouseEvent('', { button: MouseButton.Right, clientX: 0, clientY: 0 });
        const positions = service.getNonContinuousPositions(click, 0);
        const expectedPositionsCount = 2 * height * lineWidth;
        expect(positions.length).toEqual(expectedPositionsCount);
    });

    it('getContinuousContourPositions should get all continuous contour positions', () => {
        const sideLength = 20;
        const nSidesSquare = 4;
        drawingServiceSpy.baseCtx.fillStyle = BLACK.toString();
        drawingServiceSpy.baseCtx.fillRect(width / 2, height / 2, sideLength, sideLength);
        const click = new MouseEvent('', { button: MouseButton.Left, clientX: width / 2 + 1, clientY: height / 2 + 2 });
        const positions = service.getContinuousContourPositions(click, 0);
        // remove 4 because corner are counted twice
        const expectedPositionsCount = nSidesSquare * sideLength - nSidesSquare;
        expect(positions.length).toEqual(expectedPositionsCount);
    });

    it('getNonContinuousContourPositions should get all non-continuous contour positions', () => {
        const sideLength = 20;
        const nSidesSquare = 4;
        // draw 2 20x20 squares
        drawingServiceSpy.baseCtx.fillStyle = BLACK.toString();
        drawingServiceSpy.baseCtx.fillRect(0, 0, sideLength, sideLength);
        drawingServiceSpy.baseCtx.fillRect(sideLength + 1, 0, sideLength, sideLength);
        const click = new MouseEvent('', { button: MouseButton.Right, clientX: 0, clientY: 0 });
        const positions = service.getNonContinuousContourPositions(click, 0);
        const expectedPositionsCountFor1Square = nSidesSquare * sideLength;
        const expectedPositionsCount = expectedPositionsCountFor1Square * 2;
        expect(positions.length).toEqual(expectedPositionsCount);
    });

    it('getNonContinuousPositions should get all the continuous background postions', () => {
        const click = new MouseEvent('', { button: MouseButton.Left, clientX: 0, clientY: 0 });
        const positions = service.getNonContinuousPositions(click, 0);
        const expectedPositionsCount = height * width;
        expect(positions.length).toEqual(expectedPositionsCount);
    });

    it('should get all the non continuous background postions', () => {
        const click = new MouseEvent('', { button: MouseButton.Right, clientX: 0, clientY: 0 });
        const positions = service.getNonContinuousPositions(click, 0);
        const expectedPositionsCount = height * width;
        expect(positions.length).toEqual(expectedPositionsCount);
    });

    it('should return nothing on invalid positions', () => {
        const click = new MouseEvent('', { button: MouseButton.Right, clientX: -1, clientY: -1 });
        let positions = service.getNonContinuousPositions(click, 0);
        const expectedPositionsCount = 0;
        expect(positions.length).toEqual(expectedPositionsCount);
        positions = service.getContinuousPositions(click, 0);
        expect(positions.length).toEqual(expectedPositionsCount);
        positions = service.getContinuousContourPositions(click, 0);
        expect(positions.length).toEqual(expectedPositionsCount);
    });
});
