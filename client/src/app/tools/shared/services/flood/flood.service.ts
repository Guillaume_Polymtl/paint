import { Injectable } from '@angular/core';
import { PIXEL_ALPHA_OFFSET, PIXEL_BLUE_OFFSET, PIXEL_DATA_SIZE, PIXEL_GREEN_OFFSET, PIXEL_RED_OFFSET } from '@app/classes/colors/color';
import { Tool } from '@app/classes/tool';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';

@Injectable({
    providedIn: 'root',
})
export class FloodService extends Tool {
    constructor(public drawingService: DrawingService, public colorService: ColorService) {
        super(drawingService, colorService);
    }
    private replacementColor: number[] = [];

    getContinuousPositions(clickedPosition: Vec2, tolerance: number): Vec2[] {
        const continuousPixels: Vec2[] = [];
        const width = this.drawingService.canvas.width;
        const height = this.drawingService.canvas.height;
        const imageData = this.drawingService.baseCtx.getImageData(0, 0, width, height);
        this.replacementColor = [this.color.red + 1, this.color.green + 1, this.color.blue + 1, this.color.alpha + 1];
        const targetColor = this.getColorAtPosition(clickedPosition, imageData);
        this.colorPixel(clickedPosition, this.replacementColor, imageData);
        const pixelStack: Vec2[] = [];
        pixelStack.push(clickedPosition);
        while (pixelStack.length) {
            const position = pixelStack[0];
            pixelStack.shift();
            if (position.x < 0 || position.x > width - 1 || position.y < 0 || position.y > height - 1) continue;
            continuousPixels.push(position);
            const westPosition = { x: position.x - 1, y: position.y };
            const westColor = this.getColorAtPosition(westPosition, imageData);
            if (this.colorsMatch(westColor, targetColor, tolerance)) {
                this.colorPixel(westPosition, this.replacementColor, imageData);
                pixelStack.push(westPosition);
            }
            const eastPosition = { x: position.x + 1, y: position.y };
            const eastColor = this.getColorAtPosition(eastPosition, imageData);
            if (this.colorsMatch(eastColor, targetColor, tolerance)) {
                this.colorPixel(eastPosition, this.replacementColor, imageData);
                pixelStack.push(eastPosition);
            }
            const northPosition = { x: position.x, y: position.y - 1 };
            const northColor = this.getColorAtPosition(northPosition, imageData);
            if (this.colorsMatch(northColor, targetColor, tolerance)) {
                this.colorPixel(northPosition, this.replacementColor, imageData);
                pixelStack.push(northPosition);
            }
            const southPosition = { x: position.x, y: position.y + 1 };
            const southColor = this.getColorAtPosition(southPosition, imageData);
            if (this.colorsMatch(southColor, targetColor, tolerance)) {
                this.colorPixel(southPosition, this.replacementColor, imageData);
                pixelStack.push(southPosition);
            }
        }
        return continuousPixels;
    }

    getContinuousContourPositions(clickedPosition: Vec2, tolerance: number): Vec2[] {
        const contour: Vec2[] = [];
        const width = this.drawingService.canvas.width;
        const height = this.drawingService.canvas.height;
        const imageData = this.drawingService.baseCtx.getImageData(0, 0, width, height);
        const replacementColor = [this.color.red, this.color.green, this.color.blue, this.color.alpha];
        const targetColor = this.getColorAtPosition(clickedPosition, imageData);
        this.colorPixel(clickedPosition, replacementColor, imageData);
        const pixelStack: Vec2[] = [];
        pixelStack.push(clickedPosition);
        while (pixelStack.length) {
            const position = pixelStack[0];
            pixelStack.shift();
            if (position.x < 0 || position.x > width - 1 || position.y < 0 || position.y > height - 1) continue;
            const westPosition = { x: position.x - 1, y: position.y };
            const westColor = this.getColorAtPosition(westPosition, imageData);
            if (this.colorsMatch(westColor, targetColor, tolerance)) {
                this.colorPixel(westPosition, replacementColor, imageData);
                pixelStack.push(westPosition);
            } else if (!this.colorsMatch(westColor, replacementColor, tolerance)) {
                contour.push(position);
            }
            const eastPosition = { x: position.x + 1, y: position.y };
            const eastColor = this.getColorAtPosition(eastPosition, imageData);
            if (this.colorsMatch(eastColor, targetColor, tolerance)) {
                this.colorPixel(eastPosition, replacementColor, imageData);
                pixelStack.push(eastPosition);
            } else if (!this.colorsMatch(eastColor, replacementColor, tolerance)) {
                contour.push(position);
            }
            const northPosition = { x: position.x, y: position.y - 1 };
            const northColor = this.getColorAtPosition(northPosition, imageData);
            if (this.colorsMatch(northColor, targetColor, tolerance)) {
                this.colorPixel(northPosition, replacementColor, imageData);
                pixelStack.push(northPosition);
            } else if (!this.colorsMatch(northColor, replacementColor, tolerance)) {
                contour.push(position);
            }
            const southPosition = { x: position.x, y: position.y + 1 };
            const southColor = this.getColorAtPosition(southPosition, imageData);
            if (this.colorsMatch(southColor, targetColor, tolerance)) {
                this.colorPixel(southPosition, replacementColor, imageData);
                pixelStack.push(southPosition);
            } else if (!this.colorsMatch(southColor, replacementColor, tolerance)) {
                contour.push(position);
            }
        }
        return this.removeDuplicatePositions(contour);
    }

    getNonContinuousPositions(clickedPosition: Vec2, tolerance: number): Vec2[] {
        const pixels: Vec2[] = [];
        const imageData = this.drawingService.baseCtx.getImageData(
            0,
            0,
            this.drawingService.baseCtx.canvas.width,
            this.drawingService.baseCtx.canvas.height,
        );
        const clickedColor = this.getColorAtPosition(clickedPosition, imageData);
        for (let x = 0; x < imageData.width; x++) {
            for (let y = 0; y < imageData.height; y++) {
                const color = this.getColorAtPosition({ x, y }, imageData);
                if (this.colorsMatch(clickedColor, color, tolerance)) {
                    pixels.push({ x, y });
                }
            }
        }
        return pixels;
    }

    getNonContinuousContourPositions(clickedPosition: Vec2, tolerance: number): Vec2[] {
        const positions: Vec2[] = [];
        const imageData = this.drawingService.baseCtx.getImageData(
            0,
            0,
            this.drawingService.baseCtx.canvas.width,
            this.drawingService.baseCtx.canvas.height,
        );
        const clickedColor = this.getColorAtPosition(clickedPosition, imageData);
        for (let x = 0; x < imageData.width; x++) {
            for (let y = 0; y < imageData.height; y++) {
                const color = this.getColorAtPosition({ x, y }, imageData);
                if (this.colorsMatch(clickedColor, color, tolerance)) {
                    positions.push({ x, y });
                }
            }
        }
        const contour: Vec2[] = [];
        for (const position of positions) {
            const color = this.getColorAtPosition(position, imageData);
            const north = { x: position.x, y: position.y - 1 };
            const northColor = this.getColorAtPosition(north, imageData);
            if (!this.colorsMatch(color, northColor, tolerance)) {
                contour.push(north);
            }
            const south = { x: position.x, y: position.y + 1 };
            const southColor = this.getColorAtPosition(south, imageData);
            if (!this.colorsMatch(color, southColor, tolerance)) {
                contour.push(south);
            }
            const east = { x: position.x + 1, y: position.y };
            const eastColor = this.getColorAtPosition(east, imageData);
            if (!this.colorsMatch(color, eastColor, tolerance)) {
                contour.push(east);
            }
            const west = { x: position.x - 1, y: position.y };
            const westColor = this.getColorAtPosition(west, imageData);
            if (!this.colorsMatch(color, westColor, tolerance)) {
                contour.push(west);
            }
        }
        return this.removeDuplicatePositions(contour);
    }

    private removeDuplicatePositions(positions: Vec2[]): Vec2[] {
        const uniques: Vec2[] = positions.filter((position, index, self) => {
            return index === self.indexOf(position);
        });
        const shuffled = this.shuffle(uniques);
        return shuffled;
    }

    private shuffle(array: Vec2[]): Vec2[] {
        let currentIndex = array.length;
        let temporaryValue: Vec2;
        let randomIndex: number;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    colorsMatch(color1: number[], color2: number[], tolerance: number): boolean {
        // Both colors match (exacly)
        if (
            color1[PIXEL_RED_OFFSET] === color2[PIXEL_RED_OFFSET] &&
            color1[PIXEL_GREEN_OFFSET] === color2[PIXEL_GREEN_OFFSET] &&
            color1[PIXEL_BLUE_OFFSET] === color2[PIXEL_BLUE_OFFSET] &&
            color1[PIXEL_ALPHA_OFFSET] === color2[PIXEL_ALPHA_OFFSET]
        ) {
            return true;
        }
        // Already the target color
        if (
            color1[PIXEL_RED_OFFSET] === this.replacementColor[PIXEL_RED_OFFSET] &&
            color1[PIXEL_GREEN_OFFSET] === this.replacementColor[PIXEL_GREEN_OFFSET] &&
            color1[PIXEL_BLUE_OFFSET] === this.replacementColor[PIXEL_BLUE_OFFSET] &&
            color1[PIXEL_ALPHA_OFFSET] === this.replacementColor[PIXEL_ALPHA_OFFSET]
        ) {
            return false;
        }
        return false;
    }

    getColorAtPosition(position: Vec2, imageData: ImageData): number[] {
        const width = this.drawingService.canvas.width;
        const offset = (position.y * width + position.x) * PIXEL_DATA_SIZE;
        const data = imageData.data;
        const red = data[offset + PIXEL_RED_OFFSET];
        const green = data[offset + PIXEL_GREEN_OFFSET];
        const blue = data[offset + PIXEL_BLUE_OFFSET];
        const alpha = data[offset + PIXEL_ALPHA_OFFSET];
        return [red, green, blue, alpha];
    }

    colorPixel(position: Vec2, color: number[], imageData: ImageData): void {
        const width = imageData.width;
        const offset = (position.y * width + position.x) * PIXEL_DATA_SIZE;
        imageData.data[offset + PIXEL_RED_OFFSET] = color[PIXEL_RED_OFFSET];
        imageData.data[offset + PIXEL_GREEN_OFFSET] = color[PIXEL_GREEN_OFFSET];
        imageData.data[offset + PIXEL_BLUE_OFFSET] = color[PIXEL_BLUE_OFFSET];
        imageData.data[offset + PIXEL_ALPHA_OFFSET] = color[PIXEL_ALPHA_OFFSET];
    }
}
