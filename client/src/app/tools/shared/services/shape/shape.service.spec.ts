import { TestBed } from '@angular/core/testing';
import { Cardinal } from '@app/classes/anchors/cardinal';
import { ShapeService } from './shape.service';

describe('ShapeService', () => {
    let service: ShapeService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ShapeService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return the proper value when force square is true', () => {
        const mouseDownCoord = new MouseEvent('', { clientX: 0, clientY: 0 });
        const currentMousePosition = new MouseEvent('', { clientX: 1, clientY: 2 });
        const anchors = ShapeService.getAnchors(mouseDownCoord, currentMousePosition, true);
        expect(anchors.get(Cardinal.SouthEast)?.y).toEqual(1);
    });

    it('should compute the proper start position for NW/SE', () => {
        const mouseDownCoord = new MouseEvent('', { clientX: 0, clientY: 0 });
        const currentMousePosition = new MouseEvent('', { clientX: 1, clientY: 1 });
        const startPosition = ShapeService.computeStartPosition(mouseDownCoord, currentMousePosition, true);
        const expectedX = mouseDownCoord.clientX;
        expect(startPosition.x).toEqual(expectedX);
        const expectedY = mouseDownCoord.clientY;
        expect(startPosition.y).toEqual(expectedY);
    });

    it('should compute the proper start position for SE/NW', () => {
        const mouseDownCoord = new MouseEvent('', { clientX: 1, clientY: 1 });
        const currentMousePosition = new MouseEvent('', { clientX: 0, clientY: 0 });
        let startPosition = ShapeService.computeStartPosition(mouseDownCoord, currentMousePosition, true);
        let expectedX = currentMousePosition.clientX;
        expect(startPosition.x).toEqual(expectedX);
        let expectedY = currentMousePosition.clientY;
        expect(startPosition.y).toEqual(expectedY);
        startPosition = ShapeService.computeStartPosition(mouseDownCoord, currentMousePosition, false);
        expectedX = currentMousePosition.clientX;
        expect(startPosition.x).toEqual(expectedX);
        expectedY = currentMousePosition.clientY;
        expect(startPosition.y).toEqual(expectedY);
    });

    it('should compute the proper start position for SW/NE', () => {
        const mouseDownCoord = new MouseEvent('', { clientX: 0, clientY: 1 });
        const currentMousePosition = new MouseEvent('', { clientX: 1, clientY: 0 });
        let startPosition = ShapeService.computeStartPosition(mouseDownCoord, currentMousePosition, true);
        let expectedX = mouseDownCoord.clientX;
        expect(startPosition.x).toEqual(expectedX);
        let expectedY = currentMousePosition.clientY;
        expect(startPosition.y).toEqual(expectedY);
        startPosition = ShapeService.computeStartPosition(mouseDownCoord, currentMousePosition, false);
        expectedX = mouseDownCoord.clientX;
        expect(startPosition.x).toEqual(expectedX);
        expectedY = currentMousePosition.clientY;
        expect(startPosition.y).toEqual(expectedY);
    });

    it('should compute the proper start position for NE/SW', () => {
        const mouseDownCoord = new MouseEvent('', { clientX: 1, clientY: 0 });
        const currentMousePosition = new MouseEvent('', { clientX: 0, clientY: 1 });
        let startPosition = ShapeService.computeStartPosition(mouseDownCoord, currentMousePosition, true);
        let expectedX = currentMousePosition.clientX;
        expect(startPosition.x).toEqual(expectedX);
        let expectedY = mouseDownCoord.clientY;
        expect(startPosition.y).toEqual(expectedY);
        startPosition = ShapeService.computeStartPosition(mouseDownCoord, currentMousePosition, false);
        expectedX = currentMousePosition.clientX;
        expect(startPosition.x).toEqual(expectedX);
        expectedY = mouseDownCoord.clientY;
        expect(startPosition.y).toEqual(expectedY);
    });
});
