import { Injectable } from '@angular/core';
import { Cardinal } from '@app/classes/anchors/cardinal';
import { Vec2 } from '@app/classes/vec2';

@Injectable({
    providedIn: 'root',
})
export class ShapeService {
    static getAnchors(startCorner: Vec2, endCorner: Vec2, forceSquare: boolean): Map<Cardinal, Vec2> {
        let startPosition: Vec2 = { x: startCorner.x, y: startCorner.y };
        startPosition = this.computeStartPosition(startCorner, endCorner, forceSquare);
        let width = startCorner.x - endCorner.x;
        let height = startCorner.y - endCorner.y;
        width = width < 0 ? -width : width;
        height = height < 0 ? -height : height;
        if (forceSquare) {
            const squareSize = Math.min(width, height);
            width = squareSize;
            height = squareSize;
        }
        const middleX = startPosition.x + width / 2;
        const middleY = startPosition.y + height / 2;
        const northWest: Vec2 = { x: startPosition.x, y: startPosition.y };
        const north: Vec2 = { x: middleX, y: startPosition.y };
        const northEast: Vec2 = { x: startPosition.x + width, y: startPosition.y };
        const east: Vec2 = { x: startPosition.x + width, y: middleY };
        const southEast: Vec2 = { x: startPosition.x + width, y: startPosition.y + height };
        const south: Vec2 = { x: middleX, y: startPosition.y + height };
        const southWest: Vec2 = { x: startPosition.x, y: startPosition.y + height };
        const west: Vec2 = { x: startPosition.x, y: middleY };

        const boundingPoints = new Map()
            .set(Cardinal.NorthWest, northWest)
            .set(Cardinal.North, north)
            .set(Cardinal.NorthEast, northEast)
            .set(Cardinal.East, east)
            .set(Cardinal.SouthEast, southEast)
            .set(Cardinal.South, south)
            .set(Cardinal.SouthWest, southWest)
            .set(Cardinal.West, west);

        return boundingPoints;
    }

    static getDistance(start: Vec2, end: Vec2): Vec2 {
        const x = Math.abs(start.x - end.x);
        const y = Math.abs(start.y - end.y);
        return { x, y };
    }

    static computeStartPosition(startCorner: Vec2, endCorner: Vec2, forceSquare: boolean): Vec2 {
        const startPosition: Vec2 = { x: startCorner.x, y: startCorner.y };
        const width = startCorner.x - endCorner.x;
        const height = startCorner.y - endCorner.y;
        const positiveWidth = width < 0 ? -width : width;
        const positiveHeight = height < 0 ? -height : height;
        const min: number = positiveWidth < positiveHeight ? positiveWidth : positiveHeight;
        // Du bas à droite à gauche en haut
        if (width > 0 && height > 0) {
            if (!forceSquare) {
                startPosition.x = endCorner.x;
                startPosition.y = endCorner.y;
            } else {
                startPosition.x = startCorner.x - min;
                startPosition.y = startCorner.y - min;
            }

            // Du bas à gauche à droite en haut
        } else if (width < 0 && height > 0) {
            if (!forceSquare) {
                startPosition.y = endCorner.y;
            } else {
                startPosition.y = startCorner.y - min;
            }

            // Du haut à droite à gauche en bas
        } else if (width > 0 && height < 0) {
            if (!forceSquare) {
                startPosition.x = endCorner.x;
            } else {
                startPosition.x = startCorner.x - min;
            }
        } // Sinon du haut à gauche à droite en bas

        return startPosition;
    }
}
