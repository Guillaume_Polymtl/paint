import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSliderModule } from '@angular/material/slider';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';
import { SpraypaintService } from '@app/tools/spraypaint/spraypaint.service';
import { SpraypaintAttributesComponent } from './spraypaint-attributes.component';

const VALID_JETDIAMETER = 10;
const VALID_SPRAYDROPLETDIAMETER = 3;
const VALID_EMISSIONS_PER_SECOND = 2000;

describe('SpraypaintAttributesComponent', () => {
    let component: SpraypaintAttributesComponent;
    let fixture: ComponentFixture<SpraypaintAttributesComponent>;
    let serviceSpy: jasmine.SpyObj<SpraypaintService>;

    beforeEach(async(() => {
        serviceSpy = jasmine.createSpyObj('SprayService', [
            'setJetDiameter',
            'getJetDiameter',
            'setSprayDropletDiameter',
            'getSprayDropletDiameter',
            'setEmissionsPerSecond',
            'getEmissionsPerSecond',
        ]);
        TestBed.configureTestingModule({
            declarations: [SpraypaintAttributesComponent, SliderComponent],
            providers: [{ provide: SpraypaintService, useValue: serviceSpy }],
            imports: [MatSliderModule],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SpraypaintAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call getJetDiameter() on init', () => {
        expect(serviceSpy.getJetDiameter).toHaveBeenCalled();
    });

    it('should call getSprayDropletDiameter() on init', () => {
        expect(serviceSpy.getSprayDropletDiameter).toHaveBeenCalled();
    });

    it('should call getEmissionsPerSecond() on init', () => {
        expect(serviceSpy.getEmissionsPerSecond).toHaveBeenCalled();
    });

    it('should call SpraypaintServicesetJetDiameter()', () => {
        component.setJetDiameter(VALID_JETDIAMETER);
        expect(serviceSpy.setJetDiameter).toHaveBeenCalledWith(VALID_JETDIAMETER);
    });

    it('should call SpraypaintService.setSprayDropletDiameter()', () => {
        component.setSprayDropletDiameter(VALID_SPRAYDROPLETDIAMETER);
        expect(serviceSpy.setSprayDropletDiameter).toHaveBeenCalledWith(VALID_SPRAYDROPLETDIAMETER);
    });

    it('should call SpraypaintService.setEmissionsPerSecond()', () => {
        component.setEmissionsPerSecond(VALID_EMISSIONS_PER_SECOND);
        expect(serviceSpy.setEmissionsPerSecond).toHaveBeenCalledWith(VALID_EMISSIONS_PER_SECOND);
    });
});
