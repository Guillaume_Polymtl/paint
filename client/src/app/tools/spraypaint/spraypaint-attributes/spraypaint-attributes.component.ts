import { Component, OnInit } from '@angular/core';
import {
    SPRAYPAINT_DROPLET_DIAMETER_MAX,
    SPRAYPAINT_DROPLET_DIAMETER_MIN,
    SPRAYPAINT_MAX_EMISSIONS_PER_SECOND,
    SPRAYPAINT_MAX_JET_DIAMETER,
    SPRAYPAINT_MIN_EMISSIONS_PER_SECOND,
    SPRAYPAINT_MIN_JET_DIAMETER,
} from '@app/classes/tools-utility';
import { SpraypaintService } from '@app/tools/spraypaint/spraypaint.service';

@Component({
    selector: 'app-spraypaint-attributes',
    templateUrl: './spraypaint-attributes.component.html',
    styleUrls: ['./spraypaint-attributes.component.scss'],
})
export class SpraypaintAttributesComponent implements OnInit {
    readonly title: string = 'Aérosol';

    defaultJetDiameter: number;
    defaultSprayDropletDiameter: number;
    defaultEmissionsPerSecond: number;

    readonly minJetDiameter: number = SPRAYPAINT_MIN_JET_DIAMETER;
    readonly maxJetDiameter: number = SPRAYPAINT_MAX_JET_DIAMETER;

    readonly minSprayDropletDiameter: number = SPRAYPAINT_DROPLET_DIAMETER_MIN;
    readonly maxSprayDropletDiameter: number = SPRAYPAINT_DROPLET_DIAMETER_MAX;

    readonly minEmissionsPerSecond: number = SPRAYPAINT_MIN_EMISSIONS_PER_SECOND;
    readonly maxEmissionsPerSecond: number = SPRAYPAINT_MAX_EMISSIONS_PER_SECOND;

    readonly jetDiameter: string = 'Diamètre du jet';
    readonly sprayDropletDiameter: string = 'Diamètre des gouttelettes';

    constructor(private spraypaintService: SpraypaintService) {}

    ngOnInit(): void {
        this.defaultJetDiameter = this.spraypaintService.getJetDiameter();
        this.defaultSprayDropletDiameter = this.spraypaintService.getSprayDropletDiameter();
        this.defaultEmissionsPerSecond = this.spraypaintService.getEmissionsPerSecond();
    }

    setJetDiameter(jetDiameter: number): void {
        this.spraypaintService.setJetDiameter(jetDiameter);
    }

    setSprayDropletDiameter(sprayDropletDiameter: number): void {
        this.spraypaintService.setSprayDropletDiameter(sprayDropletDiameter);
    }

    setEmissionsPerSecond(emissionsPerSecond: number): void {
        this.spraypaintService.setEmissionsPerSecond(emissionsPerSecond);
    }

    formatLabel(value: number): string {
        return (value / 100).toString();
    }
}
