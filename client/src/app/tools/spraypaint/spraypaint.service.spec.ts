import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { MouseButton } from '@app/classes/mouse-buttons';
import {
    SPRAYPAINT_DROPLET_DIAMETER_MAX,
    SPRAYPAINT_DROPLET_DIAMETER_MIN,
    SPRAYPAINT_MAX_EMISSIONS_PER_SECOND,
    SPRAYPAINT_MAX_JET_DIAMETER,
    SPRAYPAINT_MIN_EMISSIONS_PER_SECOND,
    SPRAYPAINT_MIN_JET_DIAMETER,
} from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { SpraypaintService } from '@app/tools/spraypaint/spraypaint.service';

describe('SprayService', () => {
    let service: SpraypaintService;

    let mouseEvent: MouseEvent;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);

        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawServiceSpy }],
        });
        service = TestBed.inject(SpraypaintService);

        // tslint:disable:no-string-literal
        service['drawingService'].baseCtx = baseCtxStub; // Jasmine doesnt copy properties with underlying data
        service['drawingService'].previewCtx = previewCtxStub;

        mouseEvent = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Left,
        } as MouseEvent;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('setJetDiameter should change the width if correct', () => {
        const currentJetDiameter = service.getJetDiameter();
        const expectedJetDiameter = SPRAYPAINT_MAX_JET_DIAMETER - 1;
        service.setJetDiameter(expectedJetDiameter);
        expect(service.getJetDiameter()).not.toEqual(currentJetDiameter);
        expect(service.getJetDiameter()).toEqual(expectedJetDiameter);
    });

    it('setJetDiameter should set width to min if entry is smaller', () => {
        const jetDiameter = SPRAYPAINT_MIN_JET_DIAMETER - 1;
        service.setJetDiameter(jetDiameter);
        expect(service.getJetDiameter()).toEqual(SPRAYPAINT_MIN_JET_DIAMETER);
    });

    it('setJetDiameter should set width to max if entry is greater', () => {
        const jetDiameter = SPRAYPAINT_MAX_JET_DIAMETER + 1;
        service.setJetDiameter(jetDiameter);
        expect(service.getJetDiameter()).toEqual(SPRAYPAINT_MAX_JET_DIAMETER);
    });

    it('setSprayDropletDiameter should change sprayDropletDiameterthe  if correct', () => {
        const currentSprayDropletDiameter = service.getSprayDropletDiameter();
        const expectedSprayDropletDiameter = SPRAYPAINT_DROPLET_DIAMETER_MAX - 1;
        service.setSprayDropletDiameter(expectedSprayDropletDiameter);
        expect(service.getSprayDropletDiameter()).not.toEqual(currentSprayDropletDiameter);
        expect(service.getSprayDropletDiameter()).toEqual(expectedSprayDropletDiameter);
    });

    it('setSprayDropletDiameter should set sprayDropletDiameter to min if entry is smaller', () => {
        const sprayDropletDiameter = SPRAYPAINT_DROPLET_DIAMETER_MIN - 1;
        service.setSprayDropletDiameter(sprayDropletDiameter);
        expect(service.getSprayDropletDiameter()).toEqual(SPRAYPAINT_DROPLET_DIAMETER_MIN);
    });

    it('setSprayDropletDiameter should set sprayDropletDiameter to max if entry is greater', () => {
        const sprayDropletDiameter = SPRAYPAINT_DROPLET_DIAMETER_MAX + 1;
        service.setSprayDropletDiameter(sprayDropletDiameter);
        expect(service.getSprayDropletDiameter()).toEqual(SPRAYPAINT_DROPLET_DIAMETER_MAX);
    });

    it('setEmissionsPerSecond should change the number of issues per second if correct', () => {
        const currentEmissionsPerSecond = service.getEmissionsPerSecond();
        const expectedEmissionsPerSecond = SPRAYPAINT_MAX_EMISSIONS_PER_SECOND - 1;
        service.setEmissionsPerSecond(expectedEmissionsPerSecond);
        expect(service.getEmissionsPerSecond()).not.toEqual(currentEmissionsPerSecond);
        expect(service.getEmissionsPerSecond()).toEqual(expectedEmissionsPerSecond);
    });

    it('setEmissionsPerSecond should set number of issues per second to min if entry is smaller', () => {
        const emissionsPerSecond = SPRAYPAINT_MIN_EMISSIONS_PER_SECOND - 1;
        service.setEmissionsPerSecond(emissionsPerSecond);
        expect(service.getEmissionsPerSecond()).toEqual(SPRAYPAINT_MIN_EMISSIONS_PER_SECOND);
    });

    it('setEmissionsPerSecond should set number of issues per second to max if entry is greater', () => {
        const emissionsPerSecond = SPRAYPAINT_MAX_EMISSIONS_PER_SECOND + 1;
        service.setEmissionsPerSecond(emissionsPerSecond);
        expect(service.getEmissionsPerSecond()).toEqual(SPRAYPAINT_MAX_EMISSIONS_PER_SECOND);
    });

    it(' onMouseDown should set mouseDownCoord to the correct position and onMouseMove to the current Position', () => {
        const expectedResult: Vec2 = service.getPositionFromMouse(mouseEvent);
        service.onMouseDown(mouseEvent);
        service.onMouseMove(mouseEvent);
        expect(service.mouseDownCoord).toEqual(expectedResult);
    });

    it(' onMouseDown should set mouseDown to true on the left click', () => {
        service.onMouseDown(mouseEvent);
        expect(service.mouseDown).toEqual(true);
    });

    it(' onMouseDown and onMouseMove should set mouseDown property to false when call the right click', () => {
        const mouseEventRClick = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;
        service.onMouseDown(mouseEventRClick);
        service.onMouseMove(mouseEvent);
        expect(service.mouseDown).toEqual(false);
    });

    it(' onMouseUp should call clearInterval when call the left click and set mouseDown to true', () => {
        service.mouseDown = true;
        const clearIntervalSpy: jasmine.Spy = spyOn(window, 'clearInterval');
        service.onMouseUp(mouseEvent);
        expect(clearIntervalSpy).toHaveBeenCalled();
    });

    it('onMouseUp should not call clearInterval when call the right click', () => {
        const mouseEventRight = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;
        const clearIntervalSpy: jasmine.Spy = spyOn(window, 'clearInterval');
        service.onMouseUp(mouseEventRight);
        expect(clearIntervalSpy).not.toHaveBeenCalled();
    });

    it(' drawSpray should fill the rectangle of the spray ', () => {
        const fillRectSpy: jasmine.Spy = spyOn(baseCtxStub, 'fillRect');
        service.onMouseDown(mouseEvent);
        expect(fillRectSpy).toHaveBeenCalled();
    });
});
