import { Injectable } from '@angular/core';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import {
    SECOND_DURATION_MS,
    SPRAYPAINT_DEFAULT_DROPLET_DIAMETER,
    SPRAYPAINT_DEFAULT_EMISSIONS_PER_SECOND,
    SPRAYPAINT_DEFAULT_JET_DIAMETER,
    SPRAYPAINT_DROPLET_DIAMETER_MAX,
    SPRAYPAINT_DROPLET_DIAMETER_MIN,
    SPRAYPAINT_INTERVAL_MS,
    SPRAYPAINT_MAX_EMISSIONS_PER_SECOND,
    SPRAYPAINT_MAX_JET_DIAMETER,
    SPRAYPAINT_MIN_EMISSIONS_PER_SECOND,
    SPRAYPAINT_MIN_JET_DIAMETER,
} from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';

@Injectable({
    providedIn: 'root',
})
export class SpraypaintService extends Tool {
    readonly name: ToolName = ToolName.SPRAYPAINT;
    private jetDiameter: number;
    private sprayDropletDiameter: number;
    private emissionsPerSecond: number;
    private sprayIntervalID: number;
    private currentMousePosition: Vec2;

    constructor(drawingService: DrawingService, public colorService: ColorService) {
        super(drawingService, colorService);
        this.jetDiameter = SPRAYPAINT_DEFAULT_JET_DIAMETER;
        this.sprayDropletDiameter = SPRAYPAINT_DEFAULT_DROPLET_DIAMETER;
        this.emissionsPerSecond = SPRAYPAINT_DEFAULT_EMISSIONS_PER_SECOND;
    }

    getJetDiameter(): number {
        return this.jetDiameter;
    }

    setJetDiameter(jetDiameter: number): void {
        if (jetDiameter >= SPRAYPAINT_MIN_JET_DIAMETER && jetDiameter <= SPRAYPAINT_MAX_JET_DIAMETER) {
            this.jetDiameter = jetDiameter;
        } else if (jetDiameter < SPRAYPAINT_MIN_JET_DIAMETER) {
            this.jetDiameter = SPRAYPAINT_MIN_JET_DIAMETER;
        } else {
            this.jetDiameter = SPRAYPAINT_MAX_JET_DIAMETER;
        }
    }

    getSprayDropletDiameter(): number {
        return this.sprayDropletDiameter;
    }

    setSprayDropletDiameter(sprayDropletDiameter: number): void {
        if (sprayDropletDiameter >= SPRAYPAINT_DROPLET_DIAMETER_MIN && sprayDropletDiameter <= SPRAYPAINT_DROPLET_DIAMETER_MAX) {
            this.sprayDropletDiameter = sprayDropletDiameter;
        } else if (sprayDropletDiameter < SPRAYPAINT_DROPLET_DIAMETER_MIN) {
            this.sprayDropletDiameter = SPRAYPAINT_DROPLET_DIAMETER_MIN;
        } else {
            this.sprayDropletDiameter = SPRAYPAINT_DROPLET_DIAMETER_MAX;
        }
    }

    getEmissionsPerSecond(): number {
        return this.emissionsPerSecond;
    }

    setEmissionsPerSecond(emissionsPerSecond: number): void {
        if (emissionsPerSecond >= SPRAYPAINT_MIN_EMISSIONS_PER_SECOND && emissionsPerSecond <= SPRAYPAINT_MAX_EMISSIONS_PER_SECOND) {
            this.emissionsPerSecond = emissionsPerSecond;
        } else if (emissionsPerSecond < SPRAYPAINT_MIN_EMISSIONS_PER_SECOND) {
            this.emissionsPerSecond = SPRAYPAINT_MIN_EMISSIONS_PER_SECOND;
        } else {
            this.emissionsPerSecond = SPRAYPAINT_MAX_EMISSIONS_PER_SECOND;
        }
    }

    onMouseDown(event: MouseEvent): void {
        this.mouseDown = event.button === MouseButton.Left;
        if (this.mouseDown) {
            this.mouseDownCoord = this.getPositionFromMouse(event);
            this.currentMousePosition = this.mouseDownCoord;
            this.drawSpray();
            this.sprayIntervalID = window.setInterval(this.drawSpray.bind(this), SPRAYPAINT_INTERVAL_MS);
        }
    }

    onMouseUp(event: MouseEvent): void {
        if (this.mouseDown && event.button === MouseButton.Left) {
            this.currentMousePosition = this.getPositionFromMouse(event);
            this.mouseDown = false;
            clearInterval(this.sprayIntervalID);
            this.drawingService.autoSave();
        }
    }

    onMouseMove(event: MouseEvent): void {
        if (this.mouseDown) {
            this.currentMousePosition = this.getPositionFromMouse(event);
        }
    }

    getRandomFloat(min: number, max: number): number {
        return Math.random() * (max - min + 1);
    }

    private drawSpray(): void {
        const ctx = this.drawingService.baseCtx;
        ctx.fillStyle = this.color.toString();

        const density = (this.emissionsPerSecond * SPRAYPAINT_INTERVAL_MS) / SECOND_DURATION_MS;
        for (let i = 0; i < density; i++) {
            const angle = this.getRandomFloat(0, Math.PI * 2);
            const radius = this.getRandomFloat(0, this.jetDiameter);
            ctx.globalAlpha = Math.random();
            ctx.fillRect(
                this.currentMousePosition.x + radius * Math.cos(angle),
                this.currentMousePosition.y + radius * Math.sin(angle),
                this.sprayDropletDiameter,
                this.sprayDropletDiameter,
            );
        }
    }
}
