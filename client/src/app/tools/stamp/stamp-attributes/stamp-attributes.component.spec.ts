import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSliderModule } from '@angular/material/slider';
import { StampChoices } from '@app/classes/tools-utility';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';
import { StampService } from '@app/tools/stamp/stamp.service';
import { of } from 'rxjs/internal/observable/of';
import { StampAttributesComponent } from './stamp-attributes.component';

describe('StampAttributesComponent', () => {
    let component: StampAttributesComponent;
    let fixture: ComponentFixture<StampAttributesComponent>;
    let stampServiceSpy: jasmine.SpyObj<StampService>;

    beforeEach(async(() => {
        stampServiceSpy = jasmine.createSpyObj('StampService', ['setWidth', 'getWidth', 'setAngle', 'getAngle', 'setStampChoice', 'getStampChoice']);
        stampServiceSpy.setWidth.and.returnValue();
        stampServiceSpy.setAngle.and.returnValue();
        stampServiceSpy.getAngle.and.returnValue(of(0));
        TestBed.configureTestingModule({
            declarations: [StampAttributesComponent, SliderComponent],
            providers: [{ provide: StampService, useValue: stampServiceSpy }],
            imports: [MatSliderModule],
            schemas: [NO_ERRORS_SCHEMA],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StampAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call getWidth() on init', () => {
        expect(stampServiceSpy.getWidth).toHaveBeenCalled();
    });

    it('should call StampService.setWidth() on widthChange', () => {
        const VALID_WIDTH = 20;
        component.setWidth(VALID_WIDTH);
        expect(stampServiceSpy.setWidth).toHaveBeenCalledWith(VALID_WIDTH);
    });

    it('should call getAngle() on init', () => {
        expect(stampServiceSpy.getAngle).toHaveBeenCalled();
    });

    it('should call setAngle() on angleChange', () => {
        const VALID_ANGLE = 0;
        component.setAngle(VALID_ANGLE);
        expect(stampServiceSpy.setAngle).toHaveBeenCalledWith(VALID_ANGLE);
    });

    it('should call getStampChoice() on init', () => {
        expect(stampServiceSpy.getStampChoice).toHaveBeenCalled();
    });

    it('should call stampService.setStampChoice', () => {
        const value = StampChoices.StampChoices2;
        component.setStampChoice(value);
        expect(stampServiceSpy.setStampChoice).toHaveBeenCalledWith(value);
    });
});
