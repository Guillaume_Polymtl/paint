import { Component, OnDestroy, OnInit } from '@angular/core';
import { MAX_ANGLE, MIN_ANGLE, StampChoices, STAMP_MAX_WIDTH, STAMP_MIN_WIDTH } from '@app/classes/tools-utility';
import { StampService } from '@app/tools/stamp/stamp.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
@Component({
    selector: 'app-stamp-attributes',
    templateUrl: './stamp-attributes.component.html',
    styleUrls: ['./stamp-attributes.component.scss'],
})
export class StampAttributesComponent implements OnInit, OnDestroy {
    title: string = 'Étampe';

    defaultWidth: number;
    angle: number;
    stampChoice: StampChoices;
    private destroyed: Subject<boolean> = new Subject();

    readonly maxWidth: number = STAMP_MAX_WIDTH;
    readonly minWidth: number = STAMP_MIN_WIDTH;

    readonly maxAngle: number = MAX_ANGLE;
    readonly minAngle: number = MIN_ANGLE;

    readonly widthTitle: string = "Taille de l'étampe";
    readonly angleTitle: string = 'Angle de rotation';

    constructor(private stampService: StampService) {}

    ngOnInit(): void {
        this.defaultWidth = this.stampService.getWidth();
        this.stampChoice = this.stampService.getStampChoice();
        this.stampService
            .getAngle()
            .pipe(takeUntil(this.destroyed))
            .subscribe((angle: number) => {
                this.angle = angle;
            });
    }
    ngOnDestroy(): void {
        this.destroyed.next();
        this.destroyed.complete();
    }

    setWidth(width: number): void {
        this.stampService.setWidth(width);
    }

    setAngle(angle: number): void {
        this.stampService.setAngle(angle);
    }

    setStampChoice(stampChoice: StampChoices): void {
        this.stampService.setStampChoice(stampChoice);
    }
}
