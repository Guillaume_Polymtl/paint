import { TestBed } from '@angular/core/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import { MouseButton } from '@app/classes/mouse-buttons';
import { MAX_ANGLE, MIN_ANGLE, StampChoices, STAMP_MAX_WIDTH, STAMP_MIN_WIDTH } from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { StampService } from '@app/tools/stamp/stamp.service';

// tslint:disable:no-any
describe('StampService', () => {
    let service: StampService;
    let mouseEvent: MouseEvent;
    let drawServiceSpy: jasmine.SpyObj<DrawingService>;
    let drawIconSpy: jasmine.Spy<any>;

    let baseCtxStub: CanvasRenderingContext2D;
    let previewCtxStub: CanvasRenderingContext2D;

    beforeEach(() => {
        baseCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        previewCtxStub = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);

        TestBed.configureTestingModule({
            providers: [{ provide: DrawingService, useValue: drawServiceSpy }],
        });
        service = TestBed.inject(StampService);

        // Configuration du spy du service
        // tslint:disable:no-string-literal
        service['drawingService'].canvas = CanvasTestHelper.canvas();
        service['drawingService'].baseCtx = baseCtxStub; // Jasmine doesnt copy properties with underlying data
        service['drawingService'].previewCtx = previewCtxStub;

        drawIconSpy = spyOn<any>(service, 'drawIcon').and.callThrough();

        mouseEvent = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Left,
        } as MouseEvent;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('setWidth should change the width if correct', () => {
        const currentWidth = service.getWidth();
        const expectedWidth = STAMP_MAX_WIDTH - 1;
        service.setWidth(expectedWidth);
        expect(service.getWidth()).not.toEqual(currentWidth);
        expect(service.getWidth()).toEqual(expectedWidth);
    });

    it('setWidth should set width to min if entry is smaller', () => {
        const width = STAMP_MIN_WIDTH - 1;
        service.setWidth(width);
        expect(service.getWidth()).toEqual(STAMP_MIN_WIDTH);
    });

    it('setWidth should set width to max if entry is greater', () => {
        const width = STAMP_MAX_WIDTH + 1;
        service.setWidth(width);
        expect(service.getWidth()).toEqual(STAMP_MAX_WIDTH);
    });

    it('setAngle should change the angle if correct', () => {
        service.setAngle(MAX_ANGLE);
        let angle = 0;
        service.getAngle().subscribe((value: number) => {
            angle = value;
        });
        expect(angle).toEqual(MAX_ANGLE);
    });

    it('setAngle should set angle to min if entry is smaller', () => {
        const angle = MIN_ANGLE - 1;
        service.setAngle(angle);
        expect(service['angle'].getValue()).toEqual(angle + MAX_ANGLE);
    });

    it('setAngle should set angle to max if entry is greater', () => {
        const angle = MAX_ANGLE + 1;
        service.setAngle(angle);
        expect(service['angle'].getValue()).toEqual(angle - MAX_ANGLE);
    });

    it('setStampChoice should not set the choice of the stamp if not the currentStampChoice', () => {
        const stamp: StampChoices = 'test' as StampChoices;
        service.setStampChoice(stamp);
        expect(service.getStampChoice()).toEqual(StampChoices.StampChoices1);
    });

    it('setStampChoice should set the choice of the stamp ', () => {
        const stamp: StampChoices = StampChoices.StampChoices3;
        service.setStampChoice(stamp);
        expect(service.getStampChoice()).toEqual(StampChoices.StampChoices3);
    });

    it(' onMouseDown should set mouseDownCoord to the correct position and onMouseMove to the current Position', () => {
        const expectedResult: Vec2 = service.getPositionFromMouse(mouseEvent);
        service.onMouseDown(mouseEvent);
        service.onMouseMove(mouseEvent);
        expect(service.mouseDownCoord).toEqual(expectedResult);
    });

    it(' onMouseDown should set mouseDown to true on the left click', () => {
        service.onMouseDown(mouseEvent);
        expect(service.mouseDown).toEqual(true);
    });

    it(' onMouseDown and onMouseMove should set mouseDown property to false when call the right click', () => {
        const mouseEventRClick = {
            offsetX: 25,
            offsetY: 25,
            button: MouseButton.Right,
        } as MouseEvent;
        service.onMouseDown(mouseEventRClick);
        service.onMouseMove(mouseEvent);
        expect(service.mouseDown).toEqual(false);
    });

    it(' drawIcon should draw the icon on the canvas ', () => {
        const drawImageSpy: jasmine.Spy = spyOn(previewCtxStub, 'drawImage');
        const position: Vec2 = { x: 2, y: 2 };
        service.drawIcon(previewCtxStub, position);
        expect(drawImageSpy).toHaveBeenCalled();
    });

    it(' mouseWheel should set update the angle correctly', () => {
        const wheel = {
            deltaY: 5,
            deltaX: 0,
        } as WheelEvent;
        service.onMouseWheel(wheel);
        expect(drawIconSpy).toHaveBeenCalled();
    });

    it(' mouseWheel should set update on STAMP_ALT_ANGLE the angle correctly', () => {
        const wheel = {
            altKey: true,
        } as WheelEvent;
        service.onMouseWheel(wheel);
        expect(drawIconSpy).toHaveBeenCalled();
    });
});
