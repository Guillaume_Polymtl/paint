import { Injectable } from '@angular/core';
import { MouseButton } from '@app/classes/mouse-buttons';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import {
    ALT_DIVIDER_FOR_ROTATION_ANGLE,
    MAX_ANGLE,
    MIN_ANGLE,
    StampChoices,
    StampIconsPath,
    STAMP_ALT_ANGLE,
    STAMP_DEFAULT_ANGLE,
    STAMP_DEFAULT_WIDTH,
    STAMP_MAX_WIDTH,
    STAMP_MIN_WIDTH,
    STAMP_STEP_ANGLE,
} from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class StampService extends Tool {
    readonly name: ToolName = ToolName.STAMP;
    private width: number;
    private angle: BehaviorSubject<number>;
    private currentMousePosition: Vec2;
    currentStampChoice: StampChoices;
    private stampIcons: Map<StampChoices, HTMLImageElement>;

    constructor(drawingService: DrawingService, colorService: ColorService) {
        super(drawingService, colorService);
        this.width = STAMP_DEFAULT_WIDTH;
        this.angle = new BehaviorSubject(STAMP_DEFAULT_ANGLE);
        this.loadIcons();
        this.currentStampChoice = StampChoices.StampChoices1;
    }

    private loadIcons(): void {
        const stampChoices = Object.values(StampChoices);
        const stampPath = Object.values(StampIconsPath);
        this.stampIcons = new Map();
        for (let i = 0; i < stampPath.length; ++i) {
            const icon = new Image();
            icon.src = stampPath[i];
            icon.width = this.width;
            icon.height = this.width;
            this.stampIcons.set(stampChoices[i] as StampChoices, icon);
        }
    }

    getStampChoice(): StampChoices {
        return this.currentStampChoice;
    }

    setStampChoice(stampChoice: StampChoices): void {
        if (Object.values(StampChoices).includes(stampChoice)) {
            this.currentStampChoice = stampChoice;
        }
    }

    getAngle(): Observable<number> {
        return this.angle;
    }

    setAngle(angle: number): void {
        if (angle >= MIN_ANGLE && angle <= MAX_ANGLE) {
            this.angle.next(angle);
        } else if (angle < MIN_ANGLE) {
            this.angle.next(angle + MAX_ANGLE);
        } else {
            this.angle.next(angle - MAX_ANGLE);
        }
    }

    getWidth(): number {
        return this.width;
    }

    setWidth(width: number): void {
        if (width >= STAMP_MIN_WIDTH && width <= STAMP_MAX_WIDTH) {
            this.width = width;
        } else if (width < STAMP_MIN_WIDTH) {
            this.width = STAMP_MIN_WIDTH;
        } else {
            this.width = STAMP_MAX_WIDTH;
        }
    }

    onMouseDown(event: MouseEvent): void {
        this.mouseDown = event.button === MouseButton.Left;
        if (this.mouseDown) {
            this.mouseDownCoord = this.getPositionFromMouse(event);
            this.currentMousePosition = this.getPositionFromMouse(event);
            this.drawIcon(this.drawingService.baseCtx, this.mouseDownCoord);
            this.drawingService.autoSave();
        }
    }

    onMouseMove(event: MouseEvent): void {
        this.currentMousePosition = this.getPositionFromMouse(event);
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        this.drawIcon(this.drawingService.previewCtx, this.currentMousePosition);
    }

    onMouseWheel(event: WheelEvent): void {
        const changeAngle = event.altKey ? STAMP_ALT_ANGLE : STAMP_ALT_ANGLE * STAMP_STEP_ANGLE;
        const newAngle = this.angle.getValue() + Math.sign(event.deltaY) * changeAngle;
        this.setAngle(newAngle);
        this.currentMousePosition = this.getPositionFromMouse(event);
        this.drawIcon(this.drawingService.previewCtx, this.currentMousePosition);
    }

    drawIcon(ctx: CanvasRenderingContext2D, position: Vec2): void {
        const currentIcon = this.stampIcons.get(this.currentStampChoice) as HTMLImageElement;
        this.drawingService.previewCtx.clearRect(0, 0, this.drawingService.canvas.width, this.drawingService.canvas.height);
        ctx.save();
        ctx.translate(position.x, position.y);
        ctx.scale(2, 2);
        ctx.rotate((this.angle.getValue() * Math.PI) / ALT_DIVIDER_FOR_ROTATION_ANGLE);
        ctx.drawImage(currentIcon, -this.width / 2, -this.width / 2, this.width, this.width);
        ctx.restore();
    }
}
