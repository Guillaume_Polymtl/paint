import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TEXT_ALIGN } from '@app/classes/tools-utility';
import { DropDownComponent } from '@app/tools/shared/components/drop-down/drop-down.component';
import { FontStyleComponent } from '@app/tools/shared/components/font-style/font-style.component';
import { SliderComponent } from '@app/tools/shared/components/slider/slider.component';
import { TextAlignComponent } from '@app/tools/shared/components/text-align/text-align.component';
import { TextService } from '@app/tools/text/text.service';

import { TextAttributesComponent } from './text-attributes.component';

describe('TextAttributesComponent', () => {
    let component: TextAttributesComponent;
    let fixture: ComponentFixture<TextAttributesComponent>;
    let textServiceSpy: jasmine.SpyObj<TextService>;

    beforeEach(async(() => {
        (textServiceSpy = jasmine.createSpyObj('TextService', [
            'getFontSize',
            'getFontFamily',
            'getItalic',
            'getBold',
            'getAlignment',
            'setAlignment',
            'setFontSize',
            'setFontFamily',
            'setBold',
            'setItalic',
        ])),
            TestBed.configureTestingModule({
                declarations: [TextAttributesComponent, SliderComponent, DropDownComponent, FontStyleComponent, TextAlignComponent],
                imports: [BrowserAnimationsModule, MatSliderModule, MatButtonToggleModule, MatSelectModule, RouterTestingModule, MatIconModule],
                providers: [{ provide: TextService, useValue: textServiceSpy }],
            }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextAttributesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call textService.setFontSize', () => {
        const value = 0;
        component.setFontSize(value);
        expect(textServiceSpy.setFontSize).toHaveBeenCalledWith(value);
    });

    it('should call textService.setFonFamily', () => {
        const value = '';
        component.setFontFamily(value);
        expect(textServiceSpy.setFontFamily).toHaveBeenCalledWith(value);
    });

    it('should call textService.setIsBold', () => {
        const value = true;
        component.setBold(value);
        expect(textServiceSpy.setBold).toHaveBeenCalledWith(value);
    });

    it('should call text service.setIsItalic', () => {
        const value = true;
        component.setItalic(value);
        expect(textServiceSpy.setItalic).toHaveBeenCalledWith(value);
    });

    it('should call textService.setAlignment', () => {
        const value = TEXT_ALIGN.CENTER;
        component.setAlignment(value);
        expect(textServiceSpy.setAlignment).toHaveBeenCalledWith(value);
    });
});
