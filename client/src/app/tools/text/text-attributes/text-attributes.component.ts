import { Component, OnInit } from '@angular/core';
import {
    TEXT_ALIGN,
    TEXT_DEFAULT_FONT_FAMILY,
    TEXT_DEFAULT_FONT_SIZE,
    TEXT_FONT_FAMILIES,
    TEXT_MAX_FONT_SIZE,
    TEXT_MIN_FONT_SIZE,
} from '@app/classes/tools-utility';
import { TextService } from '@app/tools/text/text.service';

@Component({
    selector: 'app-text-attributes',
    templateUrl: './text-attributes.component.html',
    styleUrls: ['./text-attributes.component.scss'],
})
export class TextAttributesComponent implements OnInit {
    readonly title: string = 'Texte';
    readonly sizeTitle: string = 'Taille';
    readonly fontFamilyTitle: string = 'Police';
    maxFontSize: number = TEXT_MAX_FONT_SIZE;
    minFontSize: number = TEXT_MIN_FONT_SIZE;
    fontSize: number = TEXT_DEFAULT_FONT_SIZE;
    fontFamily: string = TEXT_DEFAULT_FONT_FAMILY;
    fontFamilies: string[] = TEXT_FONT_FAMILIES;
    isOblique: boolean = false;
    isItalic: boolean = false;
    isBold: boolean = false;
    alignment: TEXT_ALIGN = TEXT_ALIGN.RIGHT;

    constructor(private textService: TextService) {}

    ngOnInit(): void {
        this.fontSize = this.textService.getFontSize();
        this.fontFamily = this.textService.getFontFamily();
        this.isItalic = this.textService.getItalic();
        this.isBold = this.textService.getBold();
        this.alignment = this.textService.getAlignment();
    }

    setFontSize(fontSize: number): void {
        this.textService.setFontSize(fontSize);
    }

    setFontFamily(fontFamily: string): void {
        this.textService.setFontFamily(fontFamily);
    }

    setBold(isBold: boolean): void {
        this.textService.setBold(isBold);
    }

    setItalic(isItalic: boolean): void {
        this.textService.setItalic(isItalic);
    }

    setAlignment(alignment: TEXT_ALIGN): void {
        this.textService.setAlignment(alignment);
    }
}
