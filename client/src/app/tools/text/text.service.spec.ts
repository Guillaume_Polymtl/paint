import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
import {
    KeyValues,
    TEXT_ALIGN,
    TEXT_DEFAULT_FONT_SIZE,
    TEXT_FONT_FAMILIES,
    TEXT_MAX_FONT_SIZE,
    TEXT_MIN_FONT_SIZE,
} from '@app/classes/tools-utility';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { TextService } from './text.service';

// tslint:disable: no-string-literal
// tslint:disable: no-any
describe('TextService', () => {
    let service: TextService;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let refreshSpy: jasmine.SpyObj<any>;

    beforeEach(() => {
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['clearCanvas', 'autoSave']);
        drawingServiceSpy.baseCtx = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        drawingServiceSpy.previewCtx = CanvasTestHelper.canvas().getContext('2d') as CanvasRenderingContext2D;
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            providers: [{ provide: DrawingService, useValue: drawingServiceSpy }],
        }).compileComponents();

        service = TestBed.inject(TextService);
        refreshSpy = spyOn<any>(service, 'refreshPreview').and.callThrough();
        service['text'] = ['abc', '123', 'ab', '12'];
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return this.disableShortcuts', () => {
        const expectedValue = service['disableShortcuts'].getValue();
        let value = true;
        service.getDisableShortcuts().subscribe((blocked: boolean) => {
            value = blocked;
        });
        expect(value).toBeDefined();
        expect(value).toEqual(expectedValue);
    });

    it('should not do anything on key down if this.disableShortcuts is false', () => {
        service['disableShortcuts'].next(false);
        const keyDown = new KeyboardEvent('', {});
        service.onKeyDown(keyDown);
        expect(refreshSpy).not.toHaveBeenCalled();
    });

    it('should call the proper function on keydown', () => {
        const mouseDownEvent = new MouseEvent('', { clientX: 1, clientY: 1 });
        service.onMouseDown(mouseDownEvent);
        const backspaceSpy = spyOn<any>(service, 'removePreviousLetter');
        const backspace = new KeyboardEvent('', { key: KeyValues.Backspace });
        service.onKeyDown(backspace);
        expect(backspaceSpy).toHaveBeenCalled();

        const deleteSpy = spyOn<any>(service, 'removeNextLetter');
        const deleteKeyDown = new KeyboardEvent('', { key: KeyValues.Delete });
        service.onKeyDown(deleteKeyDown);
        expect(deleteSpy).toHaveBeenCalled();

        const enter = new KeyboardEvent('', { key: KeyValues.Enter });
        const insertLineSpy = spyOn<any>(service, 'insertLine');
        service.onKeyDown(enter);
        expect(insertLineSpy).toHaveBeenCalled();

        const escape = new KeyboardEvent('', { key: KeyValues.Escape });
        const resetSpy = spyOn<any>(service, 'reset');
        service.onKeyDown(escape);
        expect(resetSpy).toHaveBeenCalled();

        const moveCusorSpy = spyOn<any>(service, 'moveCursor');
        let callCount = 0;
        const arrowUp = new KeyboardEvent('', { key: KeyValues.ArrowUp });
        service.onKeyDown(arrowUp);
        callCount++;
        const arrowDown = new KeyboardEvent('', { key: KeyValues.ArrowDown });
        service.onKeyDown(arrowDown);
        callCount++;
        const arrowRight = new KeyboardEvent('', { key: KeyValues.ArrowRight });
        service.onKeyDown(arrowRight);
        callCount++;
        const arrowLeft = new KeyboardEvent('', { key: KeyValues.ArrowLeft });
        service.onKeyDown(arrowLeft);
        callCount++;
        expect(moveCusorSpy).toHaveBeenCalledTimes(callCount);
    });

    it('should confirm text on tool change', () => {
        const confirmTextSpy = spyOn<any>(service, 'confirmText');
        service.onToolChange();
        expect(confirmTextSpy).toHaveBeenCalled();
    });

    it('should set the italic to the proper value and refresh the preview', () => {
        service.setItalic(true);
        expect(refreshSpy).toHaveBeenCalled();
        expect(service.getItalic()).toBeTrue();
    });

    it('should set is bold to the proper value andrefresh the preview', () => {
        service.setBold(true);
        expect(refreshSpy).toHaveBeenCalled();
        expect(service.getBold()).toBeTrue();
    });

    it('should set the font size to the proper value', () => {
        const defaultFontSize = service.getFontSize();
        let invalidFontSize = TEXT_MAX_FONT_SIZE + 1;
        service.setFontSize(invalidFontSize);
        expect(service.getFontSize()).toEqual(defaultFontSize);
        invalidFontSize = TEXT_MIN_FONT_SIZE - 1;
        service.setFontSize(invalidFontSize);
        expect(service.getFontSize()).toEqual(defaultFontSize);
        const validFontSize = TEXT_DEFAULT_FONT_SIZE;
        service.setFontSize(validFontSize);
        expect(service.getFontSize()).toEqual(validFontSize);
        expect(refreshSpy).toHaveBeenCalled();
    });

    it('should set the font family for a valid value', () => {
        const validFamily = TEXT_FONT_FAMILIES[2];
        service.setFontFamily(validFamily);
        expect(service.getFontFamily()).toEqual(validFamily);
    });

    it('should not set the font family for an invalid value', () => {
        const defaultFamily = service.getFontFamily();
        const invalidFontFamily = TEXT_FONT_FAMILIES[2] + TEXT_FONT_FAMILIES[2];
        service.setFontFamily(invalidFontFamily);
        expect(service.getFontFamily()).toEqual(defaultFamily);
    });

    it('should set the alignment and call refresh', () => {
        service.setAlignment(TEXT_ALIGN.CENTER);
        expect(service.getAlignment()).toEqual(TEXT_ALIGN.CENTER);
        expect(refreshSpy).toHaveBeenCalled();
    });

    it('should set the alignment and call refresh', () => {
        service.setAlignment(TEXT_ALIGN.RIGHT);
        expect(service.getAlignment()).toEqual(TEXT_ALIGN.RIGHT);
        expect(refreshSpy).toHaveBeenCalled();
    });

    it('should add the letter on key down', () => {
        const aKeyDown = new KeyboardEvent('', { key: 'a' });
        service['cursorPosition'] = { x: 0, y: 0 };
        service['disableShortcuts'].next(true);
        service.onKeyDown(aKeyDown);
        expect(service['text'][0]).toEqual('aabc');
    });

    it('positionIsInsideTextBox should return true if position is inside the box', () => {
        service['textBoxPosition'] = { x: 0, y: 0 };
        service['textBoxWidth'] = 2;
        service['textBoxHeight'] = 2;
        const mouseDown = new MouseEvent('', { clientX: 1, clientY: 1 });
        service.onMouseDown(mouseDown);
        expect(refreshSpy).not.toHaveBeenCalled();
    });

    it('on arrow right, should move the cusor to the next letter', () => {
        const currentLine = 0;
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: 0, y: currentLine };
        const arrowRight = new KeyboardEvent('', { key: KeyValues.ArrowRight });
        service.onKeyDown(arrowRight);
        const expectPosition = { x: 1, y: currentLine };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('on arrow right, should move the cusor to the next line if cursor is at the end of the line', () => {
        const currentLine = 0;
        service['disableShortcuts'].next(true);
        const lineLength = service['text'][currentLine].length;
        service['cursorPosition'] = { x: lineLength, y: currentLine };
        const arrowRight = new KeyboardEvent('', { key: KeyValues.ArrowRight });
        service.onKeyDown(arrowRight);
        const expectPosition = { x: 0, y: currentLine + 1 };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('on arrow right, should not move the cursor if already at the end', () => {
        const currentLine = service['text'].length - 1;
        const lastLineLength = service['text'][currentLine].length;
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: lastLineLength, y: currentLine };
        const arrowRight = new KeyboardEvent('', { key: KeyValues.ArrowRight });
        service.onKeyDown(arrowRight);
        const expectPosition = { x: lastLineLength, y: currentLine };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('on arrow left, should move the cursor to the line above if x is 0', () => {
        const currentLine = 1;
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: 0, y: currentLine };
        const arrowLeft = new KeyboardEvent('', { key: KeyValues.ArrowLeft });
        service.onKeyDown(arrowLeft);
        const lineAboveLength = service['text'][currentLine].length;
        const expectPosition = { x: lineAboveLength, y: currentLine - 1 };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('on arrow left, should move the cursor left', () => {
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: 1, y: 0 };
        const arrowLeft = new KeyboardEvent('', { key: KeyValues.ArrowLeft });
        service.onKeyDown(arrowLeft);
        const expectPosition = { x: 0, y: 0 };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('on arrow left, should not do anything if already at the start of the text', () => {
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: 0, y: 0 };
        const arrowLeft = new KeyboardEvent('', { key: KeyValues.ArrowLeft });
        service.onKeyDown(arrowLeft);
        const expectPosition = { x: 0, y: 0 };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('on arrow down, should put the cursor at the end of the text if x is bigger than text length', () => {
        const currentLine = 1;
        const currentLineLength = service['text'][currentLine].length;
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: currentLineLength, y: currentLine };
        const arrowDown = new KeyboardEvent('', { key: KeyValues.ArrowDown });
        const lineBelowLength = service['text'][currentLine + 1].length;
        service.onKeyDown(arrowDown);
        const expectPosition = { x: lineBelowLength, y: currentLine + 1 };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('on arrow up, should move the cursor up', () => {
        const currentLine = 1;
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: 0, y: currentLine };
        const arrowUp = new KeyboardEvent('', { key: KeyValues.ArrowUp });
        service.onKeyDown(arrowUp);
        const expectPosition = { x: 0, y: 0 };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('on arrow up, should not move the cursor if cursor is already at the top', () => {
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: 0, y: 0 };
        const arrowUp = new KeyboardEvent('', { key: KeyValues.ArrowUp });
        service.onKeyDown(arrowUp);
        const expectPosition = { x: 0, y: 0 };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('on arrow down, should move the cursor down', () => {
        const currentLine = 0;
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: 0, y: currentLine };
        const arrowDown = new KeyboardEvent('', { key: KeyValues.ArrowDown });
        service.onKeyDown(arrowDown);
        const expectPosition = { x: 0, y: currentLine + 1 };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('on arrow down, should not move if cursor is already at the bottom', () => {
        const currentLine = service['text'].length - 1;
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: 0, y: currentLine };
        const arrowDown = new KeyboardEvent('', { key: KeyValues.ArrowDown });
        service.onKeyDown(arrowDown);
        const expectPosition = { x: 0, y: currentLine };
        expect(service['cursorPosition']).toEqual(expectPosition);
    });

    it('should insert a new line', () => {
        const initialLines = service['text'].length;
        const enter = new KeyboardEvent('', { key: KeyValues.Enter });
        service['disableShortcuts'].next(true);
        service.onKeyDown(enter);
        const finalLines = service['text'].length;
        expect(initialLines + 1).toEqual(finalLines);
    });

    it('on delete, it should remove the next letter', () => {
        const initialText = service['text'][0];
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: 0, y: 0 };
        const deleteDown = new KeyboardEvent('', { key: KeyValues.Delete });
        service.onKeyDown(deleteDown);
        const finalText = service['text'][0];
        expect(initialText.slice(1, initialText.length)).toEqual(finalText);
    });

    it('on delete, should copy the line below and shift every line up', () => {
        const initialText = service['text'][0];
        const lineBelow = service['text'][1];
        const lineBeBelow = service['text'][2];
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: initialText.length, y: 0 };
        const deleteDown = new KeyboardEvent('', { key: KeyValues.Delete });
        service.onKeyDown(deleteDown);
        const finalText = service['text'][0];
        expect(finalText).toEqual(initialText + lineBelow);
        expect(service['text'][1]).toEqual(lineBeBelow);
    });

    it('on delete, should not delete anything if the cursor is at the last position', () => {
        const nLines = service['text'].length;
        const lastLineY = nLines - 1;
        const lastLine = service['text'][lastLineY];
        const lastLineLength = lastLine.length;
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: lastLineLength, y: lastLineY };
        const initialText = service['text'];
        const deleteDown = new KeyboardEvent('', { key: KeyValues.Delete });
        service.onKeyDown(deleteDown);
        const finalText = service['text'];
        expect(initialText).toEqual(finalText);
    });

    it('on backspace, should not do anything if cursor is at the start of the text', () => {
        service['disableShortcuts'].next(true);
        service['cursorPosition'] = { x: 0, y: 0 };
        const initialText = service['text'];
        const deleteDown = new KeyboardEvent('', { key: KeyValues.Backspace });
        service.onKeyDown(deleteDown);
        const finalText = service['text'];
        expect(initialText).toEqual(finalText);
    });
});
