import { Injectable } from '@angular/core';
import { Tool } from '@app/classes/tool';
import { ToolName } from '@app/classes/toolbar/tool-name';
import {
    ARROW,
    CURSOR_WIDTH,
    KeyValues,
    TEXT_ALIGN,
    TEXT_DEFAULT_FONT_FAMILY,
    TEXT_DEFAULT_FONT_SIZE,
    TEXT_FONT_FAMILIES,
    TEXT_MAX_FONT_SIZE,
    TEXT_MIN_FONT_SIZE,
} from '@app/classes/tools-utility';
import { Vec2 } from '@app/classes/vec2';
import { ColorService } from '@app/color-picker/services/color.service';
import { DrawingService } from '@app/services/drawing/drawing.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class TextService extends Tool {
    readonly name: ToolName = ToolName.TEXT;
    private fontSize: number = TEXT_DEFAULT_FONT_SIZE;
    private fontFamily: string = TEXT_DEFAULT_FONT_FAMILY;
    private bold: boolean = false;
    private italic: boolean = false;
    private alignment: TEXT_ALIGN = TEXT_ALIGN.LEFT;
    private text: string[] = [''];
    private cursorPosition: Vec2 = { x: 0, y: 0 };
    private textPosition: Vec2 = { x: Number.MIN_SAFE_INTEGER, y: Number.MIN_SAFE_INTEGER };
    private actualTextPositionX: number = 0;
    private textBoxPosition: Vec2 = { x: Number.MIN_SAFE_INTEGER, y: Number.MIN_SAFE_INTEGER };
    private textBoxWidth: number = 0;
    private textBoxHeight: number = 0;
    private disableShortcuts: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(drawingService: DrawingService, colorService: ColorService) {
        super(drawingService, colorService);
        this.onInit();
    }

    getDisableShortcuts(): Observable<boolean> {
        return this.disableShortcuts;
    }

    onToolChange(): void {
        this.confirmText();
    }

    setItalic(italic: boolean): void {
        this.italic = italic;
        this.refreshPreview();
    }

    getItalic(): boolean {
        return this.italic;
    }

    setBold(bold: boolean): void {
        this.bold = bold;
        this.refreshPreview();
    }

    getBold(): boolean {
        return this.bold;
    }

    setFontSize(fontSize: number): void {
        if (fontSize <= TEXT_MAX_FONT_SIZE && fontSize >= TEXT_MIN_FONT_SIZE) {
            this.fontSize = fontSize;
        }
        this.refreshPreview();
    }

    getFontSize(): number {
        return this.fontSize;
    }

    setFontFamily(fontFamily: string): void {
        if (TEXT_FONT_FAMILIES.find((f: string) => f === fontFamily)) {
            this.fontFamily = fontFamily;
        }
        this.refreshPreview();
    }

    setAlignment(alignment: TEXT_ALIGN): void {
        this.alignment = alignment;
        this.refreshPreview();
    }

    getAlignment(): TEXT_ALIGN {
        return this.alignment;
    }

    getFontFamily(): string {
        return this.fontFamily;
    }

    onKeyDown(event: KeyboardEvent): void {
        event.preventDefault();
        if (this.disableShortcuts.getValue()) {
            this.handleKeyDown(event);
            this.refreshPreview();
        }
    }

    private refreshPreview(): void {
        const previewCtx = this.clearPreview();
        this.textBoxWidth = 0;
        this.textBoxHeight = 0;
        for (let y = 0; y < this.text.length; y++) {
            const line = this.text[y];
            const lineMetrics = previewCtx.measureText(line);
            const lineHeight = lineMetrics.actualBoundingBoxAscent + lineMetrics.actualBoundingBoxDescent;
            const lineWidth = lineMetrics.width;
            const lineY = this.textPosition.y - lineMetrics.actualBoundingBoxAscent + y * this.fontSize;
            if (y === 0) {
                this.textBoxPosition.y = lineY;
            }
            const boxHeight = lineY - this.textBoxPosition.y + lineHeight;
            this.textBoxWidth = Math.max(this.textBoxWidth, lineWidth);
            this.textBoxHeight = Math.max(this.textBoxHeight, boxHeight);
        }
        this.actualTextPositionX = this.textPosition.x;
        this.textBoxPosition.x = this.textPosition.x;
        if (this.alignment === TEXT_ALIGN.CENTER) {
            this.actualTextPositionX = this.textPosition.x + this.textBoxWidth / 2;
            this.textBoxPosition.x = this.actualTextPositionX - this.textBoxWidth / 2;
        } else if (this.alignment === TEXT_ALIGN.RIGHT) {
            this.actualTextPositionX = this.textPosition.x + this.textBoxWidth;
            this.textBoxPosition.x = this.actualTextPositionX - this.textBoxWidth;
        }
        for (let y = 0; y < this.text.length; y++) {
            this.setFont(previewCtx);
            previewCtx.fillText(this.text[y], this.actualTextPositionX, this.textPosition.y + this.fontSize * y);
            if (y === this.cursorPosition.y && this.disableShortcuts.getValue()) {
                const substringMetrics = previewCtx.measureText(this.text[y].substr(0, this.cursorPosition.x));
                const lineMetrics = previewCtx.measureText(this.text[y]);
                let lineHeight = lineMetrics.actualBoundingBoxAscent + lineMetrics.actualBoundingBoxDescent;
                let lineX = this.actualTextPositionX - lineMetrics.actualBoundingBoxLeft;
                const lineY = this.textPosition.y - lineMetrics.actualBoundingBoxAscent + y * this.fontSize;
                lineX = lineX + substringMetrics.width;
                if (!lineHeight) lineHeight = -this.fontSize;
                previewCtx.setLineDash([0]);
                previewCtx.fillRect(lineX, lineY, CURSOR_WIDTH, lineHeight);
            }
        }
        this.drawTextBox();
    }

    private drawTextBox(): void {
        const previewCtx = this.drawingService.previewCtx;
        previewCtx.strokeStyle = this.color.toString();
        previewCtx.setLineDash([1, 1]);
        previewCtx.strokeRect(this.textBoxPosition.x, this.textBoxPosition.y, this.textBoxWidth, this.textBoxHeight);
    }

    private confirmText(): void {
        this.clearPreview();
        this.setFont(this.drawingService.baseCtx);
        for (let line = 0; line < this.text.length; line++) {
            this.drawingService.baseCtx.fillText(this.text[line], this.actualTextPositionX, this.textPosition.y + this.fontSize * line);
        }
        this.drawingService.autoSave();
        this.reset();
    }

    private handleKeyDown(event: KeyboardEvent): void {
        const key = event.key;
        if (key.length === 1) {
            this.insertLetter(key);
        }
        switch (key) {
            case KeyValues.Backspace: {
                this.removePreviousLetter();
                break;
            }
            case KeyValues.Delete: {
                this.removeNextLetter();
                break;
            }
            case KeyValues.Enter: {
                this.insertLine();
                break;
            }
            case KeyValues.Escape: {
                this.reset();
                break;
            }
            case KeyValues.ArrowUp: {
                this.moveCursor(ARROW.UP);
                break;
            }
            case KeyValues.ArrowDown: {
                this.moveCursor(ARROW.DOWN);
                break;
            }
            case KeyValues.ArrowLeft: {
                this.moveCursor(ARROW.LEFT);
                break;
            }
            case KeyValues.ArrowRight: {
                this.moveCursor(ARROW.RIGHT);
                break;
            }
        }
    }

    onMouseDown(event: MouseEvent): void {
        const position = this.getPositionFromMouse(event);
        if (!this.positionIsInsideTextBox(position)) {
            this.confirmText();
            this.mouseDownCoord = position;
            this.textPosition = position;
            this.disableShortcuts.next(true);
            this.refreshPreview();
            this.drawingService.autoSave();
        }
    }

    private positionIsInsideTextBox(position: Vec2): boolean {
        return !(
            position.x < this.textBoxPosition.x ||
            position.x > this.textBoxPosition.x + this.textBoxWidth ||
            position.y < this.textBoxPosition.y ||
            position.y > this.textBoxPosition.y + this.textBoxHeight
        );
    }

    reset(): void {
        this.text = [''];
        this.cursorPosition = { x: 0, y: 0 };
        this.disableShortcuts.next(false);
    }

    private moveCursor(arrow: ARROW): void {
        let x = this.cursorPosition.x;
        let y = this.cursorPosition.y;
        switch (arrow) {
            case ARROW.UP: {
                y = Math.max(y - 1, 0);
                break;
            }
            case ARROW.DOWN: {
                const numberOfLines = this.text.length;
                y = Math.min(y + 1, numberOfLines - 1);
                break;
            }
            case ARROW.RIGHT: {
                const currentLineLength = this.text[y].length;
                if (x === currentLineLength && y < this.text.length - 1) {
                    // end of line the more text down: start of next line
                    x = 0;
                    y++;
                } else if (x < currentLineLength) {
                    // not end of line, move right
                    x++;
                }
                break;
            }
            case ARROW.LEFT: {
                if (x > 0) {
                    x--;
                } else if (x === 0 && y > 0) {
                    y--;
                    const lineAboveLength = this.text[y].length;
                    x = lineAboveLength;
                }
                break;
            }
        }
        const lineLength = this.text[y].length;
        x = Math.min(x, lineLength);
        this.cursorPosition.x = x;
        this.cursorPosition.y = y;
    }

    private insertLetter(key: string): void {
        this.cursorPosition.x++;
        const y = this.cursorPosition.y;
        const texts = this.getCurrentLineSplitByCursor();
        this.text[y] = texts[0] + key + texts[1];
    }

    private insertLine(): void {
        // middle of line: copy second half to next line
        const texts = this.getCurrentLineSplitByCursor();
        this.text[this.cursorPosition.y] = texts[0];
        this.cursorPosition.y++;
        this.cursorPosition.x = 0;
        this.text.splice(this.cursorPosition.y, 0, '');
        this.text[this.cursorPosition.y] = texts[1];
    }

    private getCurrentLineSplitByCursor(): string[] {
        const originalText = this.text[this.cursorPosition.y];
        const textBeforeCursor = originalText.slice(0, this.cursorPosition.x);
        const textAfterCursor = originalText.slice(this.cursorPosition.x, originalText.length);
        return [textBeforeCursor, textAfterCursor];
    }

    private removePreviousLetter(): void {
        let x = this.cursorPosition.x;
        let y = this.cursorPosition.y;
        const texts = this.getCurrentLineSplitByCursor();
        if (x > 0) {
            this.text[y] = texts[0].slice(0, x - 1) + texts[1];
            x--;
        } else if (x === 0 && y > 0) {
            y--;
            x = this.text[y].length;
            this.text[y] = this.text[y] + texts[0] + texts[1];
            this.text.splice(y + 1, 1);
        }
        this.cursorPosition.x = x;
        this.cursorPosition.y = y;
    }

    private removeNextLetter(): void {
        const lineLength = this.text[this.cursorPosition.y].length;
        const nLines = this.text.length;
        if (this.cursorPosition.x === lineLength && this.cursorPosition.y === nLines - 1) {
            // do nothing (we are at the end of the text)
            return;
        }
        // Equivalent of moving the cursor right once and then removing the previous letter
        this.moveCursor(ARROW.RIGHT);
        this.removePreviousLetter();
    }

    private setFont(ctx: CanvasRenderingContext2D): void {
        ctx.fillStyle = this.color.toString();
        ctx.textAlign = this.alignment;
        let font = '';
        if (this.bold) font += 'bold ';
        if (this.italic) font += ' italic ';
        font += `${this.fontSize}px ${this.fontFamily}`;
        ctx.font = font;
        ctx.lineWidth = 2;
    }

    private clearPreview(): CanvasRenderingContext2D {
        this.drawingService.clearCanvas(this.drawingService.previewCtx);
        return this.drawingService.previewCtx;
    }
}
