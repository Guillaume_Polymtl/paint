// tslint:disable: no-any
export interface Image {
    _id: any;
    title: string;
    tags: string[];
    src: string;
    width: number;
    height: number;
}
