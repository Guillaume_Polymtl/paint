import { config } from 'dotenv';
config();

export const EMAIL_KEY_API: string = process.env.EMAIL_KEY_API ? (process.env.EMAIL_KEY_API as string) : '';
export const MAX_TAGS = 7;

// Database Config
export const DATABASE_URL: string = process.env.DATABASE_URL as string;
export const DATABASE_NAME: string = process.env.DATABASE_NAME as string;
export const DATABASE_COLLECTION: string = process.env.DATABASE_COLLECTION as string;

// Regex
export const REGEX_TITLE: RegExp = /^[A-Za-z0-9- ]{3,10}$/; // Alphanumeric Expresions, 3 to 10 chars word allowed space and dash included
export const REGEX_TAG: RegExp = /^[A-Za-z0-9]{1,10}$/; // Alphanumeric Expresions, 1 to 10 chars word allowed space and dash included
