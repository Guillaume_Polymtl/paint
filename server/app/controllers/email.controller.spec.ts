import { HttpStatus } from '@common/communication/http-status';
import { expect } from 'chai';
import * as supertest from 'supertest';
import { Stubbed, testingContainer } from '../../test/test-utils';
import { Application } from '../app';
import { EmailService } from '../services/email.service';
import Types from '../types';
// tslint:disable: no-any

describe('EmailController', () => {
    let emailService: Stubbed<EmailService>;
    let app: Express.Application;
    const request = {
        to: 'test@email.com',
        payload: 'data:image/jpeg;base64,...',
        extension: 'JPEG',
        title: 'test',
    };

    beforeEach(async () => {
        const [container, sandbox] = await testingContainer();
        container.rebind(Types.SaveService).toConstantValue({
            sendEmail: sandbox.stub().resolves(),
        });
        emailService = emailService;
        emailService = container.get(Types.SaveService);
        app = container.get<Application>(Types.Application).app;
    });

    it('should pass a valid request', async () => {
        return supertest(app)
            .post('/api/email')
            .send({ to: request.to, payload: request.payload, extension: request.extension, title: request.title })
            .then((response: any) => {
                expect(response.statusCode).to.equal(HttpStatus.OK);
            });
    });


    it('sendEmail should fail if missing payload sended', async () => {
        return supertest(app)
            .post('/api/email')
            .send({ to: request.to, extension: request.extension, title: request.title })
            .then((response: any) => {
                expect(response.statusCode).to.equal(HttpStatus.NO_CONTENT);
            });
    });
    it('sendEmail should fail if bad extension sended', async () => {
        return supertest(app)
            .post('/api/email')
            .send({ to: request.to, payload: request.payload ,extension: 'bad ext', title: request.title })
            .then((response: any) => {
                expect(response.statusCode).to.equal(HttpStatus.NOT_FOUND);
            });
    });

    it('sendEmail should be UNPROCESSABLE by axios if bad recipient is sended', async () => {
        emailService.sendEmail.throws(new Error());
        return supertest(app)
            .post('/api/email')
            .send({ to: 'bad recipient', payload: request.payload, extension: request.extension, title: request.title })
            .then((response: any) => {
                /* Do Nothing */
            })
            .catch((error: Error) => {
                expect(error).to.equal(HttpStatus.UNPROCESSABLE);

            });
    });
});
