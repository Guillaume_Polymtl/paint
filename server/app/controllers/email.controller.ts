import { HttpStatus } from '@common/communication/http-status';
import { NextFunction, Request, Response, Router } from 'express';
import { inject, injectable } from 'inversify';
import { EmailService } from '../services/email.service';
import Types from '../types';

@injectable()
export class EmailController {
    router: Router;

    constructor(
        @inject(Types.EmailService)
        private emailService: EmailService,
    ) {
        this.configureRouter();
    }

    private configureRouter(): void {
        this.router = Router();

        this.router.post('/', async (req: Request, res: Response, next: NextFunction) => {
            if (req.body.payload === undefined) {
                res.sendStatus(HttpStatus.NO_CONTENT);
            } else if (!this.emailService.checkExtension(req.body.extension)) {
                res.sendStatus(HttpStatus.NOT_FOUND);
            } else {
                this.emailService
                    .sendEmail(req.body.to, req.body.payload, req.body.extension, req.body.title)
                    .then(() => {
                        res.sendStatus(HttpStatus.OK);
                    })
                    .catch((error: Error) => {
                        res.sendStatus(HttpStatus.UNPROCESSABLE);
                    });
            }
        });
    }
}
