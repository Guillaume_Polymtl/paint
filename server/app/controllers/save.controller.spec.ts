import { HttpStatus } from '@common/communication/http-status';
import { Image } from '@common/communication/image-data';
import { expect } from 'chai';
import * as supertest from 'supertest';
import { Stubbed, testingContainer } from '../../test/test-utils';
import { Application } from '../app';
import { SaveService } from '../services/save.service';
import Types from '../types';

/*tslint:disable:no-any */
describe('SaveController', () => {
    const image = {
        title: 'imageTest',
        tags: ['tag1', 'tag2'],
        src: 'data:image/png;base64,...',
        width: 300,
        height: 300,
    } as Image;
    let saveService: Stubbed<SaveService>;
    let app: Express.Application;

    beforeEach(async () => {
        const [container, sandbox] = await testingContainer();
        container.rebind(Types.SaveService).toConstantValue({
            getImages: sandbox.stub().resolves([image]),
            getImage: sandbox.stub().resolves(image),
            postImage: sandbox.stub().resolves(),
            deleteImage: sandbox.stub().resolves(),
        });
        saveService = container.get(Types.SaveService);
        app = container.get<Application>(Types.Application).app;
    });

    it('should return an array of all images', async () => {
        return supertest(app)
            .get('/api/save')
            .then((response: any) => {
                expect(response.statusCode).to.equal(HttpStatus.OK);
                expect(response.body).to.be.a('array');
            });
    });

    it('should return an an error on a get request to /all', async () => {
        saveService.getImages.rejects();
        return supertest(app)
            .get('/api/save')
            .then((response: any) => {
                expect(response.statusCode).to.equal(HttpStatus.NOT_FOUND);
            })
            .catch((error: any) => {
                expect(error);
            });
    });

    it('should post an image on a valid post request ', async () => {
        return supertest(app)
            .post('/api/save')
            .send(image)
            .then((response: any) => {
                expect(response.statusCode).to.equal(HttpStatus.OK);
            });
    });

    it('should create an error on a valid post request ', async () => {
        saveService.postImage.rejects();
        return supertest(app)
            .post('/api/save')
            .send(image)
            .then((response: any) => {
                expect(response.statusCode).to.equal(HttpStatus.BAD_REQUEST);
            })
            .catch((error: any) => {
                expect(error);
            });
    });

    it('should delete an image correctly', async () => {
        return supertest(app)
            .delete('/api/save/image')
            .then((response: any) => {
                expect(response.statusCode).to.equal(HttpStatus.NO_CONTENT);
            });
    });

    it('should send an error while attempt deleting image', async () => {
        saveService.deleteImage.rejects();
        return supertest(app)
            .delete('/api/save/image')
            .then((response: any) => {
                expect(response.statusCode).to.equal(HttpStatus.NOT_FOUND);
            });
    });
});
