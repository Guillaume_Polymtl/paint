import { HttpStatus } from '@common/communication/http-status';
import { Image } from '@common/communication/image-data';
import { NextFunction, Request, Response, Router } from 'express';
import { inject, injectable } from 'inversify';
import { SaveService } from '../services/save.service';
import Types from '../types';

@injectable()
export class SaveController {
    router: Router;

    constructor(@inject(Types.SaveService) private saveService: SaveService) {
        this.configureRouter();
    }

    private configureRouter(): void {
        this.router = Router();

        this.router.get('/', async (req: Request, res: Response, next: NextFunction) => {
            this.saveService
                .getImages()
                .then((images: Image[]) => {
                    res.json(images);
                })
                .catch((error: Error) => {
                    res.status(HttpStatus.NOT_FOUND).send(error.message);
                });
        });

        this.router.post('/', async (req: Request, res: Response, next: NextFunction) => {
            this.saveService
                .postImage(req.body)
                .then(() => {
                    res.sendStatus(HttpStatus.OK).send();
                })
                .catch((error: Error) => {
                    res.status(HttpStatus.BAD_REQUEST).send(error.message);
                });
        });

        this.router.delete('/:id', async (req: Request, res: Response, next: NextFunction) => {
            this.saveService
                .deleteImage(req.params.id)
                .then(() => {
                    res.sendStatus(HttpStatus.NO_CONTENT).send();
                })
                .catch((error: Error) => {
                    if (error.message === 'Cannot remove headers after they are sent to the client') {
                        // do nothing
                    } else {
                        res.status(HttpStatus.NOT_FOUND).send(error.message);
                    }
                });
        });
    }
}
