import { expect } from 'chai';
import { testingContainer } from '../../test/test-utils';
import Types from '../types';
import { EmailService } from './email.service';
import { HttpStatus } from '@common/communication/http-status';

describe('Email Service', () => {
    let emailService: EmailService;

    beforeEach(async () => {
        const [container] = await testingContainer();
        emailService = container.get<EmailService>(Types.EmailService);
    });


    it('checkExtension should allow valid extensions', () => {
        expect(emailService.checkExtension('JPEG')).to.be.true;
        expect(emailService.checkExtension('PNG')).to.be.true;
    });

    it('checkExtension should deny invalid extension', () => {
        expect(emailService.checkExtension('SVG')).to.be.false;
    });

     it('sendEmail should return status 200 on valid email', async () => {
        const email = 'test@gmail.com';
        const dataUrl = 'data:image/jpeg;base64,...';
        const extension = 'JPEG';
        const title = 'test';
        emailService.sendEmail(email, dataUrl, extension, title).then((status: number) => {
            expect(status).to.deep.equal(HttpStatus.OK);
        }).catch((error: Error) => {
            expect(error).to.deep.equal(HttpStatus.UNPROCESSABLE);
        });
    });

    it('sendEmail should return status BAD_REQUEST ERROR on bad request', async () => {
        const email = 'bad email';
        const dataUrl = 'data:image/jpeg;base64,...';
        const extension = 'JPEG';
        const title = 'test';
        emailService.sendEmail(email, dataUrl, extension, title).then((status: number) => {
            /* Do Nothing */
        }).catch((error: Error) => {
            expect(error).to.deep.equal(HttpStatus.UNPROCESSABLE);
        });
    }); 
    
});
