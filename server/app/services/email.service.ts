import { EMAIL_KEY_API } from '@app/constants';
import { HttpStatus } from '@common/communication/http-status';
import Axios from 'axios';
import * as FormData from 'form-data';
import { injectable } from 'inversify';
/*tslint:disable:promise-function-async */
@injectable()
export class EmailService {
    async sendEmail(email: string, dataUrl: string, extension: string, title: string): Promise<number> {
        const body = title + '.' + extension;
        const atob = (data: string) => Buffer.from(data, 'base64');
        const byteString = atob(dataUrl.split(',')[1]);
        const formData: FormData = new FormData();
        formData.append('to', email);
        formData.append('payload', byteString, { filename: body, contentType: 'image/jpeg' });
        await Axios.post('http://log2990.step.polymtl.ca/email', formData.getBuffer(), {
            headers: {
                'Content-Type': 'multipart/form-data',
                'X-Team-Key': EMAIL_KEY_API,
                ...formData.getHeaders(),
            },
        })
            .then(() => {
                return Promise.resolve(HttpStatus.OK);
            })
            .catch(() => {
                return Promise.reject(HttpStatus.UNPROCESSABLE);
            });
        return Promise.resolve(HttpStatus.BAD_REQUEST);
    }

    checkExtension(extension: string): boolean {
        return extension === 'JPEG' || extension === 'PNG';
    }
}
