import { Image } from '@common/communication/image-data';
import { rejects } from 'assert';
import { expect } from 'chai';
import { Db, MongoClient } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { SaveService } from './save.service';

describe('Save Service', () => {
    let saveService: SaveService;
    let image: Image;
    let imageTwo: Image;
    let invalidImage: Image;
    let mongoServer: MongoMemoryServer;
    let db: Db;
    let client: MongoClient;

    beforeEach(async () => {
        image = {
            _id: '1',
            title: 'imageTest',
            tags: ['tag1', 'tag2'],
            src: 'data:image/png;base64,...',
            width: 800,
            height: 800,
        } as Image;
        imageTwo = {
            _id: '2',
            title: 'imageTwo',
            tags: ['tag1', 'tag2'],
            src: 'data:image/png;base64,...',
            width: 800,
            height: 800,
        } as Image;
        invalidImage = {
            _id: '3',
            title: 'in#',
            tags: ['##', 'tag2'],
            src: 'data:image/png;base64,...',
            width: -800,
            height: 800,
        } as Image;
        saveService = new SaveService();
        mongoServer = new MongoMemoryServer();
        const mongoUri = await mongoServer.getUri();
        client = await MongoClient.connect(mongoUri, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = client.db(await mongoServer.getDbName());
        saveService.collection = db.collection('test');
        saveService.collection.insertOne(image);
    });

    afterEach(async () => {
        client.close();
    });

    it('should throw error if connecting to an invalid URL', async () => {
        try {
            new SaveService('mongodb+srv://tuBbies:null@cluster0.plwpz.mongodb.net/null?retryWrites=true&w=majority');
        } catch (error) {
            expect(error).to.not.be.undefined;
        }
    });
    it('should get all Images in Mongo Database', async () => {
        const dbData = await saveService.getImages();
        expect(dbData.length).to.equal(1);
        expect(image).to.deep.equals(dbData[0]);
    });

    it('should not get all Images in Mongo Database', async () => {
        client.close();
        try {
            await rejects(saveService.getImages());
        } catch (error) {
            expect(error).to.not.be.undefined;
        }
    });

    it('should insert a new Image', async () => {
        await saveService.postImage(imageTwo);
        let courses = await saveService.collection.find({}).toArray();
        expect(courses.length).to.equal(2);
    });
    it('should not insert an invalid Image on a database', async () => {
        await rejects(saveService.postImage(invalidImage));
        let courses = await saveService.collection.find({}).toArray();
        expect(courses.length).to.equal(1);
    });

    it('should return an error while insert on the database', async () => {
        try {
            await saveService.postImage(imageTwo);
        } catch (error) {
            let drawings = await saveService.collection.find({}).toArray();
            expect(drawings.length).to.equal(1);
            expect(error).to.not.be.undefined;
        }
    });

    it('deleteImage should delete an image when sent a valid image id', async () => {
        await saveService.deleteImage(image._id);
        const dbData = await saveService.collection.find({}).toArray();
        expect(dbData.length).to.equal(0);
    });

    it('deleteImage should not delete an image when sent an invalid image id', async () => {
        try {
            await saveService.deleteImage(invalidImage._id);
        } catch (error) {
            expect(error).to.not.be.undefined;
        }
    });
});
