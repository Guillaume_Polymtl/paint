import { Image } from '@common/communication/image-data';
import { injectable } from 'inversify';
import { Collection, MongoClient, MongoClientOptions } from 'mongodb';
import { DATABASE_COLLECTION, DATABASE_NAME, DATABASE_URL, MAX_TAGS, REGEX_TAG, REGEX_TITLE } from '../constants';
// tslint:disable: typedef

@injectable()
export class SaveService {
    client: MongoClient;
    collection: Collection<Image>;

    private options: MongoClientOptions = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    };

    constructor(url = DATABASE_URL) {
        MongoClient.connect(url, this.options)
            .then((client: MongoClient) => {
                this.client = client;
                this.collection = client.db(DATABASE_NAME).collection(DATABASE_COLLECTION);
            })
            .catch((error) => {
                throw error;
            });
    }

    async getImages(): Promise<Image[]> {
        return this.collection
            .find({})
            .toArray()
            .then((images: Image[]) => {
                return images;
            })
            .catch((error: Error) => {
                throw error;
            });
    }

    async postImage(image: Image): Promise<void> {
        if (this.checkImage(image)) {
            this.collection
                .insertOne(image)
                .then(() => {
                    /* do nothing */
                })
                .catch((error: Error) => {
                    throw error;
                });
        } else {
            throw new Error('Invalid image');
        }
    }

    async deleteImage(idImage: string): Promise<void> {
        this.collection
            .findOneAndDelete({ _id: idImage })
            .then(() => {
                /* do nothing */
            })
            .catch((error: Error) => {
                throw error;
            });
    }

    validateTitle(title: string): boolean {
        return REGEX_TITLE.test(title);
    }

    validateTags(tags: string[]): boolean {
        let validTagList = true;
        let tagCtr = 0;
        tags.forEach((tag) => {
            if (!REGEX_TAG.test(tag)) {
                validTagList = false;
            }
            tagCtr++;
        });
        return validTagList && tagCtr <= MAX_TAGS;
    }

    checkImage(image: Image): boolean {
        const containsValidTitle = this.validateTitle(image.title);
        const containsCorrectTags = this.validateTags(image.tags);
        const src = image.src !== null;
        const validWidth = image.width !== null && image.width > 0;
        const validHeight = image.height !== null && image.height > 0;
        return containsValidTitle && containsCorrectTags && validWidth && validHeight && src;
    }
}
