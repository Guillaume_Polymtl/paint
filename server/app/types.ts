export default {
    Server: Symbol('Server'),
    Application: Symbol('Application'),
    IndexController: Symbol('IndexController'),
    DateController: Symbol('DateController'),
    SaveController: Symbol('SaveController'),
    EmailController: Symbol('EmailController'),
    IndexService: Symbol('IndexService'),
    DateService: Symbol('DateService'),
    SaveService: Symbol('SaveService'),
    EmailService: Symbol('EmailService'),
};
